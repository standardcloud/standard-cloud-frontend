import Api from '../api';

class stdadministration {
	
  static getadminoverview() {
    return Api.call("/admin/overview/","GET");
  }

 
    static gettextformat() {
      return Api.call("/design/customtextformat/gettextformat/","GET");
  }
  

  static updatetextformat(updatetext)
  {	
    var data = new FormData();
    data.append("content", updatetext);
    return Api.call("/design/customtextformat/updatetextformat/","POST",data);		
  }
  
  
    static getcustomcss() {
      return Api.call("/design/customcss/getcss/","GET");
  }
    static getcompanyinformation() {
        return Api.call("/design/company/getinformation/","GET");
  }


static updatecompanyinformation(name)
  {	
    var data = new FormData();
    data.append("name", name);
    return Api.call("/design/company/updatetinformation/","POST",data);		
  }

  		
	static updatepassword(passwordaktuell, passwordnew)
  {
    var data = new FormData();
    data.append("currentpassword", passwordaktuell);
    data.append("newpassword", passwordnew);
    return Api.call("/profile/changeuserpwd/","POST",data);		
  }

  static updatecustomcss(csstext)
  {	

    var data = new FormData();
    data.append("csstext", csstext);
    return Api.call("/design/customcss/updatecustomcss/","POST",data);		
  }

}

export default stdadministration;