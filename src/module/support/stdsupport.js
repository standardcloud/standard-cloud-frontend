import Api from '../api';		
		
class Support{
		
		static sendticket(requestguid,description,comment)
		{		
			var data = new FormData();
			data.append("requestguid", requestguid);
			data.append("description", description);
			data.append("comment", comment);
			return Api.call("/support/addticket/","POST",data); 
		}
}


export default Support;