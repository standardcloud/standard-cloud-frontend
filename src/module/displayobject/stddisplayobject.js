import {BACKEND} from '../../stdsetup'
class stddisplayobject
{

		static removeHTMLTags(inputText){
		/* 
			This line is optional, it replaces escaped brackets with real ones, 
			i.e. &lt; is replaced with < and &gt; is replaced with >
		*/	
		var strInputCode = inputText.replace(/&(lt|gt);/g, function (strMatch, p1){
			return (p1 === "lt")? "<" : ">";
		});
		var strTagStrippedText = strInputCode.replace(/<\/?[^>]+(>|$)/g, "");
		return strTagStrippedText;	
	}
	
	
	static getprofilecontent(jsonobject)
		{
	  		var results = []; 
		var fieldcontent = "";
		var imagefield = 0;
		for(var j=0; j < jsonobject.fields.length;j++)
		{
		// durch die Felder
				var res = jsonobject.fields[j];

			switch(res.fieldid)
			{
				case "6dacefca-1e0d-4bdd-9c82-fe89b517242b":
				case "0cecad40-c0a1-46e3-a7a3-4bd99b7236d6":
				case "77f42b3d-8e2c-4869-bd67-6273afe7cf89":
				case "1d57184b-430b-44c8-9708-d3e3dd8bcce4":
				case "7628c3c9-6e30-4b1a-8f74-cc8ae8366d0c":
				continue;
				default:
			}

				var fieldtypeid = res.fieldtypeid;
	         	fieldcontent += "<span class=\"tooltiptext\">";
		    	
				
	switch (fieldtypeid) {
		
						    case "52a3bb09-fc62-4099-a7ff-c314074f274a":
	// URL Links
        fieldcontent += " <span class=\"fieldnamesearchbox\"> <b>" + res.name + ": </b></span><span>";
	
		for(var link=0;link < res[fieldtypeid].length;link++)
		{
		 fieldcontent += "<span>"+  res[fieldtypeid][link].description + " <img height=\"20px\" src=\"https://publicfile.standard-cloud.com/icons/link.png\"> </span>  ";
		}
		fieldcontent = fieldcontent.substring(0,fieldcontent.length -2);
	    fieldcontent += " </span>";
        break;
	
    case "cc333aeb-196d-4331-b1df-3b530fa67722":
	// Benutzer
        fieldcontent += " <span class=\"fieldnamesearchbox\"> <b>" + res.name + ": </b></span><span>";
	
		for(var user=0;user < res[fieldtypeid].length;user++)
		{
		 fieldcontent += "<span>"+  res[fieldtypeid][user].firstname + " " + res[fieldtypeid][user].lastname + "</span>; "
		}
		
		fieldcontent = fieldcontent.substring(0,fieldcontent.length -2);
		fieldcontent += " </span>";

        break;
    case "32b60712-1f4d-4a49-87a6-90de0363eef4":
	// Text
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" + res.name + ": </b></span><span>";
	    if(res[fieldtypeid].length < 70)
		{
		fieldcontent += stddisplayobject.removeHTMLTags(res[fieldtypeid]) + " </span>";
		}else
		{
		fieldcontent += stddisplayobject.removeHTMLTags(res[fieldtypeid]).substring(0,70) + "... </span>";
		}
		break;
	case "17a8dd83-0068-4e4a-bfcc-082f7f64e6f8":
	/*datetime */
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span><span>";
		/* fieldcontent += res[fieldtypeid].datetime  2015-12-11T00:00:00.000+0100*/
		if(res[fieldtypeid].datetime !=null)
		{	
	   fieldcontent += res[fieldtypeid].datetime.substring(8,10) + "." + res[fieldtypeid].datetime.substring(5,7) + "." + res[fieldtypeid].datetime.substring(0,4) + " - " + res[fieldtypeid].datetime.substring(11,13) +":" + res[fieldtypeid].datetime.substring(14,16)  +" </span>"; 
		}
		break;
	case "fd3eb39b-3f6c-4e43-baea-12135ed5bee7":	
		/*Boolean*/
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span>"; 
		fieldcontent += "<span>" +res[fieldtypeid]+ "</span>";
	break;
		case "39f6c3e6-d446-4e53-9706-67f556b06bdb":
	/*Ganzzahl */
			if(res[fieldtypeid].val.length >0 )
		{
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span><span>";
	       fieldcontent += res[fieldtypeid].val + " " + res[fieldtypeid].postfix + "</span>";
		}
	break;
	case "278bd56c-e463-4ec4-a702-82a9ecd59137":
	/* decimal */
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span><span>";
	    fieldcontent += res[fieldtypeid].val + " " + res[fieldtypeid].postfix + "</span>";
	
	break;
	/*Image */
		case "8787e054-7bb9-411a-8946-da658fbc8e9f":	
						if(res.fieldid==='c8e7d134-1005-4756-b851-828767c84e8e')
					{	
				
	/*	fieldcontent += "<span class=\"fieldnamesearchbox\">" +res.name + ": </span>"; */
	
	   fieldcontent += "<span><img style=\"float:left;max-height:800px;max-width:150px;\" src=" +res[fieldtypeid].filepath + "\"/></span>"; 
		imagefield = j;
					}
	break;
		case "8FE72E9F-09CC-4225-9135-58E042DC98A8":	
		/*selector*/
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span>"; 
	    fieldcontent += "<span>" + res[fieldtypeid] + " </span>";
	break;
	case "433711f1-0eec-4be4-b552-da385ebddbb8":	
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span><span class=\"tagobject\">"; 
		/*tags*/
	 
		for(var tag=0;tag < res[fieldtypeid].length;tag++)
		{
		fieldcontent += "<span><a href=\"/tag/" + res[fieldtypeid][tag].tagid + "/\">" +res[fieldtypeid][tag].tagname + ";</a> </span>";
		}
		fieldcontent += "</span>";
		
	break;
		case "fac261ca-829e-4f44-92fe-51a0d9d080c3":	
		/*Adresse Geolocation
		https://www.google.de/maps/place/Rosensteinstraße,+71032+Böblingen
		*/
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span>"; 
	    fieldcontent += "<span><a href=\"https://www.google.de/maps/place/" + res[fieldtypeid].adr + "\">" + res[fieldtypeid].adr +"</a> </span>";
	break;
	default:

		}
		fieldcontent += "</span>";
		var person = {elementcount:j, content:fieldcontent}; 
		fieldcontent="";
		results.push(person); 
		} 

		var ret = results[imagefield].content;		
		for(var y=0; y < results.length;y++)
		{
			if(y !== imagefield)
			{
				ret += results[y].content;
			}
		}

/*	resultComplete += searchtemplate.replace('|list|',jsonobject.listname).replace('|modifieddate|',jsonobject.modified).replace('|objecttypname|',jsonobject.objecttypname).replace('|dateticks|',jsonobject.dateticks).replace('|listid|',jsonobject.listid).replace('|linktoobject|','/list/' + jsonobject.listid + '/edit/' + jsonobject.objectid + '/').replace('|content|',fieldcontent);
	resultComplete += "</div>"; */
	return ret; 

		}


static getlistpopupcontent(jsonobject)
		{	
		if(jsonobject.state==='error')
			return "";
		


  var results = [];  
		var fieldcontent = "";
		var imagefield = -1;
		for(var j=0; j < jsonobject.fields.length;j++)
		{
		// durch die Felder
		
				var res = jsonobject.fields[j];

			switch(res.fieldid)
			{
				case "6dacefca-1e0d-4bdd-9c82-fe89b517242b":
				case "0cecad40-c0a1-46e3-a7a3-4bd99b7236d6":
				case "77f42b3d-8e2c-4869-bd67-6273afe7cf89":
				case "1d57184b-430b-44c8-9708-d3e3dd8bcce4":
				case "7628c3c9-6e30-4b1a-8f74-cc8ae8366d0c":
				continue;

				default:
			}



				var fieldtypeid = res.fieldtypeid;
	         	fieldcontent += "<span class=\"tooltiptext\">";
		    	

	switch (fieldtypeid) {
		

		
				    case "52a3bb09-fc62-4099-a7ff-c314074f274a":
	// URL Links
        fieldcontent += " <span class=\"fieldnamesearchbox\"> <b>" + res.name + ": </b></span><span>";
	
		for(var link=0;link < res[fieldtypeid].length;link++)
		{
		 fieldcontent += "<span><a href=\"" +res[fieldtypeid][link].url + "/\">"+  res[fieldtypeid][link].description + " <img height=\"20px\" src=\"https://publicfile.standard-cloud.com/icons/link.png\"> </a> ";
		}
		fieldcontent = fieldcontent.substring(0,fieldcontent.length -2);
	    fieldcontent += " </span>";
        break;
	
    case "cc333aeb-196d-4331-b1df-3b530fa67722":
	// Benutzer
        fieldcontent += " <span class=\"fieldnamesearchbox\"> <b>" + res.name + ": </b></span><span>";
	
		for(var user=0;user < res[fieldtypeid].length;user++)
		{
		 fieldcontent += "<span>"+  res[fieldtypeid][user].firstname + " " + res[fieldtypeid][user].lastname + "</span>; "
		}

		fieldcontent = fieldcontent.substring(0,fieldcontent.length -2);
		fieldcontent += " </span>";

        break;
    case "32b60712-1f4d-4a49-87a6-90de0363eef4":
	// Text
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" + res.name + ": </b></span><span>";
	    if(res[fieldtypeid].length < 70)
		{
		fieldcontent += stddisplayobject.removeHTMLTags(res[fieldtypeid]) + " </span>";
		}else
		{
		fieldcontent += stddisplayobject.removeHTMLTags(res[fieldtypeid]).substring(0,600) + "... </span>";
		}
		break;
	case "17a8dd83-0068-4e4a-bfcc-082f7f64e6f8":
	/*datetime */
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span><span>";
		/* fieldcontent += res[fieldtypeid].datetime  2015-12-11T00:00:00.000+0100*/
		if(res[fieldtypeid].datetime !=null)
		{	
	   fieldcontent += res[fieldtypeid].datetime.substring(8,10) + "." + res[fieldtypeid].datetime.substring(5,7) + "." + res[fieldtypeid].datetime.substring(0,4) + " - " + res[fieldtypeid].datetime.substring(11,13) +":" + res[fieldtypeid].datetime.substring(14,16)  +" </span>"; 
		}
	break;
		case "39f6c3e6-d446-4e53-9706-67f556b06bdb":
	/*Ganzzahl */
			if(res[fieldtypeid].val.length >0 )
		{
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span><span>";
	         fieldcontent += res[fieldtypeid].val + " " + res[fieldtypeid].postfix + "</span>";
		}
	break;
	case "278bd56c-e463-4ec4-a702-82a9ecd59137":
	/* decimal */
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span><span>";
	    fieldcontent += res[fieldtypeid].val + " " + res[fieldtypeid].postfix + "</span>";
	
	break;
	/*Image */
		case "8787e054-7bb9-411a-8946-da658fbc8e9f":
		/*	fieldcontent += "<span class=\"fieldnamesearchbox\">" +res.name + ": </span>"; */
		if(res[fieldtypeid].filepath !== undefined)
		{			
			fieldcontent += "<span><img style=\"float:left;max-height:80px;max-width:150px;\" src=" +res[fieldtypeid].filepath + "\"/></span>"; 
			imagefield = j;
		}		
	break;
		case "8FE72E9F-09CC-4225-9135-58E042DC98A8":	
		/*selector*/
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span>"; 
	    fieldcontent += "<span>" + res[fieldtypeid] + " </span>";
	break;
	case "433711f1-0eec-4be4-b552-da385ebddbb8":	
		/*tags*/
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span>"; 
		for(var tag=0;tag < res[fieldtypeid].length;tag++)
		{
		fieldcontent += "<span><a href=\"/tag/" + res[fieldtypeid][tag].tagid + "/\">" +res[fieldtypeid][tag].tagname + ";</a></span>";
		}
		break;
	case "fd3eb39b-3f6c-4e43-baea-12135ed5bee7":	
		/*Boolean*/
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span>"; 
		fieldcontent += "<span>" +res[fieldtypeid]+ "</span>";
		break;	
	case "971c3761-5565-4a65-b546-7885b8bef160":
	/*ObjectinList*/
fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span>"; 
		for(var obj=0;obj < res[fieldtypeid].length;obj++)
		{
		fieldcontent += "<span>"+ res[fieldtypeid][obj].value + " </span>";
		}

		
	break;
		case "fac261ca-829e-4f44-92fe-51a0d9d080c3":	
		/*Adresse Geolocation
		https://www.google.de/maps/place/Rosensteinstraße,+71032+Böblingen
		*/
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span>"; 
	    fieldcontent += "<span><a href=\"https://www.google.de/maps/place/" + res[fieldtypeid].adr + "\">" + res[fieldtypeid].adr +"</a> </span>";
	break;
	default:

		}
		fieldcontent += "</span>";
		var person = {elementcount:j, content:fieldcontent}; 
		fieldcontent="";
		results.push(person); 
		} 


		var ret = "";
		if(imagefield !== -1)
		{
			if(jsonobject.fields[imagefield].content !== undefined)
				ret += jsonobject.fields[imagefield].content;		
		}

				
		for(var y=0; y < results.length;y++)
		{
			if(y !== imagefield)
			{
				ret += results[y].content;
			}
		}

	return ret; 


}


			
		static getcontent(jsonobject)
		{	
		if(jsonobject.state==='error')
			return "";
		
		if(jsonobject.objecttypid==='707ce4a3-ed82-4029-ac2f-9acb4f3771bd')
		{			
			return stddisplayobject.getprofilecontent(jsonobject);
		}


  var results = [];  
		var fieldcontent = "";
		var imagefield = -1;
		for(var j=0; j < jsonobject.fields.length;j++)
		{
		// durch die Felder
		
				var  res = jsonobject.fields[j];

			switch(res.fieldid)
			{
				case "6dacefca-1e0d-4bdd-9c82-fe89b517242b":
				case "0cecad40-c0a1-46e3-a7a3-4bd99b7236d6":
				case "77f42b3d-8e2c-4869-bd67-6273afe7cf89":
				case "1d57184b-430b-44c8-9708-d3e3dd8bcce4":
				case "7628c3c9-6e30-4b1a-8f74-cc8ae8366d0c":
				continue;
				default:
			}
			


				var fieldtypeid = res.fieldtypeid;
	         	fieldcontent += "<span class=\"tooltiptext\">";
				
			

	switch (fieldtypeid) {
		

		
				    case "52a3bb09-fc62-4099-a7ff-c314074f274a":
	// URL Links
        fieldcontent += " <span class=\"fieldnamesearchbox\"> <b>" + res.name + ": </b></span><span>";
	
		for(var link=0;link < res[fieldtypeid].length;link++)
		{
		 fieldcontent += "<span><a href=\"" +res[fieldtypeid][link].url + "/\">"+  res[fieldtypeid][link].description + " <img height=\"20px\" src=\"https://publicfile.standard-cloud.com/icons/link.png\"> </a> ";
		}
		fieldcontent = fieldcontent.substring(0,fieldcontent.length -2);
	    fieldcontent += " </span>";
        break;
	
    case "cc333aeb-196d-4331-b1df-3b530fa67722":
	// Benutzer
        fieldcontent += " <span class=\"fieldnamesearchbox\"> <b>" + res.name + ": </b></span><span>";
	
		for(var user=0;user < res[fieldtypeid].length;user++)
		{
		 fieldcontent += "<span>" +  res[fieldtypeid][user].firstname + " " + res[fieldtypeid][user].lastname + "</span>; "
		}
	
		fieldcontent = fieldcontent.substring(0,fieldcontent.length -2);
		fieldcontent += " </span>";
	
        break;
    case "32b60712-1f4d-4a49-87a6-90de0363eef4":
	// Text
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" + res.name + ": </b></span><span>";
	    if(res[fieldtypeid].length < 70)
		{
		fieldcontent += stddisplayobject.removeHTMLTags(res[fieldtypeid]) + " </span>";
		}else
		{
		fieldcontent += stddisplayobject.removeHTMLTags(res[fieldtypeid]).substring(0,70) + "... </span>";
		}
		break;
	case "17a8dd83-0068-4e4a-bfcc-082f7f64e6f8":
	/*datetime */
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span><span>";
		/* fieldcontent += res[fieldtypeid].datetime  2015-12-11T00:00:00.000+0100*/
		if(res[fieldtypeid].datetime !=null)
		{	
	   fieldcontent += res[fieldtypeid].datetime.substring(8,10) + "." + res[fieldtypeid].datetime.substring(5,7) + "." + res[fieldtypeid].datetime.substring(0,4) + " - " + res[fieldtypeid].datetime.substring(11,13) +":" + res[fieldtypeid].datetime.substring(14,16)  +" </span>"; 
		}
	break;
		case "39f6c3e6-d446-4e53-9706-67f556b06bdb":
	/*Ganzzahl */
			if(res[fieldtypeid].val.length >0 )
		{
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span><span>";
	         fieldcontent += res[fieldtypeid].val + " " + res[fieldtypeid].postfix + "</span>";
		}
	break;
	case "278bd56c-e463-4ec4-a702-82a9ecd59137":
	/* decimal */
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span><span>";
	    fieldcontent += res[fieldtypeid].val + " " + res[fieldtypeid].postfix + "</span>";
	
	break;
	/*Image */
		case "8787e054-7bb9-411a-8946-da658fbc8e9f":
		/*	fieldcontent += "<span class=\"fieldnamesearchbox\">" +res.name + ": </span>"; */
		if(res[fieldtypeid].filepath !== undefined)
		{			
			fieldcontent += "<span><img style=\"float:left;max-height:80px;max-width:150px;\" src=\"" + BACKEND + "/file/"+ res[fieldtypeid].filepath + "\"/></span>"; 
			
			imagefield = j;
		}		
	break;
		case "8FE72E9F-09CC-4225-9135-58E042DC98A8":	
		/*selector*/
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span>"; 
	    fieldcontent += "<span>" + res[fieldtypeid] + " </span>";
	break;
	case "433711f1-0eec-4be4-b552-da385ebddbb8":	
		/*tags*/
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span>"; 
		for(var tag=0;tag < res[fieldtypeid].length;tag++)
		{
		fieldcontent += "<span><a href=\"/tag/" + res[fieldtypeid][tag].tagid + "/\">" +res[fieldtypeid][tag].tagname + ";</a></span>";
		}
	break;
	case "fd3eb39b-3f6c-4e43-baea-12135ed5bee7":	
		/*Boolean*/
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span>"; 
		fieldcontent += "<span>" +res[fieldtypeid]+ "</span>";
		break;	
	case "971c3761-5565-4a65-b546-7885b8bef160":
	/*ObjectinList*/
fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span>"; 
		for(var obj=0;obj < res[fieldtypeid].length;obj++)
		{
		fieldcontent += "<span>"+ res[fieldtypeid][obj].value + " </span>";
		}

		
	break;
		case "fac261ca-829e-4f44-92fe-51a0d9d080c3":	
		/*Adresse Geolocation
		https://www.google.de/maps/place/Rosensteinstraße,+71032+Böblingen
		*/
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span>"; 
	  //  fieldcontent += "<span><a href=\"https://www.google.de/maps/place/" + res[fieldtypeid].adr + "\">" + res[fieldtypeid].adr +"</a> </span>";
	  fieldcontent += "<span>" + res[fieldtypeid].adr +"</span>";
	  break;
	  default:
		}
		
		fieldcontent += "</span>";
		var person = {elementcount:j, content:fieldcontent}; 
		fieldcontent="";
		results.push(person); 
		}
		

		var ret = "";
		if(imagefield !== -1)
		{
			if(jsonobject.fields[imagefield].content !== undefined)
				ret += jsonobject.fields[imagefield].content;		
		}
	
		for(var y=0; y < results.length;y++)
		{
			if(y !== imagefield)
			{
				ret += results[y].content;
			}
		}

	return ret; 


}
	
	
	
static profilelistdata(jsonobject)
		{	
		if(jsonobject.state==='error')
			return "";
		
		if(jsonobject.objecttypid==='707ce4a3-ed82-4029-ac2f-9acb4f3771bd')
		{			
			return stddisplayobject.getprofilecontent(jsonobject);
		}

  var results = [];  
		var fieldcontent = "";
		var imagefield = 0;
		for(var j=0; j < jsonobject.fields.length;j++)
		{
		
		
		// durch die Felder
		var res = jsonobject.fields[j];
		
		var fieldtypeid = res.fieldtypeid;
		var fieldid = res.fieldid;
			switch(fieldid)
			{
			/*SYS-User */
			case "5be2d63b-b05a-421e-957b-61b288061956":
			case "26dfa998-2e2b-4abf-9f59-4a5d8853fd20":
			case "46e9b922-3add-4ce7-a030-ef550541c4a8":
			case "29bb19d4-b9d3-406d-a6ae-131177f9d349":
			case "20b62a3a-2545-4dab-a5c4-b60badd589a8":
			case "a11a1a60-0ce4-4a7a-a594-dcf5b20aeda8":
			case "76f26a5d-06a9-45e1-903e-bbf21a381835":
			case "245adad1-8745-4f5a-8a17-eb72f57a8dfe":
			case "77f42b3d-8e2c-4869-bd67-6273afe7cf89":
			case "0cecad40-c0a1-46e3-a7a3-4bd99b7236d6":	
			case "7628c3c9-6e30-4b1a-8f74-cc8ae8366d0c":
			
			continue; 
			default:
			}
			
			if(res[fieldtypeid].length === 0)
			{			
				continue;
			}
			

     	fieldcontent += "<span class=\"tooltiptext\">";
		    	
				
	switch (fieldtypeid) {
	
    case "cc333aeb-196d-4331-b1df-3b530fa67722":
	// Benutzer
        fieldcontent += " <span class=\"fieldnamesearchbox\"> <b>" + res.name + ": </b></span><span>";
	
		for(var user=0;user < res[fieldtypeid].length;user++)
		{
		 fieldcontent += "<span>"+  res[fieldtypeid][user].firstname + " " + res[fieldtypeid][user].lastname + "</span>; "
		}
	
		fieldcontent = fieldcontent.substring(0,fieldcontent.length -2);
		fieldcontent += " </span>";
	
        break;
    case "32b60712-1f4d-4a49-87a6-90de0363eef4":
	// Text
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" + res.name + ": </b></span><span>";
	    if(res[fieldtypeid].length < 70)
		{
		fieldcontent += stddisplayobject.removeHTMLTags(res[fieldtypeid]) + " </span>";
		}else
		{
		fieldcontent += stddisplayobject.removeHTMLTags(res[fieldtypeid]).substring(0,70) + "... </span>";
		}
		break;
	case "17a8dd83-0068-4e4a-bfcc-082f7f64e6f8":
	/*datetime */
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span><span>";
		/* fieldcontent += res[fieldtypeid].datetime  2015-12-11T00:00:00.000+0100*/
		if(res[fieldtypeid].datetime !=null)
		{	
	   fieldcontent += res[fieldtypeid].datetime.substring(8,10) + "." + res[fieldtypeid].datetime.substring(5,7) + "." + res[fieldtypeid].datetime.substring(0,4) + " - " + res[fieldtypeid].datetime.substring(11,13) +":" + res[fieldtypeid].datetime.substring(14,16)  +" </span>"; 
		}
	break;
		case "39f6c3e6-d446-4e53-9706-67f556b06bdb":
	/*Ganzzahl */
		if(res[fieldtypeid].val.length >0 )
		{
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span><span>";
        fieldcontent += res[fieldtypeid].val + " " + res[fieldtypeid].postfix + "</span>";
		}
	break;
	case "278bd56c-e463-4ec4-a702-82a9ecd59137":
	/* decimal */
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span><span>";
	    fieldcontent += res[fieldtypeid].val + " " + res[fieldtypeid].postfix + "</span>";
	
	break;
		case "8FE72E9F-09CC-4225-9135-58E042DC98A8":	
		/*selector*/
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span>"; 
	    fieldcontent += "<span>" + res[fieldtypeid] + " </span>";
	break;
	case "433711f1-0eec-4be4-b552-da385ebddbb8":	
	fieldcontent += "<span class=\"tagobject\"><span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span>"; 
		/*tags*/
		for(var tag=0;tag < res[fieldtypeid].length;tag++)
		{
		fieldcontent += "<span><a href=\"/tag/" + res[fieldtypeid][tag].tagid + "/\">" +res[fieldtypeid][tag].tagname + ";</a></span>";
		}
		fieldcontent += "</span>";
		break;
	case "fd3eb39b-3f6c-4e43-baea-12135ed5bee7":	
		/*Boolean*/
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span>"; 
		fieldcontent += "<span>" +res[fieldtypeid]+ "</span>";
	

		
	break;
		case "fac261ca-829e-4f44-92fe-51a0d9d080c3":	
		/*Adresse Geolocation
		https://www.google.de/maps/place/Rosensteinstraße,+71032+Böblingen
		*/
		fieldcontent += "<span class=\"fieldnamesearchbox\"><b>" +res.name + ": </b></span>"; 
	    fieldcontent += "<span><a href=\"https://www.google.de/maps/place/" + res[fieldtypeid].adr + "\">" + res[fieldtypeid].adr +"</a> </span>";
	break;
	default:

		}
		fieldcontent += "</span>";
		var person = {elementcount:j, content:fieldcontent}; 
		fieldcontent="";
		results.push(person); 
		} 

		var ret = results[imagefield].content;		
		for(var y=0; y < results.length;y++)
		{
			if(y !== imagefield)
			{
				ret += results[y].content;
			}
		}

	return ret; 


}
}
	
	
export default stddisplayobject;
