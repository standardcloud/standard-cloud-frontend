import {BACKEND} from '../../stdsetup';
import $ from 'jquery';
import Api from '../api';



class FieldManager
{
		static getfieldlist()
		{
	  //return Api.call("/admin/field/getfieldlist/","GET"); 
	  // /field/json
	  return Api.gocall("/field/json","GET"); 
		}
		
		static getfieldtypelist()
		{
			return Api.call("/admin/fieldtype/getfieldtypelist/","GET"); 
		}		

		static newfield(fieldjson,fieldtypejson, callback)
		{
			var fielddata = {};
			for(var key in fieldtypejson) {
				if (!fieldtypejson.hasOwnProperty(key)) continue;
				 fielddata[key] = fieldtypejson[key];
				}
				 fielddata.id = fieldjson.id;
				 fielddata.name = fieldjson.name;
				 fielddata.description = fieldjson.description;
				 fielddata.mandatory = fieldjson.mandatory;
				 fielddata.unique = fieldjson.unique;
				 fielddata.uniquetype = fieldjson.uniquetype;
				 fielddata.writeonce = fieldjson.writeonce;
				 fielddata.fieldtypeid = fieldjson.fieldtypeid;
	
			$.ajax({
		  type: "POST",
			data: fielddata,	
			xhrFields: {
				withCredentials: true
			 },
		  url: BACKEND + "/admin/field/new/",
		  dataType: "json",
		  success: function( data, textStatus, jqXHR) {
			callback(data); 				
				 },
		  error: function(jqXHR, textStatus, errorThrown){
				console.log('Felder konnten nicht geladen werden.');		
		  }
		  
		}); 
		}

		static getfieldsettings(fieldid)
		{
			var data = new FormData();
			data.append("fieldid", fieldid);
			return Api.call("/admin/field/getfieldsettings/","POST",data); 
		}
		
		static getfield4list(listid)
		{
			var data = new FormData();
			data.append("listid", listid);
			return Api.call("/admin/field/getfield4list/","POST",data); 
		}
		
		static getfield4objecttype(objecttypeid)
		{
			var data = new FormData();
			data.append("objecttypeid", objecttypeid);
			return Api.call("/admin/field/getfield4objecttype/","POST",data); 
		}
		
		static removefield = function(fieldid)
		{
			var data = new FormData();
			data.append("id", fieldid);
			return Api.call("/admin/field/remove/","POST",data); 
		}
		
	}	
	
export default FieldManager;





