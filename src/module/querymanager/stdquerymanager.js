import $ from 'jquery';
import {BACKEND} from '../../stdsetup';
import Api from '../api';


class QueryManager
{
		static sendquery(title,description,objecttype,list,field,filter,joins, callback)
		{
		   var QueryData = {};
		   QueryData.title = title;
		   QueryData.description = description;
		   QueryData.objecttype = JSON.stringify(objecttype);
		   QueryData.list = JSON.stringify(list);
		   QueryData.field = JSON.stringify(field);
		   QueryData.filter = filter;
		   QueryData.joins = joins;
			$.ajax({
			  type: "POST",
			  url: BACKEND + "/query/new/",
				data: QueryData,
				xhrFields: {
					withCredentials: true
				 },
			  dataType: "json",
		  success: function(data, textStatus, jqXHR) {
						callback(data);
		   },
		  error: function(jqXHR, textStatus, errorThrown){

		  }

		});

		}

		static newquery(queryid,title,description,lists,fields,joins,filters,filterjson,publicquery,publicqueryname,sort,objectlimit,callback)
		{
		   var QueryData = {};
		   QueryData.title = title;
		   QueryData.description = description;
		   QueryData.queryid = queryid;
		   QueryData.publicquery = publicquery;
			 QueryData.objectlimit = objectlimit;
			 QueryData.publicqueryname = publicqueryname;
		   QueryData.list = JSON.stringify(lists);
		   QueryData.field = JSON.stringify(fields);
		   QueryData.filter = JSON.stringify(filters);
		   QueryData.joins = JSON.stringify(joins);
		   QueryData.filterjson = JSON.stringify(filterjson);
		   QueryData.sort = JSON.stringify(sort);
			$.ajax({
			  type: "POST",
			  url: BACKEND + "/query/new/",
					dataType: "json",
					xhrFields: {
						withCredentials: true
					 },
			  data: QueryData,
		  success: function(data, textStatus, jqXHR) {
						callback(data);
		   },
		  error: function(jqXHR, textStatus, errorThrown){

		  }

		});

		}



	 static getquerysetting(queryid)
		{
			var data = new FormData();
			data.append("queryid", queryid);
			return Api.call("/query/edit/" ,"POST",data);
		}




		static removequery(queryid)
		{
			var data = new FormData();
			data.append("queryid", queryid);
			return Api.call("/query/remove/" ,"POST",data);
		}


		static getquery(queryid)
		{
			return Api.call("/queryresult/" +queryid +"/","GET"); 
		}

	  static getpublicquery(queryid)
		{
			return Api.call("/publicqueryresult/" +queryid +"/","GET"); 
		}

		static getqueries()
		{
			return Api.call("/queries/","GET"); 
		}

		static getmyqueries()
		{
			return Api.call("/myqueries/","GET"); 
		}


		static getqueriesfiltered(listid)
		{
			return Api.call("/queries/listfilterresult/" + listid +"/","GET"); 
		}



		static setmyquery(jueryid,querystatus)
		{
			var data = new FormData();
			data.append("queryid", jueryid);
			data.append("status", querystatus);
			return Api.call("/query/setmyquery/" ,"POST",data);
		}



		static getquerycount(listid)
		{
			var data = new FormData();
			data.append("listid", listid);
			return Api.call("/query/listcount/" ,"POST",data);
		}

	}

export default QueryManager;

