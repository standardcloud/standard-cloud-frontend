import Api from '../api';

class CommentManager
{
static getcomment(ChangeLogID)
		{		
			var data = new FormData();
			data.append("ChangeLogID", ChangeLogID);
      return Api.call("/api/comment/getcomment/","POST",data); 
		}

static addcomment(ChangeLogID, comment)
		{	
			var data = new FormData();
			data.append("ChangeLogID", ChangeLogID);
			data.append("Comment", comment);
      return Api.call("/api/comment/addcomment/","POST",data); 
		}
		
		
static updatecomment(commentid,comment)
		{	
			var data = new FormData();
			data.append("id", commentid);
			data.append("comment", comment);
      return Api.call("/api/comment/updatecomment/","POST",data); 

		}
		
static removecomment(commentid)
		{	
			var data = new FormData();
			data.append("commentid", commentid);
      return Api.call("/api/comment/removecomment/","POST",data); 
	}

}
 export default CommentManager;
