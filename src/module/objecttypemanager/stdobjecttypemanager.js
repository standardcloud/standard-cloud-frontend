import Api from '../api';

class ObjectTypeManager
{

		static getobjecttypes()
		{
			//return Api.gocall("/control/objecttype/getobjecttypes/","GET"); 
			return Api.gocall("/objecttype/json","GET"); 
     	}


	
	static deletefield(fieldid,objecttypeid)
		{		
			var data = new FormData();
			data.append("objecttypeid", objecttypeid);
			data.append("fieldid", fieldid);
			return Api.call("/objecttype/removefield/","POST",data); 
 		}

	static addfield(fieldid,objecttypeid)
		{		
			var data = new FormData();
			data.append("objecttypeid", objecttypeid);
			data.append("fieldid", fieldid);
			return Api.call("/objecttype/addfield/","POST",data); 
   	}



	static getobjecttypesettings(objecttypeid)
		{		
			var data = new FormData();
			data.append("objecttypeid", objecttypeid);
			return Api.call("/objecttype/getsettings/","POST",data); 
		}
			

static getviews(objecttypeid)
		{		
			var data = new FormData();
			data.append("objecttypeid", objecttypeid);
			return Api.call("/admin/view/getview4objecttype/","POST",data); 
		}

			
			
static new(name,nameplural, description)
		{		
			var data = new FormData();
			data.append("name", name);
			data.append("nameplural", nameplural);
			data.append("description", description);
			return Api.call("/objecttype/new/","POST",data); 
			}
			


static setobjecttypesettings(objecttypeid,name,nameplural,description,enableanalytics,navigationfieldid)
		{		
			var data = new FormData();
			data.append("objecttypeid", objecttypeid);
			data.append("name", name);
			data.append("nameplural", nameplural);
			data.append("description", description);
			data.append("enableanalytics", enableanalytics);
			data.append("navigationfieldid", navigationfieldid);
			return Api.call("/objecttype/basis/","POST",data); 
		}
			


static deleteobjecttype(objecttypeid)
		{

			var data = new FormData();
			data.append("objecttypeid", objecttypeid);
			return Api.call("/objecttype/remove/","POST",data); 
		}
	}
	
	
export default ObjectTypeManager;