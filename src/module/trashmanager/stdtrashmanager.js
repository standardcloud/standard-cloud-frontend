import Api from '../api';		
class TrashManager
{
		static getall()
		{	  
			return Api.call("/admin/trash/getitems/","GET");	  
		}

		static getTrashListItems(listid)
		{
			var data = new FormData();
			data.append("listid", listid);
			return Api.call("/admin/trash/getlistitems/","POST",data);
		}

		static getTrashItem(id)
		{
			var data = new FormData();
			data.append("id", id);
			return Api.call("/admin/trash/getitem/","POST",data);
		}

		static restoreTrashItem(id)
		{
			var data = new FormData();
			data.append("id", id);
			return Api.call("/admin/trash/restoreobject/","POST",data); 
		}

		static deleteTrashItem(id)
		{
			var data = new FormData();
			data.append("id", id);
			return Api.call("/admin/trash/deleteobject/","POST",data); 
		}

}
export default TrashManager;
