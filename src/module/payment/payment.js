import $ from 'jquery';
import {BACKEND} from '../../stdsetup';


	class Paymentmanager{
	
		static saveStripeToken(token, callback)
		{	
			 $.ajax({
			  type: "POST",
			  xhrFields: {
				withCredentials: true
			 },
			  url: `${BACKEND}/payment/savestripetoken/`,
			  data: token,
			  dataType: "json",
			  success: function( data, textStatus, jqXHR) {	
	            callback(data);
					},
			  error: function(jqXHR, textStatus, errorThrown){
			 callback(null);
			 }});
		}

		static getPayment(callback)
		{	
			var data = {};
			data.t = "123";
			 $.ajax({
			  type: "POST",
			  xhrFields: {
				withCredentials: true
			 },
			  url: `${BACKEND}/payment/getcustomer/`,
			  data: data,
			  dataType: "json",
			  success: function( data, textStatus, jqXHR) {	
	            callback(data);
					},
			  error: function(jqXHR, textStatus, errorThrown){
			 callback(null);
			 }});
		}
  		
	}	
		
	
export default Paymentmanager;





