import moment from 'moment';
import Api from '../api';		
		

class UserManager
{


	static compare(a, b) {
		const genreA = a.name.toUpperCase();
		const genreB = b.name.toUpperCase();	
		let comparison = 0;
		if (genreA > genreB) {
			comparison = 1;
		} else if (genreA < genreB) {
			comparison = -1;
		}
		return comparison;
	}


	static getfollowUserdata(userid)
	{
		var data = new FormData();
		data.append("objectid", userid);
		return Api.call("/api/follow/getstatus/","POST",data);
	}



		static getusers()
		{
			return Api.call("/api/user/getusers/","GET");//.then(data => data.sort(self.compare)); 
		}
				

		static searchuser(userterm)
		{
			var data = new FormData();
			data.append("term", userterm);
			return Api.call("/api/user/getusers/","POST",data);
		}

	static getuser(userid)
		{			
			return Api.call('/user/getuser/' + userid +'/' ,"GET");
	}

static gettimeline(callback)
	{
		var self = this;
		let results=[];

		Api.call('/user/gettimeline/' ,"GET").then(data =>
			{
				var today = new Date();
				var mindate = new Date();
				mindate.setDate(today.getDate() - 2);
				var maxdate = new Date();
				maxdate.setDate(today.getDate() +2);
			for (var i = 0; i < data.content.length; i++) { 		
						var datebox = {};				
						datebox.objecttypname = data.data[i][0];
						datebox.listname = data.data[i][1];
						datebox.listid = data.data[i][2];	
						datebox.fieldname = data.data[i][3];
						datebox.date = self.parseDate(data.data[i][4]);
						datebox.datemoment = moment(self.parseDate(data.data[i][4])).fromNow();
						datebox.datestr = data.data[i][4].replace('00:00','');
						datebox.objectid = data.data[i][5];
						datebox.content = ""
						results.push(datebox);				
			
				}
callback(results);

			});

	}
	

	parseDate(input) {
		 var tag = input.substring(0,2);
		 var monat = input.substring(3,5);
		 var jahr = input.substring(6,10);
		 var stunde = input.substring(11,13);
		 var minute = input.substring(14,16);
		 return new Date(jahr, monat-1, tag,stunde,minute);
	 }

static gethistory(callback)
	{
				 let results = [];
				 Api.call('/user/gethistory/' ,"GET").then(data =>
					{
						for (let i=0; i < data.content.length; i++) { 		
							var eventbox = {};			
							eventbox.list = data.content[i][0];
							eventbox.object = data.content[i][1];
							eventbox.type = data.content[i][2];	
							eventbox.date = data.content[i][3];
							eventbox.listid = data.content[i][4];
							eventbox.objectid = data.content[i][5];
							eventbox.content = "";
							results.push(eventbox);		
					}
					callback(results);		
					});
	}

	static newuser(email,firstname,lastname,type,pwd)
	{
		var data = new FormData();
		data.append("mailaddress", email);
		data.append("passwordtype", type);
		data.append("firstname", firstname);
		data.append("lastname", lastname);
		data.append("userpwd", pwd);
		return Api.call("/admin/usermanagement/newuser/","POST",data);
	}		



		

	
	static deleteuser(userid) 
	{
		sessionStorage.clear();
		return Api.call('/user/' + userid + '/',"DELETE");
}	


		

		
static actvateuser(userid,active)
		{			
			var data = new FormData();
			data.append("userid", userid);
			data.append("active", active);
			sessionStorage.clear();	
			return Api.call("/admin/usermanagement/activateuser/","POST",data);
    }
		
		
		static resetpassword(resetuserid,passwordtype,password)
		{			
			var data = new FormData();
			data.append("passwordtype", passwordtype);
			data.append("resetuserid", resetuserid);
			data.append("userpwd", password);
			return Api.call("/admin/usermanagement/resetuserpassword/","POST",data);
		}
		
		
	

	static updatestatus(userid,status)
		{			

			var data = new FormData();
			data.append("userid", userid);
			data.append("status", status);
			return Api.call("/api/userstatus/updateuserstatus/","POST",data);	
		}
		
		
	static addpersonalpost(text)
		{	
			var data = new FormData();
			data.append("CommentText", text);
			return Api.call("/user/addpersonalpost/","POST",data);	
		}

		static editpersonalpost(changeid,commenttext)
		{	
			var data = new FormData();
			data.append("changeid", changeid);
			data.append("commenttext", commenttext);
			return Api.call("/user/editpersonalpost/","POST",data);
		}
		
		static removepersonalpost(changeid)
		{	

			var data = new FormData();
			data.append("changeid", changeid);
			return Api.call("/user/removepersonalpost/","POST",data);
		}
	

	}
export default UserManager;



