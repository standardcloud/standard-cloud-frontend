import {BACKEND} from '../stdsetup';
import {GOBACKEND} from '../stdsetup';
/*
var exampleSocket = new WebSocket("ws://localhost:8000/ws");
exampleSocket.onmessage = function (event) {
  console.log(event.data);
}
*/
class Api
{
	static async call(url,Method, Parameter) {

        var request;
        switch(Method)
        {
         case "GET":
         request = new Request(BACKEND +url, {
                method: "GET",mode: "cors",credentials: "include"
              });	
         break;	
         case "DELETE":
         request = new Request(BACKEND +url, {
                method: "DELETE",mode: "cors",credentials: "include"
              });	
         break;	
         case "POST":
         request = new Request(BACKEND +url, {
            method: 'POST',mode: "cors",credentials: "include",
            headers: {
                'Accept': 'application/json'
              },
              body: Parameter
          });	
          break;
          default:
                   
        }
        return fetch(request).then((response) => response.json()).catch(error => console.log(error));	
  }
  
  static async gocall(url,Method, Parameter) {

    var request;
    switch(Method)
    {
     case "GET":
     request = new Request(GOBACKEND +url, {
            method: "GET",credentials: "include"
          });	
     break;	
     case "DELETE":
     request = new Request(GOBACKEND +url, {
            method: "DELETE",credentials: "include"
          });	
     break;	
     case "POST":
     request = new Request(GOBACKEND +url, {
        method: 'POST',mode: "cors",credentials: "include",
        headers: {
            'Accept': 'application/json'
          },
          body: JSON.stringify(Parameter)
      });	
      break;
      default:
               
    }
    return fetch(request).then((response) => response.json()).catch(error => console.log(error));	
}
}
export default Api;