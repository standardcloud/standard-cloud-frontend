import Api from '../api';

class ConnectionManager
{
		static getConnection4Object(ObjectID, ListID)
		{		
			var data = new FormData();
			data.append("listid", ListID);
			data.append("objectid", ObjectID);
	    	return Api.call("/list/objectconnection/","POST",data);		  
        }
}
export default ConnectionManager;