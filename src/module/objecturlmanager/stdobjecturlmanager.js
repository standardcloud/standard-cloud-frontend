class ObjectURLManager
{		
		static geturl(listid, objectid,callback)		
		{
			var lists = window.store.getState().list;
			console.log(lists);
			var list = lists.find(function(li) {
				return li.id === listid;

			  });
			var settings = list;
     		var url = "/list/" + listid + "/edit/" + objectid ;		

			if(settings.openeditmode)
            {
				if(settings.defaultviewedit==="")
				{
					url+= "/editmode/";
				}else
				{
					url += "/objectview/" + settings.defaultviewedit + "/editmode/";
				}
			}else
			{
				if(settings.defaultviewread==="")
				{
					url+= "/readmode/";
				}else
				{
					url+= "/objectview/" + settings.defaultviewread + "/readmode/";
				}            
			}  
			callback(url);
			
		}
		
		static getediturl(listid, objectid,callback)		
		{			
	
			var lists = window.store.getState().list;
			var list = lists.find(l=> l.id === listid);
			var url = "/list/" + listid + "/edit/" + objectid ;		

				if(list.defaultviewedit==="")
				{
					url+= "/editmode/";
				}else
				{
					url += "/objectview/" + list.defaultviewedit + "/editmode/";
				}	
			callback(url);
			
		}
}

  
  export default ObjectURLManager;
  