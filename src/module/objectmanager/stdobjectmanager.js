import Api from '../api';
import {BACKEND} from '../../stdsetup';
import $ from 'jquery';
class stdobjectmanager
{
		static getobject(listid,objectid)
		{
			return Api.call("/list/" + listid +"/edit/" + objectid + "/json/","GET");
		}

		static async getSingleObject(listid,objectid)
		{
		   	return Api.call("/list/" + listid +"/edit/" + objectid + "/singleobjectjson/","GET");
		}
		
		static getlistdata(listid,callback)
		{
			return Api.call("/listresult/" + listid +"/","GET");			  
		}

		static getlistdatasorted(listid,sortfieldid,order,filter)
		{	
			let formData = new FormData();
			formData.append('listid', listid);
			formData.append('sortfieldid', sortfieldid);
			formData.append('order', order);
			formData.append('filter',filter);
			return Api.call("/listresult/getlistsorted/","POST",formData);
			}

		static sortObject(listid,firstid,nextid)
		{		
			let formData = new FormData();
			formData.append('listid', listid);
			formData.append('firstid', firstid);
			formData.append('nextid', nextid);
			return Api.call("/object/sort/","POST",formData);
		}




		static addObject(listid,data)
		{		
		let formData = new FormData();
		formData.append('listid', listid);
		formData.append('element', JSON.stringify(data));
		return Api.call("/object/add/","POST",formData);
		}

		
		static editObject(listid,objectid,data)
		{	
			let formData = new FormData();
			formData.append('listid', listid);
			formData.append('objectid', objectid);
			formData.append('element', JSON.stringify(data));			
			return Api.call("/object/edit/","POST",formData);
		}

		static getlistdatafiltered(listid,filter,callback)
		{		 
		 var ListDatafilter = {};
		 console.log(listid);
		 ListDatafilter.listid = listid;
		 ListDatafilter.filter =  filter;		
			$.ajax({
				type: "POST",
				url: BACKEND + "/listresult/getlistfiltered/",
				data: ListDatafilter,
				xhrFields: {
					withCredentials: true
				 },
				dataType: "json",
				success: function( data, textStatus, jqXHR) {
					callback(data);
				},
				error: function(jqXHR, textStatus, errorThrown){
				console.log( textStatus); 		
				}
				
			}); 
		}


		/*
		static getlistdatafiltered(listid,filter)
		{			
		
			let formData = new FormData();
			formData.append('listid', listid);
			formData.append('filter', filter);
			return Api.call("/listresult/getlistfiltered/","POST",formData);
		}
		*/


				
		static getmorelistdata(cacheid,listid,objectcount)
		{
			let formData = new FormData();
			formData.append('listid', listid);
			formData.append('cacheid', cacheid);			
			formData.append('objectcount', objectcount);	
			return Api.call("/listresult/loadmore/","POST",formData);
		}


		static getlookupvalues(fieldid, remotelistid, listfield, objectid) 
		{
			let formData = new FormData();
			formData.append('fieldid', fieldid);
			formData.append('remotelistid', remotelistid);			
			formData.append('listfield', listfield);	
			formData.append('objectid', objectid);
			return Api.call("/control/connectobjectinlist/getlookupfieldvalues/","POST",formData);
    	}



	}	
		
	
export default stdobjectmanager;






