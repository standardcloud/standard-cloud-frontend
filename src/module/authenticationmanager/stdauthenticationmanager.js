import Api from '../api';

import {BACKEND} from '../../stdsetup';
import $ from 'jquery';

class stdauthenticationmanager{

		static setCookie(cname, cvalue, exdays) {
			var d = new Date();
			d.setTime(d.getTime() + (exdays*24*60*60*1000));
			var expires = "expires="+ d.toUTCString();
			document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	}

		static standardcloudlogin(username,password,autologin,callback)
		{
			var self = this;
			let formData = new FormData();
			formData.append('UserName', username);
			formData.append('Password', password);
			formData.append('autologin', autologin);
			Api.call("/auth/standardcloudlogin/","POST",formData).then(data =>
				{
					if(data.state==="ok")
					{
							self.setCookie("loginguid",data.content.loginguid,100);
							callback(data);
					}else
					{
						callback(data);
					}
				});
	
		}

		static getauthenticationprovidersetting(callback)
		{
		 return	Api.call("/admin/authenticationprovider/getprovidersettings/","GET");
		}

		static setauthenticationprovidersetting(jsonconfiguration,callback)
{
	  $.ajax({
	  type: "POST",
	  data: jsonconfiguration,
	  xhrFields: {
		withCredentials: true
	 },
	  url: `${BACKEND}/admin/authenticationprovider/setprovidersettings/`,
	  success: function( data, textStatus, jqXHR) {
		callback(data);
	  },  
	   error: function(jqXHR, textStatus, errorThrown){	  
	  }
	  }); 
}

static getuserdata(callback)
{
if(window.location.href.indexOf("auth") > -1)
{
	return;
}
Api.call("/user/userdata/","GET").then(data=>
	{
		if(data.state ==="authrequired")
		{
			window.location.href = "/auth/";
		}	 
			callback(data); 
		} 
	);
}

static authenticationproviderlist()
{
return Api.call("/auth/getauthenticationproviderlist/","GET");
}

}
		
export default stdauthenticationmanager;
