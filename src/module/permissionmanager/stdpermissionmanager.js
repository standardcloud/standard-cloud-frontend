import Api from '../api';

class Permissionmanager
{	
	 static getsystemrights()
		{
			return Api.call("/admin/permissionmgmt/getsystemrights/","GET"); 
		}
		
		static getsystemright(rightid)
		{
			var data = new FormData();
			data.append("id", rightid);
			return Api.call("/admin/permissionmgmt/getsystemrights/","POST",data); 
		}
		
	  static getobjecttypepermission(objecttypeid)
		{
			var data = new FormData();
			data.append("id", objecttypeid);
			return Api.call("/admin/permissionmgmt/getobjecttypepermission/","POST",data); 
		}

		static getlistpermission(listid)
		{
			var data = new FormData();
			data.append("id", listid);
			return Api.call("/admin/permissionmgmt/list/getpermission/","POST",data); 

		}
		
		
	static removeobjecttypepermission(keyid,objecttypeid)
		{

			var data = new FormData();
			data.append("keyid", keyid);
			data.append("objecttypeid", objecttypeid);
			return Api.call("/admin/permissionmgmt/objecttype/removeobjecttypepermission/","POST",data); 
		}
		
		
		static setobjecttypepermission(keyid,rightid,objecttypeid)
		{
			var data = new FormData();
			data.append("keyid", keyid);
			data.append("rightid", rightid);
			data.append("objecttypeid", objecttypeid);
			return Api.call("/admin/permissionmgmt/objecttype/objecttypepermission/","POST",data); 
		}
		
		static removelistkey(keyid,listid)
		{
			var data = new FormData();
			data.append("keyid", keyid);
			data.append("listid", listid);
			return Api.call("/admin/permissionmgmt/list/removekey/","POST",data); 
		}

		static removequerykey(keyid,queryid,rightid)
		{
			var data = new FormData();
			data.append("keyid", keyid);
			data.append("queryid", queryid);
			data.append("rightid", rightid);
			return Api.call("/admin/permissionmgmt/query/removekey/","POST",data); 		
		}
		
		
		
		
		static newkey(name,description)
		{
			var data = new FormData();
			data.append("name", name);
			data.append("description", description);
			return Api.call("/admin/permissionmgmt/key/newkey/","POST",data); 	
		}
		
		
	  static updatekey = function(id,name,description,userids)
		{
			var data = new FormData();
			data.append("id", id);
			data.append("name", name);
			data.append("description", description);
			data.append("userids", userids);
			return Api.call("/admin/permissionmgmt/key/updatekey/","POST",data); 			
		}
				
	static addquerykey(keyid,rightid,queryid)
		{

			var data = new FormData();
			data.append("keyid", keyid);
			data.append("rightid", rightid);
			data.append("queryid", queryid);
			return Api.call("/admin/permissionmgmt/query/addkey/","POST",data); 		
		}
		

		static addlistkey(keyid,rightid,listid)
		{

			var data = new FormData();
			data.append("keyid", keyid);
			data.append("keyrightid", rightid);
			data.append("listid", listid);
			return Api.call("/admin/permissionmgmt/list/addkey/","POST",data); 		
		}
		
		
		static setobjectpermission(listid,enable,fieldid,permissionid)
		{

			var data = new FormData();
			data.append("listid", listid);
			data.append("enable", enable);
			data.append("fieldid", fieldid);
			data.append("permissionid", permissionid);
			return Api.call("/admin/permissionmgmt/list/addkey/","POST",data); 	
		}
		

	static getquerypermission(queryid)
		{
			var data = new FormData();
			data.append("queryid", queryid);
			return Api.call("/admin/permissionmgmt/query/getquerypermission/","POST",data);
		}
		
		
		static getkeys(name)
		{
			var data = new FormData();
			data.append("name", name);
			return Api.call("/admin/permissionmgmt/key/getkey/","POST",data);		
		}
		
		
static removekeyfromuser(userid,keyid)
		{	
			var data = new FormData();
			data.append("claimid", keyid);
			data.append("personid", userid);
			return Api.call("/admin/key/removekeyuser/","POST",data);		
		}
		
static getkey(keyid)
		{

			var data = new FormData();
			data.append("keyid", keyid);
			return Api.call("/admin/key/getkey/","POST",data);	
		}


static updatesystemrights(rightid,keyids)
		{
			var data = new FormData();
			data.append("rightid", rightid);
			data.append("keyids", keyids);
			return Api.call("/admin/key/addsystemkey/","POST",data);
		}

	
static getkeylist()
		{
			return Api.call("/admin/permissionmgmt/key/getkeylist/","GET");		
		}	

		
static getfields4objectpermission(listid)
		{
			var data = new FormData();
			data.append("listid", listid);
			return Api.call("/admin/permissionmgmt/list/getFields4ObjectPermission/","POST",data);	
		}
		
		
static getpermission4object(objectid,listid)
		{

			var data = new FormData();
			data.append("listid", listid);
			data.append("objectid", objectid);
			return Api.call("/admin/permissionmgmt/getpermission4object/","POST",data);	
		}
		
				
	
		static getrights = function()
		{
			return Api.call("/admin/permissionmgmt/getrights/","GET");	
		}
	
	}	
	
export default Permissionmanager;