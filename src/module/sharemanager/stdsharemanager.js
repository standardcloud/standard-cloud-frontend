import Api from '../api';

class ShareManager
{
	static addshare(objectid)
		{		 
			let formData = new FormData();
			formData.append('ObjectID', objectid);
			return Api.call("/share/addobjectshare/","POST",formData);
		}
}
	
export default ShareManager;






