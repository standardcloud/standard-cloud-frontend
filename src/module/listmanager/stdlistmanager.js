import {BACKEND} from '../../stdsetup';
import $ from 'jquery';
import Api from '../api';

class Listmanager
{			
		static getlists()
		{
			return Api.gocall("/list/json","GET");
		}
static newlist(name, description, objecttypeid, callback)
{
				   var newList = {};
				   newList.name = name
				   newList.description = description
				   newList.objecttypeid = objecttypeid;		
					   $.ajax({
						  type: "POST",
						  url: BACKEND + "/list/new/",
						  dataType: "json",
						  xhrFields: {
							withCredentials: true
						 },
						  data: newList,
						  success: function( data, textStatus, jqXHR) {		
							  callback(data);							 
					   },
					  error: function(jqXHR, textStatus, errorThrown){
						callback(null);
						}
					   });		
			  
}

		
static getmylists(callback)
{
		
					   $.ajax({
						  type: "GET",
						  url: BACKEND + "/mylist/json",
						  dataType: "json",
						  xhrFields: {
							withCredentials: true
						 },
						  success: function( data, textStatus, jqXHR) {	
						  callback(data);
					   },
					  error: function(jqXHR, textStatus, errorThrown){
						callback(null);
						}
					   });					
}

static setsettings(list)
{			
	return Api.gocall("/list/basic","POST",list);
}

/*
static setversionsettings(listid,versionenabled,versioncount,noversionlimit, callbacklistsettings)
{			
		    var ViewSettingsData = {};
			ViewSettingsData.listid = listid;
			ViewSettingsData.versionenabled = versionenabled;
			ViewSettingsData.versioncount = versioncount;
			ViewSettingsData.noversionlimit = noversionlimit;
			 $.ajax({
					type: "POST",
					url: BACKEND + "/list/versionsettings/",
					data: ViewSettingsData,
					xhrFields: {
						withCredentials: true
					 },
					dataType: "json",
					success: function( data, textStatus, jqXHR) {
						callbacklistsettings(data);				
					},
					error: function(jqXHR, textStatus, errorThrown){
					console.log( textStatus); 
					}
					
					}); 

}
*/







static setobjectviewsettings(listid,openineditmode,enableconnection,enablemeeting,versionenabled,newviewid,readviewid,editviewid,callbacklistsettings)
		{			
		    var ViewSettingsData = {};
			ViewSettingsData.listid = listid;
			ViewSettingsData.openineditmode = openineditmode;
			ViewSettingsData.enableconnection = enableconnection;
			ViewSettingsData.enablemeeting = enablemeeting;
			ViewSettingsData.versionenabled = versionenabled;
			ViewSettingsData.newviewid = newviewid;
			ViewSettingsData.readviewid = readviewid;
			ViewSettingsData.editviewid = editviewid;			
			 $.ajax({
					type: "POST",
					url: BACKEND + "/list/object_view/",
					data: ViewSettingsData,
					dataType: "json",
					xhrFields: {
						withCredentials: true
					 },
					success: function( data, textStatus, jqXHR) {
											callbacklistsettings(data);				
					},
					error: function(jqXHR, textStatus, errorThrown){
					console.log( textStatus); 
					}
					
					}); 

}


static getVersions4Object(listid,objectid,callbackversion)
		{			
		    var ViewSettingsData = {};
			ViewSettingsData.listid = listid;
			ViewSettingsData.objectid = objectid;	
			 $.ajax({
					type: "POST",
					url: BACKEND + "/list/objectversionhistory/",
					data: ViewSettingsData,
					dataType: "json",
					xhrFields: {
						withCredentials: true
					 },
					success: function( data, textStatus, jqXHR) {
						callbackversion(data);				
					},
					error: function(jqXHR, textStatus, errorThrown){
					console.log( textStatus); 
					}
					
					}); 

        }

		static restoreObjectVersion(listid,objectid,version,callbackversion)
		{			
		    var ViewSettingsData = {};
			ViewSettingsData.listid = listid;
			ViewSettingsData.objectid = objectid;	
			ViewSettingsData.version = version;
			 $.ajax({
					type: "POST",
					url: BACKEND + "/list/restoreversion/",
					data: ViewSettingsData,
					dataType: "json",
					xhrFields: {
						withCredentials: true
					 },
					success: function( data, textStatus, jqXHR) {
						callbackversion(data);				
					},
					error: function(jqXHR, textStatus, errorThrown){
					console.log( textStatus); 
					}
					
					}); 

        }




/*
		static setlistviewsettings(listid,sortfieldids,sortfiledid,sortfieldorder,enableobjectpreview,enablesingleclick,showeditbutton,showdeletebutton,callbacklistsettings)
		{			
		    var ViewSettingsData = {};
			ViewSettingsData.listid = listid;
			ViewSettingsData.sortfieldids = sortfieldids;
			ViewSettingsData.sortfiledid = sortfiledid;	
			ViewSettingsData.sortfieldorder = sortfieldorder;
			ViewSettingsData.enableobjectpreview = enableobjectpreview;
			ViewSettingsData.enablesingleclick = enablesingleclick;
			ViewSettingsData.showeditbutton = showeditbutton;			
			ViewSettingsData.showdeletebutton = showdeletebutton;			
			 $.ajax({
					type: "POST",
					url: BACKEND + "/list/list_view/",
					data: ViewSettingsData,
					xhrFields: {
						withCredentials: true
					 },
					dataType: "json",
					success: function( data, textStatus, jqXHR) {
						callbacklistsettings(data);				
					},
					error: function(jqXHR, textStatus, errorThrown){
					console.log( textStatus); 
					}
					
					}); 

		}
		*/









		
		/*old */
		static setlistsettings(listid,openineditmode,newviewid,readviewid,editviewid,sortfieldids,sortfield,sortfieldorder, callbacklistsettings)
		{			
		    var ViewSettingsData = {};
			ViewSettingsData.listid = listid;
			ViewSettingsData.openineditmode = openineditmode;
			ViewSettingsData.newviewid = newviewid;
			ViewSettingsData.readviewid = readviewid;
			ViewSettingsData.editviewid = editviewid;
			ViewSettingsData.sortfieldids = sortfieldids;
			ViewSettingsData.sortfield = sortfield;
			ViewSettingsData.sortfieldorder = sortfieldorder;			
			 $.ajax({
					type: "POST",
					url: BACKEND + "/list/listviewsettings/",
					data: ViewSettingsData,
					xhrFields: {
						withCredentials: true
					 },
					dataType: "json",
					success: function( data, textStatus, jqXHR) {
						callbacklistsettings(data);				
					},
					error: function(jqXHR, textStatus, errorThrown){
					console.log( textStatus); 
					}
					
					}); 

		}
		
		static getlistsettings(listid, callbacklistsettings)
		{		

					$.ajax({
									  type: "GET",
									  url: BACKEND +"/list/" + listid +"/listsettings/json/",
									  dataType: "json",
									  xhrFields: {
										withCredentials: true
									 },
									  success: function( data, textStatus, jqXHR) { 
										callbacklistsettings(data);				  
									   },
									  error: function(jqXHR, textStatus, errorThrown){
					}
									  
					});
		}

		
		
static deletelist(listid, callback)
{
 	var serveraddress = BACKEND+ '/list/' + listid + '/';
    $.ajax({
        url: serveraddress,
		type: 'DELETE',   	
		xhrFields: {
			withCredentials: true
		 },		
        success: function(returnobject) { 	
	
			callback(returnobject);
		},
        error: function() { 

		callback(false);
		}
    });
}
		
static deleteobject(objectid,listid, callback) 
{
var DeleteData ={};
DeleteData.objectid = objectid;
DeleteData.listid = listid;
 $.ajax({
  type: "POST",
  xhrFields: {
	withCredentials: true
 },
  url: BACKEND + "/list/removeobject/",
  data: DeleteData,
  dataType: "json",
  success: function( data, textStatus, jqXHR) {
	callback(data);
   },
  error: function(jqXHR, textStatus, errorThrown){
	callback(null);
  }  
});	
}
}

		
export default Listmanager;
