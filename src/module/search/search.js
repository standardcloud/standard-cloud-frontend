import Api from '../api';

class SearchManager
{		
		static search(searchterm, callback)
		{		 
            if(searchterm.length > 3)
            {
                var data = new FormData();
                data.append("term", searchterm);
                Api.call("/search/getresults/","POST",data).then(data =>
                    {
                        callback(data);
                    });        
            }

        }
    
        static resetindex()
    {
    var data = new FormData();
    data.append("userid", "noneed");
    return Api.call("/admin/search/indexreset/","POST",data);
}

}
export default SearchManager;






