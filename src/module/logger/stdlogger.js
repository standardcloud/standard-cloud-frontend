import Api from '../api';
class Logger
{
	static getlog(logid)
	{ 
		let formData = new FormData();
		formData.append('logid', logid);
		return Api.call("/log/getlog/","POST",formData);
	}
		
	static getlogs()
	{ 
			let formData = new FormData();
			formData.append('loglevel', "error");
			return Api.call("/log/getlogs/","POST",formData);
	}
}
export default Logger;






