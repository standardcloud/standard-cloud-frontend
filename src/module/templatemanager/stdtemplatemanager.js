import Api from '../api';	

class TemplateManager
{

static gettemplate(templateid)
		{		
			var data = new FormData();
			data.append("templateid", templateid);
			return Api.call("/api/template/gettemplate/","POST",data); 	
		}
		
static gettemplates()
		{		
			return Api.call("/api/template/gettemplates/","GET"); 		
		}
		
		
static updatetemplate(templateid, content)
		{		
			var data = new FormData();
			data.append("templateid", templateid);
			data.append("content", window.btoa(content));
			return Api.call("/api/template/updatetemplate/","POST",data); 			
		}
	
	
	}
	
export default TemplateManager;


