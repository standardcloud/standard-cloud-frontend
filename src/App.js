import React, { useEffect } from 'react';
import { Route } from 'react-router-dom';
import UserBox from './component/user/userbox/userbox';
import './App.css';
import { useDispatch,useSelector } from "react-redux";
import { withRouter,Switch } from 'react-router-dom';
import {loadUser} from './actions/user';
import {loadUsers} from './actions/users';
import {loadFields} from './actions/field';
import {loadLists} from './actions/list';
import {loadObjecttypes} from './actions/objecttype';
import StandardCloudLoginPage from './pages/login/StandardCloudlogin';
import LoginProviderListPage from './pages/login/SelectLoginProvider';
import ConnectionPage from './pages/list/connection/connection';
import StdBreadCrump from './component/navigation/StdBreadCrumb';
import Startseite from './pages/startseite';
import NotificationsSystem from 'reapop';
import theme from 'reapop-theme-wybo';
import Listpage from './pages/list/lists';
import MyListPage from './pages/list/mylist';
import Adminpage from './pages/admin/adminoverviewpage';
import NewListPage from './pages/list/newlist';
import ObjecttypeOverviewPage from './pages/admin/objecttype/objecttype';
import SingleObjectTypePage from './pages/admin/objecttype/singleobjecttype';
import LoggerPage from './pages/admin/logs/logger';
import SearchPage from './pages/admin/search/searchsettingspage';
import SingleListPage from './pages/list/list';
import DesignAdminOverview from './pages/admin/design/designpage';
import EditTextFormatPage from './pages/admin/design/textformat/textformatpage';
import EditCSSPage from './pages/admin/design/css/editcsspage';
import EditTemplatePage from './pages/admin/design/template/templatepage';
import CompanyInfo from './pages/admin/design/companyinfo/companyinfo';
import TrashPage from './pages/admin/trash/trash';
import TrashItemPage from './pages/admin/trash/sigletrashitem';
import PermissionOverview from './pages/admin/permission/permissionoverview';
import UserList from './pages/admin/usermanagement/userlistmanagementpage';
import Profilepage from './pages/user/userprofile';
import IFollow from './pages/admin/follow/ifollow';
import FieldPage from './pages/admin/field/field';
import KeyListePage from './pages/admin/permission/key/keylist';
import SystemRightPage from './pages/admin/permission/right/systemright';
import SingeKeyPage from './pages/admin/permission/key/singlekeypage';
import NewKeyPage from './pages/admin/permission/key/newkeypage';
import SingleSystemPermissionRightPage from './pages/admin/permission/right/singlesystemrightpage';
import SingleField from './pages/admin/field/newfield';
import QueryListPage from './pages/query/querylist';
import AddQuerytoMyList from './pages/query/addmyquerylist';
import SingleQueryPage from './pages/query/query';
import EditQueryPage from './pages/query/queryeditor';
import EditQueryPermissionPage from './pages/query/permission/querypermissioneditor';
import ProfileListPage  from './pages/user/userlistprofilepage';
import ListSettingsOverView from './pages/list/listsettingsbuttonspage';
import ListBasicSettings from './pages/list/listsettingsbasic';
import ListVersionSettings from './pages/list/listversion';
import ListViewButtonsPage from './pages/list/listviewbuttonspage';
import ListObjectViewPage from './pages/list/object_viewsettings';
import ObjectVersionPage from './pages/list/version/objectversions';
import ListListViewPage from './pages/list/list_viewsettings';
import ReadObjectPage from './pages/object/readobject';
import EditObjectPage from './pages/object/editobject';
import NewObjectTypePage from './pages/admin/objecttype/newobjecttype';
import ObjectTypeBasicSettings from './pages/admin/objecttype/objecttypesettingsbasicpage';
import ObjectTypeSettingsOverviewPage from './pages/admin/objecttype/objecttypesettingsbuttonspage';
import PasswordResetPage from './pages/admin/usermanagement/passwordresetuserpage';
import ChangeMyPassword from './pages/user/changemypasswordpage';
import NewuserPage from './pages/admin/usermanagement/newuserpage';
import ListPermission from './pages/list/list_permission';
import PreloaderStart from './component/preloader/preloaderstart';
import PaymentPage from './pages/admin/payment/payment';
import ListTrashPage from './pages/list/trash/list_trash';
import AuthenticationProviderPage from './pages/admin/usermanagement/authenticationprovidersetting';
import ErrorPage from './pages/error/error';
import FilePage from './pages/file/file';
import {loadQueries} from './actions/query';
import CSVImportPage from './pages/list/csvimport/csvimport';
//import ReactGA from 'react-ga';


function App(props){

  const dispatch = useDispatch();
  const user = useSelector(state => state.user);
  const field = useSelector(state => state.field);
  const list = useSelector(state => state.list);
  const objecttype = useSelector(state => state.objecttype);

  useEffect(()=>
  {
    console.log("in useEffect")
    if((window.location.href.indexOf("publicquery") === -1) &&  (window.location.href.indexOf("auth") === -1) )
    {
       if(user.lastname==="")
      {
      dispatch(loadUser());
      }
     preLoadData();
     console.log("load data");
    }
  

function preLoadData()
{
  dispatch(loadLists()); 
  dispatch(loadFields());
  dispatch(loadUsers());
  dispatch(loadObjecttypes());
  dispatch(loadQueries());
  
}
},[window.location.href]);

    if(window.location.href.indexOf("publicquery") === -1 && (window.location.href.indexOf("auth") === -1 ))
    {
      if(field.length===0 || list.length ===0 || objecttype.length ===0)
      return(<div id="loadlistsbox"><span id="loadingtext">Lade Standard-Cloud<PreloaderStart/></span></div>);
    }
var navigation = null;
if(window.location.href.indexOf("publicquery") === -1)
{
  navigation = <div className="stdpage-header">
  <div className="stdpage-header-content">
    <UserBox user={user}/> 
    <StdBreadCrump/>
    </div>
</div>
}

    return (
	<div className="page secondary fixed-header">
    {navigation}
		<div className="stdpage-region">
    <Switch>
    <Route exact path="/publicquery/:id/" component={SingleQueryPage} /> 
		<Route exact path="/auth/standard-cloud/" component={StandardCloudLoginPage} />   
    <Route exact path="/auth/" component={LoginProviderListPage} /> 
    <Route exact path="/mylist/" component={MyListPage} />  
    <Route exact path="/admin/list/new/" component={NewListPage} />  
    <Route exact path="/file/:filename" component={FilePage} />  
    <Route exact path="/list/:listid/newobject/" component={EditObjectPage}/>  
    <Route exact path="/list/:listid/edit/:objectid/readmode/" component={ReadObjectPage}/>  
    <Route exact path="/list/:listid/edit/:objectid/editmode/" component={EditObjectPage}/>  
    <Route exact path="/list/:listid/edit/:objectid/version/" component={ObjectVersionPage} />
    <Route exact path="/list/:listid/edit/:objectid/connection/" component={ConnectionPage} />     
    <Route exact path="/list/:listid/edit/version" component={ListVersionSettings} /> 
    <Route exact path="/list/:listid/edit/generalsetting/" component={ListBasicSettings} /> 
    <Route exact path="/list/:listid/edit/permission/" component={ListPermission} /> 
    <Route exact path="/list/:listid/edit/trash/" component={ListTrashPage} /> 
    <Route exact path="/list/:listid/edit/csvimport/" component={CSVImportPage} /> 
    <Route exact path="/list/:listid/edit/" component={ListSettingsOverView} />  
    <Route exact path="/list/:listid/edit/view/" component={ListViewButtonsPage} /> 
    <Route exact path="/list/:listid/edit/view/object_view/" component={ListObjectViewPage} /> 
    <Route exact path="/list/:listid/edit/view/list_view/" component={ListListViewPage} />    
    <Route exact path="/listfilter/:listid/" component={QueryListPage} /> 
    <Route exact path="/list/:listid/" component={SingleListPage} />  
    <Route exact path="/list/" component={Listpage} />   
    <Route exact path="/admin/objecttype/modify/:objecttypeid/edit/generalsetting/" component={ObjectTypeBasicSettings} /> 
    <Route exact path="/admin/objecttype/modify/:objecttypeid/edit/" component={ObjectTypeSettingsOverviewPage} /> 
    <Route exact path="/admin/objecttype/modify/:objecttypeid/" component={SingleObjectTypePage} />  
    <Route exact path="/admin/objecttype/new/" component={NewObjectTypePage} />  
    <Route exact path="/admin/objecttype/" component={ObjecttypeOverviewPage} />  
    <Route exact path="/admin/log/" component={LoggerPage} />  
    <Route exact path="/admin/corpsettings/" component={CompanyInfo} />   
    <Route exact path="/admin/design/template/" component={EditTemplatePage} />   
    <Route exact path="/admin/design/editorformat/" component={EditTextFormatPage} />   
    <Route exact path="/admin/design/customcss/" component={EditCSSPage} />   
    <Route exact path="/admin/design/" component={DesignAdminOverview} />    
    <Route exact path="/trash/:trashid/" component={TrashItemPage} />    
    <Route exact path="/admin/trash" component={TrashPage} />    
    <Route exact path="/admin/permissionmgmt/systemright/:id/" component={SingleSystemPermissionRightPage} />  
    <Route exact path="/admin/permissionmgmt/systemright/" component={SystemRightPage} />  
    <Route exact path="/admin/permissionmgmt/key/new/" component={NewKeyPage} /> 
    <Route exact path="/admin/permissionmgmt/key/:id/edit/" component={SingeKeyPage} />  
    <Route exact path="/admin/permissionmgmt/key/" component={KeyListePage} />  
    <Route exact path="/admin/permissionmgmt/" component={PermissionOverview} /> 
    <Route exact path="/admin/usermanagement/authenticationprovider/" component={AuthenticationProviderPage} /> 
    <Route exact path="/admin/usermanagement/newuser/" component={NewuserPage} /> 
    <Route exact path="/admin/usermanagement/resetpassword/:userid" component={PasswordResetPage} />       
    <Route exact path="/admin/usermanagement/" component={UserList} />   
    <Route exact path="/admin/payment/" component={PaymentPage} />  
    <Route exact path="/ifollow/" component={IFollow} />  
    <Route exact path="/profile/:id/changepassword" component={ChangeMyPassword} />  
    <Route exact path="/profile/:id/" component={Profilepage} />      
    <Route exact path="/profile/" component={ProfileListPage} />      
    <Route exact path="/error/:logid/" component={ErrorPage} />   
    <Route exact path="/selectqueries/" component={AddQuerytoMyList} />    
    <Route exact path="/query/edit/:id/" component={EditQueryPage} />
    <Route exact path="/queryeditor/" component={EditQueryPage} />
    <Route exact path="/query/" component={QueryListPage} /> 
    <Route exact path="/query/:id/" component={SingleQueryPage} />   
    <Route exact path="/query/edit/:id/permission/" component={EditQueryPermissionPage} />  
    <Route exact path="/admin/field/modify/:id/" component={SingleField} />  
    <Route exact path="/admin/field/new/" component={SingleField} />  
    <Route exact path="/admin/field/" component={FieldPage} />  
    <Route exact path="/admin/search/" component={SearchPage} />     
    <Route exact path="/admin/" component={Adminpage} />  
    <Route exact path="/" component={Startseite} />  
    </Switch> 
		</div>

    <NotificationsSystem theme={theme}/>
	</div>
    );
  }


export default App;


