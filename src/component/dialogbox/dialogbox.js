import React from 'react';
import './dialogbox.css';

class Dialogbox extends React.Component {

componentWillUnmount()
{
	var ties = document.getElementsByClassName("stdtile");
for(let t of ties) {
		t.style.display="block";
	 }

}
componentDidMount() 
{
	var ties = document.getElementsByClassName("stdtile");
for(let t of ties) {
		t.style.display="none";
	 }
}

closedialogprevent(e)
{
	e.stopPropagation();
}


    render() {
					return (
					<div onClick={this.props.closedialog} id="stddialogboxbackground">
				
                    <div onClick={this.closedialogprevent} className="contentboxcontainer stddialogboxwrapper">
					<div className="contentcontainerheader"><h2 id="dialogbox-headerline" dangerouslySetInnerHTML={{__html: this.props.title}}></h2><div id="dialogbox-close" onClick={this.props.closedialog}>X</div></div>
					<div className="contentcontainertext">
					{this.props.children}
					</div>
					</div>
			
					</div>
					);
			  }
			  
			}		  
		
export default Dialogbox;
