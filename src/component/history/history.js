import React from 'react';
import Objecturlmanager from '../../module/objecturlmanager/stdobjecturlmanager';
import {Timeline, TimelineEvent} from 'react-event-timeline'
import {connect} from 'react-redux';
import { Link } from "react-router-dom";
import { withRouter } from 'react-router-dom';
import {loadHistory,updateHistoryObject} from '../../actions/history';
import './history.css';

class HistoryWidget extends React.Component {      
    constructor(props) {
        super(props);
        this.handleHistorySearchTextChange = this.handleHistorySearchTextChange.bind(this);
        this.actiontext = this.actiontext.bind(this);
        this.state = {searchvalue:"" };
      }

 componentDidMount() {
     this.props.loadHistory();
}
                
geticon(resultobject) {
        switch(resultobject.type)
        {
            case "0":
            return <i title="angeschaut" className="material-icons">visibility</i>;
            case "1":
            return <i title="hinzugefügt" className="material-icons">note_add</i>;
            case "2":
            return <i title="gelöscht" className="material-icons">delete</i>;		
            case "3":
            return <i title="bearbeitet"  className="material-icons">pan_tool</i>;
            default:
            return "";
            
        }
	  }


openobject(historyobject)
{  
    var sss = this;
    Objecturlmanager.geturl(historyobject.listid,historyobject.objectid,function(url)
			{
            // window.location.href=url;
            sss.props.history.push(url);
		    });	 
}

handleHistorySearchTextChange(event) {
    this.setState({searchvalue: event.target.value.toLowerCase()});            
}

showdetails(resultobject)
{

    this.props.updateHistoryObject(resultobject);
}

actiontext(history,link)
{
    switch(history.type)
    {
        case "0":
        return <div>{history.object} der Liste <Link to={link}>{history.list}</Link> angeschaut</div>;
        case "1":
        return <div>{history.object} der Liste <Link to={link}>{history.list}</Link> hinzugefügt</div>;
        case "2":
        return <div>{history.object} aus Liste <Link to={link}>{history.list}</Link> gelöscht</div>;
        case "3":
        return <div>{history.object} in Liste <Link to={link}>{history.list}</Link> bearbeitet</div>;
        default:
        return "";        
    }
}


render() {
        var selff = this;
       
		var  historyentrys = this.props.historyobjects.filter(function(history) {
            if(selff.state.searchvalue.length===0 ||( (history.list.toLowerCase().indexOf(selff.state.searchvalue)!==-1) || (history.object.toLowerCase().indexOf(selff.state.searchvalue)!==-1) ) )
            {
                return true;
            }
            return false;

        }).map(function(history) {		
            
            
                    var icon = selff.geticon(history);
                    var link = "/list/" + history.listid + "/";
                    var actiontext = selff.actiontext(history,link);
            var title = <span className="history_listlink">{actiontext}</span>;
                    
                    return (
                <TimelineEvent onMouseOver={()=>selff.showdetails(history)}  key={history.objectid}  title={title}
                createdAt={history.date}
                icon={icon}
                iconColor="black"
 >
      <div className="history_objectbody" dangerouslySetInnerHTML={{__html: history.content}} onClick={()=>selff.openobject(history)}></div>
 </TimelineEvent>
                    );

                              
            }
    
    );


	return (
        <div id="history-box-div">
        <input id="historysearchbox" value={this.state.searchvalue}  onChange={this.handleHistorySearchTextChange} placeholder="Filter" type="search" autoComplete="off"/><br/>
        <div id="history_box">
        <Timeline>
            {historyentrys}            
        </Timeline>
        </div>
</div>
    );
	
}	  

}

function mapStateToProps(state) {
    return { historyobjects: state.history};
  }
  export default withRouter(connect(mapStateToProps,{loadHistory,updateHistoryObject})(HistoryWidget));


