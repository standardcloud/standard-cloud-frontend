import {addNotification as notify} from 'reapop';
class stddialog  {

		static ok(title,content)
		{	
			this.okdialog(title,content);
        }
        
		static error(title,content)
		{
	
			this.errordialog(title,content);
		};
		
		
		static okdialog(title,content)
		{	
		
			window.store.dispatch(notify({
				title: title,
				message: content,
				image:'https://publicfile.standard-cloud.com/icons/ok.png',
				status: 'success',
				dismissible: true,
				dismissAfter: 3000
			  }));  
		};
		
		static errordialog(title,content)
		{	
			window.store.dispatch(notify({
				title: title,
				message: content,
				status: 'error',
				image:'https://publicfile.standard-cloud.com/icons/denied.png',
				dismissible: true,
				dismissAfter: 5000
			  }));  
		}
    }
		
export default stddialog;






