import React,{memo} from 'react';
import {BACKEND} from "../../../stdsetup";
import { Link } from "react-router-dom";
import style from './userbox.module.css';
function UserBox(props){
    if(!props.user)
    return (null);
    
   window.languageid = props.user.languageid;
   // window.company = props.user.company;
    var imgurl = BACKEND +"/file/" + props.user.img;
    if(props.user.img.indexOf("anonymususer") > 0)
    {
        imgurl = props.user.img
    }

     var url ="/profile/" + props.user.userid;
	return (
<div className={style.userlogin}>
    <Link id="profilelink" to={url}>
        <div className={style.name}>
        <span className={style.firstname} dangerouslySetInnerHTML={{__html: props.user.firstname}}></span>
        <span className={style.lastname} dangerouslySetInnerHTML={{__html: props.user.lastname}}></span>
        </div>
        <div className={style.avatar}>
            <div><img src={imgurl} alt="Profilseite anzeigen"/></div>
        </div>
    </Link>
</div>
    );	
}	  
   

export default memo(UserBox);

