import React from 'react';
import UserManager from '../../../module/usermanager/stdusermanager';
import {BACKEND}  from '../../../stdsetup';
import './stdadduser.css';

class AddUser extends React.Component {
	    constructor(props) {
		super(props);
		this.state = {users:[],user:{},adduserbutton:true};
		this.searchuser = this.searchuser.bind(this);
		this.addusermodus = this.addusermodus.bind(this);
		this.myRef = React.createRef();
  }			  

searchuser(e)
{
	var self = this;
	UserManager.searchuser(e.target.value).then(data =>
	{
		self.setState({users:data});	
	});
	if(e.target.value.length ===0)
	{
		self.myRef.current.style.display = "none";		
	}else
	{
		self.myRef.current.style.display = "block";		
	}
}	
selectuser(user)
{
	this.props.adduser(user);
	this.setState({users:[]});
	this.setState({adduserbutton:!this.state.adduserbutton});	
}

addusermodus()
{
	this.setState({adduserbutton:!this.state.adduserbutton});
}



render() {

	if(this.state.adduserbutton)
		{
			return (<img className="addusericonaddusercomponent" alt="Hinzufügen" onClick={this.addusermodus} src="https://publicfile.standard-cloud.com/icons/add.png" />);
		}
	var results = this.state.users.map(function(user)
		{
			var imgurl;
			if(user.img.length>0)
			{
				imgurl = BACKEND + "/file/" + user.img;
			}else
			{
				imgurl = "https://publicfile.standard-cloud.com/icons/anonymususer.jpg";
			}
			return(<tr key={user.id} className="adduseruserrow" onClick={() => self.selectuser(user)}><td className="adduserrowimg"><img alt={user.namr} src={imgurl}/></td><td>{user.name}</td></tr>);
		});

	var self = this;
	return (
	<div className="addusercomponent"><input type="text" className="adduserinputfield" placeholder="Benutzer suchen" onChange={this.searchuser}/><br/>
	<div className="adduseruserbox" ref={this.myRef}>
	<table className="addusercomponenttbl">
	<tbody>
	{results}
	</tbody>
	</table>
	</div>
	</div>
    );
	
}	  
  }
export default AddUser;

