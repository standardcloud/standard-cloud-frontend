import React from 'react';
import AddUser from '../../user/adduser/stdadduser';
import {BACKEND}  from '../../../stdsetup';
import './userlist.css';
function UserList(props) {
  
  function clickevent(user)
  {
	if(typeof this.props.clickevent !== "undefined")
	  {
		props.clickevent(user);
	  }
	}
	
	function showuser(user)
  {
		props.history.push("/profile/" + user.id + "/");
	}

	var adduserhtml = null;

	if(typeof props.adduser !== "undefined")
	  {
		adduserhtml = <AddUser adduser={props.adduser}/>
		}
	

	var users = props.users.map(function(user)
	{
		var imgurl = BACKEND + "/file/" + user.img;
		var title = "Klick, um Benutzer " + user.name + " zu löschen."
		return(<span key={user.id} onClick={() => showuser(user)}   title={title} className="userlistuserbox"><img alt={user.name} onClick={() => clickevent(user)} className="userlistuserpicture" src={imgurl}/>{user.name}</span>);
	});
		return (
		<div>
			<div>
			{users}
			</div>
			<div>{adduserhtml}</div>
		</div>
        );
		}

export default UserList;
