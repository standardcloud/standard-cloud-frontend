import React from 'react';
import Objecturlmanager from '../../module/objecturlmanager/stdobjecturlmanager';
import DisplayObject from '../../module/displayobject/stddisplayobject';
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom';
import SearchManager from '../../module/search/search';
import './search.css';

class SearchWidget extends React.Component {      
    constructor(props) {
        super(props);
        this.handleSearchTextChange = this.handleSearchTextChange.bind(this);
        this.state = {searchvalue:"",results:[]};
      }

 componentDidMount() {
    
}
searchobjects(searchterm)
{
var self = this;
if(searchterm.length > 2)
{
SearchManager.search(searchterm,function(data)
{   
    self.setState({results:data.result});
});
}else
{
    self.setState({results:[]});
}

}
                
openobject(searchobject)
{  
    var sss = this;
    Objecturlmanager.geturl(searchobject.listid,searchobject.objectid,function(url)
			{
            sss.props.history.push(url);
		    });	 
}

handleSearchTextChange(event) {
    this.setState({searchvalue: event.target.value});     
    this.searchobjects(event.target.value);         
}


render() {

    var result = [];
   
    for (let i = 0; i < this.state.results.length; i++) { 		
        var objectbox = {};			
        objectbox.fieldcontent = DisplayObject.getcontent(this.state.results[i]);
        if(objectbox.fieldcontent==="")
        {
            continue;
        }
            objectbox.listid = this.state.results[i].listid;
            objectbox.objectid = this.state.results[i].objectid;
            objectbox.objecttypname = this.state.results[i].objecttypname;                         
            result.push(objectbox);								
        }
    
        var selff = this;
		var  searchresult = result.map(function(analytics) {	
            return (
                    <div key={analytics.objectid} onClick={()=>selff.openobject(analytics)} className="search_objectbox">
                        <div className="headerobjectbox" dangerouslySetInnerHTML={{__html: analytics.objecttypname}}></div>
                        <div className="search_box_inner">
                                <div className="analyticsboxwrapper" dangerouslySetInnerHTML={{__html: analytics.fieldcontent}} ></div>
                        </div>
                    </div>
              	
                        );      
              
    });

	return (
        <div id="search-box-div">
        <input id="historysearchbox" value={this.state.searchvalue}  onChange={this.handleSearchTextChange} placeholder="Suche" type="search" autoComplete="off"/><br/>
        <div id="search_box">
{searchresult}
        </div>
        </div>
    );
	
}	  

}

  export default withRouter(connect(null,{})(SearchWidget));


