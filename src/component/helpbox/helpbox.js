import React from 'react';
import style from './helpbox.module.css';
	
  class HelpBox extends React.Component {
	  	    constructor(props) {
    super(props);
	this.state = {};
  }


		  render() {
					return (  
	<div className={style.helpboxcontainer}>
	<div className={style.helpboxcontainerheader}><h2>{this.props.title}</h2></div>
	<div className={style.helpboxcontainertext}>
	{this.props.children}
	</div>
	</div>
);
						
					

			  }
			  }
	
	

export default HelpBox;
