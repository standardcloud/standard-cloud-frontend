import React from 'react';
import './searchabledropdown.css';
class SearchAbleDropDown extends React.Component {
	constructor(props) {
	super(props);
	this.onChangeSearchbox = this.onChangeSearchbox.bind(this);
	this.state = {searchterm:""};
	}

handleClickEvent(element)
			  {			
				  if(!element.selected)
				  {				
						this.props.onselect(element);	 
				  }else
				  {			
						this.props.onremove(element);
				  }
				}
				
onChangeSearchbox(e)
	{
		this.setState({searchterm:e.target.value});
	}

			  render () {		
				var selfsearch = this;			  
				var  values = [];
				this.props.objects.forEach(element => {

				if(element.name.toUpperCase().includes(selfsearch.state.searchterm.toUpperCase()))
				{
					values.push (
						<tr onClick={selfsearch.handleClickEvent.bind(selfsearch, element)} key={element.id}>
						<td className="dropdownsearchboxcheckboxtd">
						<input checked={element.selected} type="checkbox" />
						</td>           
						<td dangerouslySetInnerHTML={{__html: element.name}}></td>
						</tr>
					  );       
				}
				});	
				
		return (
				<div>
				<input className="dropdownsearchbox" onChange={this.onChangeSearchbox}  placeholder="Filter" type="search" value={this.state.valuesearchtext} autoComplete="off"/>
				<div className="dropdownsearchboxdiv">
				<table>
					<tbody>
					{values}
					</tbody>
				</table>
				</div>
				</div>
				);
				}
			}
export default SearchAbleDropDown;
