import React,{useEffect,useRef} from 'react';
import {connect,useDispatch,useSelector} from 'react-redux';
import DisplayObject from '../../module/displayobject/stddisplayobject';
import { withRouter } from 'react-router-dom';
import {loadAnalytics} from '../../actions/analytics';
import ObjectUrlManager from '../../module/objecturlmanager/stdobjecturlmanager';
import style from './analyticswidget.module.css';


function AnalyticsWidget(props) {    
    const dispatch = useDispatch();
    const analytics = useSelector(s=>s.analytics);
    const myRef = useRef(null);

  
useEffect(()=>
{
     dispatch(loadAnalytics());
},[]);


function getobjectvisiblecount()
{
    if(myRef.current === null)
    return 0;

    
        var heightcount = (window.innerHeight-80) / 175;
    var widthcount = myRef.current.offsetWidth / 300;
    if(widthcount < 1.0)
        return 0;

var result = Math.floor(heightcount) * Math.floor(widthcount);
    return result
}

                
function openobject(analyticsobject)
{
			ObjectUrlManager.geturl(analyticsobject.listid,analyticsobject.objectid,function(url)
					{
                            props.history.push(url);
					});								 
}

						

   let countobjectanalytics = getobjectvisiblecount();
    var result = [];
    var numberanalytics = analytics.length;
    if(countobjectanalytics < numberanalytics)
    {
        numberanalytics = countobjectanalytics;
    }
    for (let i = 0; i < numberanalytics; i++) { 		
        var objectbox = {};			
        objectbox.fieldcontent = DisplayObject.getcontent(analytics[i]);
        if(objectbox.fieldcontent==="")
        {
            continue;
        }
            objectbox.listid = analytics[i].listid;
            objectbox.objectid = analytics[i].objectid;
            objectbox.objecttypename = analytics[i].objecttypename;                         
            result.push(objectbox);								
        }




var selff = this;
		var  analyticsresult = result.map(function(analyticobj) {	
            return (
                    <div key={analyticobj.objectid} onClick={openobject.bind(analyticobj)} className={style.objectbox}>
                        <div className={style.headerobjectbox} dangerouslySetInnerHTML={{__html: analyticobj.objecttypename}}></div>
                        <div className={style.analyticsbox}>
                                <div className={style.analyticsboxwrapper} dangerouslySetInnerHTML={{__html: analyticobj.fieldcontent}} ></div>
                        </div>
                    </div>
              	
                        );      
              
    });


	return (
        <div ref={myRef}>
{analyticsresult}
      </div>
    );
	
}




  export default AnalyticsWidget;