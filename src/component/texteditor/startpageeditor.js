import React from 'react';

import Trumbowyg from 'react-trumbowyg';

import 'react-trumbowyg/dist/trumbowyg.min.css'
import '../../../node_modules/trumbowyg/dist/plugins/colors/ui/trumbowyg.colors.css'

import '../../../node_modules/trumbowyg/dist/plugins/table/trumbowyg.table';
import '../../../node_modules/trumbowyg/dist/plugins/base64/trumbowyg.base64.min';
import '../../../node_modules/trumbowyg/dist/plugins/pasteimage/trumbowyg.pasteimage.min';
import '../../../node_modules/trumbowyg/dist/plugins/colors/trumbowyg.colors.min';
import '../../../node_modules/trumbowyg/dist/plugins/emoji/trumbowyg.emoji.min';
import '../../../node_modules/trumbowyg/dist/plugins/cleanpaste/trumbowyg.cleanpaste.min';

import './startseiteeditor.css'

// ADD THIS LINE. ADJUST THE BEGINNING OF THE PATH AS NEEDED FOR YOUR PROJECT
//import '../../node_modules/trumbowyg/dist/plugins/table/trumbowyg.table';

export default class MyComponent extends React.Component {

    render() {
        var idselector = "trumboweditor" + this.props.fieldid;
        return (
            <div>
                    <Trumbowyg id={idselector}
                    resetCss={false}
                    imageWidthModalEdit={true}
                                                              buttons={
                            [
                                'base64',
                                'formatting',
                                'btnGrp-semantic',
                                'foreColor',
                                'backColor',
                                'removeformat',
                                'btnGrp-justify',
                                'cleanpaste'                                                                                              
                            ]
                        }                       
                        data={this.props.value}
                        placeholder='Was möchtne Sie mitteilen?'
                        onChange={this.props.onChange}
                        ref="trumbowyg"
                    />
            </div>
        );
    }
}