import React from 'react';

import Trumbowyg from 'react-trumbowyg';
import 'react-trumbowyg/dist/trumbowyg.min.css'
import '../../../node_modules/trumbowyg/dist/plugins/colors/ui/trumbowyg.colors.css'

import '../../../node_modules/trumbowyg/dist/plugins/table/trumbowyg.table';
import '../../../node_modules/trumbowyg/dist/plugins/base64/trumbowyg.base64.min';
import '../../../node_modules/trumbowyg/dist/plugins/pasteimage/trumbowyg.pasteimage.min';
import '../../../node_modules/trumbowyg/dist/plugins/colors/trumbowyg.colors.min';
import '../../../node_modules/trumbowyg/dist/plugins/emoji/trumbowyg.emoji.min';
import '../../../node_modules/trumbowyg/dist/plugins/cleanpaste/trumbowyg.cleanpaste.min';



// ADD THIS LINE. ADJUST THE BEGINNING OF THE PATH AS NEEDED FOR YOUR PROJECT
//import '../../node_modules/trumbowyg/dist/plugins/table/trumbowyg.table';

export default class MyComponent extends React.Component {

    render() {
        var idselector = "trumboweditor" + this.props.fieldid;
         return (
            <div>
                    <Trumbowyg id={idselector}
                    resetCss={false}
                    autogrow={true}
                    imageWidthModalEdit={true}
                    buttons={
                            [
                               ['viewHTML'],
                                 ['base64'],
                                ['formatting'],
                                ['bold', 'italic', 'underline', 'strikethrough'],
                                ['link'],
                                ['foreColor',
                                 'backColor'],
                                 'removeformat',
                                ['insertImage'],
                                'btnGrp-justify',
                                ['table'],
                                'btnGrp-lists',
                                'cleanpaste',                             
                               ['fullscreen']
                                
                            ]
                        }                       
                        data={this.props.value}
                        placeholder='Type your text!'
                        onChange={this.props.onChange}
                        ref="trumbowyg"
                    />
            </div>
        );
    }
}