import React,{useState} from 'react';
import { withRouter } from 'react-router-dom';
import {useDispatch,useSelector} from 'react-redux';
import StdButton from '../button/stdbutton';
import Contentbox from '../../component/contentbox/contentbox';
import SearchComponent from '../../component/search/search';
import AdminButtonBox from '../../component/adminbuttonbox/adminbuttonbox';
import History from '../history/history';
import ListOfList from '../../component/list/listoflists';
import QueryListBox from '../../component/query/querylistbox/querylistbox';
import Trans from '../../module/translatoinmanager/translationmanager';
import Analyticswidget from '../../component/analyticswidget/analyticswidget';
import UserStream from '../../component/stream/userstream';
import {addPersonalPost} from '../../actions/stream';
import StartpageEditor from '../../component/texteditor/startpageeditor';
import Autolinker from 'autolinker';
import './startpagecenter.css';

function StartpageButton(props) {    
const dispatch = useDispatch();
    // this.state ={screenwidth:document.body.offsetWidth,personalposttext:"",resetstring:" ",showaddpost:false,selectedbutton:{list:true,mylist:false,query:false,search:false,history:false,admin:false,stream:false,analytics:false}};

    // index for selected button
    //list 0
    //mylist 1
    //query 2
    //search 3
    //history 4
    // admin 5
    //stream 6
    //analytics 7

    const [personalposttext,setPersonalposttext] = useState("");
    const [resetstring,setResetstring] = useState("");
    const [selectedbutton,setSelectedbutton] = useState(0);
    const screenwidth = document.body.offsetWidth;
    const [showaddpost,setShowPersonalPost] = useState(false);

    const user = useSelector(state => state.user);


    
    const lists = useSelector(state => state.list);
    const objecttypes = useSelector(state => state.objecttype);
    const mylists = useSelector(state => state.mylist)
    const querys = useSelector(state => state.query)
   
    console.log(lists);

    if(user.firstname==="")
    return null;


  function personalPostTextUpdate(e)
  {
    setPersonalposttext(e.target.innerHTML);
  }

  function postPersonalPost()
  {
   let linkedtext = Autolinker.link(personalposttext);
   dispatch(addPersonalPost(linkedtext));
   setPersonalposttext("");
   setResetstring("");
  }


      let middlecomponent;
      switch(selectedbutton)
      {
        case 4:
           middlecomponent = <span id="startpagecenter-middlecomponent-history" ><Contentbox title="&nbsp;"><History /></Contentbox></span>;     
           break;
        case 3:
         middlecomponent =  <span id="startpagecenter-middlecomponent-search"><Contentbox title="&nbsp;">< SearchComponent /></Contentbox></span>;
          break;
         case 0:
         middlecomponent =  <span id="startpagecenter-middlecomponent-listbox"><Contentbox title="&nbsp;"><ListOfList small={true} data={lists} objecttypes={objecttypes}  /></Contentbox></span>
          break;
         case 1:
          middlecomponent =  <span id="startpagecenter-middlecomponent-mylistbox"><Contentbox title="&nbsp;"><ListOfList small={true} objecttypes={objecttypes} data={lists.filter(f=>f.owner ===user.id)} /></Contentbox></span>
          break;
          case 2:
          middlecomponent = <span id="startpagecenter-middlecomponent-query"><Contentbox title="&nbsp;"><QueryListBox data={querys} /></Contentbox></span>
        break;
          case 5:
          middlecomponent =  <span id="startpagecenter-middlecomponent-adminbox"><Contentbox title="&nbsp;"><AdminButtonBox /></Contentbox></span>;
        break;
      }
        var personalpost = null;
        if(selectedbutton === 6)
        {
          var pubbutton = null;
          var cssheight = "nocss";
        
          var addpersonalpostbutton = <div onClick={()=>{setShowPersonalPost(false)}} id="addpersonpostbutton-disable"><a href="Schließen">-</a></div>;
          if(showaddpost)
          {
            cssheight ="startseite-reducestreamheight"
            personalpost =   <div id="startseite-addpersonalpostbox-div"><StartpageEditor fieldid="Startseite" value={this.state.resetstring} onChange={personalPostTextUpdate} />
                             <div onClick={postPersonalPost} id="addpersonpostbutton"><a href="Veröffentlichen">{Trans.t("publishpost")}</a></div></div>
          }else
          {
            addpersonalpostbutton = <div onClick={()=>{setShowPersonalPost(true)}} id="addpersonpostbutton"><a href="Öffnen">+</a></div>
          }
  

         

          middlecomponent =  <span id="startpagecenter-middlecomponent-stream"><Contentbox title="&nbsp;"><div id="start-userstream" className={cssheight}>
          {addpersonalpostbutton}
          {personalpost}
          {pubbutton}
          <UserStream />
          </div></Contentbox></span>;
        }
        if(selectedbutton===7)
        {
          middlecomponent =  <span id="startpagecenter-middlecomponent-analytics"><Contentbox title="&nbsp;"><div id="start-analytics"> <Analyticswidget /> </div></Contentbox></span>;
        }

        
       
    
        let list = <StdButton url="list/" name={Trans.t("lists")} cssclass="stdtile icon bglists" count={user.startpage.list.count} iconurl="https://publicfile.standard-cloud.com/icons/startseite/list.svg" />
    
        let mylist;
        if(user.startpage.mylist.visible)
        {
          mylist = <StdButton url="mylist/" name={Trans.t("mylists")} cssclass="stdtile icon bgmylists" count={user.startpage.mylist.count} iconurl="https://publicfile.standard-cloud.com/icons/startseite/mylist.svg" />
        }
        let admin;
        if(user.startpage.adminbuttonvisible)
        {
          admin= <StdButton  url="admin/" name={Trans.t("admin")} cssclass="stdtile icon bgadministration"  iconurl="https://publicfile.standard-cloud.com/icons/startseite/admin.svg" />
        }
        let query;
        if(user.startpage.query.visible)
        {
          query= <StdButton url="query/" cssclass="stdtile icon bgqueries" name={Trans.t("queries")} count={user.startpage.query.count} iconurl="https://publicfile.standard-cloud.com/icons/startseite/query.svg" />
        }

        let analytics;
        if(screenwidth < 1500)
        {
          analytics = <span  className={`${selectedbutton}`}  onMouseEnter={() => {setSelectedbutton(7)}}> 
          <StdButton  name="Analytics" cssclass="stdtile icon bganalytics" iconurl="https://publicfile.standard-cloud.com/icons/startseite/mylist.svg" />
          </span>
          personalpost = <span  className={`${selectedbutton.stream}`}  onMouseEnter={() => {setSelectedbutton(6)}}> 
                    <StdButton  name="Stream" cssclass="stdtile icon btstream" iconurl="https://publicfile.standard-cloud.com/icons/startseite/mylist.svg" />
                    </span>
        }
      

        return (
          <div id="startpagebuttons"   >
          <div id="starpagebuttons-navbuttons">
          {personalpost}

          <span  className={`${(selectedbutton===0) ? 'true' : "false"}`}  onMouseEnter={() => {setSelectedbutton(0)}}> 
          {list}
          </span>
          <span  className={`${(selectedbutton===1) ? 'true' : "false"}`} onMouseEnter={() => {setSelectedbutton(1)}}>           
          {mylist}            
          </span>
          
          <span className={`${(selectedbutton===2) ? 'true' : "false"}`} onMouseEnter={() => {setSelectedbutton(2)}}> 
          {query}
          </span>
          <span  className={`${(selectedbutton===3) ? 'true' : "false"}`} onMouseEnter={() => {setSelectedbutton(3)}}> 
          <StdButton callbackfunction={() => setSelectedbutton(3)} cssclass="stdtile icon btsearch" name={Trans.t("search")} iconurl="https://publicfile.standard-cloud.com/icons/startseite/search.svg" />     
          </span>

          <span className={`${(selectedbutton===4) ? 'true' : "false"}`} onMouseEnter={() => {setSelectedbutton(4)}}> 
           <StdButton  callbackfunction={() => setSelectedbutton(4)}cssclass="stdtile icon bthistory" name={Trans.t("history")} iconurl="https://publicfile.standard-cloud.com/icons/startseite/log.svg" />     
          </span>

          <span  className={`${(selectedbutton===5) ? 'true' : "false"}`} onMouseEnter={() => {setSelectedbutton(5)}}> 
          {admin}
          </span>
          {analytics}
          </div>
          <div>   
        {middlecomponent}
          </div>
          </div>
        );
      }
      StartpageButton.whyDidYouRender = true
export default StartpageButton;

