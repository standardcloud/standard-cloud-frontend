import React from 'react';
function PreloaderStart() {
	return (
	<span>
      <img src="https://publicfile.standard-cloud.com/icons/loader_yes.svg" alt="Loading Standard-Cloud" />
    </span>
    );
  }
export default PreloaderStart;

