import React,{useState,useEffect} from 'react';
import BT from '../../component/button/stdbutton';
import ListManager from '../../module/listmanager/stdlistmanager';
import Api from '../../module/api';
import { withRouter } from 'react-router-dom';
import {useSelector} from 'react-redux';
import dialog from '../../component/dialog/stddialog';
import ObjectUrlManager from '../../module/objecturlmanager/stdobjecturlmanager';
import {loadListObjects} from '../../actions/objects';
import DialogBox from '../dialogbox/dialogbox';
import './editobjectbar.css';
import Button from 'muicss/lib/react/button';
import Stream from '../../component/stream/stream';
import ObjectStream from '../../module/stream/stdstream';
import FollowObjectDialogBox from '../../component/follow/objectfollow';

function EditObjectBar(props) {
    const [removedialog,setRemovedialog] = useState(false);
    const [followcount,setFollowcount] = useState(0);
    const [commentbox,setCommentbox] = useState(false);
    const [followdata,setFollowdata] = useState({del:false,mod:false});
    const [followbox,setFollowbox] = useState(false);
    const [objectstream,setObjectstream] = useState([]);

    const {user} = useSelector(state => state.user);
useEffect(()=>
{
  getfollowObjectData();
},[props.objectid]);

	

function clickcomment()
	{
  setCommentbox(!commentbox);
    ObjectStream.getobjectstream(this.props.objectid).then(data =>
    {
      setObjectstream(data.Stream);
    });
  }

function getfollowcount() 
{
  return 0;
  var followcount = 0;
  if(this.state.followdata.mod)
  followcount++;
  if(this.state.followdata.del)
  followcount++;

  return followcount;
} 

function clicksubmit()
{
  props.saveObject();
}

function clickback()
{
window.history.back();
}

function clickedit()
{
            ObjectUrlManager.getediturl(props.list.id,props.objectid,function(url)
                { 
                   props.history.push(url);
                });
}

function removeobject () {

      var listsettings = props.list;
   if(!listsettings.confirmobjectremove)
   {
    finalremove();
   }else
   {
    setRemovedialog(false);
   }  
}
function finalremove()
{
  var objectid = this.props.objectid;
  var listid = this.props.list.id;	  

  ListManager.deleteobject(objectid,listid,function(data)
  {
    if(data.state==="ok")
    {
      props.history.push("/list/" + listid + "/");
      props.loadListObjects(props.match.params.listid);
   //fixit   dialog.ok(self.props.listsettings.objecttype.name,"wurde gelöscht.");
    }else
    {
      dialog.errordialog("Error","Objekt konnte nicht gelöst werden");
    }
  });
}



function getfollowObjectData()
{
  if(props.mode!=="new")
  {
  var data = new FormData();
  data.append("objectid", props.objectid);
  return Api.call("/api/follow/getstatus/","POST",data).then(data =>
    {
      setFollowdata(data);
    }); 
  }
}



function clickfollow () {
setFollowbox(!followbox);
  }

function clickconnection() {
  props.history.push("/list/" + props.list.id + "/edit/" +  props.objectid + "/connection/");
   }

function clickversion () {
  props.history.push("/list/" + props.list.id + "/edit/" +  props.objectid + "/version/");
   }

  function closecommentbox()
   {
      setCommentbox(false);
   }
   function closefollowbox()
   {
    setFollowbox(false);
   }
   
   
function getCommentbox()
{
  if(commentbox)
  {
  return <div id="objectstreambox"><DialogBox closedialog={closecommentbox} title="Kommentare"><div id="stremboxinside"><Stream stream={this.state.objectstream} user={this.props.user} /></div></DialogBox></div>
  }
}

if(!props.list)
{
  return (null);
}

function getRemoveDialog()
{
    if(removedialog)
    {
    return (<DialogBox closedialog={()=>{setRemovedialog(false)}}  title="Löschen ?">
    <div className="contentcontainertextremoveobject">
    <h1><div>Wollen Sie den <strong> {props.listsettings.objecttype.name} </strong> wirklich löschen?</div></h1>

          <Button onClick={()=>{finalremove()}}  color="danger">JA, LÖSCHEN</Button>
          <Button  onClick={()=>{setRemovedialog(false)}} >NEIN</Button>
      </div>
    </DialogBox>)
    }
}

function getFollowbox()
{
  if(followbox)
    return <FollowObjectDialogBox followdata={followdata} closedialog={closefollowbox} objectid={props.objectid} listsetting={props.list} />
}



function getButtons()
{
if(props.mode==="new")
{
    return (<div><BT disabled={props.disabled} callbackfunction={clicksubmit} name="Speichern"  iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
    <BT  callbackfunction={clickback} name="Abbrechen"   iconurl="https://publicfile.standard-cloud.com/icons/return.png" /></div>);
}
if(props.mode==="edit")
{
  return (<div>
  <BT  callbackfunction={clicksubmit} disabled={props.disabled} name="Speichern"  iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
      <BT  callbackfunction={clickback} name="Abbrechen"   iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
  <BT  callbackfunction={removeobject}  name="Löschen"    iconurl="https://publicfile.standard-cloud.com/icons/delete.png" />

        {props.list.comment === true &&
  <BT  callbackfunction={clickcomment} name="Kommentare"    iconurl="https://publicfile.standard-cloud.com/icons/comment.png" />
        }
        {props.list.version === true &&
  <BT  callbackfunction={clickversion} name="Versionen"    iconurl="https://publicfile.standard-cloud.com/icons/versions.png" />
        }
        {props.list.connection === true &&
  <BT  callbackfunction={clickconnection}  name="Verbindungen"    iconurl="https://publicfile.standard-cloud.com/icons/connections.png" />
        }
        {props.list.follow === true &&
  <BT callbackfunction={clickfollow} name="Folgen" count={getfollowcount()}    iconurl="https://publicfile.standard-cloud.com/icons/follow.png" />
        }


  </div>);
}

if(props.mode==="read")
{
return <div><BT  callbackfunction={clickedit} name="Bearbeiten"   iconurl="https://publicfile.standard-cloud.com/icons/edit.png" /></div>
}
}
	return (
          <div id="buttoneditorbox">
          {getRemoveDialog()}
          {getCommentbox()}
          {getFollowbox()}
          {getButtons()}
         </div>   
    );
	
}
 

export default EditObjectBar;

