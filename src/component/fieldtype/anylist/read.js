import React from 'react';
import moment from 'moment';
import 'moment/locale/de';
class FieldTypeDateTimeRead extends React.Component {

render() {
    console.log(this.props.field);
    var createddate;
    switch(this.props.field.custom.datetimeoptions)
    {
        case "2":
        createddate= moment(this.props.value.datetime).locale("de").format("llll");
        break;
        case "1":
        createddate= moment(this.props.value.datetime).locale("de").format("L");
        break;
        default:

    }


return(<div className="fieldtypeboxdatetimeread">
   {createddate}
		</div>);
}	  
  }   
export default FieldTypeDateTimeRead;

