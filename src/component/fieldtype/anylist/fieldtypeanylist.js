import React from 'react';


class FieldTypeSublist extends React.Component {
	    constructor(props) {
		super(props);
		this.handlechangeallowmultiplelists = this.handlechangeallowmultiplelists.bind(this);
		this.state = {anylistfieldsettings:{allowmultiplelists:false}};		
  }			  
componentDidMount() 
{
	if(this.props.setcustomsettings  !== undefined)
	{	
	this.setState({anylistfieldsettings:this.props.setcustomsettings});
	this.props.getcustomsettings(this.props.setcustomsettings);
	}
}
getcustomsettings()
{
	   this.props.getcustomsettings(this.state.anylistfieldsettings);
}
handlechangeallowmultiplelists(e)
{
	var oldstate = this.state.anylistfieldsettings;
	oldstate.allowmultiplelists = !oldstate.allowmultiplelists;
	this.setState({sublistfieldsettings:oldstate});
	this.getcustomsettings();
}

render() {
		return(<div>
				<div className="stdcheckboxregion">
					<input id="allowmultiplelinks"  checked={this.state.anylistfieldsettings.allowmultiplelists}  type="checkbox" value="allowmultiplelinks"   className="checkbox"/>
					<label onClick={this.handlechangeallowmultiplelists}  for="allowmultiplelinks">Mehrere Links/URLs erlauben</label>
		    	</div>
		</div>);
}	  
  }   
export default FieldTypeSublist;

