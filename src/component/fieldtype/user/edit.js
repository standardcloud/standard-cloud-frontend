import React from 'react';
import Userlist from '../../../component/user/userlist/userlist';
import dialog from '../../dialog/stddialog'
class FieldTypeUserEdit extends React.Component {
  constructor(props) {
    super(props);
    var users = [];
    if(this.props.value)
    {
      users = this.props.value;
    }
		this.state = {removedialoguser:null,users:users};
		this.removeuser = this.removeuser.bind(this);
    this.adduser = this.adduser.bind(this);
    this.updatevalue = this.updatevalue.bind(this);
  }	


  adduser(user)
  {
    var self = this;

    for(let i=0;i<this.state.users.length;i++)
      {
        var u = self.state.users[i];
             if(u.id === user.id)
          {
            dialog.error(user.name,"bereits in Liste vorhanden.")
            return;
          }          
      }
  
    var oldst = this.state.users.slice();
    oldst.push(user);
    this.setState({users:oldst});
    this.updatevalue(oldst);

  }
  removeuser(user)
  {
    var self = this;
    var newlist = []
    for(let i=0;i<this.state.users.length;i++)
    {
      var u = self.state.users[i];
      if(u.id !== user.id)
        {
          newlist.push(u);
        }          
    }    
    this.setState({users:newlist});
    this.updatevalue(newlist);
  }

updatevalue(oldst)
{
  var useridstring = "";
  for(let i=0;i<oldst.length;i++)
  {
    useridstring +=oldst[i].id+";";    
  }
  this.props.valueUpdate(this.props.field.id,useridstring);
}

  
render() {
  var userlist = null;
  if(this.state.users)
  {
    userlist = <Userlist adduser={this.adduser} clickevent={this.removeuser} users={this.state.users} />
  }
return(<div className="fieldtypeuserread">
   {userlist}
		</div>);
}	  
  }   


export default FieldTypeUserEdit;