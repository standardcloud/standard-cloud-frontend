import React from 'react';
import {BACKEND} from '../../../stdsetup';
import { withRouter } from 'react-router-dom';
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import './read.css';
class FieldTypeUserRead extends React.Component {
  
render() {

 if(!Array.isArray(this.props.value))
 {
    var user = this.props.users.find(f=>f.id === this.props.value);
    var pictureurl = BACKEND + "/file/" + user.img;
    var name = user.name;
    var profileurl = "/profile/" + user.id + "/";
    return(<span key={user.id}><Link to={profileurl}><img className="readimageuserpicture" alt={name} src={pictureurl}/>{name}</Link></span>);
 }

  var userlist = this.props.value.map(function(user)
  {
    var pictureurl = BACKEND + "/file/" + user.img;
    var name = user.name;
    var profileurl = "/profile/" + user.id + "/";
  return(<span key={user.id}><Link to={profileurl}><img className="readimageuserpicture" alt={name} src={pictureurl}/>{name}</Link></span>);
  });

return(<div className="fieldtypeuserread">
   {userlist}
		</div>);
}	  
  }   


function mapStateToProps({users}, ownProps) {
  return { users:users};
}

export default withRouter(connect(mapStateToProps)(FieldTypeUserRead));