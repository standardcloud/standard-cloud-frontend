import React from 'react';

  class FieldTypeUser extends React.Component {
	    constructor(props) {
		super(props);
		this.handledefaultuser = this.handledefaultuser.bind(this);
		this.changeMultipleUser = this.changeMultipleUser.bind(this);
		this.getcustomsettings = this.getcustomsettings.bind(this);
		this.state = {userfieldsettings:{multipleusers:false,defaultuser:false}};		
  }			  
componentDidMount() 
{
	if(this.props.setcustomsettings  !== undefined)
	{	
		this.setState({userfieldsettings:this.props.setcustomsettings});
		this.props.getcustomsettings(this.props.setcustomsettings);
	}else
	{
		this.getcustomsettings();
	}
}
getcustomsettings()
{
	   this.props.getcustomsettings(this.state.userfieldsettings);
}

handledefaultuser(e)
{
	var oldstate = this.state.userfieldsettings;
	oldstate.defaultuser = !oldstate.defaultuser;
	this.setState({userfieldsettings:oldstate});
	this.getcustomsettings();
}
changeMultipleUser()
{
	var oldstate = this.state.userfieldsettings;
	oldstate.multipleusers = !oldstate.multipleusers;
	this.setState({userfieldsettings:oldstate});
	this.getcustomsettings();
}

render() {
		return(<div id="fieldtypetextsettingsbox">
		        <div className="stdcheckboxregion">
				<input id="enablemultipleusers"  checked={this.state.userfieldsettings.multipleusers} type="checkbox" value="enablemultipleusers"   className="checkbox"/>
				<label onClick={this.changeMultipleUser}  htmlFor="enablemultipleusers">Es dürfen mehrere Benutzer im Feld enthalten sein</label>
			</div>
			<div className="stdcheckboxregion">
				<input id="defaultuser"  checked={this.state.userfieldsettings.defaultuser} type="checkbox" value="defaultuser"   className="checkbox"/>
				<label onClick={this.handledefaultuser}  htmlFor="defaultuser">Der angemeldete Benutezr soll standardmäßig im Feld enthalten sein.</label>
			</div>
		</div>);
}	  
  }   
export default FieldTypeUser;

