import React from 'react';
class FieldTypeFloatEdit extends React.Component {
	constructor(props) {
    super(props);
    if(this.props.value ===null || this.props.value.length===0)
    {
      this.state = {value:""};
    }else
    {
      this.state = {value:this.props.value.val};
    }      
 
    this.changeText = this.changeText.bind(this);
  }

  isNumber(n) { return !isNaN(parseFloat(n)) && !isNaN(n - 0) }

  changeText(e)
  {

    if (this.isNumber(e.target.value))
    {
      this.setState({value:e.target.value});
      this.props.valueUpdate(this.props.field.id,e.target.value);
    }
  }  
render() {

return(<div className="fieldtypeboxfloatedit">
     <input type="text" onChange={this.changeText} value={this.state.value} /><span dangerouslySetInnerHTML={{__html:this.props.field.custom.postfix}}></span>
		</div>);
}	  
  }   
export default FieldTypeFloatEdit;

