import React from 'react';


  class FieldTypeFloat extends React.Component {
	    constructor(props) {
		super(props);
		this.handlemininteger = this.handlemininteger.bind(this);
		this.handlemaxinteger = this.handlemaxinteger.bind(this);
		this.handledefaultvalue = this.handledefaultvalue.bind(this);
		this.handlepostfix = this.handlepostfix.bind(this);
		this.changenumberdecimals = this.changenumberdecimals.bind(this);
		this.state = {floatfieldsettings:{mininteger:"",maxinteger:"",defaultvalue:"",numberdecimals:""}};		
  }			  
componentDidMount() 
{
	if(this.props.setcustomsettings  !== undefined)
		{	
	this.setState({floatfieldsettings:this.props.setcustomsettings});
	this.props.getcustomsettings(this.props.setcustomsettings);
		}
	else
	{
		this.props.getcustomsettings(this.state.floatfieldsettings);	
	}
}
getcustomsettings()
{
	   this.props.getcustomsettings(this.state.floatfieldsettings);
}
handleDefaultValueChange(e)
{
	var oldstate = this.state.floatfieldsettings;
	oldstate.defaultvalue = e.target.value;
	this.setState({floatfieldsettings:oldstate});
	this.getcustomsettings();
}
handlemininteger(e)
{
	var oldstate = this.state.floatfieldsettings;
	oldstate.mininteger = e.target.value;
	this.setState({floatfieldsettings:oldstate});
	this.getcustomsettings();
}
handlemaxinteger(e)
{
	var oldstate = this.state.floatfieldsettings;
	oldstate.maxinteger = e.target.value;
	this.setState({floatfieldsettings:oldstate});
	this.getcustomsettings();
}

handledefaultvalue(e)
{
	var oldstate = this.state.floatfieldsettings;
	oldstate.defaultvalue = e.target.value;
	this.setState({floatfieldsettings:oldstate});
	this.getcustomsettings();
}
handlepostfix(e)
{
	var oldstate = this.state.floatfieldsettings;
	oldstate.postfix = e.target.value;
	this.setState({floatfieldsettings:oldstate});
	this.getcustomsettings();
}
changenumberdecimals(e)
{
	console.log(e.target.value);
	var oldstate = this.state.floatfieldsettings;
	oldstate.numberdecimals = e.target.value;
	this.setState({floatfieldsettings:oldstate});
	this.getcustomsettings();
}


render() {
	var selfrender = this;
		return(<div id="fieldtypetextsettingsbox">
		<table><tbody><tr><td className="fieldtypesettingsboxtdnames">Minimalwert:</td>
		<td><input type="text" className="validate[custom[integer]]" value={this.state.floatfieldsettings.mininteger} onChange={selfrender.handlemininteger} data-prompt-position="topLeft:30"  name="mininteger"/></td></tr>

 <tr><td className="fieldtypesettingsboxtdnames">Maximalwert:</td>
		<td><input type="text" className="validate[custom[integer]]" value={this.state.floatfieldsettings.maxinteger} onChange={selfrender.handlemaxinteger} data-prompt-position="topLeft:30"  name="maxinteger"/></td></tr>
		<tr><td className="fieldtypesettingsboxtdnames">Anzahl Dezimalstellen:</td>
		<td>
		<select onChange={this.changenumberdecimals}>
			<option selected={this.state.floatfieldsettings.numberdecimals==="1"} value="1">1</option>
			<option selected={this.state.floatfieldsettings.numberdecimals==="2"} value="2">2</option>
			<option selected={this.state.floatfieldsettings.numberdecimals==="3"} value="3">3</option>
			<option selected={this.state.floatfieldsettings.numberdecimals==="4"} value="4">4</option>
		</select>
		</td></tr>
 <tr><td className="fieldtypesettingsboxtdnames">Standardwert:</td>
		<td><input type="text" className="validate[custom[integer]]" value={this.state.floatfieldsettings.defaultvalue} onChange={selfrender.handledefaultvalue} data-prompt-position="topLeft:30"  name="defaultvalue"/></td></tr>

 <tr><td className="fieldtypesettingsboxtdnames">Postfix(z.B. €, $):</td>
		<td><input type="text" className="validate[custom[integer]]" value={this.state.floatfieldsettings.postfix} onChange={selfrender.handlepostfix} data-prompt-position="topLeft:30"  name="postfix"/></td></tr>
		</tbody>
 </table>
		</div>);
}	  
  }   
export default FieldTypeFloat;

