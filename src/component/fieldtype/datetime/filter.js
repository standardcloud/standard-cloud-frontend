import React from 'react';
import '../../../../node_modules/react-date-range/dist/styles.css'
import '../../../../node_modules/react-date-range/dist/theme/default.css';
import DE from '../../../../node_modules/react-date-range/dist/locale/de';
import { DateRange } from 'react-date-range';
import moment from 'moment';
import './filter.css';

class FilterDatetime extends React.Component {
	constructor(props) {
    super(props);
    this.handleSelect = this.handleSelect.bind(this);
    this.state ={selectionRange:{ startDate: new Date(),endDate: new Date(),key: 'selection'}};
  }
	handleSelect(ranges){
    this.setState({selectionRange:ranges.selection});

    var filterlist =[];
    var len = this.props.objectresult[0].length;
   for(let i=0;i< this.props.objectresult.length;i++)
   {
     if(this.props.objectresult[i][this.props.index].length ===0)
     {
     filterlist.push(this.props.objectresult[i][len-1])
     continue;
    }
     var datavalue = moment(this.props.objectresult[i][this.props.index], "DD.MM.YYYY HH:mm");
     var startdate = ranges.selection.startDate;

     var enddate = ranges.selection.endDate;
  var tomorrow = new Date();
  tomorrow.setDate(enddate.getDate() + 1);

  if((datavalue > startdate) && (datavalue < tomorrow))
      {    
   
      }else
      {
        filterlist.push(this.props.objectresult[i][len-1])
      }
   }
   console.log(filterlist);
this.props.filtercallback(this.props.field.id,filterlist);

	}




render() {
return(<div className="datime-filter-box-div">
                <DateRange locale={DE} 
                color="#da532c"
                ranges={[this.state.selectionRange]}
                         onChange={this.handleSelect}
                />
		</div>);
}	  
  }   

export default FilterDatetime;