import React from 'react';
 class FieldTypeDateTime extends React.Component {
	    constructor(props) {
		super(props);
		this.handleDateTimeType = this.handleDateTimeType.bind(this);
		this.getcustomsettings = this.getcustomsettings.bind(this);
		this.state = {datetimefieldsettings:{datetimeoptions:"1",defaultvaluedate:"nodate",enabletimeline:false}};		
  }			  
componentDidMount() 
{
	if(this.props.setcustomsettings  !== undefined)
		{	
	this.setState({datetimefieldsettings:this.props.setcustomsettings});
	this.props.getcustomsettings(this.props.setcustomsettings);
		}
}
getcustomsettings()
{
	   this.props.getcustomsettings(this.state.datetimefieldsettings);
}
handleDateTimeType(value)
{
	var o = this.state.datetimefieldsettings;
	o.datetimeoptions = value;
	this.setState({datetimefieldsettings:o});
	this.getcustomsettings();
}
handledefaultvaluedate(value)
{
	var o = this.state.datetimefieldsettings;
	o.defaultvaluedate = value;
	this.setState({datetimefieldsettings:o});
	this.getcustomsettings();
}
changeEnableTimeline()
{
	var o = this.state.datetimefieldsettings;
	o.enabletimeline = !o.enabletimeline;
	this.setState({datetimefieldsettings:o});
	this.getcustomsettings();
}

render() {
var selfrender = this;

return(<div id="fieldtypepicturesettingsbox">
<table>
	<tbody>
<tr><td className="tdpictureratio">Format</td><td>
<div className="stdradioboxregion"><input className="radio" checked={this.state.datetimefieldsettings.datetimeoptions==="1"} onChange={()=>selfrender.handleDateTimeType("1")}  id="date"  type="radio"  name="datetime" value="1"/>
<label htmlFor="date">Datum</label>
</div>
<div className="stdradioboxregion"><input className="radio"  checked={this.state.datetimefieldsettings.datetimeoptions==="2"}  onChange={()=>selfrender.handleDateTimeType("2")}  id="datetime"  type="radio"  name="datetime" value="2"/>
<label htmlFor="datetime">Datum und Uhrzeit</label>
</div>
</td></tr><tr>
	<td><br/>Standardwert</td><td><br/>
	<div className="stdradioboxregion"><input className="radio"  checked={this.state.datetimefieldsettings.defaultvaluedate==="nodate"} onChange={()=>selfrender.handledefaultvaluedate("nodate")}  id="nodate"  type="radio"  name="defaultvaluedate" value="nodate"/>
<label htmlFor="nodate">keine Vorauswahl</label>
</div>
<div className="stdradioboxregion"><input className="radio" checked={this.state.datetimefieldsettings.defaultvaluedate==="currentdate"} onChange={()=>selfrender.handledefaultvaluedate("currentdate")}  id="currentdate"  type="radio"  name="defaultvaluedate" value="currentdate"/>
<label htmlFor="currentdate">Aktuelles Datum</label>
</div><br/>
		</td></tr>
		<tr><td>Timeline</td>
		<td>
			<div className="stdcheckboxregion">
		<input  checked={selfrender.state.datetimefieldsettings.enabletimeline} type="checkbox" id="enabletimeline"  className="checkbox"/>
		<label onClick={()=>selfrender.changeEnableTimeline()}  htmlFor="enabletimeline"><strong>Listen mit diesem Datum in der Timeline anzeigen.</strong></label>
</div>
</td>
			</tr>
</tbody>
</table>
		</div>);
}	  
  }   
export default FieldTypeDateTime;

