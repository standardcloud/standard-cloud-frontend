import React from 'react';
import moment from 'moment';
import 'moment/locale/de';
import DatePicker from 'react-datepicker';
import './edit.css';

import 'react-datepicker/dist/react-datepicker.css';
class FieldTypeDateTimeRead extends React.Component {
    constructor(props) {
        super(props);

        this.state = {selectedDate:moment()};
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        return;
        var self = this;
        var dd = null;   
        if(this.props.value)
        {         
            if(this.props.value.datetime)
            {
            dd = moment(this.props.value.datetime);
            
            self.setState({selectedDate:dd});
            }else
            {              
                
                  self.setState({selectedDate:null});
          
            }         
        }else
        {         
            var formateddate = null;
            console.log(self.props.field.custom.defaultvaluedate);
              if(self.props.field.custom.defaultvaluedate!=="nodate")
              {
                formateddate = moment(); //this.state.selectedDate.format('DD.MM.YYYY HH:mm:ss');
                dd =  formateddate.format('DD.MM.YYYY HH:mm:ss');
                self.props.valueUpdate(this.props.field.id,dd);
            }
            else
            {
                formateddate = null;     
             
            }  
           self.setState({selectedDate:formateddate});  
        }
    }

    handleChange(date) {
        return;
        if(date !=null)
        {
            this.setState({selectedDate: date});
            var formateddate =  date.format('DD.MM.YYYY HH:mm:ss');
            this.props.valueUpdate(this.props.field.id,formateddate);
        }else
        {
            this.setState({selectedDate: date});
            this.props.valueUpdate(this.props.field.id,"");
        }
}

render() {

    var createddate = null;
    try{
    switch(this.props.field.custom.datetimeoptions)
    {
        case "1":
        createddate= null;//<DatePicker selected={this.state.selectedDate} onChange={this.handleChange} />;
        break;
        /*
        case "2":
        createddate= <DatePicker    
        showTimeSelect
        timeFormat="HH:mm"
        timeIntervals={15}
        dateFormat="LLL"
        selected={this.state.selectedDate} onChange={this.handleChange} />;
        break;
        */
        default:

    }
}catch(err)
{
   
}

return(<div className="fieldtypeboxdatetimeread">
   {createddate}
		</div>);
}	  
  }   
export default FieldTypeDateTimeRead;

