import React from 'react';
import Contentbox from '../../contentbox/contentbox';

class FieldTypeSelector extends React.Component {
	    constructor(props) {
		super(props);
		this.handleSelectorTypeChange = this.handleSelectorTypeChange.bind(this);
		this.changechoice = this.changechoice.bind(this);
		this.changedefaultvalue = this.changedefaultvalue.bind(this);
		this.changeAuswahl = this.changeAuswahl.bind(this);
		this.state = {selectorfieldsettings:{choice:"",selectortype:"dropdown",defaultvalue:""}};		
  }			  
componentDidMount() 
{
	if(this.props.setcustomsettings  !== undefined)
		{	
	this.setState({selectorfieldsettings:this.props.setcustomsettings});
	this.props.getcustomsettings(this.props.setcustomsettings);
		}
}
getcustomsettings()
{
	 this.props.getcustomsettings(this.state.selectorfieldsettings);	 
}

handleSelectorTypeChange(typetext)
{
	var oldstate = this.state.selectorfieldsettings;
	oldstate.selectortype = typetext;
	this.setState({selectorfieldsettings:oldstate});
	this.props.getcustomsettings(oldstate);
}
changechoice(e)
{
	var oldstate = this.state.selectorfieldsettings;
	oldstate.choice = e.target.value;
	this.setState({selectorfieldsettings:oldstate});
	this.getcustomsettings(oldstate);
}
changedefaultvalue(e)
{
	//defaultvalue
	var oldstate = this.state.selectorfieldsettings;
	oldstate.defaultvalue = e.target.value;
	this.setState({selectorfieldsettings:oldstate});
	this.getcustomsettings(oldstate);
}
changeAuswahl(auswahl)
{

	var oldstate = JSON.parse(JSON.stringify(this.state.selectorfieldsettings));
	if(this.state.selectorfieldsettings.defaultvalue.indexOf(auswahl+";") < 0)
		{
			oldstate.defaultvalue = oldstate.defaultvalue + auswahl +";";		
		}else
		{
			oldstate.defaultvalue = oldstate.defaultvalue.replace((auswahl+";"),"");
		}
		this.setState({selectorfieldsettings:oldstate});
}


render() {
	var defaultvalue = null;
	var renderself = this;
	if(this.state.selectorfieldsettings.selectortype ==="dropdown" || this.state.selectorfieldsettings.selectortype ==="optionsfeld")
		{
			var vl = renderself.state.selectorfieldsettings.choice.split('\n');
			console.log(vl);
			//vl.unshift(<option></option>);
			var optionfalues = vl.map(function(auswahl)
			{
				return(<option key={auswahl} >{auswahl}</option>)
			});
			console.log(renderself.state.selectorfieldsettings.defaultvalue);
				defaultvalue =<select value={renderself.state.selectorfieldsettings.defaultvalue} onChange={renderself.changedefaultvalue}  size="1">
								{optionfalues}
	    					</select>
		}
		if(this.state.selectorfieldsettings.selectortype ==="mehrfachauswahl")
			{
				var vvl = renderself.state.selectorfieldsettings.choice.split('\n');
				var checkboxes = vvl.map(function(auswahl)
					{
						
						return(<div key={auswahl} className="stdcheckboxregion">
						<input id={auswahl}  checked={renderself.state.selectorfieldsettings.defaultvalue.indexOf(auswahl + ";") !==-1} type="checkbox" value={auswahl} className="checkbox"/>
						<label onClick={()=>renderself.changeAuswahl(auswahl)}  htmlFor={auswahl}><strong>{auswahl}</strong></label>
						</div>);
					});
				defaultvalue =<div>
					{checkboxes}
					</div>

			}



	var selfrender = this;
		return(<div id="fieldtypeselectorsettingsbox">
Geben Sie jede Auswahlmöglichkeit in einer neuen Zeile ein: : <br/> 
<textarea value={this.state.selectorfieldsettings.choice} onChange={this.changechoice} className="validate[required]" data-prompt-position="topLeft:30" name="taValues" id="taValues" cols="25" rows="7"></textarea> <br/>
Auswahl anzeigen als: <br />
<div className="stdradioboxregion"><input checked={this.state.selectorfieldsettings.selectortype === "dropdown"}  onChange={()=>selfrender.handleSelectorTypeChange("dropdown")} className="radio" id="dropdown"  type="radio"  name="slViewControl" value="dropdown"/>
			<label htmlFor="dropdown">Dropdown</label>
 		</div>
		<div className="stdradioboxregion"><input checked={this.state.selectorfieldsettings.selectortype === "optionsfeld"}  onChange={()=>selfrender.handleSelectorTypeChange("optionsfeld")} className="radio" id="optionsfeld"  type="radio"  name="slViewControl" value="optionsfeld"/>
			<label htmlFor="optionsfeld">Optionsfeld</label>
 		</div>
		<div className="stdradioboxregion"><input checked={this.state.selectorfieldsettings.selectortype === "mehrfachauswahl"}  onChange={()=>selfrender.handleSelectorTypeChange("mehrfachauswahl")} className="radio" id="mehrfachauswahl"  type="radio"  name="slViewControl" value="mehrfachauswahl"/>
			<label htmlFor="mehrfachauswahl">Kontrollkästchen (Mehrfachauswahl zulassen)</label>
 		</div>
		 <div><br/>
		 <Contentbox title="Standardwert / Vorauswahl:">
			 <div id="fieldtypeselectordefaultbox">
		 {defaultvalue}
		 </div>
		</Contentbox>
		</div></div>);
}	  
  }   
export default FieldTypeSelector;

