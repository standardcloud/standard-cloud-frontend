import React from 'react';


class FilterSelector extends React.Component {
	constructor(props) {
    super(props);
    this.singledropdown = this.singledropdown.bind(this);
    this.changeFilter = this.changeFilter.bind(this);
  }


changeFilter(e)
{
 var filterlist =[];
 var len = this.props.objectresult[0].length;
 console.log(e.target.value);
for(let i=0;i< this.props.objectresult.length;i++)
{
  if(this.props.objectresult[i][this.props.index].indexOf(e.target.value) === -1)
  {    
    filterlist.push(this.props.objectresult[i][len-1])
  }  
}

this.props.filtercallback(this.props.field.id,filterlist);

}

singledropdown()
{
var dropdownvalues = this.props.field.custom.choice.split("\n").map(function(value)
{
  	return(<option key={value}>{value}</option>);
});

  var dropdown = <select onChange={this.changeFilter}>
          <option key=""></option>
                {dropdownvalues}
                </select>;
  return dropdown;
}


render() {
return(<div>
    {this.singledropdown()}
		</div>);
}	  
  }   

export default FilterSelector;