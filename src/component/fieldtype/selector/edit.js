import React from 'react';

class FieldTypeSelectorRead extends React.Component {
	constructor(props) {
    super(props);
    var initvalue = "";
    if(this.props.value !==null)
    {
        initvalue = this.props.value;
    }
    this.state = {selectedvalue:initvalue};
    this.getDropdown = this.getDropdown.bind(this);
    this.changeValueDropdown = this.changeValueDropdown.bind(this);
    this.onOptionsFieldChanged = this.onOptionsFieldChanged.bind(this);
    this.getOptionsField = this.getOptionsField.bind(this);
    this.onCheckboxFieldChanged = this.onCheckboxFieldChanged.bind(this);

  }


getDropdown()
{


  var selects = this.props.field.custom.choice.split("\n").map(function(value)
  {
  return(<option key={value}>{value}</option>);
  });
  if(this.state.selectedvalue.length===0)
  {
    selects.unshift(<option key=""></option>);
  }
  return(<div><select onChange={this.changeValueDropdown} defaultValue={this.props.value} size="1">
  {selects}
  </select></div>);  
  
}

onCheckboxFieldChanged(e)
{
  var newvalue
  if(this.state.selectedvalue.indexOf(e.target.value) === -1)
  {
   
     newvalue = this.state.selectedvalue + e.target.value + "; "
    this.setState({selectedvalue:newvalue});
  }else
  {    
    newvalue = this.state.selectedvalue.replace(e.target.value+";","");
    this.setState({selectedvalue:newvalue});
  }

  this.props.valueUpdate(this.props.field.id,newvalue);
}
changeValueDropdown(e)
{
  this.props.valueUpdate(this.props.field.id,e.target.value);
}
onOptionsFieldChanged(e)
{
  this.setState({selectedvalue:e.target.value});
  this.props.valueUpdate(this.props.field.id,e.target.value);
}
getOptionsField()
{
  var self = this;
  var namesel = "sel" + this.props.field.id;
  var selects = this.props.field.custom.choice.split("\n").map(function(value)
  {
  return(<div key={value}><input   checked={self.state.selectedvalue === value} 
    onChange={self.onOptionsFieldChanged}  type="radio" name={namesel} id={value} value={value}/>
  <label htmlFor={value}>{value}</label> </div>);
  });

  return(<div>
      <fieldset>
      {selects}
  </fieldset>
  </div>); 
}
getMehrfachAusfahl()
{
  var self = this;
  var selects = this.props.field.custom.choice.split("\n").map(function(value)
  {

  return(<div key={value}><input   checked={(self.state.selectedvalue.indexOf(value) > -1)} 
    onChange={self.onCheckboxFieldChanged}  type="checkbox"  id={value} value={value}/>
  <label htmlFor={value}>{value}</label> </div>);
  });
return(<div>
{selects}
  </div>); 
}


render() {
  var inputcontrol = null;
  switch(this.props.field.custom.selectortype)
  {
    case "dropdown":
        inputcontrol = this.getDropdown();   
    break;
    case "optionsfeld":
    inputcontrol = this.getOptionsField();
    break;
    case "mehrfachauswahl":
    inputcontrol = this.getMehrfachAusfahl();
    break;
    default:

  }


return(<div className="fieldtypeselectorread">
{inputcontrol}
		</div>);
}	  
  }   

export default FieldTypeSelectorRead;