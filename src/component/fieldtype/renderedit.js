import React from 'react';
import RenderDateTimeEdit from './datetime/edit';
import RenderTextEdit from './text/edit';
import RenderTextPictureEdit from './picture/edit';
import RenderUserEdit from './user/edit';
import RenderSelectorEdit from './selector/edit';
import RenderAnyListEdit from './anylist/edit';
import RenderFloatEdit from './float/edit';
import RenderFileEdit from './file/edit';
import RenderIntegerEdit from './integer/edit';
import RenderSwitchEdit from './switch/edit';
import RenderGeodataEdit from './geodata/edit';
import RenderURLEdit from './url/edit';
import RenderLookupEdit from './lookup/edit';
import RenderSubList from './sublist/edit';
import LanguageEdit from './language/edit';
class RenderEdit extends React.Component {

    shouldComponentUpdate(nextProps, nextState)
    {
return false;
    }


render() {
    switch(this.props.field.type)
    {
        case "17a8dd83-0068-4e4a-bfcc-082f7f64e6f8":
        return(<RenderDateTimeEdit valueUpdate={this.props.valueUpdate}  field={this.props.field} value={this.props.value} />);
        case "32b60712-1f4d-4a49-87a6-90de0363eef4":
        return(<RenderTextEdit valueUpdate={this.props.valueUpdate} field={this.props.field} value={this.props.value} />);
        case "8787e054-7bb9-411a-8946-da658fbc8e9f":
        return(<RenderTextPictureEdit objectid={this.props.objectid} valueUpdate={this.props.valueUpdate}  field={this.props.field} value={this.props.value} />);   
        case "cc333aeb-196d-4331-b1df-3b530fa67722":
        return(<RenderUserEdit valueUpdate={this.props.valueUpdate}  field={this.props.field} value={this.props.value} />);     
        case "59d19ffb-7090-46ca-9021-b79c697142d1":
        return(<RenderFileEdit objectid={this.props.objectid}  valueUpdate={this.props.valueUpdate}  field={this.props.field} value={this.props.value} />);   
        case "8FE72E9F-09CC-4225-9135-58E042DC98A8":
        return(<RenderSelectorEdit valueUpdate={this.props.valueUpdate}  field={this.props.field} value={this.props.value} />);     
        case "bb46d3a0-a9cc-411a-9f2a-b3b016a863f1":
        return(<RenderAnyListEdit valueUpdate={this.props.valueUpdate}  field={this.props.field} value={this.props.value} />);    
        case "278bd56c-e463-4ec4-a702-82a9ecd59137":
        return(<RenderFloatEdit valueUpdate={this.props.valueUpdate} field={this.props.field} value={this.props.value} />);   
        case "39f6c3e6-d446-4e53-9706-67f556b06bdb":
        return(<RenderIntegerEdit valueUpdate={this.props.valueUpdate}  field={this.props.field} value={this.props.value} />);  
        case "fd3eb39b-3f6c-4e43-baea-12135ed5bee7":
        return(<RenderSwitchEdit valueUpdate={this.props.valueUpdate}  field={this.props.field} value={this.props.value} />);  
        case "fac261ca-829e-4f44-92fe-51a0d9d080c3":
        return(<RenderGeodataEdit valueUpdate={this.props.valueUpdate}  field={this.props.field} value={this.props.value} />);  
        case "52a3bb09-fc62-4099-a7ff-c314074f274a":
        return(<RenderURLEdit valueUpdate={this.props.valueUpdate}  field={this.props.field} value={this.props.value} />);  
        case "971c3761-5565-4a65-b546-7885b8bef160":
        return(<RenderLookupEdit valueUpdate={this.props.valueUpdate}  field={this.props.field} value={this.props.value} />);  
        case "8c2a7b49-2584-4296-a39b-32717e56510b":
        return(<RenderSubList valueUpdate={this.props.valueUpdate}  field={this.props.field} value={this.props.value} />);  
        case "e4fe0940-7542-46ef-85a4-50b2ed3289a5":
        return(<LanguageEdit valueUpdate={this.props.valueUpdate}  field={this.props.field} value={this.props.value} />);  
        default:
    }

return(<div>

		</div>);
}	  
  }   
export default RenderEdit;

