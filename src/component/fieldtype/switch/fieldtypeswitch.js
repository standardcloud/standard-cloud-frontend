import React from 'react';

  class FieldTypeSwitch extends React.Component {
	    constructor(props) {
		super(props);
		this.handlechangeswitch = this.handlechangeswitch.bind(this);
		this.getcustomsettings = this.getcustomsettings.bind(this);
		this.state = {switchfieldsettings:{booleandefaultvalue:false}};		
  }			  
componentDidMount() 
{
	if(this.props.setcustomsettings  !== undefined)
	{	
this.setState({switchfieldsettings:this.props.setcustomsettings});
this.props.getcustomsettings(this.props.setcustomsettings);
	}
}
getcustomsettings()
{
	   this.props.getcustomsettings(this.state.switchfieldsettings);
}


handlechangeswitch()
{
	var oldstate = this.state.switchfieldsettings;
	oldstate.booleandefaultvalue = !oldstate.booleandefaultvalue;
	this.setState({switchfieldsettings:oldstate});
	this.getcustomsettings();
}



render() {
		return(<div id="fieldtypetextsettingsbox">
		        <div className="stdcheckboxregion">
				<input id="enablemultipleusers"  checked={this.state.switchfieldsettings.booleandefaultvalue} type="checkbox" value="enablemultipleusers"   className="checkbox"/>
				<label onClick={this.handlechangeswitch}  htmlFor="enablemultipleusers">Schalter standardmäßig an</label>
			</div>

		</div>);
}	  
  }   
export default FieldTypeSwitch;
