import React from 'react';

class FieldTypeSwitchEdit extends React.Component {
	constructor(props) {
    super(props);
    console.log(this.props.value);
    if(this.props.value ===null)
    {
      this.state = {value:false};
      this.props.valueUpdate(this.props.field.id,false);
    }else
    {
       this.state = {value:this.props.value};
       this.props.valueUpdate(this.props.field.id,this.props.value);
    }
    this.changeCheckbox = this.changeCheckbox.bind(this);

  }
 
  changeCheckbox(e)
  {
    this.setState({value:!this.state.value});
    this.props.valueUpdate(this.props.field.id,!this.state.value);
  }

render() {

return(<div className="fieldtypeboxdatetimeread">
 	              <div className="stdcheckboxregion">
                            <input id="analytics" readOnly  checked={this.state.value} type="checkbox" value="enableanalytics" className="checkbox"/>
                            <label onClick={this.changeCheckbox}  htmlFor="analytics"></label>
                </div>
 		</div>);
}	  
  }   
export default FieldTypeSwitchEdit;

