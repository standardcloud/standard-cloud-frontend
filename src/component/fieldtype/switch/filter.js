import React from 'react';
import './filter.css';

class FilterSwitch extends React.Component {
	constructor(props) {
    super(props);
    this.state={searchvalue:""};
    this.onSwitchChance = this.onSwitchChance.bind(this);
  }

  onSwitchChance(e)
{
  this.setState({searchvalue:e.target.value});

  var filterlist =[];
  var len = this.props.objectresult[0].length;
 for(let i=0;i< this.props.objectresult.length;i++)
 {
   if(this.props.objectresult[i][this.props.index].toLowerCase().indexOf(e.target.value.toLowerCase()) === -1)
   {    
     filterlist.push(this.props.objectresult[i][len-1])
   }  
 }
 
 this.props.filtercallback(this.props.field.id,filterlist);
}


render() {

return(<div>
<select onChange={this.onSwitchChance} defaultValue={this.state.searchvalue} >
  <option value=""></option>
  <option value="Ja">Ja</option>
  <option value="Nein">Nein</option>
</select> 
		</div>);
}	  
  }   

export default FilterSwitch;