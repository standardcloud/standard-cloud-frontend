import React from 'react';

class FieldTypeSwitchRead extends React.Component {

render() {

return(<div className="fieldtypeboxswitchread">
 	              <div className="stdcheckboxregion">
                            <input id="analytics" readOnly  checked={this.props.value} type="checkbox" value="enableanalytics" className="checkbox"/>
                            <label   htmlFor="analytics"></label>
                </div>
		</div>);
}	  
  }   
export default FieldTypeSwitchRead;

