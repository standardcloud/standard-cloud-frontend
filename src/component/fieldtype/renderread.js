import React from 'react';
import RenderDateTimeRead from './datetime/read';
import RenderTextRead from './text/read';
import RenderTextPicture from './picture/read';
import RenderUserRead from './user/read';
import RenderSelectorRead from './selector/read';
import RenderAnyListRead from './anylist/read';
import RenderFloatRead from './anylist/read';
import RenderIntegerRead from './integer/read';
import RenderFileRead from './file/read';
import RenderSwitchRead from './switch/read';
import RenderUrlRead from './url/read';
import RenderGeoDataRead from './geodata/read';
import RenderSubListRead from './sublist/read';
import LanguageRead from './language/read';
import RenderLookupRead from './lookup/read';
class RenderRead extends React.Component {

render() {

    switch(this.props.field.type)
    {
        case "17a8dd83-0068-4e4a-bfcc-082f7f64e6f8":
        return(<RenderDateTimeRead field={this.props.field} value={this.props.value} />);
        case "32b60712-1f4d-4a49-87a6-90de0363eef4":
        return(<RenderTextRead field={this.props.field} value={this.props.value} />);
        case "8787e054-7bb9-411a-8946-da658fbc8e9f":
        return(<RenderTextPicture field={this.props.field} value={this.props.value} />);   
        case "cc333aeb-196d-4331-b1df-3b530fa67722":
        return(<RenderUserRead field={this.props.field} value={this.props.value} />);     
        case "8FE72E9F-09CC-4225-9135-58E042DC98A8":
        return(<RenderSelectorRead field={this.props.field} value={this.props.value} />); 
        case "59d19ffb-7090-46ca-9021-b79c697142d1":
        return(<RenderFileRead objectid={this.props.objectid}  field={this.props.field} value={this.props.value} />);       
        case "bb46d3a0-a9cc-411a-9f2a-b3b016a863f1":
        return(<RenderAnyListRead field={this.props.field} value={this.props.value} />);    
        case "278bd56c-e463-4ec4-a702-82a9ecd59137":
        return(<RenderFloatRead field={this.props.field} value={this.props.value} />);   
        case "39f6c3e6-d446-4e53-9706-67f556b06bdb":
        return(<RenderIntegerRead field={this.props.field} value={this.props.value} />);  
        case "fd3eb39b-3f6c-4e43-baea-12135ed5bee7":
        return(<RenderSwitchRead  field={this.props.field} value={this.props.value} />);  
        case "52a3bb09-fc62-4099-a7ff-c314074f274a":
        return(<RenderUrlRead field={this.props.field} value={this.props.value} />);  
        case "fac261ca-829e-4f44-92fe-51a0d9d080c3":
        return(<RenderGeoDataRead  field={this.props.field} value={this.props.value} />);  
        case "8c2a7b49-2584-4296-a39b-32717e56510b":
        return(<RenderSubListRead field={this.props.field} value={this.props.value} />);  
        case "e4fe0940-7542-46ef-85a4-50b2ed3289a5":
        return(<LanguageRead field={this.props.field} value={this.props.value} />);  
        case "971c3761-5565-4a65-b546-7885b8bef160":
        return(<RenderLookupRead   objectid={this.props.objectid}   field={this.props.field} value={this.props.value} />);  
        default:
    }

return(<div>

		</div>);
}	  
  }   
export default RenderRead;

