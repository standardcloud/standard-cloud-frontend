
import React from 'react';
import dialog from '../../dialog/stddialog';

  class FieldTypeText extends React.Component {
	    constructor(props) {
		super(props);
		this.handleMaxTextlengthChange = this.handleMaxTextlengthChange.bind(this);
		this.handleTextTypeChange = this.handleTextTypeChange.bind(this);
		this.handleDefaultValueChange = this.handleDefaultValueChange.bind(this);
		this.handlePreviewlengthChange = this.handlePreviewlengthChange.bind(this);
		this.handleChangeTextboxPlacehoder = this.handleChangeTextboxPlacehoder.bind(this);
		this.state = {textfieldsettings:{maxtextlength:"",editortype:"",defaultvalue:"",previewlength:""}};		
  }			  
componentDidMount() 
{
	if(this.props.setcustomsettings  !== undefined)
	{	
		this.setState({textfieldsettings:this.props.setcustomsettings});
		this.props.getcustomsettings(this.props.setcustomsettings);
	}
}
getcustomsettings()
{
	   this.props.getcustomsettings(this.state.textfieldsettings);
}
handleDefaultValueChange(e)
{
	var oldstate = this.state.textfieldsettings;
	oldstate.defaultvalue = e.target.value;
	this.setState({textfieldsettings:oldstate});
	this.getcustomsettings();
}
handleTextTypeChange(typetext)
{
	var oldstate = this.state.textfieldsettings;
	oldstate.editortype = typetext;
	this.setState({textfieldsettings:oldstate});
	this.getcustomsettings();
}
handleChangeTextboxPlacehoder(e)
{
	var oldstate1 = this.state.textfieldsettings;
	oldstate1.texboxplacehoder = e.target.value;
	this.setState({textfieldsettings:oldstate1});
	this.getcustomsettings();
}
handlePreviewlengthChange(e)
{
	var self = this;
	if(isNaN(e.target.value))
		{
			dialog.error("Previewtextlänge","muss eine Zahl sein.");
			var oldstate = this.state.textfieldsettings;
			oldstate.previewlength = "";
			self.setState({textfieldsettings:oldstate});
		}else
		{
		var oldstate2 = this.state.textfieldsettings;
		oldstate2.previewlength = e.target.value;
		self.setState({textfieldsettings:oldstate2});
	}
	this.getcustomsettings();
}

handleMaxTextlengthChange(e)
{
	var self = this;
	if(isNaN(e.target.value))
		{
			dialog.error("Textlänge","muss eine Zahl sein.");
			var oldstate = this.state.textfieldsettings;
			oldstate.maxtextlength = "";
			self.setState({textfieldsettings:oldstate});
		}else
		{
		var oldstate88 = this.state.textfieldsettings;
		oldstate88.maxtextlength = e.target.value;
		self.setState({textfieldsettings:oldstate88});
	}
	this.getcustomsettings();
}

render() {
	var selfrender = this;
		return(<div id="fieldtypetextsettingsbox">
		<table><tbody><tr><td className="fieldtypesettingsboxtdnames">Maximale Textlänge(Zeichen):</td>
		<td><input type="text" className="validate[custom[integer]]" value={this.state.textfieldsettings.maxtextlength} onChange={selfrender.handleMaxTextlengthChange} data-prompt-position="topLeft:30"  name="tbnumberoflines"/></td></tr>
		<tr><td>Geben Sie den zulässigen Texttyp an: </td><td>
		<div className="stdradioboxregion"><input onChange={()=>selfrender.handleTextTypeChange("simpletext")} checked={this.state.textfieldsettings.editortype==="simpletext"} className="radio" id="simpletext"  type="radio"  name="texttype" value="simpletext"/>
			<label htmlFor="simpletext">Eine Textzeile</label>
 		</div>
		<div className="stdradioboxregion"><input onChange={()=>selfrender.handleTextTypeChange("multiline")} checked={this.state.textfieldsettings.editortype==="multiline"} className="radio" id="multiline"  type="radio"  name="texttype" value="multiline"/>
			<label htmlFor="multiline">Mehrere Textzeilen </label>
 		</div>
		<div className="stdradioboxregion"><input onChange={()=>selfrender.handleTextTypeChange("richtext")} checked={this.state.textfieldsettings.editortype==="richtext"} className="radio" id="richtext"  type="radio"  name="texttype" value="richtext"/>
			<label htmlFor="richtext">Richtext Editor</label>
 		</div>
		 </td>
  </tr>
 <tr>
 <td>Standardwert:</td><td><textarea value={this.state.textfieldsettings.defaultvalue} onChange={selfrender.handleDefaultValueChange} name="tbdefaultvalue"></textarea>
 </td>
 </tr>
 </tbody>
 </table><h1>Vorschau-Einstellungen</h1>
 <table><tbody><tr><td>Anzahl Zeichen:</td><td>
 <input type="text" value={this.state.textfieldsettings.previewlength} onChange={selfrender.handlePreviewlengthChange}  className="validate[custom[integer]]" data-prompt-position="topLeft:30"  name="tbpreviewlength"/></td></tr>
<tr><td>Feldhintergrund</td><td><select name="texboxplacehoder" onChange={this.handleChangeTextboxPlacehoder} value={this.state.textfieldsettings.texboxplacehoder}>
<option value="texboxplacehoder-0"></option>
<option value="texboxplacehoder-1">Feldname in Textbox anzeigen</option>
<option value="texboxplacehoder-2">Feldbeschreibung in Textbox anzeigen</option>
</select></td></tr></tbody></table>
		</div>);
}	  
  }   
export default FieldTypeText;
