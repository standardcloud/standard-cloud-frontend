import React from 'react';
import './filter.css';

class FilterText extends React.Component {
	constructor(props) {
    super(props);
    this.state={searchvalue:""};
    this.onTextChance = this.onTextChance.bind(this);
  }

onTextChance(e)
{
  this.setState({searchvalue:e.target.value});

  var filterlist =[];
  var len = this.props.objectresult[0].length;
 for(let i=0;i< this.props.objectresult.length;i++)
 {
   if(this.props.objectresult[i][this.props.index].toLowerCase().indexOf(e.target.value.toLowerCase()) === -1)
   {    
     filterlist.push(this.props.objectresult[i][len-1])
   }  
 }
 
 this.props.filtercallback(this.props.field.id,filterlist,e.target.value);
}


render() {

return(<div>
<input type="text" className="text-filter-input" onChange={this.onTextChance} value={this.state.searchvalue} />
		</div>);
}	  
  }   

export default FilterText;