import React from 'react';
//import RichtextEditor from '../../texteditor/richtexteditor';
import './edit.css';
import TrumbowygEditor from '../../texteditor/TrumbowygEditor';
class FieldTypeTextEdit extends React.Component {
  constructor(props) {
    super(props);
    this.handleTextChange = this.handleTextChange.bind(this);
    this.updateRichtext = this.updateRichtext.bind(this);
    this.editload = true;
    this.state = {value:""};
    
  }
  componentDidMount(){
    var value = "";
    if(this.props.value !=null)
    {
      value = this.props.value;
    }

    this.setState({value:value});
  }
  componentWillReceiveProps(nextProps)
{  
  if(this.editload)
  {
  if (this.state.value !== nextProps.value && nextProps.value !=null) 
     {
       this.setState({value:nextProps.value});
       this.editload=false;
     }
  }
}


  
  handleTextChange(e)
  {
   // this.setState({value:e.target.value});
    this.props.valueUpdate(this.props.field.id,e.target.value);
  }
  updateRichtext(e)
  {
     this.props.valueUpdate(this.props.field.id,e.target.innerHTML);
  }

render() {
var editor = null;
var value = this.state.value;
if(!value)
{
  value = "";
}

switch(this.props.field.custom.editortype)
{
  case "simpletext":
  editor = <input className="editsimpletextfieldinput"  type="text" onChange={this.handleTextChange} id={this.props.field.id}  defaultValue={value}/>;
  break;
  case "multiline":
  editor = <textarea className="editmultilinetextfieldinput" id={this.props.field.id} defaultValue={this.props.value} onChange={this.handleTextChange}  rows="5"></textarea>;
  break;
  case "richtext":
  editor =  <TrumbowygEditor fieldid={this.props.field.id} value={value} onChange={this.updateRichtext}/> // <RichtextEditor htmlcontent={value} onChange={this.updateRichtext} />
  break;
  default:
}
return(<div className="fieldtypeboxtextedit">
{editor}
		</div>);
}	  
  }   
export default FieldTypeTextEdit;

