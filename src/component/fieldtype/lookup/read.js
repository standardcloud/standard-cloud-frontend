import React from 'react';
import { withRouter } from 'react-router-dom';
import ObjectManager from '../../../module/objectmanager/stdobjectmanager';
import './read.css';
import ObjectUrlManager from '../../../module/objecturlmanager/stdobjecturlmanager';

class FieldTypeLookupRead extends React.Component {

	constructor(props) {
    super(props);
    this.state = {selectobjects:[]};
    this.getlookupvalues = this.getlookupvalues.bind(this);
    this.onClickObject = this.onClickObject.bind(this);
  }

  componentDidMount() {						
    this.getlookupvalues(this.props.field.id,this.props.field.custom.listid,this.props.field.custom.fields);
}

onClickObject(data)
{
  var sself= this;
	ObjectUrlManager.geturl(data.listid,data.id,function(url)
			{
		
            sself.props.history.push(url);
		    });	  
}

getlookupvalues(fieldid, remotelistid, listfield) {

  var selfinlookup = this;
  var result = [];
  ObjectManager.getlookupvalues(fieldid,remotelistid,listfield,this.props.match.params.objectid).then(data =>
    {
      for(var i = 0; i < data.length; i++ )
        {
     
          if(data[i].connected ===true)
          {
            var selectedentry = {};
            selectedentry.id = data[i].id;			 
            selectedentry.connected =data[i].connected;
            selectedentry.listid = data[i].listid;
            selectedentry.value = data[i].value;            
            result.push(selectedentry);			
          }
        }
        selfinlookup.setState({selectobjects:result});
    });

}

render() {
var self =this;
var   availableobjects = []

this.state.selectobjects.forEach(function(data)
{
availableobjects.push(<tr className="read-link-tr" onClick={()=>{self.onClickObject(data)}} key={data.id}><td>{data.value}</td></tr>)
});

return(<div className="fieldtypeboxlookupread">
					<table>
						<tbody>
					{availableobjects}
						</tbody>
					</table>
		</div>);
}	  
  }   
export default withRouter(FieldTypeLookupRead);

