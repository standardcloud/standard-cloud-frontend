import React from 'react';

import ObjectTypeManager from '../../../module/objecttypemanager/stdobjecttypemanager';
import Listselector from '../../../component/list/listselector';

class FieldTypeLookup extends React.Component {
	    constructor(props) {
		super(props);
		this.getcustomsettings = this.getcustomsettings.bind(this);
		this.listupdate = this.listupdate.bind(this);
		this.deletelist = this.deletelist.bind(this);
		this.selectfield = this.selectfield.bind(this);
		this.changeobjectcount = this.changeobjectcount.bind(this);
		this.state = {lookupsettings:{fields:"",listid:"",objectcount:"1"},list:{id:""},objecttype:{fields:[]}};		
  }			  
componentDidMount() 
{
	console.log(this.props.setcustomsettings);
	if(this.props.setcustomsettings  !== undefined)
	{	
		console.log(this.props.setcustomsettings);
		this.setState({lookupsettings:this.props.setcustomsettings});
		this.props.getcustomsettings(this.props.setcustomsettings);
		var listob = {};
		listob.id = this.props.setcustomsettings.listid;
		listob.name = this.props.setcustomsettings.listname;
		listob.description = this.props.setcustomsettings.listdescription;
		listob.objecttypeid = this.props.setcustomsettings.objecttypeid;
		if(this.props.setcustomsettings.listid !=="")
		{
		this.listupdate(listob);
		}
	}
}
getcustomsettings()
{
	   this.props.getcustomsettings(this.state.lookupsettings);
}
listupdate(list)
{
	var self= this;

	ObjectTypeManager.getobjecttypesettings(list.objecttypeid).then(data=>
	{
		self.setState({objecttype:data});

		var old = self.state.lookupsettings;
		old.listid = list.id;

		
		self.setState({lookupsettings:old},self.getcustomsettings());
	
		;
		
	});

}
deletelist()
{
	this.setState({list:{id:""}});
	this.setState({lookupsettings:{fields:"",listid:"",objectcount:"1"}});
	this.setState({objecttype:{fields:[]}});
}
changeobjectcount(e)
{
	var old = this.state.lookupsettings;
	old.objectcount = e.target.value;
	this.setState({lookupsettings:old});
	this.getcustomsettings();
}
selectfield(fieldid)
{
	var oldvalue = this.state.lookupsettings.fields;
	if(oldvalue.indexOf(fieldid) === -1)
		{
			oldvalue = oldvalue + (fieldid  + ";");
			console.log(oldvalue);
		}else
		{
			oldvalue = oldvalue.replace(fieldid  + ";","");		
			console.log(oldvalue);		
		}
	var oldstate = this.state.lookupsettings;
	oldstate.fields = oldvalue;
	this.setState({lookupsettings:oldstate});
	this.getcustomsettings();
}


render() {
	var selfrender = this;
	var listdata = null;
	var fieldcheckboxcontrol = null;
	if(this.state.list.id.length > 0)
		{
			listdata = <div id="fieldtypelookupbox">
				<table>
					<tbody>
					<tr><td>
				{this.state.list.name}<br/>({this.state.list.description})</td>
				<td>
					<img id="deleteimgfieldtypelookup" onClick={this.deletelist} src="https://publicfile.standard-cloud.com/icons/delete.png" alt="entfernen" />
				</td></tr>
				</tbody>
		    	</table>
			   </div>




		}else
		{
			listdata = <Listselector listupdate={this.listupdate} />
		}

		fieldcheckboxcontrol = selfrender.state.objecttype.fields.map(function(field)
		{
			return (<div key={field.id} className="stdcheckboxregion">
            <input id={field.id}  type="checkbox" value={field.id} checked={selfrender.state.lookupsettings.fields.indexOf(field.id)!==-1}  className="checkbox"/>
            <label onClick={()=> selfrender.selectfield(field.id)}  htmlFor={field.id}><strong dangerouslySetInnerHTML={{__html:field.name}}></strong></label>
      		</div>);
		});

		return(<div id="fieldtypelookupsettingsbox">
			<table>
				<tbody>
				<tr>
				<td>Liste</td><td>{listdata}</td>
				</tr>
				<tr>
				<td>Anzahl an Objekten</td><td>
				<select defaultValue={this.state.lookupsettings.objectcount} onChange={this.changeobjectcount}>
				<option  value="1">1 {this.state.list.objecttype} erlauben</option>
				<option value="2">Beliebie Anzahl an {this.state.list.objecttype} erlauben</option>
				</select>

				</td>
				</tr>
				<tr>
				<td>Eingabeformular</td><td><br/><br/>
				{fieldcheckboxcontrol}
				</td>
				</tr>
				</tbody>
			</table>
		</div>);
}	  
  }   
export default FieldTypeLookup;
