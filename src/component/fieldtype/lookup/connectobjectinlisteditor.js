import React from 'react';
import {BACKEND} from '../../../stdsetup';
import { withRouter } from 'react-router-dom';
import $ from 'jquery';

import ObjectUrlManager from '../../../module/objecturlmanager/stdobjecturlmanager';
import './connectobjectinlisteditor.css';
class connectobjectinlistwidget extends React.Component {
	  	    constructor(props) {
    super(props);
	this.state = {selectobjects:[],editmode:false,searchvalue:"",startvalue:""};
	this.clickedit = this.clickedit.bind(this);
	this.setFieldIDValues = this.setFieldIDValues.bind(this);
	this.openobject = this.openobject.bind(this);
	this.handleConnectSearchTextChange = this.handleConnectSearchTextChange.bind(this);
	this.selectobject = this.selectobject.bind(this);

  }

        			  componentDidMount() {
						
                this.getlookupvalues(this.props.fieldid,this.props.remotelistid,this.props.listfield);
				}

				handleConnectSearchTextChange(event) {
    this.setState({searchvalue: event.target.value.toLowerCase()});            
}

getlookupvalues(fieldid, remotelistid, listfield) {
		var selfinlookup = this;
    var result = [];
			 var jsondata = {};
			 jsondata.fieldid = fieldid;
			 jsondata.remotelistid = remotelistid;
			 jsondata.listfield = listfield;			 
			 jsondata.objectid = this.props.match.params.objectid
			 
		   $.ajax({
			  type: "POST",
			  xhrFields: {
				withCredentials: true
			 },
			  url: BACKEND + "/control/connectobjectinlist/getlookupfieldvalues/",
			  data: jsondata,
			  success: function( data, textStatus, jqXHR) {		
			  var updatevalue ="";
				for(var i = 0; i < data.length; i++ )
					{
						   var selectedentry = {};
						   selectedentry.id = data[i].id;			 
						   selectedentry.connected =data[i].connected;
						   selectedentry.listid = data[i].listid;
						   selectedentry.value = data[i].value;
						if(data[i].connected ===true)
						{
							updatevalue +=data[i].id +';'						}

						result.push(selectedentry);				 
					}
		  selfinlookup.props.valueUpdate(selfinlookup.props.fieldid,updatevalue);
		  selfinlookup.setState({startvalue:updatevalue});
          selfinlookup.setState({selectobjects:result});
		  },  
			error: function(jqXHR, textStatus, errorThrown){				
		 console.log("..error");
		  }
		}); 
	
	
	
        }

				clickedit()
				{					
					if(this.props.value)
					{		
				   this.setState({startvalue: this.props.value});  
					}	
				   this.setState({editmode:true});
				}

selectobject(ob)
{
	var fs = this;
	
	var newarray = [];
	var switchtrue = false
	if(ob.connected ===false)
	{
		switchtrue = true;
	}
	for(let i=0; i < this.state.selectobjects.length;i++)
	{
		var obj = this.state.selectobjects[i];
		;
		if(obj.id === ob.id)
		{
			obj.connected = !obj.connected;

		}else
		{
			if(fs.props.maxobjects ===1 && switchtrue===true)
			{
				obj.connected=false; 		
			}			
		}
		newarray.push(obj);
	}
	this.setState({selectobjects:newarray});
	this.setFieldIDValues();
}

setFieldIDValues()
{
var selfinlookup = this;
var newvalue = ""

for(let i=0;i< this.state.selectobjects.length;i++)
{
	var ob = this.state.selectobjects[i];
	if(ob.connected)
	{
		newvalue += "add" + ob.id + ";";
	}
}

var oldarray = this.state.startvalue.split(";");

for(var j=0;j<oldarray.length;j++)
{
	var val =	oldarray[j];
	if(newvalue.indexOf(val) === -1)
	{
		newvalue +="rem" + 	val + ";";	
	}
}

selfinlookup.props.valueUpdate(selfinlookup.props.fieldid,newvalue);



													

}

openobject(objectelement)
{  

    var sself= this;
	ObjectUrlManager.geturl(objectelement.listid,objectelement.id,function(url)
			{
		
            sself.props.history.push(url);
		    });	  
}

render() {
 var selfrender = this;
		var  selectedobjects = [];
		this.state.selectobjects.forEach(function(ob) {	
				if(ob.connected)
					{	
			//var link = "/list/" + ob.listid + "/edit/" + ob.id + "/";
            selectedobjects.push (
                   <li key={ob.id} className="connectobjectlistelement">				
                      <span onClick={()=>selfrender.openobject(ob)} dangerouslySetInnerHTML={{__html:ob.value}} ></span>
                    </li>); 
				 }
                             
              
    });

	var availableobjects = [];
	this.state.selectobjects.forEach(function(connection) {
				if(connection.value.toLowerCase().indexOf(selfrender.state.searchvalue) > -1 ||  selfrender.state.searchvalue ==="")
				{				
					availableobjects.push (
						<tr key={connection.id} onClick={()=>selfrender.selectobject(connection)}>
				<td className="connectobjectlistwidgeticon" >
					{connection.connected === true &&
					<img alt="ausgewählt" src="https://publicfile.standard-cloud.com/icons/checked.png" />
					}
					{connection.connected === false &&
					<img alt="nicht ausgewählt"  src="https://publicfile.standard-cloud.com/icons/unchecked.png" />
					}
				</td>           
							<td dangerouslySetInnerHTML={{__html:connection.value}} ></td>
					</tr>
		);
		}

	});

	return (

         <div className="connectobjectinlistwidgetcontainer">
				    {selfrender.state.editmode === false &&
					     <div className="connectobjectinlistselected">
              <ul>     
          {selectedobjects}
              </ul>
			  			  	<div>
		<img className="editobjectlisticon" alt="Bearbeiten"  onClick={selfrender.clickedit} title="Bearbeiten"   src={"https://publicfile.standard-cloud.com/icons/edit.png"} />
			</div>
              </div>
			}

			

	  {selfrender.state.editmode === true &&
				<div className="connectobjectinlistfilter" >
					<input className="connectobjectinlistfilterinput"  value={this.state.searchvalue}  onChange={this.handleConnectSearchTextChange} placeholder="Filter" type="search" autoComplete="off"/>
					<table className="editconnectionobjects">
						<tbody>
					{availableobjects}
						</tbody>
					</table>
				</div>
		}
</div>

    );
	
}	  
  }
  


export default withRouter(connectobjectinlistwidget);