import React from 'react';
import FilterSelector from './selector/filter';
import FilterDate from './datetime/filter';
import FilterText from './text/filter';
import FilterPicture from './picture/filter';
import FilterUser from './user/filter';
import FilterAnyList from './anylist/filter';
import FilterFloat from './float/filter';
import FilterInteger from './integer/filter';
import FilterFile from './file/filter';
import FilterSwitch from './switch/filter';
import FilterUrl from './url/filter';
import FilterGeoData from './geodata/filter';
import FilterSubList from './sublist/filter';
import FilterLanguage from './language/filter';
class RenderFilter extends React.Component {

render() {

    switch(this.props.field.fieldtypeid)
    {
        case "17a8dd83-0068-4e4a-bfcc-082f7f64e6f8":
        return(<FilterDate field={this.props.field} filtercallback={this.props.filtercallback} index={this.props.index} objectresult={this.props.objectresult} />);
        case "32b60712-1f4d-4a49-87a6-90de0363eef4":
        return(<FilterText field={this.props.field} filtercallback={this.props.filtercallback}  index={this.props.index} objectresult={this.props.objectresult}  />);
        case "8787e054-7bb9-411a-8946-da658fbc8e9f":
        return(<FilterPicture field={this.props.field} filtercallback={this.props.filtercallback}  index={this.props.index} objectresult={this.props.objectresult}  />);   
        case "cc333aeb-196d-4331-b1df-3b530fa67722":
        return(<FilterUser field={this.props.field} filtercallback={this.props.filtercallback}  index={this.props.index} objectresult={this.props.objectresult}  />);     
        case "8FE72E9F-09CC-4225-9135-58E042DC98A8":
        return(<FilterSelector field={this.props.field} filtercallback={this.props.filtercallback} index={this.props.index} objectresult={this.props.objectresult} />); 
        case "59d19ffb-7090-46ca-9021-b79c697142d1":
        return(<FilterFile  field={this.props.field} filtercallback={this.props.filtercallback}  index={this.props.index} objectresult={this.props.objectresult} />);       
        case "bb46d3a0-a9cc-411a-9f2a-b3b016a863f1":
        return(<FilterAnyList field={this.props.field} filtercallback={this.props.filtercallback}  index={this.props.index} objectresult={this.props.objectresult}  />);    
        case "278bd56c-e463-4ec4-a702-82a9ecd59137":
        return(<FilterFloat field={this.props.field} filtercallback={this.props.filtercallback}   index={this.props.index} objectresult={this.props.objectresult} />);   
        case "39f6c3e6-d446-4e53-9706-67f556b06bdb":
        return(<FilterInteger field={this.props.field} filtercallback={this.props.filtercallback}  index={this.props.index} objectresult={this.props.objectresult} />);  
        case "fd3eb39b-3f6c-4e43-baea-12135ed5bee7":
        return(<FilterSwitch  field={this.props.field} filtercallback={this.props.filtercallback}  index={this.props.index} objectresult={this.props.objectresult}  />);  
        case "52a3bb09-fc62-4099-a7ff-c314074f274a":
        return(<FilterUrl field={this.props.field} filtercallback={this.props.filtercallback} index={this.props.index} objectresult={this.props.objectresult} />);  
        case "fac261ca-829e-4f44-92fe-51a0d9d080c3":
        return(<FilterGeoData  field={this.props.field} filtercallback={this.props.filtercallback}  index={this.props.index} objectresult={this.props.objectresult}  />);  
        case "8c2a7b49-2584-4296-a39b-32717e56510b":
        return(<FilterSubList field={this.props.field} filtercallback={this.props.filtercallback}  index={this.props.index} objectresult={this.props.objectresult}  />);  
        case "e4fe0940-7542-46ef-85a4-50b2ed3289a5":
        return(<FilterLanguage field={this.props.field} filtercallback={this.props.filtercallback}  index={this.props.index} objectresult={this.props.objectresult}  />);
        default:  
    }

return(<div>

		</div>);
}	  
  }   
export default RenderFilter;

