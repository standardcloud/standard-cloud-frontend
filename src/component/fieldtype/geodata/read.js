import React from 'react';
import MapWithSearchBox from "./MapWithASearchBox";

class FieldTypeGeodataRead extends React.Component {
	constructor(props) {
    super(props);
    this.locationUpdate = this.locationUpdate.bind(this);
  }

  locationUpdate(data)
  {
  
  }

  render() {

    var imagemap  =null 
    var markers =[];
      if(!(this.props.value.length > 0) && this.props.value.adr.length > 0)
      {
      var marker = {};
      marker.position={ lat: this.props.value.lat, lng: this.props.value.log};
      markers.push(marker) ;
      imagemap=  <MapWithSearchBox title={this.props.value.adr} markers={markers} lat={this.props.value.lat}  lng={this.props.value.log} valueUpdate={this.locationUpdate}/>
    }else
    {
      imagemap=  <MapWithSearchBox markers={[]} lat={1}  lng={1} valueUpdate={this.locationUpdate}/>
    }
    
        return(<div className="fieldtypeboxgeodataread">
    {imagemap}

        </div>);
  }
    }
export default FieldTypeGeodataRead;

