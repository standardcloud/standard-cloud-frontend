import React from 'react';

class FieldTypeLanguageEdit extends React.Component {
	constructor(props) {
    super(props);
    this.changeValueDropdown = this.changeValueDropdown.bind(this);
  }
  changeValueDropdown(e)
  {
    this.props.valueUpdate(this.props.field.id,e.target.value);
  }
render() {
return(<div className="fieldtypeboxlanguageedit">
    <select onChange={this.changeValueDropdown} defaultValue={this.props.value}  size="1">
      <option value="1">German</option>
      <option value="2">English</option>
      <option value="3">Espanol</option>
      <option value="4">French</option>
    </select>
		</div>);
}	  
  }   
export default FieldTypeLanguageEdit;
