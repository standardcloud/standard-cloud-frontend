import React from 'react';

class FieldTypeLanguageRead extends React.Component {


render() {
  var output = null;
  switch(this.props.value)
  {
    case "1":
    output = "German";
    break;
    case "2":
    output = "English";
    break;
    case "3":
    output = "Espanol";
    break;
    case "4":
    output = "French";
    break;
    default:
    output ="-";    
  }

return(<div className="fieldtypeboxlanguageread">
           {output}
		</div>);
}	  
  }   
export default FieldTypeLanguageRead;
