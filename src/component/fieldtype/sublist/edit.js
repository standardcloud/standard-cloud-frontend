import React from 'react';
import BT from '../../button/stdbutton';
import Dialog from '../../../component/dialog/stddialog';
import $ from 'jquery';
import {BACKEND} from './../../../stdsetup';
import { withRouter } from 'react-router-dom';
import {loadLists} from './../../../actions/list';
import {connect} from 'react-redux';
import './edit.css';
class FieldTypeSublistRead extends React.Component {
  constructor(props) {
    super(props);
    this.state = {listidcreated:""};		
		this.createList = this.createList.bind(this);
	
  }		
createList()
{
  var self = this;
  var jsondata = {};

  jsondata.listname = this.props.field.custom.listname;
  jsondata.objecttypeid =  this.props.field.custom.objecttypeid;
  jsondata.objectid = this.props.match.params.objectid;
  jsondata.parentlistid =  this.props.match.params.listid 
  jsondata.fieldid = this.props.field.id;

   $.ajax({
  type: "POST",
  data: jsondata,
  xhrFields: {
    withCredentials: true
   },
  url: BACKEND+ "/control/list/addlist/",
 success: function( data, textStatus, jqXHR) {
   if(data.state==="ok")
   {
    self.setState({listidcreated:data.content.listid});
    self.props.valueUpdate(self.props.field.id,data.content.listid);
    self.props.loadLists();
    Dialog.okdialog('OK','Liste ersellt.');		
  }else
  {
    self.props.valueUpdate(self.props.field.id,"");
    Dialog.errordialog('Liste','konnte nicht erstellt werden.');		
  }

  },  
    error: function(jqXHR, textStatus, errorThrown){
Dialog.errordialog('Fehler','Liste konnte nicht erzeugt werden!');			
//  alert(textStatus);
  }
}); 
}


render() {
var self = this;
var renderbutton = null;

if(this.state.listidcreated.length > 1)
{
  var buttontitle1 = self.props.field.custom.listname +  " anzeigen";
  var url1 = "/list/" + self.state.listidcreated + "/";
  renderbutton = <BT url={url1} iconurl="https://publicfile.standard-cloud.com/icons/list.png" name={buttontitle1} />
}
else
{



if(this.props.value && (this.props.value.length > 0))
{
  var buttontitle = this.props.value.listname ;
  var url = "/list/" + this.props.value.listid + "/";
   renderbutton = <BT url={url} count={this.props.value.objectcount} iconurl="https://publicfile.standard-cloud.com/icons/list.png" name={buttontitle} />
}else
{ 
  if(window.location.href.indexOf("newobject") === -1) 
  {
      renderbutton = <BT callbackfunction={this.createList} iconurl="https://publicfile.standard-cloud.com/icons/list.png" name="Liste erzeugen" />
  }else
  {
    var title = self.props.field.custom.listname + "-Liste kann erst generiert werden, nachdem das Objekt gespeichert wurde.";
    renderbutton = <div><div>{title}</div><br/>
    <BT disabled={true} iconurl="https://publicfile.standard-cloud.com/icons/list.png" name="" />
    </div>
  }
}
}
return(<div className="fieldtypeboxsublistedit">
{renderbutton}
		</div>);
}	  
  }   

export default withRouter(connect(null,{loadLists})(FieldTypeSublistRead));
