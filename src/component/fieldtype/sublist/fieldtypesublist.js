import React from 'react';
import Objecttypeselector from '../../objecttype/objecttypeselector/stdobjecttypeselector';

class FieldTypeSublist extends React.Component {
	    constructor(props) {
		super(props);
		this.getcustomsettings = this.getcustomsettings.bind(this);
		this.handlechangelistname = this.handlechangelistname.bind(this);
		this.updateobjecttypeid = this.updateobjecttypeid.bind(this);
		this.state = {sublistfieldsettings:{objecttypeid:"",listname:""}};		
  }			  
componentDidMount() 
{
	if(this.props.setcustomsettings  !== undefined)
	{	
this.setState({sublistfieldsettings:this.props.setcustomsettings});
this.props.getcustomsettings(this.props.setcustomsettings);
	}
}
getcustomsettings()
{
	   this.props.getcustomsettings(this.state.sublistfieldsettings);
}
handlechangelistname(e)
{
	var oldstate = this.state.sublistfieldsettings;
	oldstate.listname = e.target.value;
	this.setState({sublistfieldsettings:oldstate});
	this.getcustomsettings();
}
updateobjecttypeid(objecttypeid)
{
	var oldstate = this.state.sublistfieldsettings;
	oldstate.objecttypeid =objecttypeid;
	this.setState({sublistfieldsettings:oldstate});
}


render() {
	var selfrender = this;
		return(<div id="fieldtypetextsettingsbox">
 
 <table><tbody><tr><td>Listenvorlage</td><td>
	 <div id="fieldtypetextsettingsboxlistenvorlage">
 <Objecttypeselector objecttypeupdate={this.updateobjecttypeid} />
 </div>
 </td></tr>
<tr><td>Listenname</td><td>
<input id="fieldtypesublistlistname" type="text" value={this.state.sublistfieldsettings.listname} onChange={selfrender.handlechangelistname}   name="listname"/>
	</td></tr></tbody></table>
		</div>);
}	  
  }   
export default FieldTypeSublist;

