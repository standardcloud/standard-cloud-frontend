import React from 'react';

  class FieldTypeInteger extends React.Component {
	    constructor(props) {
		super(props);
		this.handlemininteger = this.handlemininteger.bind(this);
		this.handlemaxinteger = this.handlemaxinteger.bind(this);
		this.handledefaultvalue = this.handledefaultvalue.bind(this);
		this.handlepostfix = this.handlepostfix.bind(this);
		this.state = {integerfieldsettings:{mininteger:"",maxinteger:"",defaultvalue:"",postfix:""}};		
  }			  
componentDidMount() 
{
	if(this.props.setcustomsettings  !== undefined)
		{	
	this.setState({integerfieldsettings:this.props.setcustomsettings});
	this.props.getcustomsettings(this.props.setcustomsettings);
		}else
		{
			this.props.getcustomsettings(this.state.integerfieldsettings);	
		}
		
}
getcustomsettings()
{
	   this.props.getcustomsettings(this.state.integerfieldsettings);
}
handleDefaultValueChange(e)
{
	var oldstate = this.state.integerfieldsettings;
	oldstate.defaultvalue = e.target.value;
	this.setState({integerfieldsettings:oldstate});
	this.getcustomsettings();
}
handlemininteger(e)
{
	var oldstate = this.state.integerfieldsettings;
	oldstate.mininteger = e.target.value;
	this.setState({integerfieldsettings:oldstate});
	this.getcustomsettings();
}
handlemaxinteger(e)
{
	var oldstate = this.state.integerfieldsettings;
	oldstate.maxinteger = e.target.value;
	this.setState({integerfieldsettings:oldstate});
	this.getcustomsettings();
}

handledefaultvalue(e)
{
	var oldstate = this.state.integerfieldsettings;
	oldstate.defaultvalue = e.target.value;
	this.setState({integerfieldsettings:oldstate});
	this.getcustomsettings();
}
handlepostfix(e)
{
	var oldstate = this.state.integerfieldsettings;
	oldstate.postfix = e.target.value;
	this.setState({integerfieldsettings:oldstate});
	this.getcustomsettings();
}


render() {
	var selfrender = this;
		return(<div id="fieldtypetextsettingsbox">
		<table><tbody><tr><td className="fieldtypesettingsboxtdnames">Minimalwert:</td>
		<td><input type="text" className="validate[custom[integer]]" value={this.state.integerfieldsettings.mininteger} onChange={selfrender.handlemininteger} data-prompt-position="topLeft:30"  name="mininteger"/></td></tr>

 <tr><td className="fieldtypesettingsboxtdnames">Maximalwert:</td>
		<td><input type="text" className="validate[custom[integer]]" value={this.state.integerfieldsettings.maxinteger} onChange={selfrender.handlemaxinteger} data-prompt-position="topLeft:30"  name="maxinteger"/></td></tr>

 <tr><td className="fieldtypesettingsboxtdnames">Standardwert:</td>
		<td><input type="text" className="validate[custom[integer]]" value={this.state.integerfieldsettings.defaultvalue} onChange={selfrender.handledefaultvalue} data-prompt-position="topLeft:30"  name="defaultvalue"/></td></tr>

 <tr><td className="fieldtypesettingsboxtdnames">Postfix(z.B. €, $):</td>
		<td><input type="text" className="validate[custom[integer]]" value={this.state.integerfieldsettings.postfix} onChange={selfrender.handlepostfix} data-prompt-position="topLeft:30"  name="postfix"/></td></tr>

 </tbody></table>
		</div>);
}	  
  }   
export default FieldTypeInteger;

