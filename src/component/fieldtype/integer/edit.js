import React from 'react';

class FieldTypeIntegerEdit extends React.Component {
	constructor(props) {
    super(props);
    if(this.props.value===null || this.props.value.length ===0)
{
  this.state = {value:""};
}else
{
  this.state = {value:this.props.value.val};
}
   
    this.changeText = this.changeText.bind(this);
  }

  changeText(e)
  {
    var newvalue = parseInt(e.target.value, 10);
    if (!isNaN(newvalue))
    {
      this.setState({value:newvalue});
      this.props.valueUpdate(this.props.field.id,e.target.value);
    }
  }  
render() {

return(<div className="fieldtypeboxintegerread">
     <input type="text" onChange={this.changeText} value={this.state.value} />{this.props.field.custom.postfix}
		</div>);
}	  
  }   
export default FieldTypeIntegerEdit;

