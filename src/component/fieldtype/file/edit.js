import React from 'react';
import BT from '../../button/stdbutton';
import Dropzone from "dropzone";
import {BACKEND} from '../../../stdsetup';
import ReactAudioPlayer from 'react-audio-player';


class FieldTypeFileEdit extends React.Component {
	constructor(props) {
    super(props);
    this.state = {value:this.props.value};
    this.uploadfile = this.uploadfile.bind(this);
    this.initUploader = this.initUploader.bind(this);
    this.openFile = this.openFile.bind(this);
    this.deletefile = this.deletefile.bind(this);
  }

  componentDidMount(){
    this.initUploader();
  }

  openFile()
  {
    var openurl = BACKEND + this.state.value.filepath;
    let anchor = document.createElement("a");
    var self = this;
    
  const request = new Request(openurl, {
    method: 'GET',credentials: "include"
  });
  
    fetch(request)
        .then(response => response.blob())
        .then(blobby => {
            let objectUrl = window.URL.createObjectURL(blobby);
    
            anchor.href = objectUrl;
            anchor.download = self.state.value.filename;
            anchor.click();
    
            window.URL.revokeObjectURL(objectUrl);
        });
  }
  initUploader()
  {
   
    var self = this;
 //     this.props.valueUpdate(this.props.field.id,e.target.value);


   var uploadurl = BACKEND + "/file-upload/" + this.props.field.id + "-" + this.props.objectid + "/";
   var myDropzone = new Dropzone(this.refs.uploadfilebox, { url: uploadurl,   headers: {
    // remove Cache-Control and X-Requested-With
    // to be sent along with the request
    'Cache-Control': null,
    'X-Requested-With': null
},  withCredentials: true,paramName: "file[]", maxFilesize: 100,createImageThumbnails:false, maxFiles: 1, addRemoveLinks: true, previewsContainer: this.refs.fileuploadpreview ,parallelUploads: 1, uploadMultiple: false});
   
myDropzone.on("success", function(file) {

  var newstate = {};
  newstate.filepath = "/file/" + self.props.field.id + "-" + self.props.objectid + "/" + file.name;
  newstate.filename =  file.name;
  newstate.fileid = self.props.field.id + "-" + self.props.objectid;
  self.setState({value:newstate});
  self.props.valueUpdate(self.props.field.id,self.props.field.id + "-" + self.props.objectid);
});
myDropzone.on("error", function(error) {
  console.log("Error Image Upload");

});
myDropzone.on("maxfilesexceeded", function(file) {
  this.removeAllFiles();
  this.addFile(file);
});



  }
  
  deletefile()
  {
    this.setState({value:{}});
    this.props.valueUpdate(this.props.field.id,"");
  }
  setcuts(c)
  {
       this.setState({imagecut:c});
  }



  uploadfile()
  {
    this.refs.uploadfilebox.click();
  }

render() {

var value = this.state.value;
var deletebutton = null;
var opendocument = null;
var audioplayer = null;
//x.length === 0
// _.isEmpty(value) 
if(!value || value.length===0 || value.fileid==="")
{
  value = {filename:""};
}else
{
  var openname = this.state.value.filename + " öffnen";
  opendocument = <BT iconurl="https://publicfile.standard-cloud.com/icons/read64.png" callbackfunction={this.openFile} name={openname} />  
  deletebutton = <BT iconurl="https://publicfile.standard-cloud.com/icons/delete.png" callbackfunction={this.deletefile} name="Löschen" />  
}

if(value.filename.indexOf("mp3") > 0)
{
var filepath = BACKEND + this.props.value.filepath;
audioplayer = <div><ReactAudioPlayer controls src={filepath}/></div>
    
}



return(<div className="fieldtypeboxfileedit">
{audioplayer}
<div className="uploadimageboxbuttons">
{opendocument}
{deletebutton}
</div>
<div ref='uploadfilebox' className="uploadimagebox" >
<BT name="Upload" callbackfunction={this.uploadfile} iconurl="https://publicfile.standard-cloud.com/icons/attachfile.png" />
</div>
<div ref='fileuploadpreview'>
</div>
		</div>);
}	  
  }   
export default FieldTypeFileEdit;

