import React from 'react';
import BT from '../../button/stdbutton';
import {BACKEND} from '../../../stdsetup';
import ReactAudioPlayer from 'react-audio-player';
import './read.css';
class FieldTypeFileRead extends React.Component {
	constructor(props) {
    super(props);
    this.openurl = this.openurl.bind(this);
  }

openurl()
{
  var openurl = BACKEND + this.props.value.filepath;
  let anchor = document.createElement("a");
  var self = this;
  
const request = new Request(openurl, {
  method: 'GET',credentials: "include"
});

  fetch(request)
      .then(response => response.blob())
      .then(blobby => {
          let objectUrl = window.URL.createObjectURL(blobby);
  
          anchor.href = objectUrl;
          anchor.download = self.props.value.filename;
          anchor.click();
  
          window.URL.revokeObjectURL(objectUrl);
      });

}

render() {
  var openbutton = null;
  var audioplayer =null;
  if(this.props.value.fileid)
  {
    openbutton = <BT iconurl="https://publicfile.standard-cloud.com/icons/read64.png" name={this.props.value.filename} callbackfunction={this.openurl}/>;
    if(this.props.value.filename.indexOf("mp3") > 0)
    {
    var filepath = BACKEND + this.props.value.filepath;
  
    audioplayer = <div><ReactAudioPlayer controls src={filepath}/></div>
        
    }
  }



 
return(<div className="fieldtypeboxfileread">
{audioplayer}
    {openbutton}
		</div>);
}	  
  }   
export default FieldTypeFileRead;

