
import React from 'react';

class FieldTypeUrl extends React.Component {
	    constructor(props) {
		super(props);
		this.handlechangeopeninnewwindow = this.handlechangeopeninnewwindow.bind(this);
		this.handlechangeallowmultiplelinks = this.handlechangeallowmultiplelinks.bind(this);
		this.getcustomsettings = this.getcustomsettings.bind(this);
		this.state = {urlfieldsettings:{openinnewwindow:false,allowmultiplelinks:false}};		
  }			  
componentDidMount() 
{
	if(this.props.setcustomsettings  !== undefined)
	{	
		this.setState({urlfieldsettings:this.props.setcustomsettings});
		this.props.getcustomsettings(this.props.setcustomsettings);
	}
}
getcustomsettings()
{
	   this.props.getcustomsettings(this.state.urlfieldsettings);
}


handlechangeopeninnewwindow()
{
	var oldstate = this.state.urlfieldsettings;
	oldstate.openinnewwindow = !oldstate.openinnewwindow;
	this.setState({urlfieldsettings:oldstate});
	this.getcustomsettings();
}
handlechangeallowmultiplelinks()
{
	var oldstate = this.state.urlfieldsettings;
	oldstate.allowmultiplelinks = !oldstate.allowmultiplelinks;
	this.setState({urlfieldsettings:oldstate});
	this.getcustomsettings();
}

render() {
		return(<div id="fieldtypetextsettingsbox">
		        <div className="stdcheckboxregion">
					<input id="openinnewwindow"  checked={this.state.urlfieldsettings.openinnewwindow} type="checkbox" value="openinnewwindow"   className="checkbox"/>
					<label onClick={this.handlechangeopeninnewwindow}  htmlFor="openinnewwindow">Link in neuem Fenster/Tab öffnen</label>
		    	</div>
				<div className="stdcheckboxregion">
					<input id="allowmultiplelinks"  checked={this.state.urlfieldsettings.allowmultiplelinks}  type="checkbox" value="allowmultiplelinks"   className="checkbox"/>
					<label onClick={this.handlechangeallowmultiplelinks}  htmlFor="allowmultiplelinks">Mehrere Links/URLs erlauben</label>
		    	</div>

		</div>);
}	  
  }   
export default FieldTypeUrl;

