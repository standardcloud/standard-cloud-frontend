import React from 'react';
import UrlFieldEditor from './urlfieldeditor';
class FieldTypeUrlRead extends React.Component {

render() {
return(<div className="fieldtypeurlread">
  <UrlFieldEditor urls={this.props.value} readmode={false} field={this.props.field} valueUpdate={this.props.valueUpdate}/>
		</div>);
}	  
  }   
export default FieldTypeUrlRead;

