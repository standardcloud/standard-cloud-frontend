import React from 'react';
import './urlfieldeditor.css';

class UrlFieldEditor extends React.Component {
  constructor(props) {
    super(props);
    this.addurlmode = this.addurlmode.bind(this);
    this.updateUrlInputValue = this.updateUrlInputValue.bind(this);
    this.updateDescriptionInputValue = this.updateDescriptionInputValue.bind(this);
    this.clickdelete = this.clickdelete.bind(this);
    this.canceladdurl = this.canceladdurl.bind(this);
    this.addurl = this.addurl.bind(this);
    this.updateFieldData = this.updateFieldData.bind(this);
    var urls =[];
    if(this.props.urls)
    {
      urls = this.props.urls;
    }

		this.state = {editmode:false,urls:urls,url:"",newurldescription:"",newurl:""};		
  }		



  updateUrlInputValue(e)
  {
    this.setState({newurl: e.target.value});
  }

    updateDescriptionInputValue(e)
  {
    this.setState({newurldescription: e.target.value});
  }

  addurlmode()
  {
    this.setState({editmode:true});    
  }
  updateFieldData()
  {
    var self =this;
     var str = "";

    for(let i=0;i < this.state.urls.length;i++)
    {
      var urlinfor = self.state.urls[i];

      str += urlinfor.url + ";" + urlinfor.description +";";
    }
 


     this.props.valueUpdate(this.props.field.id,str);
  }
    addurl()
    {
      if(this.state.newurl==="" || this.state.newurldescription==="")
      {
        return;        
      }


    	var newLink ={}; 
		newLink.url = this.state.newurl;
		newLink.description = this.state.newurldescription;
       var newurls = this.state.urls;
       newurls.push(newLink);

        this.setState({urls:newurls},this.updateFieldData);

       this.setState({editmode:false});
      this.setState({newurldescription:""});
      this.setState({newurl:""});

    }

    canceladdurl()
    {
      this.setState({editmode:false});
    }

    clickdelete(urlobj)
    {
        var newurls = [];
        for(var i = 0;i<this.state.urls.length;i++)
        {
          if(urlobj.url !== this.state.urls[i].url)
          {
            newurls.push(this.state.urls[i]);
          }
        }
      this.setState({urls:newurls},this.updateFieldData);      
    }

ShowEditmenu(props)
{
if(this.state.editmode)
{
return <table className="fieldtypeurlwidgettable">
  <tbody>
<tr>
	<td>URL</td><td><input value={this.state.newurl}  onChange={this.updateUrlInputValue} /></td>
	<td  rowSpan="2">
	<img className="addurlbutton" alt="URL anfügen"   title="URL anfügen" onClick={this.addurl} src="https://publicfile.standard-cloud.com/icons/ok.png"></img>
  <span> </span>
  <img className="addurlbutton" alt="URL doch nicht anfügen" title="URL doch nicht anfügen" onClick={this.canceladdurl} src="https://publicfile.standard-cloud.com/icons/cancel.png"></img>
	</td>
</tr>
<tr>
	<td >Text</td><td><input value={this.state.newurldescription} onChange={this.updateDescriptionInputValue} /></td>
</tr>
</tbody>
</table>
}else
{
  return null;
}

}
render () {
var selfrender = this;
		var  urls = this.state.urls.map(function(url) {	
            return (
    <tr key={url.url}>
        <td>
          <div><a href={url.url} title="Link öffnen" >{url.description} <img alt={url.description} src="https://publicfile.standard-cloud.com/icons/link.png"/>
		 
		</a> 
		
			<div className="deletebuttondivurlfieldwidget" onClick={() => selfrender.clickdelete(url)} >
			<img  title ="Link entfernen" alt="Link entfernen" src="https://publicfile.standard-cloud.com/icons/delete_micro.png"></img>
			</div>
      </div>
	</td>
    </tr> 		 
    );
  });
  
  let showeditmenu = this.ShowEditmenu();


	return (
<div className="fieldtypeurlcard" >
  <table className="fieldtypeurlwidgettableurllist">
<tbody>
    {urls} 
</tbody>
</table>
{showeditmenu}
<img onClick={this.addurlmode} className="addurlbuttonimg" alt ="Link hinzufügen" title ="Link hinzufügen" src="https://publicfile.standard-cloud.com/icons/addurl_micro.png"></img>
<br/>
</div>
    );
	
}	  
}

export default UrlFieldEditor;