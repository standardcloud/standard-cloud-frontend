import React from 'react';
import './urlfieldeditor.css';
class FieldTypeUrlRead extends React.Component {

render() {

  var  urls = this.props.value.map(function(url) {	
          return (
  <tr key={url.url}>
      <td>
        <div><a href={url.url} title="Link öffnen" >{url.description} <img alt="Link öffnen" src="https://publicfile.standard-cloud.com/icons/link.png"/>   
  </a> 
    </div>
</td>
  </tr> 		 
  );
});


return(<div className="fieldtypeurlread">
  <table className="fieldtypeurlwidgettableurllist">
<tbody>
    {urls} 
</tbody>
</table>
		</div>);
}	  
  }   
export default FieldTypeUrlRead;

