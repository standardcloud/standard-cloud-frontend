import React from 'react';
import BT from '../../button/stdbutton';
import Dropzone from "dropzone";
import $ from 'jquery';
import {BACKEND} from '../../../stdsetup';
import jcrop from 'jquery-jcrop'
import Dialog from '../../dialog/stddialog';
import 'jquery-jcrop/css/jquery.Jcrop.min.css';

class FieldTypeImageEdit extends React.Component {
	constructor(props) {
    super(props);
    this.state = {value:this.props.value,cutterenabled:false};
    this.uploadimage = this.uploadimage.bind(this);
    this.initUploader = this.initUploader.bind(this);
    this.deleteimage = this.deleteimage.bind(this);
    this.setcuts = this.setcuts.bind(this);
    this.enablecropimage = this.enablecropimage.bind(this);
    this.cropimage = this.cropimage.bind(this);

  }

  componentDidMount(){
    this.initUploader();
  }
  initUploader()
  {
   
    var self = this;

   var uploadurl = BACKEND + "/file-upload/" + this.props.field.id + "-" + this.props.objectid + "/";
   var myDropzone = new Dropzone(this.refs.uploadimagebox, { url: uploadurl,   headers: {
    // remove Cache-Control and X-Requested-With
    // to be sent along with the request
    'Cache-Control': null,
    'X-Requested-With': null
},  withCredentials: true, maxFilesize: 100, acceptedFiles:"image/*",paramName: "file[]",createImageThumbnails:false, maxFiles: 1, addRemoveLinks: true, previewsContainer: this.refs.imageuploadpreview ,parallelUploads: 1, uploadMultiple: false});
   
myDropzone.on("success", function(file) {

  var newstate = {};
  newstate.filepath = self.props.field.id + "-" + self.props.objectid + "/" + file.name + "/";
  newstate.picturename =  file.name;
  newstate.fileid = self.props.field.id + "-" + self.props.objectid;
  self.setState({value:newstate});
  self.props.valueUpdate(self.props.field.id,self.props.field.id + "-" + self.props.objectid);
});
myDropzone.on("error", function(error) {
  console.log("Error Image Upload");

});
myDropzone.on("maxfilesexceeded", function(file) {
  this.removeAllFiles();
  this.addFile(file);
});



  }
  
  deleteimage()
  {
    this.setState({value:{}});
    this.props.valueUpdate(this.props.field.id,"");
  }
  setcuts(c)
  {
       this.setState({imagecut:c});
  }


  enablecropimage()
  {
    switch(this.props.field.custom.aspectratio)
    {
      case "0":
      $(this.refs.cropimage).Jcrop(  {boxWidth: 450, boxHeight: 400,onSelect : this.setcuts},function(){
        window.jcrop_api = this;
      });   
      break;
      case "1":
      $(this.refs.cropimage).Jcrop(  {aspectRatio: 3 / 4, boxWidth: 450, boxHeight: 400,onSelect : this.setcuts},function(){
        window.jcrop_api = this;
      });   
      break;
      case "2":
      $(this.refs.cropimage).Jcrop(  {aspectRatio: 9 / 16, boxWidth: 450, boxHeight: 400,onSelect : this.setcuts},function(){
        window.jcrop_api = this;
      });   
      break;
      case "3":
      $(this.refs.cropimage).Jcrop(  {aspectRatio: this.props.field.custom.formatx / this.props.field.custom.formaty , boxWidth: 450, boxHeight: 400,onSelect : this.setcuts},function(){
        window.jcrop_api = this;
      });   
      break;
      default:

    }


   // var jcrop_api;
   // var imagecropstr = "/kopflos/public/orginal_" + self.state.user.id + ".jpg?" + (new Date().toLocaleString()).replace(",","").replace(" ","");  
    

     //jcrop_api.setImage(imagecropstr); 
     this.setState({cutterenabled:true});
  }



  cropimage(callback)
  {
    var selfcrop = this;

		     var jsondata = {};
			       jsondata.x = this.state.imagecut.x;
             jsondata.y = this.state.imagecut.y;
             jsondata.h = this.state.imagecut.h;
             jsondata.w = this.state.imagecut.w;
             jsondata.filename = this.state.value.picturename
             jsondata.fileid = this.state.value.fileid
		   $.ajax({
				type: "POST",
				xhrFields: {
          withCredentials: true
        },
			  url:  `${BACKEND}/imagecrop/`,
			  data: jsondata,
			  success: function( data, textStatus, jqXHR) {               
         if(data.state==="ok")
         {
          Dialog.ok("Bild","zugeschnitten");
          window.jcrop_api.destroy();
          selfcrop.refs.cropimage.src = selfcrop.refs.cropimage.src+"?time="+new Date().getTime();
          selfcrop.setState({cutterenabled:false});
         }else
         {
          Dialog.error("Bild",data.content.msg);
          window.jcrop_api.destroy();
         }
		  },  
			error: function(jqXHR, textStatus, errorThrown){	
                      console.log("fehler beim update..");
		  }
		}); 		
		
  }

  uploadimage()
  {
    this.refs.uploadimagebox.click();
  }

render() {

var value = this.state.value;
var deletebutton = null;
var cropimagebutton = null;
var cutbutton = null;


var noimage = (value ===null || value.length===0) ? true :false;
if(this.props.field.custom.cropimage && noimage===false && this.state.cutterenabled===false)
{
  cropimagebutton = <BT iconurl="https://publicfile.standard-cloud.com/icons/editimage.png" callbackfunction={this.enablecropimage} name="Bild zuschneiden" />  
}


if(this.state.cutterenabled)
{
  cutbutton = <BT iconurl="https://publicfile.standard-cloud.com/icons/cut.png" callbackfunction={this.cropimage} name="Zuschneiden" />
}

if(!value)
{
  value = {};
}else
{
  deletebutton = <BT iconurl="https://publicfile.standard-cloud.com/icons/delete.png" callbackfunction={this.deleteimage} name="Löschen" />  
}


  var img = null;

if(!noimage)
{
  var imgurl = BACKEND  + "/file/" + this.state.value.filepath + "?time="+new Date().getTime();;
  img = <img  ref='cropimage' alt={this.state.value.picturename} src={imgurl} />
}
return(<div className="fieldtypeboximageread">
{img}
<div className="uploadimageboxbuttons">
{deletebutton}
{cropimagebutton}
{cutbutton}
</div>
<div ref='uploadimagebox' className="uploadimagebox" >
<BT name="Upload" callbackfunction={this.uploadimage} iconurl="https://publicfile.standard-cloud.com/icons/attachfile.png" />
</div>
<div ref='imageuploadpreview'>
</div>
		</div>);
}	  
  }   
export default FieldTypeImageEdit;

