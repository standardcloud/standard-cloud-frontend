import React from 'react';

class FieldTypePicture extends React.Component {
	    constructor(props) {
		super(props);
		this.handleAllowCrop = this.handleAllowCrop.bind(this);
		this.handleradiochange = this.handleradiochange.bind(this);
		this.changeformatx = this.changeformatx.bind(this);
		this.changeformaty = this.changeformaty.bind(this);
		this.state = {picturefieldsettings:{cropimage:false,aspectratio:"0",formatx:"",formaty:""}};		
  }			  
componentDidMount() 
{
	if(this.props.setcustomsettings  !== undefined)
	{	
this.setState({picturefieldsettings:this.props.setcustomsettings});
this.props.getcustomsettings(this.props.setcustomsettings);
	}
}
getcustomsettings()
{
	   this.props.getcustomsettings(this.state.picturefieldsettings);
}
handleAllowCrop(status)
{
var od = this.state.picturefieldsettings;
od.cropimage = status;
this.setState({picturefieldsettings:od});
this.getcustomsettings();
}
handleradiochange(value)
{
	var od = this.state.picturefieldsettings;
	od.aspectratio = value;
	this.setState({picturefieldsettings:od});
	this.getcustomsettings();
}
changeformaty(e)
{
	var od = this.state.picturefieldsettings;
	od.formaty = e.target.value;
	this.setState({picturefieldsettings:od});
	this.getcustomsettings();
}
changeformatx(e)
{
	var od = this.state.picturefieldsettings;
	od.formatx = e.target.value;
	this.setState({picturefieldsettings:od});
	this.getcustomsettings();
}

render() {
var selfrender = this;
var customformat = null;
if(this.state.picturefieldsettings.cropimage)
	{
		var customratio = null;
		if(selfrender.state.picturefieldsettings.aspectratio==="3")
			{
				customratio = <div>Format: x:<input value={this.state.picturefieldsettings.formatx} onChange={this.changeformatx} type="text" name="formatx"/>y: <input type="text" onChange={this.changeformaty} value={this.state.picturefieldsettings.formaty} name="formaty"/></div>
			}


		customformat = <tr><td> Bildverh&auml;ltnis</td><td>		
	<div className="stdradioboxregion">
	<input className="radio" id="allowcropno0" checked={this.state.picturefieldsettings.aspectratio==="0"}  type="radio" onChange={()=>selfrender.handleradiochange("0")}   name="aspectratio" value="0"/>
	<label htmlFor="allowcropno0">frei</label>
	</div>	
	<div className="stdradioboxregion">
	<input className="radio" id="aspectratio1" checked={this.state.picturefieldsettings.aspectratio==="1"}  type="radio" onChange={()=>selfrender.handleradiochange("1")}   name="aspectratio" value="1"/>
	<label htmlFor="aspectratio1">fix 4:3</label>
	</div>	
	<div className="stdradioboxregion"><input className="radio" checked={this.state.picturefieldsettings.aspectratio==="2"}  onChange={()=>selfrender.handleradiochange("2")}  id="aspectratio2"  type="radio"  name="aspectratio" value="2"/>
	<label htmlFor="aspectratio2">fix 16:9</label>
	</div>	
	<div className="stdradioboxregion"><input className="radio" id="aspectratio3" checked={this.state.picturefieldsettings.aspectratio==="3"}  onChange={()=>selfrender.handleradiochange("3")}   type="radio"  name="aspectratio" value="3"/>
	<label htmlFor="aspectratio3">Eigenes Format definieren</label>
	</div>	
	{customratio}
	</td></tr>
	}
	
		return(<div id="fieldtypepicturesettingsbox">
<table>
	<tbody>
<tr><td className="tdpictureratio">Zuschneiden erlauben</td><td>
<div className="stdradioboxregion"><input className="radio" checked={this.state.picturefieldsettings.cropimage}  onChange={()=>selfrender.handleAllowCrop(true)}  id="allowcropyes"  type="radio"  name="allowcrop" value="1"/>
<label htmlFor="allowcropyes">JA</label>
</div>
<div className="stdradioboxregion"><input className="radio" checked={this.state.picturefieldsettings.cropimage===false}   onChange={()=>selfrender.handleAllowCrop(false)} id="allowcropno"  type="radio"  name="allowcrop" value="2"/>
<label htmlFor="allowcropno">NEIN</label>
</div>
</td></tr>
{customformat}
</tbody>
</table>
		</div>);
}	  
  }   
export default FieldTypePicture;