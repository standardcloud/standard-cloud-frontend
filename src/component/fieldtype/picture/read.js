import React from 'react';
import {BACKEND} from '../../../stdsetup';

class FieldTypeImageRead extends React.Component {

render() {

var imgsrc = BACKEND + "/file/" + this.props.value.filepath;
return(<div className="fieldtypeboximageread">
<img alt={this.props.value.picturename} src={imgsrc} />
		</div>);
}	  
  }   
export default FieldTypeImageRead;

