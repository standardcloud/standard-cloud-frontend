import React from 'react';
import dialog from '../../dialog/stddialog';

class FieldTypeKey extends React.Component {
	    constructor(props) {
		super(props);
		this.handlechangekey= this.handlechangeshandlechangekeywitch.bind(this);
		this.getcustomsettings = this.getcustomsettings.bind(this);
		this.state = {keyfieldsettings:{multipleclaims:false}};		
  }			  
componentDidMount() 
{
	if(this.props.setcustomsettings  !== undefined)
		{	
	this.setState({keyfieldsettings:this.props.setcustomsettings});
	this.props.getcustomsettings(this.props.setcustomsettings);
		}
}
getcustomsettings()
{
	   this.props.getcustomsettings(this.state.keyfieldsettings);
}


handlechangekey()
{
	var oldstate = this.state.keyfieldsettings;
	oldstate.multipleclaims = !oldstate.multipleclaims;
	this.setState({keyfieldsettings:oldstate});
	this.getcustomsettings();
}



render() {
	var selfrender = this;
		return(<div id="fieldtypetextsettingsbox">
		        <div className="stdcheckboxregion">
				<input id="enablemultipleusers"  checked={this.state.keyfieldsettings.multipleclaims} type="checkbox" value="enablemultipleusers"   className="checkbox"/>
				<label onClick={this.handlechangekey}  for="enablemultipleusers">Mehrere Schlüssel dürfen im Feld enthalten sein</label>
			</div>

		</div>);
}	  
  }   
export default FieldTypeKey;

