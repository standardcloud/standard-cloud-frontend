import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom';
import Objecturlmanager  from '../../module/objecturlmanager/stdobjecturlmanager';
import Displayobject from '../../module/displayobject/stddisplayobject';
import Objectmanager from '../../module/objectmanager/stdobjectmanager';
import {loadTimeline} from '../../actions/timeline';
import BigCalendar from 'react-big-calendar';
import '../../../node_modules/react-big-calendar/lib/css/react-big-calendar.css'
import './timeline.css';

import moment from 'moment';
moment.locale('de');
// Setup the localizer by providing the moment (or globalize) Object
// to the correct localizer.
BigCalendar.momentLocalizer(moment); // or globalizeLocalizer

class Timeline extends React.Component {      
constructor(props) {
    super(props);
    this.state = {searchvalue:"" };
  }
    
			  componentDidMount() {
                  this.props.loadTimeline();
				}

openobject(timeobject)
{  
    Objecturlmanager.geturl(timeobject.listid,timeobject.objectid,function(url)
			{
			window.location.href=url;
		    });	  
}

handleHistorySearchTextChange(event) {
    this.setState({searchvalue: event.target.value.toLowerCase()});            
}

showdetails(resultobject)
{
    Objectmanager.getobject(resultobject.listid,resultobject.objectid).then(data => {this.updatecontent(data)});
}

updatecontent(resultobject) {	
 var newtimelinelist =[];
 for(let i=0;i<this.state.timeline.length;i++)
 {
     var co = this.state.timeline[i];
     if(co.objectid === resultobject.objectid)
     {        
        co.content=Displayobject.getcontent(resultobject);
     }
     newtimelinelist.push(co);
 }
	this.setState({timeline:newtimelinelist});	
    }
    
render() {
    if(!this.props.timeline)
        return(<div>Loading..</div>);

console.log(this.props.timeline);
/*
        {
            id: 0,
            title: 'All Day Event very long title',
            allDay: true,
            start: new Date(2015, 3, 0),
            end: new Date(2015, 3, 1),
          },
      */
 



	return (
        <div>
            <input id="timelinesearchbox" placeholder="Filter" type="search"  autoComplete="off"/><br/>
            <div>
            <BigCalendar
        events={[]}
        startAccessor='startDate'
        endAccessor='endDate'
      />   
            </div>
            
      </div>
    );
    }	  
    }

  function mapStateToProps(state) {
    return { timeline: state.timeline};
  }
  export default withRouter(connect(mapStateToProps,{loadTimeline})(Timeline));


