import $ from 'jquery';
import Dialog from '../dialog/stddialog';
import ObjectManager from '../../module/objectmanager/stdobjectmanager';
import DisplayObject from '../../module/displayobject/stddisplayobject';
import Opentip from 'opentip2';

class ObjectPreview
{

static setToolTip()
{
var self = this;
	setTimeout(function(){
			
	$(".streamobjectlink").mouseover(
	function() {
	try{
	var myOpentip = new Opentip($(this),{"escapeContent":false,"background":"White","shadow":false,"borderColor":"black","borderRadius":0});
	var start = $(this).data('objecturl').indexOf("/list/") +6;
	var listid = $(this).data('objecturl').substring(start,start+36);
	start = $(this).data('objecturl').indexOf("/edit/") +6;
	var objectid = $(this).data('objecturl').substring(start,start+36);
	self.getObjectData(listid,objectid,myOpentip);
	setTimeout(function(){ myOpentip.hide(); }, 4000);
	}catch(err)
	{
		
	}
}).mouseout(function() {
for(let i = 0; i < Opentip.tips.length; i ++) { Opentip.tips[i].hide(); }  
    
  });
	}, 2000);
}

static getObjectData(listid,objectid,myOpentip)
{
	ObjectManager.getobject(listid,objectid).then(data=>
	{		
		if(data)
		{
			  for(let i = 0; i < Opentip.tips.length; i ++) { Opentip.tips[i].hide(); }
				myOpentip.setContent(DisplayObject.getcontent(data));
				myOpentip.show();  
		}else
		{
			for(let x = 0; x < Opentip.tips.length; x ++) { Opentip.tips[x].hide(); } 			  
			Dialog.errordialog('Fehler','Objekt nicht vorhanden.');
			
		}
		
	});

	
}

}
export default ObjectPreview;
