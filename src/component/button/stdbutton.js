import React from 'react';
import Dialog from '../dialog/stddialog';
import { withRouter } from 'react-router-dom';
import './stdbutton.css';


	class StdButton  extends React.Component {    
		constructor(props) {
			super(props);
			this.state = {name:"",count:"",iconurl:"",showbutton:true,url:"",cssclass:"stdtile icon"};
			this.handleButtonClick = this.handleButtonClick.bind(this);
		}

			  componentDidMount() {
				this.setState({iconurl:this.props.iconurl});	

			
					if(typeof this.props.cssclass !== "undefined")
					{
						this.setState({cssclass:this.props.cssclass});
				  }

				}

				handleButtonClick(data)
				{
					data.stopPropagation();
				
					if(this.props.disabled)
					{
						Dialog.errordialog("Schaltfläche","ist deaktiviert");
						return;
					}				
							if(typeof this.props.url !== "undefined" && this.props.url != null )
					{
						if(this.props.reload)
						{
							
							window.location.href=this.props.url;
						}else
						{
						this.props.history.push(this.props.url);
						}
											
					}
					
					if(typeof this.props.callbackfunction === "undefined" )
					{
						
					}else
					{
						this.props.callbackfunction(data);
					}
				}


			  render() {
					var css = this.state.cssclass;
					if(this.props.disabled)
					{
						css += " stdbuttondisabled";
					}


			  if(this.state.showbutton)
			  {
				return (
				<div tabIndex="1200" onClick={this.handleButtonClick}  className={css}>
				<b className="check"></b>
				<div className="tile-content">
					 <img alt={this.props.name} src={this.state.iconurl} />
				</div>
				<div className="brand">    
					 <span className="name" >{this.props.name}</span>
					 <span className="badge" >{this.props.count}</span>
				</div>
		</div>
				);
			  }
			  }
}

export default withRouter(StdButton);
