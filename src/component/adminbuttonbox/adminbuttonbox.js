import React,{useEffect,useState} from 'react';
import StdButton from '../../component/button/stdbutton';
import Administration from '../../module/administration/stdadministration';
import Trans from '../../module/translatoinmanager/translationmanager';

function AdminButtonBox(){

	const [objecttypecount,setObjecttypecount] = useState("");
	const [fieldcount,setFieldcount] = useState("");
	const [usercount,setUsercount] = useState("");
	const [trashcount,setTrashcount] = useState("");

	useEffect(()=> {
function loadoverviewsettings()
{
		Administration.getadminoverview().then(data=>
	{
		if(data.state==="ok")
		{
		setObjecttypecount(data.content.objecttypecount);
		setFieldcount(data.content.fieldcount);
		setUsercount(data.content.usercount);
		setTrashcount(data.content.trashcount);
		}
	});
}
loadoverviewsettings();
},[]);


		return (
	<div>
		<StdButton count="1" url="/admin/list/new/"  name={Trans.t("newlist")} iconurl="https://publicfile.standard-cloud.com/icons/addlist.png" />
        <StdButton  url="/admin/objecttype/" count={objecttypecount} name={Trans.t("listtemplates")} iconurl="https://publicfile.standard-cloud.com/icons/objecttype.png" />
		<StdButton  url="/admin/field/" count={fieldcount}  name={Trans.t("fields")} iconurl="https://publicfile.standard-cloud.com/icons/addcolumn.png" />
		<StdButton  url="/ifollow/" name={Trans.t("subscriptions")} iconurl="https://publicfile.standard-cloud.com/icons/follow.png" />
		<StdButton  url="/admin/usermanagement/" count={usercount}  name={Trans.t("users")} iconurl="https://publicfile.standard-cloud.com/icons/adduser.png" />
	    <StdButton  url="/admin/permissionmgmt/" name={Trans.t("accessmanagement")} iconurl="https://publicfile.standard-cloud.com/icons/claim.png" />	
	    <StdButton  url="/admin/trash/" count={trashcount}  name={Trans.t("trash")} iconurl="https://publicfile.standard-cloud.com/icons/trash.png" />
	    <StdButton  url="/admin/design/"  name={Trans.t("customisation")} iconurl="https://publicfile.standard-cloud.com/icons/design.png" />	
		<StdButton  url="/admin/corpsettings/"  name={Trans.t("companyinformation")} iconurl="https://publicfile.standard-cloud.com/icons/organization.png" />
		<StdButton  url="/admin/payment/"  name={Trans.t("paymentinfo")} iconurl="https://publicfile.standard-cloud.com/icons/payment.png" />	
		<StdButton  url="/admin/log/" name={Trans.t("logs")} iconurl="https://publicfile.standard-cloud.com/icons/logger.png" />	
		<StdButton  url="/admin/search/" name={Trans.t("search")} iconurl="https://publicfile.standard-cloud.com/icons/searchsettings.png" />	
		<StdButton reload  url="https://standard-cloud.zendesk.com/hc/de" name={Trans.t("help")} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />	
	</div>
        );
		}

  

export default AdminButtonBox;	