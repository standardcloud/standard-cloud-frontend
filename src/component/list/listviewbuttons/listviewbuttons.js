import React,{memo} from 'react';
import Trans from '../../../module/translatoinmanager/translationmanager';
import BT from '../../button/stdbutton';


function Listviewputtons({list,objecttype,selectedobjects,followcount,viewmode,boxviewClick,followbuttonClick,exportexcelbuttonClick})
{

    const listsettingsurl = "/list/" +list.id + "/edit";
    const addobjecttext = Trans.addobject(objecttype.name);
    function getNewObjectUrl()
    {
        return (list.defaultviewnew !=="") ? '/list/' + list.id + '/newobject/objectview/' +list.newviewid + '/newmode/' :  '/list/' + list.id + '/newobject/'
    }
    function getViewButtion()
    {
        return (viewmode === 0) ? <BT callbackfunction={boxviewClick}    name="Box-Ansicht" iconurl="https://publicfile.standard-cloud.com/icons/boxview.png" />   :<BT callbackfunction={boxviewClick}    name="Listenansicht" iconurl="https://publicfile.standard-cloud.com/icons/list.png" />;
    }

    function getEditMultipleValuesButton()
    {
        if(selectedobjects.length > 1)
        {
        const editmultipleobjecttext = Trans.editmultipleobject(objecttype.nameplural); 
        return <div id="editmultiple"><BT callbackfunction={this.editmultipleObjectsClick}  name={editmultipleobjecttext}  count={selectedobjects.length} iconurl="https://publicfile.standard-cloud.com/icons/edit.png" /></div> 	
        }
    }

function getSortButtion()
{
if(list.enablemanualsort && (list.sortfield.id==="7628c3c9-6e30-4b1a-8f74-cc8ae8366d0c") && viewmode===0 )
{
    return <BT callbackfunction={this.sortbuttonClick}    name="Zeilen sortieren" iconurl="https://publicfile.standard-cloud.com/icons/sort.png" />  
}
}

function getQueryButton()
{
    if(list.querycount !=="0")
    {
        const queryurl = "/listfilter/" + list.id + "/";
    return <div id="queries">
    <BT  url={queryurl}  count={list.querycount} name={Trans.t("queries")} iconurl="https://publicfile.standard-cloud.com/icons/filter.png" />  
    </div>
    }
}


    return(
              <div>
              <div id="newobjectbutton">
              		<BT url={getNewObjectUrl()} name={addobjecttext} count="1" iconurl="https://publicfile.standard-cloud.com/icons/addobject.png" />          
              </div>		
              {getEditMultipleValuesButton()}
               <div  id="editstreamnotifications">
               <BT callbackfunction={followbuttonClick}  count={followcount} name={Trans.t("follow")} iconurl="https://publicfile.standard-cloud.com/icons/follow.png" />     
               </div>
               <BT callbackfunction={exportexcelbuttonClick}   count="1" name="Excel-Export" iconurl="https://publicfile.standard-cloud.com/icons/excel.png" />   
               {getViewButtion()}
               {getSortButtion()}
               {getQueryButton()}
              <BT name={Trans.t("settings")} url={listsettingsurl}  count="11" iconurl="https://publicfile.standard-cloud.com/icons/admin.png" />           
              </div>   
              );
 }

 export default memo(Listviewputtons);