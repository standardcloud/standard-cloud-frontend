import React,{memo} from 'react';
import styles from './listinfobox.module.css';
function Listinfobox({list})
{
    return(
    <div id={styles.listinfobox}>
    <div id={styles.listinfoboxheader}><h1 dangerouslySetInnerHTML={{__html: list.name}}></h1></div>
    <div id={styles.listinfoboxdescription}><h4 dangerouslySetInnerHTML={{__html:list.description}}></h4></div>
    </div>);
}
export default memo(Listinfobox)