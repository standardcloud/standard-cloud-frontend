import $ from 'jquery';
import dialog from '../dialog/stddialog';
class Sortcolumnmanager
{

	static clientsortcolumn(column,fields, objectresult,order)
		{
                          var count = 0;
                /*Cont auszublendende Spalte ermitteln */
                for(let i=0;fields.length >= i;i++)
                {
                    count++;
                    if(column.id === fields[i].id)
                    {       
                        count--;                  
                        break;
                    }
                }

                       var self = this;
                
              /* alert(JSON.stringify(column)); name und id */
			                 try {
								 
								 if(column.fieldtypeid ==="")
									 return;
								 

                    switch (column.fieldtypeid) {
	                     case "39f6c3e6-d446-4e53-9706-67f556b06bdb":
					//	 log.info("Number sort");
                            self.numberSort(column,count,objectresult,order);
                            break;
					case "278bd56c-e463-4ec4-a702-82a9ecd59137":
				//	log.info("Number sort");
                            self.numberSort(column,count,objectresult,order);
                            break;
                        case "17a8dd83-0068-4e4a-bfcc-082f7f64e6f8":
					//	log.info("Date sort");
                            self.dateSort(column,count,objectresult,order);
                            break;
				  case "971c3761-5565-4a65-b546-7885b8bef160":
				  	//	log.info("Selector-> ObjectinList Textsort");
							self.stringSort(true,column,count,objectresult,order);
							break;

				  case "8FE72E9F-09CC-4225-9135-58E042DC98A8":
						//log.info("Selector-> Text sort");
							self.stringSort(false,column,count,objectresult,order);
							break;
				case "52a3bb09-fc62-4099-a7ff-c314074f274a":
					//	log.info("URL-> Text sort");
							self.stringSort(true,column,count,objectresult,order);
							break;
					  case "cc333aeb-196d-4331-b1df-3b530fa67722":
					//	log.info("Person-> Text sort");
							self.stringSort(true,column,count,objectresult,order);
							break;
               case "59d19ffb-7090-46ca-9021-b79c697142d1":
				//		log.info("Document -> Text sort");
							self.stringSort(true,column,count,objectresult,order);
							break;							
                        case "32b60712-1f4d-4a49-87a6-90de0363eef4":
				//		log.info("Text sort");
							self.stringSort(false,column,count,objectresult,order);
                            break;
                            default:
                    }
                }
                catch (err) {
                    // Always remember to handle those errors that could occur during a user interaction
                    dialog.errordialog("Fehler",err);
                }


		}

                   // Generic sort method for numbers and strings
        static stringSort(htmlcontent,column,count,objectresult,order) { // Pass in the column object
                //var sortresult = 
                objectresult.sort(function (a, b) {  
                    
                    var playerA ="";
                    var playerB="";
                    if(htmlcontent)
                    {
                       playerA = $(a[count]).text();
                       playerB = $(b[count]).text();   
                    }else
                    {
                        playerA = a[count];
                        playerB = b[count];   
                    }

                    playerA = playerA.toUpperCase();
                    playerB = playerB.toLocaleUpperCase();

				   if (playerA < playerB) {						
                        return (order === "asc") ? -1 : 1;
                        
                    }
                    else if (playerA> playerB) {                  
						return (order === "asc") ? 1 : -1;
                        
                    }
                    else {
                        return 0
                    }
                });
            }

          static numberSort(column,count,objectresult,order) {
              
                //var sortresult = 
                objectresult.sort(function (a, b) {
					
                    var playerA = a[count], playerB = b[count];
                    if (order === "asc") {
                        return playerA - playerB;
                    }
                    else {
                        return playerB - playerA;
                    }
                });

              //  Dispatcher.action("result.update",sortresult);
            }

			            // Sort by date
            static dateSort(column,count,objectresult,order) 
            {
            var thissort = this;
      
              objectresult.sort(function (a, b) {	
                               
                                
                    if (order === "asc") {
                               return new Date(thissort.parsedate(a[count])) - new Date(thissort.parsedate(b[count]));
                    }
                    else {
                        
                        return new Date(thissort.parsedate(b[count])) - new Date(thissort.parsedate(a[count]));
                    }
                });

                // Dispatcher.action("result.update",sortresult);
            }

          static parsedate(input)
           {
            
	    	try {
				  var parts = input.match(/(\d+)/g);
				  // note parts[1]-1
				 var returndate = new Date(parts[2], parts[1]-1, parts[0]);
				 if(returndate != null)
				 {
					 return returndate;	 
				 }else
				 {
					 return new Date();	 
				 }
                }
                catch(err) {
                    return new Date();	
                }
            }


}
export default Sortcolumnmanager;

