import React from 'react';
import {BACKEND} from '../../stdsetup';
import $ from 'jquery';
import Autosuggest from 'react-autosuggest';
import './listselector.css';

  class Listelector extends React.Component {
	  	    constructor(props) {
    super(props);
	this.state = {lists:[],suggestions:[],value:"",listid:"",listname:""};
	this.handleConnectSearchTextChange = this.handleConnectSearchTextChange.bind(this);
	this.showAllLists = this.showAllLists.bind(this);
	this.getSuggestionValue = this.getSuggestionValue.bind(this);
	this.loadlists = this.loadlists.bind(this);
	this.onChange = this.onChange.bind(this);
	this.onSuggestionSelected = this.onSuggestionSelected.bind(this);
	this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(this);
	this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(this);
	this.loadlists();
  }


  handleConnectSearchTextChange(event) {
		    this.setState({listname: event.target.value});            
}

getSuggestions(value)
{
	const inputValue = value.trim().toLowerCase();
	const inputLength = inputValue.length;
  
	return inputLength === 0 ? [] : this.state.lists.filter(lang =>
	  lang.name.toLowerCase().slice(0, inputLength) === inputValue
	);
}


loadlists()
{
	var self = this;
	$.ajax({
		url: BACKEND + "/list/json",
		dataType: "json",
		xhrFields: {
			withCredentials: true
		 },
		success: function( data, textStatus, jqXHR) {	
		   self.setState({lists:data});
		},
		error: function(jqXHR, textStatus, errorThrown){
		}
	});
}

getSuggestionValue(suggestion) {
	return suggestion.name;
  }
renderSuggestion(suggestion) {
	return (
	  <div className="selectoritembox"><strong>{suggestion.name}</strong><br/>{suggestion.description}</div>
	);
  }
showAllLists()
{

}
  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested()
  {
    this.setState({
      suggestions: []
    });
  }

onSuggestionsFetchRequested({ value })
{
	var self = this;
    this.setState({
      suggestions: self.getSuggestions(value)
    });
}
onSuggestionSelected(event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method })
{
	this.props.listupdate(suggestion);
}
shouldRenderSuggestions(value) {
	return true;
  /*return value.trim().length > 0; */
}

onChange = (event,{newValue}) => {
    this.setState({
      value: newValue
		});
  };


		  render() {
			var value = this.state.value;
				const inputProps = {
				  placeholder: '',
				  value,
				  onChange: this.onChange
				};
					return (  
						<div className="objecttypeselectorbox">
							<div>
								   <Autosuggest
									suggestions={this.state.suggestions}
									getSuggestionValue={this.getSuggestionValue}
									renderSuggestion={this.renderSuggestion}
			   					shouldRenderSuggestions = {this.shouldRenderSuggestions}
									onSuggestionSelected ={this.onSuggestionSelected}
									onSuggestionsFetchRequested = {this.onSuggestionsFetchRequested}
									onSuggestionsClearRequested={this.onSuggestionsClearRequested}
									inputProps={inputProps}
								/>												
								
		   	</div></div>);
						
					

    }
}
	
export default Listelector;
