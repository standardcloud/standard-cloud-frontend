
import React from 'react';
import RenderFilter from '../fieldtype/renderfilter';

class fieldfilter extends React.Component {
    
      constructor(props) {
            super(props);
            this.myDialogbox = React.createRef();
            this.myDialogboxImage = React.createRef();
            this.hidefilter = this.hidefilter.bind(this);
            this.state ={hidden:false};
      }



      hidefilter()
      {
        this.setState({hidden:true});
      }



render() {
    var filedfilter = null;

    
    filedfilter = <RenderFilter  field={this.props.field} filtercallback={this.props.filtercallback} index={(this.props.fieldindex+1)}  objectresult={this.props.objectresult}/>
    var filterdialogselector = "filterdialog" +  this.props.field.id;
    var cssclasses = "filterdialog"
    var iconname =  this.props.field.name + " Filter"
    if(this.state.hidden)
    {
        cssclasses += " fieldfilter-hide";
    }
    return (          
        <div onClick={(e)=>{	e.stopPropagation();}}  ref={this.myDialogbox} id={filterdialogselector} className={cssclasses}>
        <div title={iconname} className="filterimage-box"> <img alt="" onClick={()=>{this.setState({hidden:false})}} className="filterimage-img" ref={this.myDialogboxImage} src="https://publicfile.standard-cloud.com/icons/filter_listview_big.png"/></div><div className="filterheader">{this.props.field.name}</div>
        <div className="fieldfilter-dialogbox-hide" onClick={this.hidefilter}> - </div>
        <div className="fieldfilter-dialogbox-close" onClick={this.props.closedialog}> X </div>
        <div className="fieldfilter-body-div">{filedfilter}</div>
        </div>
        );
}
}
export default fieldfilter;