import React from 'react';
import ObjectUrlManager from '../../module/objecturlmanager/stdobjecturlmanager';
import Listmanager from '../../module/listmanager/stdlistmanager';
import DisplayObject from '../../module/displayobject/stddisplayobject';
import Objectmanager from '../../module/objectmanager/stdobjectmanager';
import BT from '../button/stdbutton';
import Opentip from 'opentip2';
import dialog from '../dialog/stddialog';
import sortColumn from './sortcolumn';
import { withRouter } from 'react-router-dom';
import {BACKEND} from '../../stdsetup';
import {loadMore,filterbyServer} from '../../actions/objects';
import {connect} from 'react-redux';
import './listview.css';
import StdCheckbox from '../checkbox/checkbox';
import DialogBox from '../../component/dialogbox/dialogbox';
import Button from 'muicss/lib/react/button';
import FieldFilter from './fieldfilter';
   
import './fieldfilter.css';

class ListView extends React.Component {
    constructor(props) {
        super(props);
        this.deleted = false;
        this.isfullscreen = false;
        this.dofullscreen = this.dofullscreen.bind(this);
        this.handleDelObject = this.handleDelObject.bind(this);
        this.handleEditObject = this.handleEditObject.bind(this);
        this.handleFilterClick = this.handleFilterClick.bind(this);
        this.handleSearchTextChange = this.handleSearchTextChange.bind(this);
        this.filtercallback = this.filtercallback.bind(this);
        this.handleSortClick = this.handleSortClick.bind(this);
        this.createfilterlist = this.createfilterlist.bind(this);
        this.closefilterdialog = this.closefilterdialog.bind(this);
        this.handleHideClick = this.handleHideClick.bind(this);
        this.infinitescroll = this.infinitescroll.bind(this);
        this.getconnectedlistobject = this.getconnectedlistobject.bind(this);
        console.log(this.props.meta);
        this.state = {visiblecount:500,invisiblefields:[],filters:{},filterobjectids:[],filterfields:[],deleteobjectid:"",searchtext:"",serversearchtext:"",searchvalue:this.props.meta.filter,sort:{fieldid:"",order:""},selectedobjects:[]};


      }
  componentDidMount() {      
   
                 var element = document.getElementById("listview-box-div");
                 var searchbox = document.getElementById("listview-searchbox-div");
                  var self = this;
                  document.addEventListener("fullscreenchange", function(){ 
                    var isFullScreen = document.fullScreen || 
                    document.mozFullScreen || 
                    document.webkitIsFullScreen;
                    if(!isFullScreen)
                    {
                        element.style.overflowY = "visible";
                        self.isfullscreen = false;     
                        searchbox.style.top ="50px";
                        const tableheaders = document.querySelectorAll('#fixtableheader th');
                        tableheaders.forEach(t => {
                             t.style.top ="80px";
                          });
                               
                    }else
                    {
                    searchbox.style.top ="0px";
                    const tableheaders = document.querySelectorAll('#fixtableheader th');
                    tableheaders.forEach(t => {
                         t.style.top ="35px";
                      });
                    }
                });
                this.infinitescroll();
            }


infinitescroll()
{  

    // daten neu laden
 
    var self = this;  
    const lazyloadrows = document.getElementById('div-bottom-visible');
    const config = {
  rootMargin: '5000px 0px'
    };

let observer = new IntersectionObserver(
    function(entry)
    {
             if (entry[0].intersectionRatio > 0) {
                 var currentvisiblecount = self.state.visiblecount +250
                self.setState({visiblecount:currentvisiblecount});
                console.log("more view")
             if(self.props.objectresult.length < self.props.meta.totalobjects)
             {
                if(self.props.meta.rendertype==="server" && self.props.loading===false)
                {
                    console.log("load more");
                    self.props.loadMore(self.props.meta);             
                }
            }
           if(self.props.meta.totalobjects <= self.state.visiblecount)
           {     
               console.log("stop entry");
               stopObserve(entry);
           }
      }   
    }, config);
observer.observe(lazyloadrows);

function stopObserve(entry)
{
    observer.unobserve(entry[0].target);
}

}


handleFilterClick = function(field,e)
{
    var self = this;
    e.stopPropagation();
   // var bb = e.target.getBoundingClientRect();
  //  field.x = bb.x;
   // field.y = bb.y;
   var newfieldfilterlist = [];
   for(let i=0;self.state.filterfields.length > i; i++)
   {
       var ob = self.state.filterfields[i];
       if(ob.id!==field.id)
       {
        newfieldfilterlist.push(ob);
       }
           
   }
   newfieldfilterlist.push(field);
    this.setState({filterfields:newfieldfilterlist});
}

stopTDPropagation(e) {
    e.stopPropagation();
  }
 

  filtercallback(fieldid,filterids,filtervalue)
  {
    var newfilters = Object.assign({}, this.state.filters);
    newfilters[fieldid] = filterids;
    this.setState({filters:newfilters},this.createfilterlist);
    var newfilterfields =[];
    for(let i=0;i< this.state.filterfields.length;i++)
    {
        var newvalue = this.state.filterfields[i];
        if(this.state.filterfields[i].id === fieldid)
        {            
            newvalue.value = filtervalue;
        }
        newfilterfields.push(newvalue);
    }

        this.setState({filterfields:newfilterfields});

  }

  createfilterlist()
  {
      var self = this;
    var  newfilterlist = [];
    for(var fieldidobject in this.state.filters) {
        newfilterlist.push(...self.state.filters[fieldidobject]);
     }
    this.setState({filterobjectids:newfilterlist});
  }


handleSearchTextChange(event) {
    this.setState({searchvalue: event.target.value});   
    if(this.props.meta.rendertype==="server")
                  {                      
                     this.setState({serversearchtext: event.target.value}); 
                     console.log(event.target.value);
                     this.props.filterbyServer(this.props.meta.listid,event.target.value);
                  }else
                  {
                 this.setState({searchtext: event.target.value});   
                 } 
                
                              
                }
                
handleDelObject(objectid,listid) 
            {
           var selfdelete = this;
                        Listmanager.deleteobject(objectid,listid,function(data)
                        {
                                if(data.state==="ok")
                                {
                                    dialog.okdialog("OK","Objekt wurde gelöst");
                                    selfdelete.setState({deleteobjectid:""});
                                }else
                                {
                                    dialog.errordialog("Error","Objekt konnte nicht gelöst werden");
                                }
                        });
                
            }

               handleDeleteObject(objectid,e)
              {
                     /* fck event bubbling */
                 e.stopPropagation();
                 var selfdel = this;
		
                        if(selfdel.props.list.confirmobjectremove ===true)
                        {
                            selfdel.setState({deleteobjectid:objectid});
                        }
                        else
                        {				
                        selfdel.handleDelObject(objectid,selfdel.props.match.params.listid);
                        }     
              }



              selectObject(objectid,boolenvalue)
              {
          
               // Dispatcher.action("result.objectselected",objectid);
              }
                getObjectData(listid,objectid,myOpentip)
                {
                    Objectmanager.getobject(listid,objectid,function(data)
                    {		
                        if(data)
                        {
                            for(var i = 0; i < Opentip.tips.length; i ++) { Opentip.tips[i].hide(); }
                                myOpentip.setContent(DisplayObject.getlistpopupcontent(data));
                                myOpentip.show();  
                        }else
                        {
                            for(var k = 0; k < Opentip.tips.length; k ++) { Opentip.tips[k].hide(); } 			  
                            dialog.errordialog('Fehler','Objekt nicht vorhanden.');
                            
                        }
                        
                    });

                    
                }
              rightClick = function (objectid,event)
              {
                    if(this.props.list.objectpreview)
                    {
                    event.preventDefault();                 
                    for(var i = 0; i < Opentip.tips.length; i ++) { Opentip.tips[i].hide(); }         
                    }               
              }

              showPopup = function(objectid)
              {
                  var fsf = this;
                  if(this.props.list.objectpreview)
                  {
                    var objectselector = "#" +objectid;
                    setTimeout(function () {              
                            if(document.getElementById(objectid))
                            {
                                
                                if(objectselector.length)
                                {
                                    var myOpentip = new Opentip(objectselector,{"escapeContent":false,"background":"White","shadow":false,"borderColor":"black","borderRadius":0});
                                    fsf.getObjectData(fsf.props.match.params.listid,objectid,myOpentip);
                                }
                            }
                            },1000);
                     }
                 
              }
         

         handleEditObject = function(objectid,singleclick)
              {        
                  var self = this;
                 if(this.deleted)
                  {         
                      this.deleted=false;
                    return;
                  }
                  

                  if((!(this.props.list.singleclick)) && (singleclick===true) )
                  { 
                      for(var i = 0; i < Opentip.tips.length; i ++) { Opentip.tips[i].hide(); } 	
                        return;
                  }
                  
                ObjectUrlManager.geturl(this.props.match.params.listid,objectid,function(url)
                {
                
                  self.props.history.push(url);

                });           
              
              }

              dofullscreen()
              {
               // var elem = document.getElementById("listview-box-div");
               // listview-searchbox-div 
               var elem = document.getElementById("listview-box-div");
               var searchbox = document.getElementById("listview-searchbox-div");
               var self = this;
                    if(this.isfullscreen)
                    {

                        if (document.exitFullscreen) {
                            document.exitFullscreen();
                        } else if (document.mozCancelFullScreen) { /* Firefox */
                            document.mozCancelFullScreen();
                        } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
                            document.webkitExitFullscreen();
                        } else if (document.msExitFullscreen) { /* IE/Edge */
                            document.msExitFullscreen();
                        }
                     //   elem.style.overflowY = "visible";
                     searchbox.style.top = "50px";
                        self.isfullscreen = false;;
                    }else
                    {
                     // overflow-y: auto;
                        elem.style.overflowY = "auto";
                        searchbox.style.top = "0";
                        if (elem.requestFullscreen) {
                            elem.requestFullscreen();
                        } else if (elem.mozRequestFullScreen) { /* Firefox */
                            elem.mozRequestFullScreen();
                        } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
                            elem.webkitRequestFullscreen();
                        } else if (elem.msRequestFullscreen) { /* IE/Edge */
                            elem.msRequestFullscreen();
                        }

                        self.isfullscreen = true;
                    }
           
            }

              handleSortClick(field)
              {   
                  var newsort = {};
                  if(field.id === this.state.sort.fieldid)
                  {
                    if(this.state.sort.order ==="asc")
                    {
                    newsort.order = "desc";
                    newsort.fieldid = field.id;   
                    }else
                    {
                    newsort.order = "asc";
                    newsort.fieldid = field.id;   
                    }

                  }else
                  {                  
                  newsort.order = "asc";
                  newsort.fieldid = field.id;                  
                  }
                  this.setState({sort:newsort});    
             
                  if(this.props.meta.rendertype==="server")
                  {    
                      var parameter = {};
                      parameter.fieldid = field.id;
                      parameter.order = newsort.order;
                      parameter.listid = this.props.list.id;
                      parameter.filter = this.state.serversearchtext; 
                      parameter.filterfields = this.state.filterfields;         
                      this.props.filterbyServer(this.props.list.id,parameter)

                  }else
                  {                    
                  // sortColumn.clientsortcolumn(field,this.props.list.listsettings.fields,this.props.objectresult,newsort.order);        
                  sortColumn.clientsortcolumn(field,this.props.list.defaultviewfields,this.props.objectresult,newsort.order);        
                  }      
                  this.infinitescroll()
              }

             handleHideClick = function(field)
              {
                this.setState(prevState => ({
                    invisiblefields: [...prevState.invisiblefields, field.id]
                  }));
                  dialog.okdialog(field.name,"wird ausgeblendet.");
              }
              handleShowFieldClick(fieldid)
              {
                var fieldarray = [...this.state.invisiblefields]; 
                var index = fieldarray.indexOf(fieldid);
                if (index !== -1) {
                    fieldarray.splice(index, 1);
                  this.setState({invisiblefields: fieldarray});
                }
              }


              getconnectedlistobject(listid)
              {
                return this.props.lists.find(function(li) { return li.id === listid;});
              }
              
              getcolumnnumber(fieldid)
              {
                for(let i=0;i<this.props.list.defaultviewfields.length;i++)
                {

                    if(fieldid === this.props.list.defaultviewfields[i])
                    {
                        return i;
                    }
                    
                }
                return 0;
              }


              closefilterdialog=function(field)
              {
                  var self = this;
                  var newfieldfilterlist = [];
                  for(let i=0;self.state.filterfields.length > i; i++)
                  {
                      var ob = self.state.filterfields[i];
                      if(ob.id!==field.id)
                      {
                       newfieldfilterlist.push(ob);
                      }                          
                  }
                this.setState({filterfields:newfieldfilterlist});


                var newfilters = Object.assign({}, this.state.filters);
                newfilters[field.id] = [];
                this.setState({filters:newfilters},this.createfilterlist);


              }

			  render() {
                 let selftd = this;
                 let listobjecttype = this.props.objecttypes.find(f=>f.id === this.props.list.objecttype);
                 let fieldlen = this.props.list.defaultviewfields.length;
       
       /*
       golang update
       for(let i=0;i<this.props.list.listsettings.fields.length;i++)
            {
                if(this.props.list.listsettings.fields[i].defaultview)
                {
                    fieldlen++;
                }
            }    
            */   
                /*header */
            var headertable = [];
           // this.props.list.listsettings.fields.forEach(function(field) {
            this.props.list.defaultviewfields.forEach(function(fieldid) {
                //fixit
                let field = selftd.props.fields.find(f=>f.id ===fieldid);
         
                //invisible if hiddenarray
                if(!selftd.state.invisiblefields.includes(field.id))
                {

                  //  if(field.defaultview)
                 //      { 

                /*define css class*/
                var cssclassarrow ="fa-arrow-invisible";
                if(selftd.state.sort.fieldid === field.id)
                {
                    if(selftd.state.sort.order ==="asc")
                    {
                        cssclassarrow="fa-arrow-up";                        
                    }else
                    {
                        cssclassarrow="fa-arrow-down";
                    }
                }


                var fieldfilterdialog = null;
                         for(let i=0;i< selftd.state.filterfields.length;i++)
                {
                    if(field.id === selftd.state.filterfields[i].id)
                    {
                        var fieldfiltered = selftd.props.fields.find(function(li) {
                            return li.id === field.id;
                          });
                        var fieldindex = selftd.getcolumnnumber(field.id) -1;             
                        fieldfilterdialog = <FieldFilter closedialog={selftd.closefilterdialog.bind(this,fieldfiltered)} field={fieldfiltered}   filtercallback={selftd.filtercallback} fieldindex={fieldindex}  objectresult={selftd.props.objectresult} />
                    }
                }

            
                headertable.push (
                        <th key={field.id} className="sortable" onClick={selftd.handleSortClick.bind(this,field)}>
                    		<div className="fieldbuttondiv">
                            <span className="listheadertbl" dangerouslySetInnerHTML={{__html: field.name}}></span><span className={cssclassarrow}></span>
                            <div title="Spalte verstecken" className="listview-hide-icon-div" onClick={selftd.handleHideClick.bind(this,field)}><img src="https://publicfile.standard-cloud.com/icons/hide_icon.png" alt="Spalte verstecken"/></div>
                            <div title="Spaltenfilter anzeigen" onClick={(e) =>{selftd.handleFilterClick(field,e)}} className="filterbuttonlistview"><img alt="Spaltenfilter anzeigen" src="https://publicfile.standard-cloud.com/icons/filter_listview.png"/>
                            </div>
                        </div>
                        {fieldfilterdialog}
                   </th>
                );  

               // default view ende }   
            }  
               });
               
               headertable.unshift(<th key="8678"></th>);   
               var editbuttonsth;
              // if(selftd.props.list.listsettings.showeditbutton || selftd.props.list.listsettings.showdeletebutton )
              if(selftd.props.list.editbutton || selftd.props.list.deletebutton )
                {                
                 editbuttonsth=<th key="12234"></th>
                }
                /*header */


            var objectcount = 0;
            var tblentrys = [];
       
            for(var j=0;j < this.props.objectresult.length;j++)
            {
                var item =  this.props.objectresult[j];
                var resultrow = [];
                     if(selftd.state.searchtext.length !==0)
                     {
                        var filteritem = item.toString().toLowerCase().replace(item[item.length-1],"");
                      if(selftd.state.searchtext.indexOf(' ') >= 0)
                      {                                     
                            var strarray = selftd.state.searchtext.split(" ");
                            for (var ik = 0; ik < strarray.length; ik++) 
                            {
                            if (filteritem.indexOf(strarray[ik].toLowerCase()) === -1 )
                                {                           
                                        continue;
                                } 
                            }
                      }else
                      {   
                            if (filteritem.indexOf(selftd.state.searchtext.toLowerCase()) === -1 )
                            {                           
                                    continue;
                            }
                       }
                    }
                    /*Suche läuft auch über die GUID */
                  const  objectid = item[fieldlen];
             
                    if(selftd.state.filterobjectids.includes(objectid))
                    {
                        continue;
                    }
                    


                    var keyid =  objectid + "0001";
               resultrow.push(<td onClick={selftd.stopTDPropagation} key={keyid}>
                        <StdCheckbox changeCheckbox={selftd.selectObject} objectid={objectid} value={false}  />
               </td>);  
              
                for(var ix=0;ix<fieldlen;ix++)
                {
                    //  if(selftd.state.invisiblefields.includes(selftd.props.list.listsettings.fields[ix].id))
                    if(selftd.state.invisiblefields.includes(selftd.props.list.defaultviewfields[ix].id))
                    continue;
                    
                        var val = item[ix];
                    //fixit
                    let fieldtypeid = selftd.props.fields.find(f=>f.id ===selftd.props.list.defaultviewfields[ix]).fieldtypeid;
                    switch(fieldtypeid)
                    {
                        
                        case "8787e054-7bb9-411a-8946-da658fbc8e9f":                      
                        if(val !=="")
                        {
                            val = "<img src=" + BACKEND +"/file/" + val +" />";
                        }
                        break;
                        case "8c2a7b49-2584-4296-a39b-32717e56510b":
                        if(val !=="")
                        {
                            var url = "/list/" + val + "/";
                            
                            var connectedlistobject = selftd.getconnectedlistobject(val);
                            //comment beginnt
                            selftd.props.lists.find(function(li) {
                                return li.id === val;
                              });
                              //comment ende
                              if(connectedlistobject)
                              {
                                resultrow.push(<td key={ix} ><BT cssclass="stdtile half" count={connectedlistobject.objectcount} url={url} name={connectedlistobject.objecttypeplural} iconurl="https://publicfile.standard-cloud.com/icons/list.png" /></td>); 
                              }else
                              {
                                resultrow.push(<td key={ix} ></td>);
                              }
                            continue;
                        }
                        break;
                        default:
                      }
                
                    resultrow.push(<td key={ix} dangerouslySetInnerHTML={{__html: val}}></td>);          
                      
                }
                objectcount++;
                var editrow =null;
            
                if(selftd.props.list.editbutton && selftd.props.list.deletebutton )
                {
                    editrow = <td key="992" className="tdeditcol">
                        <img alt="Bearbeiten" onClick={selftd.handleEditObject.bind(this,objectid,false)}   className="minibuttonimg" src="https://publicfile.standard-cloud.com/icons/edit.png" title="Bearbeiten" />      
                        <img alt="Löschen" onClick={selftd.handleDeleteObject.bind(this,objectid)}  className="minibuttonimg" src="https://publicfile.standard-cloud.com/icons/delete.png" title="Löschen"/> 
                        </td>
                }else
                {
                    if(selftd.props.list.editbutton)
                    {
                        editrow = <td key="993"  className="tdedsingl">
                        <img alt="Bearbeiten" onClick={selftd.handleEditObject.bind(this,objectid,false)}   className="minibuttonimg" src="https://publicfile.standard-cloud.com/icons/edit.png" title="Bearbeiten" />  
                        </td>
                    }else
                    {
                        if(selftd.props.list.deletebutton)
                        {
                        editrow = <td key="994" className="tdedsingl">
                          <img alt="Löschen" onClick={selftd.handleDeleteObject.bind(this,objectid)}  className="minibuttonimg" src="https://publicfile.standard-cloud.com/icons/delete.png" title="Löschen"/> 

                        </td>
                        }
                    }

                }
            
                if(this.state.visiblecount > objectcount)
                {
                tblentrys.push (
                    <tr id={objectid} key={objectid}  onContextMenu={selftd.rightClick.bind(this,objectid)} onMouseEnter={selftd.showPopup.bind(this,objectid)}  onClick={selftd.handleEditObject.bind(this,objectid,true)}  onDoubleClick={selftd.handleEditObject.bind(this,objectid,false)}>{resultrow}{editrow}        
                    </tr>
                    );       
    }
    }
let loadingdiv = null;

if(selftd.props.meta.rendertype==="client")
{
    // loadingdiv ='';  
}else
{   

    if(selftd.props.loading===false && (selftd.props.objectresult.length < selftd.props.meta.totalobjects))
    {
     loadingdiv =<div id="bottomdivbox"><div className="loader"></div>Lade..</div>
    }
}
let objectcountrender="X";
if(this.props.meta.rendertype==="server")
{
objectcountrender = this.props.meta.totalobjects;
}else
{    
    objectcountrender = objectcount;
}
let pointersingleclick =""
if(this.props.list.singleclick)
{
    pointersingleclick="pointersingleclick"
 }
var objectname = null;
if(this.props.objectresult.length === 1)
{
   objectname = listobjecttype.name; //this.props.list.listsettings.objecttype.name;    
}else
{
   objectname = listobjecttype.nameplural; //this.props.list.listsettings.objecttype.nameplural;
}

var removedialog = null;
if(this.state.deleteobjectid.length > 0)
{
  var deltext = <div>Wollen Sie den <strong> {this.props.list.listsettings.objecttype.name} </strong> wirklich löschen?</div>
removedialog = <DialogBox closedialog={()=>{this.setState({deleteobjectid:""})}}  title="Löschen ?">
<div className="contentcontainertextremoveobject">
<h1>{deltext}</h1>

      <Button onClick={this.handleDelObject.bind(this,selftd.state.deleteobjectid,selftd.props.match.params.listid)}  color="danger">JA, LÖSCHEN</Button>
      <Button  onClick={()=>{this.setState({deleteobjectid:""})}} >NEIN</Button>
	</div>
 </DialogBox>
}


var hiddenfiled = this.state.invisiblefields.map((fieldid)=>{
    var fieldname = selftd.props.list.listsettings.fields.find(f=>f.id===fieldid).name;
    return(<div onClick={this.handleShowFieldClick.bind(this,fieldid)} className="invisiblefield-field-box" key={fieldid}><img alt="Anzeigen" src="https://publicfile.standard-cloud.com/icons/show_list.png"></img>{fieldname}</div>);
});




				return (
				<div id="listview-box-div">
                                    <div id="listview-searchbox-div">
                                        <div><input id="listsearchbox" value={this.state.searchvalue}  onChange={this.handleSearchTextChange}  placeholder="Filter" type="search" autoComplete="off"/></div>                                   
                                        <div id="listfilderstd">{hiddenfiled}</div>
                                        <div className="tblsearchresultcount">{objectcountrender} {objectname}</div>
                                        <div id="fullscreentd"><img className="fullscreen-icon" alt="Vollbild" src="https://publicfile.standard-cloud.com/icons/fullscreen.png" onClick={this.dofullscreen} /></div>    
                                    </div>
                <table id="tbllistdataresult">
                <thead id="fixtableheader"><tr>
                {headertable}
                {editbuttonsth}
                </tr>
                </thead>
                <tbody className={pointersingleclick}>             
                {tblentrys}            
                </tbody>
                </table>
                <div id="div-bottom-visible"></div>
                {loadingdiv}
                {removedialog}
  		        </div>
				);

	}
}

function mapStateToProps({ list,field,objecttype}) {
    return { lists: list,fields:field,objecttypes:objecttype};
  }
  
  export default withRouter(connect(mapStateToProps,{loadMore,filterbyServer})(ListView));
  
 
