import React from 'react';
import $ from 'jquery';
import jqueryui from 'jquery-ui';
	
  class ListSelector extends React.Component {
	  	    constructor(props) {
    super(props);
	this.state = {listid:"",listname:""};
	this.handleConnectSearchTextChange = this.handleConnectSearchTextChange.bind(this);
	this.showAllLists = this.showAllLists.bind(this);

  }
				handleConnectSearchTextChange(event) {
		    this.setState({listname: event.target.value});            
}
showAllLists()
{
	$(this.refs.autocomplete).autocomplete("search", " " );
}
	componentDidMount() {
		var tt = this;
   $(this.refs.autocomplete).autocomplete({
        minLength: 1,
        autoFocus: false,
        cacheLength: 1,
        scroll: true,
        highlight: true,
        source: function(request, response) {
            $.ajax({
                url: "/list/json",
                dataType: "json",
                data: request,
                success: function( data, textStatus, jqXHR) {
                   var items = data;
                    response(items);
                },
                error: function(jqXHR, textStatus, errorThrown){
				alert('error getting lists');
                     console.log( textStatus);
                }
            });
        },
		    select: function (a, ui) {
      tt.setState({listname: ui.item.label});   
			tt.setState({listid: ui.item.id});   
			tt.props.listupdate(ui.item);
		return false;
    },
	    focus: function(event, ui) {
				  tt.setState({listname: ui.item.label});   
					tt.setState({listid: ui.item.id});   
        return false; // Prevent the widget from inserting the value.
    }}).data("ui-autocomplete")._renderItem = function( ul, item ) {
	        var rederelementstyle= '<a><div  class="list_item_containerobjecttype"><div class="labelobjecttypename"><b>' + item.name + '</b></div><div class="descriptionobjecttype">' + item.description + '</div></div></a>';
	  return $("<li></li>").data("objecttypeselector-ui-autocomplete-item", item).append(rederelementstyle).appendTo(ul);
    }
}
		  render() {
					return (  
						<div className="objecttypeselectorbox">
							<div>
									<div className="textboxgroup">      
												<input type="text" className="stdtextbox" required  onChange={this.handleConnectSearchTextChange}  value={this.state.objecttypname}  ref="autocomplete" />
												<span className="texthighlight"></span>
										    <span className="textbar"></span>
											<label>Listenvorlage</label>
									</div>
									<img onClick={this.showAllLists} id="listdropdown" src="https://publicfile.standard-cloud.com/icons/downarrow.png"  alt="Alle Liste"/>
								
		   	</div></div>);
						
					

			  }
			  }
	
	

export default ListSelector;
