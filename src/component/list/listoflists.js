
import React,{useState,useEffect,useRef} from 'react';
import { withRouter } from 'react-router-dom';
import './listoflists.css';
function ListOfLists(props) {
const [searchtext,setSearchtext] = useState("");
const [selectedobject,setSelectedobject] = useState(0);
let selectedlistid = "";
const searchInput = useRef(null);

useEffect(()=>
{

    if(document.body.offsetWidth > 1500)
    {
    searchInput.current.focus(); 
    setSelectedobject(0);
    document.addEventListener("keydown", selectorfunction, false);
    }
    return ()=>{document.removeEventListener("keydown", selectorfunction, false)}
});



 function handleClickList(listid)
  {
     props.history.push("/list/" + listid + "/");
  }

  function selectorfunction(e)
  {
      var self = this;
    var code = e.keyCode || e.which;
    var count;
    if(code === 40)
    {
        count = selectedobject + 1;
    setSelectedobject(count);
    }
    if(code === 38)
    {
    count = selectedobject - 1;
    setSelectedobject(count);
    }     
    if(code === 13)
    {
        if(selectedlistid !=="" && selectedobject > 0)
        {
            props.history.push("/list/" + selectedlistid+ "/");
        }
    }   
  }


function addlistobject(list)
 {
     var addobjecturl = "";
     if(list.newviewid !=="")
     {
       addobjecturl= '/list/' + list.id + '/newobject/objectview/' + list.newviewid + '/newmode/';
     }else
     {
       addobjecturl = '/list/' + list.id + '/newobject/';	
     }	
    props.history.push(addobjecturl);
 }

 function handleSearchTextChange(event) {           
   setSearchtext(event.target.value.toLowerCase());     
   setSelectedobject(0);               
 }


    var rowcount = 0;
 var rows = props.data.map(function(list) {    


						if(list.id==="3bbcda83-10b6-4c3d-a843-4259520ffb1a" || list.id==="99043cfa-9092-421b-8f34-566e1e6849fd" || list.parentlist !=="")
							{
							return null;
							}


        if(searchtext !== "")
        {
            if(list.name.toLowerCase().indexOf(searchtext)> -1 || list.description.toLowerCase().indexOf(searchtext) > -1)
            {

            }else
            {
               return null;
            }          
        }


        let objecttype = props.objecttypes.find(f=>f.id === list.objecttype);

        var addtext = objecttype.name + " hinzufügen"; //list.listsettings.objecttype.name + " hinzufügen";
        var showlisttext = list.name + " anzeigen";
        var listdescription = list.description;
        if(props.small)
        {
            var lenght = list.name.length + list.description.length;
            if(lenght > 45)
            {
                var newcut =  40 - list.name.length;
                listdescription = listdescription.substring(0,newcut) + "..";
                showlisttext =  list.description;
            }

        }

        
        var selectedcss = null;
        rowcount++;
        if(rowcount === selectedobject)
        {
            selectedcss = "selectedrow"
            selectedlistid = list.id;        
        }

            return (
        <tr className={selectedcss} title={showlisttext} key={list.id} onClick={handleClickList.bind(this,list.id)}>
            <td><span className="listnametext"  dangerouslySetInnerHTML={{__html:list.name}} ></span> <span className="listdescription" dangerouslySetInnerHTML={{__html:" - " + listdescription }} ></span><span title={addtext} onClick={(e)=>{e.stopPropagation();addlistobject(list)}} className="listoflists-addlist-span"><img className="addlistimage" src="https://publicfile.standard-cloud.com/icons/add.svg" alt="Objekt hinzufügen" /></span></td>
            <td className="listcountcol"><span  dangerouslySetInnerHTML={{__html:list.count + " "+ objecttype.nameplural}} ></span></td>			
        </tr>
            )
        });     


		return (          
<div id="listofalllists" className="stdcard stdcard-listoflists" >
<input id="listoflistssearchbox"  ref={searchInput}  onChange={handleSearchTextChange}  placeholder="Filter" type="search"  autoComplete="off"/><br/>
<div id="listoflists-listbox-div">
<table id="listviewtable">
     <tbody>
     {rows}
    </tbody> 
</table>
</div>
</div>
        );
}

export default withRouter(ListOfLists);