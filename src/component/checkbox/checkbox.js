import React from 'react';
import './checkbox.css';
class StdCheckboxListview extends React.Component {
	constructor(props) {
    super(props);
    this.state = {value:this.props.value};
    this.changeCheckbox = this.changeCheckbox.bind(this);
    this.stopLabelPropagation = this.stopLabelPropagation.bind(this);

  }
  stopLabelPropagation(e) {
    e.stopPropagation();
  }
 
  changeCheckbox(e)
  {
  
   // this.setState({value:!this.state.value});
    console.log(this.props.objectid);
  //  this.props.changeCheckbox(this.props.objectid,!this.state.value);
    e.stopPropagation();
  }

render() {
var checkboxid = this.props.objectid +"checkbox";
return(<div className="listpagecheckbox">
 	              <div className="stdcheckboxregion" >
                            <input  onClick={this.changeCheckbox} id={checkboxid}  type="checkbox" value="enableanalytics" className="checkbox"/>
                            <label onClick={ this.stopLabelPropagation } htmlFor={checkboxid} ></label>
                </div>
 		</div>);
}	  
  }   
export default StdCheckboxListview;

