import React,{memo} from 'react';
import {useSelector} from 'react-redux';
import { Link } from "react-router-dom";

function StdBreadCrumb(props){    
 
 const navigation = useSelector(state => state.navigation);  
        let navigationresult =[];
for(let i=0; i < navigation.length;i++)
{
    let navigationtitle = navigation[i].title
    let url = navigation[i].url;
    if((i) !==navigation.length)
    {        
        if(url)
        {
            navigationresult.push(<Link key={url}  to={url}>{navigationtitle}  > </Link>);
        }else
        {
            navigationresult.push(<Link key="nonav" to="#" >{navigationtitle}</Link>); 
        }
    }
}
   return( <h1>{navigationresult}</h1>);
}
export default memo(StdBreadCrumb);

