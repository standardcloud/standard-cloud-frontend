import React from 'react';
import PermissionManager from '../../../module/permissionmanager/stdpermissionmanager';
import { withRouter } from 'react-router-dom';

class AddKey extends React.Component {
	    constructor(props) {
		super(props);
		this.state = {keys:[],key:{},adduserbutton:true};
		this.searchkey = this.searchkey.bind(this);
		this.addkeymodus = this.addkeymodus.bind(this);
  }			  

searchkey(e)
{
	var self = this;
	PermissionManager.getkeys(e.target.value).then(data =>
	{
		self.setState({keys:data});	
	});
}	
selectkey(key)
{
	this.props.addkey(key);
	this.setState({keys:[]});
	this.setState({addkeybutton:!this.state.addkeybutton});	
}
showkeypage(key)
{
	this.props.history.push("/admin/permissionmgmt/key/" + key.id + "/edit/");
}

addkeymodus()
{
	this.setState({addkeybutton:!this.state.addkeybutton});
}

render() {

	if(this.state.addkeybutton)
		{
			return (<img alt="Hinzufügen" className="addusericonaddusercomponent" onClick={this.addkeymodus} src="https://publicfile.standard-cloud.com/icons/add.png" />);
		}
	var results = this.state.keys.map(function(key)
		{
			var imgurl = "https://publicfile.standard-cloud.com/icons/key_micro.png";
			return(<tr key={key.id} className="adduseruserrow" onClick={() => self.selectkey(key)}><td className="adduserrowimg"><img alt="Schlüssel" src={imgurl}/></td><td>{key.name}</td></tr>);
		});

	var self = this;
	return (
	<div className="addusercomponent"><input type="text" className="adduserinputfield" placeholder="Schlüssel suchen" onChange={this.searchkey}/><br/>
	<div className="adduseruserbox">
	<table className="addusercomponenttbl">
	<tbody>
	{results}
	</tbody>
	</table>
	</div>
	</div>
    );
	
}	  
  }
export default withRouter(AddKey);

