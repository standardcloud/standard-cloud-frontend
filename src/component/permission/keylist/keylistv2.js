import React from 'react';
import AddKey from '../addkey/addkey';

  class KeyListV2 extends React.Component {
	constructor(props) {
    super(props);
	this.clickevent = this.clickevent.bind(this);
  }
  
  clickevent(key)
  {
	if(typeof this.props.clickevent !== "undefined")
	  {
		this.props.clickevent(key);
	  }
	}
render(){
	var self = this;
	var addkeyhtml = null;
	if(typeof this.props.addkey !== "undefined")
		{
			addkeyhtml = <AddKey addkey={this.props.addkey}/>
		}
	var keys = this.props.keys.map(function(key)
	{
		var imgurl = "https://publicfile.standard-cloud.com/icons/key_micro.png"
		var title = "Klick, um Schlüssel " + key.name + " zu löschen."
		return(<span key={key.id} onClick={() => self.clickevent(key)}  title={title} className="userlistuserbox"><img className="userlistuserpicture" alt={key.name} src={imgurl}/>{key.name}</span>);
	});

		return (
		<div>
			<div>
			{keys}
			</div>
			<div>{addkeyhtml}</div>
		</div>
        );
		}
  };
export default KeyListV2;
