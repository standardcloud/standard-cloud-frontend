import React from 'react';

  class AddRight extends React.Component {
	    constructor(props) {
		super(props);
		this.state = {right:{},adduserbutton:true};
		this.addkeymodus = this.addkeymodus.bind(this);
		this.selectright = this.selectright.bind(this);
		
  }			  

selectright(right)
{
	this.props.addright(right);
	this.setState({addkeybutton:!this.state.addkeybutton});	
}

addkeymodus()
{
	this.setState({addkeybutton:!this.state.addkeybutton});
}



render() {

	if(this.state.addkeybutton)
		{
			return (<img className="addusericonaddusercomponent" alt="Hinzufügen" onClick={this.addkeymodus} src="https://publicfile.standard-cloud.com/icons/add.png" />);
		}

	var results = this.props.rights.map(function(right)
		{
			var imgurl = "https://publicfile.standard-cloud.com/icons/right_micro.png";
			return(<tr className="addrightrrow" onClick={() => self.selectright(right)}><td className="adduserrowimg"><img alt="Recht" src={imgurl}/></td><td>{right.name}</td></tr>);
		});

	var self = this;
	return (
	<div className="addusercomponent">
	<div className="adduseruserbox">
	<table className="addusercomponenttbl">
	<tbody>
	{results}
	</tbody>
	</table>
	</div>
	</div>
    );
	
}	  
  }

export default AddRight;
