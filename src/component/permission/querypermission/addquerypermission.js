import React from 'react';
import Permissionmanager from '../../../module/permissionmanager/stdpermissionmanager';
import dialog from '../../../component/dialog/stddialog';
import KeyList2 from '../../permission/keylist/keylistv2';
import './addquerypermission.css';
class AddQueryPermission extends React.Component {
	constructor(props) {
		  super(props);
		this.changePermissionLevel = this.changePermissionLevel.bind(this);
		this.saveKey = this.saveKey.bind(this);
		this.addkey = this.addkey.bind(this);
		this.state= {permissionlevelid:"",keyid:""};
	}
resetpermissionlevel(reset)
{
	this.setState({permissionlevelid:""});
}
changeKeyID(permissionkeyid)
{
	this.setState({keyid:permissionkeyid});
}
changePermissionLevel(permissionlevel)
{
	this.setState({permissionlevelid:permissionlevel.target.value});
}
saveKey()
{
	var self = this;
if(this.state.keyid.length==="")
{
	dialog.okdialog("Schlüssel", "muss nun ausgewählt werden.");		
	return;
}	
if(this.state.permissionlevelid.length==="")
{
	dialog.okdialog("Berechtigung", "muss nun ausgewählt werden.");		
	return;
}	

console.log(this.state.keyid);
Permissionmanager.addquerykey(this.state.keyid,this.state.permissionlevelid,this.props.queryid).then(returndata =>
{
	if(returndata.state==="error")
	{
		dialog.error("Berechtigung", "konnte nicht hinzugefügt werden.");	
	}else
	{
	dialog.okdialog("Berechtigung", "wurde hinzugegügt.");	
	}

	self.setState({keyid:""});		
	self.setState({permissionlevelid:""});									
});
}

addkey(key)
{
this.setState({keyid:key.id},()=> {this.saveKey()});
}

render () {	

        var self = this;
        var options = self.props.permissionlevels.map(function(level) {
            return (
            <option key={level.id} value={level.id}>
                    {level.name}
            </option>
            )
        });
		return (
			<div >
				<table className="stdtable tblkeyquerypermission">
					<thead><tr><th>Berechtigung</th><th>Schlüssel</th></tr></thead><tbody><tr>
						<td>
           <select value={this.state.permissionlevelid} onChange={this.changePermissionLevel}>
				<option  value="">                    
                </option>
                {options}
            </select>
			</td><td>
			<KeyList2 keys={[]} addkey={this.addkey}/>
			</td></tr>
			</tbody></table><br/>
			</div>
				);
				
			  }
}

export default AddQueryPermission;
