
import React from 'react';
import BT from '../../../component/button/stdbutton';
import FieldManager from '../../../module/fieldmanager/stdfieldmanager';
import dialog from '../../../component/dialog/stddialog';
import DialogBox from '../../../component/dialogbox/dialogbox';
import ObjectTypeManager from '../../../module/objecttypemanager/stdobjecttypemanager';
import Button from 'muicss/lib/react/button';
import Trans from '../../../module/translatoinmanager/translationmanager';
import './addfieldtoobjecttype.css';

class AddFieldToObjecttype extends React.Component {
	    constructor(props) {
    super(props);
  this.state = {searchtext:"",fields:[],addfielddialog:false};	
  this.addfieldbuttonclick = this.addfieldbuttonclick.bind(this);
  this.loadfields = this.loadfields.bind(this);
  this.handleAddFieldtoObjecttype = this.handleAddFieldtoObjecttype.bind(this);
  this.handleClickAddFieldDialogField = this.handleClickAddFieldDialogField.bind(this);
  this.handleSearchTextChange = this.handleSearchTextChange.bind(this);

  }

  handleSearchTextChange(event) {           
    this.setState({searchtext: event.target.value.toLowerCase()});                    
  }

loadfields()
{
  var self = this;
  FieldManager.getfieldlist().then(data=>
{
 if(data.state ==="ok")
  {
  self.setState({fields:data.content});
 
  }
});
}

handleClickAddFieldDialogField(field)
{
  this.setState({addfielddialog:true});
  this.setState({selectedfield:field});
}


  		componentDidMount() {

				}
addfieldbuttonclick()
{
this.loadfields();
}

handleAddFieldtoObjecttype()
{
  this.setState({addfielddialog:false});
  var sf = this;
  ObjectTypeManager.addfield(this.state.selectedfield.id,this.props.objecttype.id).then(data=>
  {   
        dialog.ok(sf.state.selectedfield.name,"wurde der Listenvorlage " + sf.props.objecttype.name + "hinzugefügt.");
        sf.setState({searchtext:""});
        sf.props.update();      
  });
}


render(){
  var self = this;
  var dialogbox = null;
  if(this.state.addfielddialog)
  {
    dialogbox = <DialogBox title="Feld anfügen?" closedialog={()=>{this.setState({addfielddialog:false});}}>
    <div id="addfieldtoobjecttypebox">
Möchten Sie das Feld {self.state.selectedfield.name} anfügen <br/><br/>

<Button  onClick={()=>{self.handleAddFieldtoObjecttype()}}>Feld anfügen</Button>
<Button  onClick={()=>{this.setState({addfielddialog:false});}} >NEIN</Button>
</div>
    </DialogBox>
  }



var fieldrowshtml = this.state.fields.map(function(field)
{
  if(self.state.searchtext !== "")
  {
      if(field.name.toLowerCase().indexOf(self.state.searchtext)> -1 || field.description.toLowerCase().indexOf(self.state.searchtext) > -1 || field.fieldgroup.toLowerCase().indexOf(self.state.searchtext) > -1)
      {

      }else
      {
         return null;
      }          
  }


  var fieldinobjecttype = false;
    for(var j=0;j< self.props.objecttype.fields.length;j++)
      {
        if(self.props.objecttype.fields[j].id === field.id)
          {
            fieldinobjecttype = true;
          }
      }
      if(fieldinobjecttype)
        return null;

  var titlenew = "Feld " + field.name + " zur Listenvorlage " + self.props.objecttype.name + " hinzufügen.";
  return(<tr key={field.id} title={titlenew} className="fieldrowrow" onClick={() => self.handleClickAddFieldDialogField(field)}><td dangerouslySetInnerHTML={{__html:field.name}}></td><td dangerouslySetInnerHTML={{__html:field.fieldgroup}}></td><td dangerouslySetInnerHTML={{__html:field.description}}></td><td dangerouslySetInnerHTML={{__html:field.fieldtyp}}></td>
  <td className="delfiledtd"><img alt="Hinzufügen" src="https://publicfile.standard-cloud.com/icons/add.png"/></td>
  </tr>);
});

var fieldtabl = null;
if(fieldrowshtml.length > 1)
  {
 fieldtabl = <div id="addfieldtoobjecttypefieldlistdiv">
<input id="listoflistssearchbox" onChange={this.handleSearchTextChange}  placeholder="Filter" type="search"  autoComplete="off"/><br/>
      <table id="addfieldtoobjecttypefieldtbl" className="stdtable">
      <thead>
        <tr>
         <th>{Trans.t("name")}</th><th>{Trans.t("group")}</th><th>{Trans.t("description")}</th><th>Typ</th><th></th>    
        </tr></thead><tbody>
          {fieldrowshtml}
          </tbody>
            </table>             
            </div>;
    
  }

  var buttons = null;
    if(fieldrowshtml.length < 1)
      {
        buttons = <BT  callbackfunction={this.addfieldbuttonclick} name={Trans.t("fieldadd")} iconurl="https://publicfile.standard-cloud.com/icons/addcolumn.png" />;
      }else
      {
        buttons = <div id="addfieldtoobjecttypeheader"><h2>Folgende Felder können hinzugefügt werden:</h2></div>
      }
        return (<div>
          <div id="addfieldtoobjecttypebuttonbar">
          	{buttons}
            </div>
            {dialogbox}
        {fieldtabl}
</div>);
		}

  }
export default AddFieldToObjecttype;

