define(['react','reactdom','jsx!component/button/button.jsx','stdobjecttypemanager','jquery','jqueryui','stddialog','jsx!component/contentbox/contentbox.jsx','jsx!component/helpbox/helpbox.jsx'], function(React,ReactDOM) {
  var buttoncomponent = require("jsx!component/button/button.jsx");
  var Contentbox =  require("jsx!component/contentbox/contentbox.jsx");
  var Helpbox =  require('jsx!component/helpbox/helpbox.jsx');
  var button = new buttoncomponent();
  var objecttypemanager = require("stdobjecttypemanager");
 var dialog = require("stddialog");
function ObjectTypeBasicSettings() {

  this.ObjectTypeBasicSettings = React.createClass({
	  getInitialState: function() {
    return {changedvalue:false,fields:[]};
  },
componentDidMount: function() {
this.loadobjecttypesettings();
},
loadobjecttypesettings:function()
{
		var test = window.location.href;
		var testRE = test.match("modify/(.*)/edit");
		var objecttypeid = testRE[1];	
		var selfobjecttypesettings = this;
        this.setState({objecttypeid:objecttypeid});
			objecttypemanager.getobjecttypesettings(objecttypeid).then(settings =>
			{          
				selfobjecttypesettings.setState({id:settings.id});
				selfobjecttypesettings.setState({name:settings.name});
				selfobjecttypesettings.setState({nameplural:settings.nameplural});
				selfobjecttypesettings.setState({description:settings.description});            
				selfobjecttypesettings.setState({enableanalytics:settings.enableanalytics	});  	
				selfobjecttypesettings.setState({fields:settings.fields}); 
		     	selfobjecttypesettings.setState({navigationfieldid:settings.navigationfieldid}); 	
				selfobjecttypesettings.setState({changedvalue:false});	
			});
},
saveBasicSettings: function()
{
	console.log(this.state.navigationfieldid);
objecttypemanager.setobjecttypesettings(this.state.id,this.state.name,this.state.nameplural,this.state.description,
this.state.enableanalytics,this.state.navigationfieldid).then(returndata =>
{
	if(returndata.state =="ok")
	{
		dialog.okdialog("Basiseinstellungen", "erfolgreich gespeichert.");
	}
});
this.setState({changedvalue:false});
},
changeAnalytics:function(e)
{
this.setState({changedvalue:true});
this.setState({enableanalytics:!this.state.enableanalytics});
},


changeObjectTypeName:function(e)
{
this.setState({changedvalue:true});
this.setState({name:e.target.value});
},
changeObjectTypePluralName:function(e)
{
this.setState({changedvalue:true});
this.setState({nameplural:e.target.value});
},
changeObjectTypeDescription:function(e)
{
this.setState({changedvalue:true});
this.setState({description:e.target.value});
},
changeNavigation:function(nav)
{
	this.setState({changedvalue:true});
	console.log(nav.target.value);
	this.setState({navigationfieldid:nav.target.value});
},
render: function () { 
    var selfrender = this;
	var savebutton = <button.view  disabled={true} name="Speichern" iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
	if(this.state.changedvalue)
	{
		savebutton = <button.view  name="Speichern" callbackfunction={this.saveBasicSettings} iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
	}
	  var options = selfrender.state.fields.map(function(field) {
            return (
            <option key={field.id} value={field.id}>
                    {field.name}
            </option>
            )
        });
var windowstitle = "Einstellungen für " + this.state.name;
		return (

        <div>
			 <Contentbox title={windowstitle}>
				 <div id="objecttypesettingsbasicpageinnerbox">
			<div id="listnamebox">
					<div className="textboxgroup">      
							<input required className="stdtextbox" type = "text" name= "tbName" onChange={this.changeObjectTypeName} value={this.state.name} />
				<span className="texthighlight"></span>
				<span className="textbar"></span>
				<label>Name</label>
			</div>
			<div className="textboxgroup">      
				<input required className="stdtextbox" type = "text" name= "tbListName" onChange={this.changeObjectTypePluralName} value={this.state.nameplural} />
				<span className="texthighlight"></span>
				<span className="textbar"></span>
				<label>Plural(Mehrzahl)</label>
			</div>
			
			</div>
			<div className="textboxgroup">      
				<textarea required name= "tbListDescription" className="stdtextbox" onChange={this.changeObjectTypeDescription} value={this.state.description}></textarea>
				<span className="texthighlight"></span>
				<span className="textbar"></span>
				<label>Beschreibung</label>
			</div>
	
		 Bestimmen Sie das Feld für die Navigation (oben Breadcrumb)<br/>
		 <select value={this.state.navigationfieldid} onChange={this.changeNavigation}>
				<option  value="">                    
                </option>
                {options}
            </select>
			
		
          <br/> <br/> 
						<div className="stdcheckboxregion">
                            <input id="enableanalytics"  checked={this.state.enableanalytics} type="checkbox" value="enableanalytics"   className="checkbox"/>
                            <label onClick={this.changeAnalytics}  for="enableanalytics"> <strong>Analytics</strong>  erlauben.</label>
     	             </div><br/>    	
					</div>
			</Contentbox>
		<div id="objecttypesettingsbasicpagefooter">
	 {savebutton}
			 <button.view  name="Hilfe" url="https://standard-cloud.zendesk.com/hc/de/articles/115002416889-Listeneinstellungen" iconurl="https://publicfile.standard-cloud.com/icons/help.png" />
	
	</div>
				 <Helpbox title="Hilfe">
Hier können Sie grundlegende Einstellungen vornehmen.
</Helpbox>

			 </div>
        );

		}
  });

  }

  ObjectTypeBasicSettings.prototype.init = function () {

	 ReactDOM.render(<this.ObjectTypeBasicSettings />,  document.getElementById('objecttypesettingsbasicsettings'));
  };

  return ObjectTypeBasicSettings;

});