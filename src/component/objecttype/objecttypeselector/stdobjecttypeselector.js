import React from 'react';
import ObjectTypeManager from '../../../module/objecttypemanager/stdobjecttypemanager';
import Autosuggest from 'react-autosuggest';
import './stdobjecttypeselector.css';
import Trans from '../../../module/translatoinmanager/translationmanager';
  class ObjectTypeSelector extends React.Component {
	  	    constructor(props) {
    super(props);
	this.state = {objecttypes:[],objecttypeid:"",suggestions:[],objecttypname:"",value:""};
	this.handleConnectSearchTextChange = this.handleConnectSearchTextChange.bind(this);
	this.showAllLists = this.showAllLists.bind(this);
	this.getSuggestionValue = this.getSuggestionValue.bind(this);
	this.loadobjecttypes = this.loadobjecttypes.bind(this);
	this.onChange = this.onChange.bind(this);
	this.onSuggestionSelected = this.onSuggestionSelected.bind(this);
	this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(this);
	this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(this);
	this.loadobjecttypes();
  }


  handleConnectSearchTextChange(event) {
		    this.setState({objecttypname: event.target.value});            
}

getSuggestions(value)
{
	const inputValue = value.trim().toLowerCase();
	const inputLength = inputValue.length;
  
	return inputLength === 0 ? [] : this.state.objecttypes.filter(lang =>
	  lang.name.toLowerCase().slice(0, inputLength) === inputValue
	);
}


loadobjecttypes()
{	
	var self = this;
	ObjectTypeManager.getobjecttypes().then(data =>
		{
			self.setState({objecttypes:data.content});
		});
}

getSuggestionValue(suggestion) {
	return suggestion.name;
  }
renderSuggestion(suggestion) {
	switch(suggestion.id)
	{
		case "78181ef4-fa9c-4dff-9ffb-a6d1fb3408ea":
		case "707ce4a3-ed82-4029-ac2f-9acb4f3771bd":
		return;
		default:
	}
	
	return (
	  <div className="selectoritembox"><strong>{suggestion.name}</strong><br/>{suggestion.description}</div>
	);
  }
showAllLists()
{

}
  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested()
  {
    this.setState({
      suggestions: []
    });
  }

onSuggestionsFetchRequested({ value })
{
	var self = this;
    this.setState({
      suggestions: self.getSuggestions(value)
    });
}
onSuggestionSelected(event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method })
{
	this.props.objecttypeupdate(suggestion.id);
}
shouldRenderSuggestions(value) {
	return true;
  /* return value.trim().length > 0; */
}

onChange = (event,{newValue}) => {
	console.log(newValue);
    this.setState({
      value: newValue
		});
//		this.props.objecttypeupdate(newValue);
  };


		  render() {
			var value = this.state.value;
				// Autosuggest will pass through all these props to the input.
				const inputProps = {
				  placeholder: '',
				  value,
				  onChange: this.onChange
				};

var listtemplate  = Trans.t("listtemplate");

					return (  
						<div className="objecttypeselectorbox">
							<div>
							{listtemplate}
									<Autosuggest
									suggestions={this.state.suggestions}
									getSuggestionValue={this.getSuggestionValue}
									renderSuggestion={this.renderSuggestion}
			   					shouldRenderSuggestions = {this.shouldRenderSuggestions}
									onSuggestionSelected ={this.onSuggestionSelected}
									onSuggestionsFetchRequested = {this.onSuggestionsFetchRequested}
									onSuggestionsClearRequested={this.onSuggestionsClearRequested}
									inputProps={inputProps}
								/>												
								
		   	</div></div>);
						
					

    }
}
	
export default ObjectTypeSelector;
