import React from 'react';
import CommentPost from './commentpost';

function CommentList(props){
if(props.comments)
{
          let  Comments = props.comments.map(function(comment) {			
      return (
		  <CommentPost changelogid={comment.changelogid} dateshort={comment.dateshort}  date={comment.date} username={comment.username} userid={comment.userid} id={comment.id} key={comment.id} userpicture={comment.userpicture} comment={comment.comment}/>
      );       
    });  
	    return (
            <div>
            {Comments}  
            </div>
    );
}else
{
	    return (
            <div>
            
            </div>
    );	
	
}	

}

export default CommentList;