import moment from 'moment';
import 'moment/locale/de';
import React,{useState} from 'react';
import {BACKEND} from '../../stdsetup';
import CommentList from "./commentlist";
import ShareManager from '../../module/sharemanager/stdsharemanager';
import { Link } from "react-router-dom";
import Dialog from '../../component/dialog/stddialog';
import {useDispatch} from 'react-redux';
import {addLike, addComment,getUserStream} from '../../actions/stream';

import { withRouter } from 'react-router-dom';
import ObjectUrlManager from '../../module/objecturlmanager/stdobjecturlmanager';
import './userevent.css';
import './userevent-mobil.css';
import DialogBox from '../dialogbox/dialogbox';
import UserManager from '../../module/usermanager/stdusermanager';
import StartpageEditor from '../../component/texteditor/startpageeditor';


function UserEvent(props) {

	const dispatch = useDispatch();
//		this.state = {currentuser:{},commenttext:"",showsavebutton:false,personalpost:this.props.eventdata.Text};
	const [commenttext,setCommenttext] = useState("");
	const [personalpost,setPersonalpost] = useState("");
	const [showsavebutton,setShowsavebutton] = useState(false);
	const [commenteditopen,setCommenteditopen] = useState(false);

	let user = props.users.find(u=>u.id === props.eventdata.user);

	function savepersonalpost()
			{
				UserManager.editpersonalpost(props.eventdata.ID,personalpost).then(status =>
				{
					if(status.state ==="ok")
					{	
						Dialog.okdialog("Beitrag", " wurde gespeichert.");	
						var now = new Date();
						var ticks = now.getTime();
						dispatch(getUserStream(ticks));				

					}else
					{
						Dialog.errordialog("Beitrag", "konnte nicht gea&umlndert werden.");	
									}
					
				});
			
			}

			function handleEditPersonalPost()
			{
			 setCommenteditopen(true);
			 }

			  function onChangecommenttext(e)
			  {	
				setCommenttext(e.target.value);
				if(e.target.value.length!==0)
				{
					setShowsavebutton(true);
				}else
				{
					setShowsavebutton(false);
				}				
			  }

			 function handleShowObject(e)
			  {
				if(e.target.dataset.objecturl)
				{
					
					props.history.push(e.target.dataset.objecturl);
					return;
				}
		  
		   if(props.eventdata.Action==="DEL")
		   {

		   }else
		   {
			ObjectUrlManager.geturl(props.eventdata.list,props.eventdata.object,function(data)
				{
					props.history.push(data);				
				});
			}
			  }

			 function handleLikeButtonClick()
			  {
				dispatch(addLike(props.eventdata.id));				
			  }

			  function handleAddCommentButtonClick()
			  {			
				dispatch(addComment(props.eventdata.id,commenttext));
					setCommenttext("");
			  }

			  function handleShareButtonClick()
			  {				
	  
					  ShareManager.addshare(props.eventdata.id).then(returnvalue =>
					  {
						if(returnvalue.state==="ok")
						{
							var newcount = (Number(props.eventdata.sharecount) + 1);
							var newevent = props.eventdata;
							newevent.sharecount = newcount;
						//	selfshare.setState({eventdata:newevent});
						}else
						{
							Dialog.errordialog("Verteilen", "Inhalt konnte nicht geteilt werden.");				
						}		
					  });
			  }		  

	
				var editbutton='';
					//console.log(this.props.lists);
					
				if(user === undefined)
				return null;
					let list = props.lists.find(u=>u.id === props.eventdata.list);
					// hallo 
				  if(props.eventdata.user === props.currentuser.userid && props.eventdata.objecttyp===0)
				  {	
					editbutton = <img onClick={handleEditPersonalPost} src="https://publicfile.standard-cloud.com/icons/editk.png" alt="Bearbeiten" className="editpersonalpost"/>	  
				  }
				  
				  
				  
	       var savebutton ='';
			if(showsavebutton)
			{
				savebutton = <div className="commentbutton" onClick={handleAddCommentButtonClick}><a href="Speichern">Speichern</a></div>
			}

			  var profilurl = "/profile/" + props.eventdata.user + "/";
				//fixit
				var pictureurl = BACKEND +"/file/" + user.img;
			  var username = user.name //this.props.eventdata.UserName;
			  var action = props.eventdata.action;
			  var objecttype = props.eventdata.objecttyp;
			  var date = props.eventdata.date;			 
			  var eventtext = props.eventdata.text;			
			  var currentuserpicture = BACKEND + "/file/" + props.currentuser.img;
			  var currentusername = props.currentuser.firstname;
			  
			  var daterelativetime = moment(props.eventdata.DateShort).fromNow();
			       
			  
			  		var link = '';
					  var urllink ="";	
		if(objecttype === 3)
		{	//liste
			urllink = "/list/" + props.eventdata.list + "/";
			// dangerouslySetInnerHTML={{__html: this.state.listsetting.listtitle}}>
			// link = <a href={urllink}> {this.props.eventdata.ObjectName} </a>	
			//dadsf
			link = <Link dangerouslySetInnerHTML={{__html: list.name}} to={urllink}/> 
			// link = <a href={urllink} dangerouslySetInnerHTML={{__html: this.props.eventdata.ObjectName}} ></a>	
		}
		
		if(objecttype==='CSVImport')
		{
            urllink = "/list/" + props.eventdata.list + "/";
            link = <a href={urllink} dangerouslySetInnerHTML={{__html: props.eventdata.ObjectName}} ></a>	
		}

		if(objecttype === 'User')
		{		
			if(action === 'SHARE')
			{
				link ="";
				//link = "<a href=\"/list/" + item.listid + "\">" +  item.ObjectName +"</a>";		
			}else
			{
				//liste
				link ="";
				//link = "<a href=\"/list/" + item.ObjectID + "\/edit/" + item.ElementID  +  "/\">" +  "User-Stream" +"</a>";		
			}
		}

		var imageicon ="";
		
		switch(action) {
		case 2:
			imageicon = "https://publicfile.standard-cloud.com/icons/stream-delete.png";
			break;
		case 1:
			imageicon = "https://publicfile.standard-cloud.com/icons/stream-new-object.png";
			break;
		case 3:
		imageicon = "https://publicfile.standard-cloud.com/icons/stream-edit-object.png";
		break;
		case "Profile":
		imageicon = "https://publicfile.standard-cloud.com/icons/home.png";
		/*imageicon =""; */
		break;
		case "SHARE":
		imageicon = "https://publicfile.standard-cloud.com/icons/stream-user.png";
		break;
		default:
		imageicon = action;
			
		}
					
		var commenteditopencomponent = null;
		if(commenteditopen)
		{
			commenteditopencomponent = <DialogBox closedialog={()=>{setCommenteditopen(false);}} title="Beitrag bearbeiten">
			<div id="userevent-personalpostremove-box-div">
			<StartpageEditor fieldid="Startseite" value={props.eventdata.Text} onChange={(e)=>{setPersonalpost(e.target.innerHTML)}} />
			<div onClick={()=>{savepersonalpost()}}>
			SPEICHERN
			</div>	
			<div onClick={()=>{setCommenteditopen(false);}}>
			ABBRECHEN
			</div>	
			</div>
			</DialogBox>
		}

			  
				return (
<div className="topicbox"><table className="tbltopictemplate"><tbody><tr className="actionrow">
<td width="50px" className="picturestrem"  rowSpan="2">
<Link to={profilurl}><img  height="50px" width="50px" src={pictureurl} alt={username}/>
</Link></td><td className="streamusername">
<Link to={profilurl}>{username}</Link>
</td><td width="50px">
<div className="streamactionspan"><img alt="Aktion" src={imageicon} /></div>
</td><td className="stream-objectname-td">{link}</td><td className="streamtdright"><span className="strdate"> {date}&#160;<br/>
<span className="cmttimestamp">{daterelativetime}</span> &#160;</span></td></tr><tr>
<td colSpan="4" className="streamcontentbox"><div className="topicstream" onClick={handleShowObject} dangerouslySetInnerHTML={{__html: eventtext}}>
</div>{editbutton}</td></tr><tr><td></td><td className="streameventtoolbar" colSpan="5"><span onClick={handleLikeButtonClick} className="streamlike">
<img alt="Ich finde diese Aktion gut" src="https://publicfile.standard-cloud.com/icons/like.png"/>
(<span>{props.eventdata.likes})</span></span> | <span className="streamshare" onClick={handleShareButtonClick} >
<img alt="Ich m&ouml;chte diese Aktion teilen." src="https://publicfile.standard-cloud.com/icons/share.png" />
(<span>{props.eventdata.shares}</span>)</span>
</td>
</tr>
<tr><td></td><td className="streamsharetd" colSpan="4">
<div className="streamcommentsdiv">
<CommentList key={props.eventdata.ID} changelogid={props.eventdata.ID} comments={props.eventdata.comments} />
</div></td></tr>
<tr><td></td><td colSpan="4" width="50px" className="personstreamwidth">
<img className="streamcurrentuserpicture"  height="50" width="50" src={currentuserpicture} alt={currentusername} />
<textarea value={commenttext} onChange={onChangecommenttext}  className="_userstreamcomment"></textarea>
	{savebutton}
</td></tr></tbody></table>
{commenteditopencomponent}
</div>);
			  }
			  

export default withRouter(UserEvent);
