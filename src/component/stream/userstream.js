import React,{useEffect,memo} from 'react';
import Stream from './stream';
import {useDispatch,useSelector} from 'react-redux';
import {getUserStream} from '../../actions/stream'
import ToolTip from '../objectpreview/objectpreview';
function UserStream(props) {    
const dispatch = useDispatch();

const stream = useSelector(state => state.stream);
const user = useSelector(state=>state.user);
const lists = useSelector(state=>state.list);
const users = useSelector(state=>state.users);
    useEffect(()=>
    {
        var now = new Date();
        var ticks = now.getTime();
        dispatch(getUserStream(ticks));
        ToolTip.setToolTip();
    },[]);
    if(users.legth===0 || lists.legth===0 || users.legth===0)
    return;

    return(<Stream stream={stream} user={user} lists={lists} users={users}/>);
    
}


export default UserStream;

