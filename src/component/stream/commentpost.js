import moment from 'moment';
import React,{useState} from 'react';
import {BACKEND} from '../../stdsetup';
import {connect,useDispatch} from 'react-redux';
import {updateComment,removeComment} from '../../actions/stream';
import DialogBox from '../dialogbox/dialogbox';
import { Link } from "react-router-dom";
import Button from 'muicss/lib/react/button';
import './commentpost.css';
function CommentPost(props) {
const dispatch = useDispatch();
const [commenteditopen,setCommenteditopen] = useState(false);
const [commenttext,setcommenttext] = useState("");




	function updatetext(e)
	{
		setcommenttext(e.target.value);
	}
	function updateComment()
	{
		dispatch(updateComment(props.id,commenttext));
		setCommenteditopen(false);
	}
 function	removeComment()
	{
		dispatch(removeComment(this.props));
		setCommenteditopen(false);
	}
	
	
			 function handleEditComment()
			 {
				setCommenteditopen(true); 
			 }


				var dialogbox = null;
				if(commenteditopen)
				{
					dialogbox = <DialogBox visible={true} title="Kommentar bearbeiten">
					<div id="commentpost-editcommentbox-div">
					<textarea onChange={this.updatetext} defaultValue={this.state.commenttext}>
					</textarea>
					<div id="commentpost-footerbox-div">
					<Button onClick={this.updateComment}>
					Aktualisieren
					</Button>	
					<Button color="danger" onClick={this.removeComment}>
					Löschen
					</Button>
					</div>
					</div>
					</DialogBox>
				}


				  var commentdate = moment(props.dateshort).fromNow();
				  var editbutton='';
				  if(props.userid === props.user.userid)
				  {
					editbutton = <img onClick={this.handleEditComment} src="https://publicfile.standard-cloud.com/icons/editk.png" alt="Bearbeiten" className="editcomment"/>	  
				  }
				  
var picurl = BACKEND + "/file/" + props.userpicture + "/";	
var profileurl = "/profile/" + props.userid + "/";		  
		return (         
<div className="commentpostdiv">
<table><tbody>
	<tr><td className="commentprofiletd" rowSpan="2" width="50px"><a href={profileurl}><img alt={props.userid}  height="50px" width="50px" src={picurl}/></a>
</td><td colSpan="2" className="commenttd">
<span className="cmtdate">
{commentdate}
<span className="cmttimestamp" data-livestamp={props.dateshort}>
</span>
</span>
<Link to={profileurl}>{props.username}</Link>

</td>
</tr><tr><td>
<div data-commenttext={props.id} className="commentobject">{props.comment}</div>
</td><td className="commentposteditcol">{editbutton}</td></tr></tbody></table>
{dialogbox}
</div>
        );
		}
		



export default CommentPost;