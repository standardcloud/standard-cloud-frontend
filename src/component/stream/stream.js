import React from 'react';
import UserEvent from './userevent';

import './stream.css';
import './stream-mobil.css';
function Stream(props) {   
	if(!props.stream)
	return(<div>Lade Stream</div>);
	
	console.log(props.lists);
	console.log(props.user);
	console.log(props.users);

    const eventlist = props.stream.map(function(item) {    
		return (
	        <UserEvent key={item.id} users={props.users}  lists={props.lists} currentuser={props.user} eventdata={item}   />	
         );       
    });  		
		return (
			<div id="socialobjects">         
			<span id="stream-socialobjects-span">
			{eventlist}
			</span>
		</div>
        );
}	  

export default Stream;

