import React from 'react';
import Autosuggest from 'react-autosuggest';
import './listselector.css';
import { withRouter } from 'react-router-dom';
import {connect} from 'react-redux';
import Trans from '../../module/translatoinmanager/translationmanager';

class ListSelector extends React.Component {
	constructor(props) {
    super(props);
	this.state = {suggestions:[],objecttypname:"",value:""};
	this.handleConnectSearchTextChange = this.handleConnectSearchTextChange.bind(this);
	this.showAllLists = this.showAllLists.bind(this);
	this.getSuggestionValue = this.getSuggestionValue.bind(this);
	this.onChange = this.onChange.bind(this);
	this.onSuggestionSelected = this.onSuggestionSelected.bind(this);
	this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(this);
	this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(this);
  }


  handleConnectSearchTextChange(event) {
		    this.setState({objecttypname: event.target.value});            
}

getSuggestions(value)
{
	const inputValue = value.trim().toLowerCase();
	const inputLength = inputValue.length;
  
	return inputLength === 0 ? [] : this.props.lists.filter(lang =>
	  lang.name.toLowerCase().slice(0, inputLength) === inputValue
	);
}


getSuggestionValue(suggestion) {
	return suggestion.name;
  }
renderSuggestion(suggestion) {

	return (
	  <div className="selectoritembox"><strong>{suggestion.name}</strong><br/>{suggestion.description}</div>
	);
  }
showAllLists()
{

}
  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested()
  {
    this.setState({
      suggestions: []
    });
  }

onSuggestionsFetchRequested({ value })
{
	var self = this;
    this.setState({
      suggestions: self.getSuggestions(value)
    });
}
onSuggestionSelected(event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method })
{
    this.props.history.push("/list/" + suggestion.id + "/");
}
shouldRenderSuggestions(value) {
	return true;
  /* return value.trim().length > 0; */
}

onChange = (event,{newValue}) => {
    this.setState({
      value: newValue
		});
  };


		  render() {
			var value = this.state.value;
				// Autosuggest will pass through all these props to the input.
				const inputProps = {
				  placeholder: Trans.t("lists")+".. ",
				  value,
				  onChange: this.onChange
				};



					return (  
						<div className="listselectorbox">
							<div>
									<Autosuggest
                                    suggestions={this.state.suggestions}
									getSuggestionValue={this.getSuggestionValue}
									renderSuggestion={this.renderSuggestion}
			   				        shouldRenderSuggestions = {this.shouldRenderSuggestions}
									onSuggestionSelected ={this.onSuggestionSelected}
									onSuggestionsFetchRequested = {this.onSuggestionsFetchRequested}
									onSuggestionsClearRequested={this.onSuggestionsClearRequested}
									inputProps={inputProps}
								/>												
								
		   	</div></div>);
						
					

    }
}
	

function mapStateToProps({list}, ownProps) {
    return { lists:list};
  }
  
  export default  withRouter(connect(mapStateToProps)(ListSelector));