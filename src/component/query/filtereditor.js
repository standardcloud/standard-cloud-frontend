import React from 'react';
import querybuilder from 'jQuery-QueryBuilder';
import $ from 'jquery';
import './filtereditor.css';

class FilterEditor  extends React.Component {    
    constructor(props) {
        super(props);
		this.state = {};
		this.loadFilter = this.loadFilter.bind(this);
		this.myQueryFilter = React.createRef();
	}
		  
      componentDidMount()
      {
		if(this.props.fields.length !==0)
		{
		this.loadFilter();
		}  
	}
	componentDidUpdate(prevProps) {
		// Typical usage (don't forget to compare props):
		var self = this;

			self.loadFilter();

	  }

loadFilter()
	{	
	const node = this.myQueryFilter.current;
if(node===null)
return;


	$(node).queryBuilder('destroy'); 
	var filterfields = [];

					for(var i = 0; i < this.props.fields.length; i++)
					{
			
						var newfs = {};
						newfs.id = this.props.fields[i].id;
						newfs.field = this.props.fields[i].id;
						newfs.label = this.props.fields[i].name;
						switch(this.props.fields[i].fieldtype)
						{
							case 'Text':
							newfs.type= 'string';
							break;
							case 'Ganzzahl':
							newfs.type= 'integer';
							break;
							case 'Auswahlfeld':
							newfs.type= 'string';
							break;
							case 'Ja oder Nein':							
							newfs.type= 'boolean';
							break;
							case 'Kommazahl':
							newfs.type= 'double';
							break;
							case 'Datum und Uhrzeit':
							newfs.type= 'datetime';
							break;
							default:
							
							
						}							
						filterfields.push(newfs);
					}	
	
					$(node).queryBuilder({filters:filterfields,lang_code: 'de',allow_empty: true, rules: {
				condition: 'AND',
				rules:[]
			  }}); 
	}

render () {	
	if(this.props.fields.length ===0)
		return null;






		return (
			<div ref={this.myQueryFilter} id="queryeditor" >
			</div>
				);				
			}
}
export default FilterEditor;

