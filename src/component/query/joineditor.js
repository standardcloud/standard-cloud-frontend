import React from 'react';
import BT from '../button/stdbutton';
import Contentbox from '../../component/contentbox/contentbox';
class JoinEditor  extends React.Component {    
    constructor(props) {
		super(props);
		this.changeleftselect = this.changeleftselect.bind(this);
		this.addJoin = this.addJoin.bind(this);
		this.changejointype = this.changejointype.bind(this);
		this.deleteJoin = this.deleteJoin.bind(this);
       this.state = {leftlistid:"",jointype:"",rightlistid:""};
      }

addJoin()
{
	var join = {};
	join.left = this.state.leftlist;
	join.right = this.state.rightlist;
	join.jointype = this.state.jointype;
//	Dispatcher.action("join.add",join);
	
}
deleteJoin(join)
{
//Dispatcher.action("join.remove",join);
}
changeleftselect(e)
{	
	var leftjoin = {};
	leftjoin.id = e.target.value.substring(0,36);	
	leftjoin.name = e.target.value.substring(36);
	leftjoin.type = "list";
    this.setState({leftlist:leftjoin});

}

changerightselect(e)
{
	var rightjoin = {};
	rightjoin.id = e.target.value.substring(0,36);
	rightjoin.name = e.target.value.substring(36);
	rightjoin.type="list";
	this.setState({rightlist:rightjoin});
}

changejointype(e)
{
	this.setState({jointype:e.target.value});
}

render () {	


			var self =this;  
	  var  currentjoins = this.props.joins.map(function(join){
      return (
			<tr key={join.left.id}>
			<td>{join.left.name}</td> 
			<td><strong>{join.jointype}</strong></td> 
			<td>{join.right.name}</td>
			<td><div className="querydeletebutton" onClick={self.deleteJoin.bind(self, join)}  ><img alt="Löschen" src="https://publicfile.standard-cloud.com/icons/cancel.png"/></div></td>
			</tr>
			 );      
			});
			  
			  
			  
		var  renderselectedoptionsleft = [];
		this.props.lists.forEach(function(list){			
			if(list.selected)
		{
 var val = list.id + list.name 
 renderselectedoptionsleft.push (
	 
			<option value={val} key={list.id}>{list.name}</option>
				);       
	  }
	});
	
			var  renderselectedoptionsright =[];
			 this.props.lists.forEach(function(list){			
			if(list.selected)
		{
			var val = list.id + list.name
			renderselectedoptionsright.push (
	  
			<option value={val} key={list.id}>{list.name}</option>
				);       
	  }
    });


		return (
				<div className="joineditorbox"><br/>
				<Contentbox title="Listen verbinden">
				<div>
				<table className="stdtable">
				<thead><tr>
				<th>Liste</th><th>Typ</th><th>Liste</th><th></th></tr>
				</thead><tbody>
				{currentjoins}
				</tbody>
				</table>
				</div>
				
				<table className="stdtable">
				<tbody>
				<tr><td>
				<select onChange={this.changeleftselect}>
				<option value=""></option>				
			    {renderselectedoptionsleft} 
				</select>
				<select onChange={this.changejointype}>
				<option value=""></option>
				<option value="join">JOIN</option>
				<option value="leftjoin">LEFTJOIN</option>
				<option value="rightjoin">RIGHTJOIN</option>
				</select>
			    <select onChange={this.changerightselect}>
				<option value=""></option>				
			    {renderselectedoptionsright} 
				</select>
			    </td>
				<td><BT callbackfunction={this.addJoin} name="Verbindung hinzufügen" iconurl="https://publicfile.standard-cloud.com/icons/add.png" /></td>
				</tr>
				</tbody>
				</table>
				</Contentbox>
				</div>
				);
			  }

	}

export default JoinEditor;
