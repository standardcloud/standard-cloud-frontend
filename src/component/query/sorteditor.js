
import React from 'react';


class SortEditor  extends React.Component {    
    constructor(props) {
        super(props);
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleOrderChange = this.handleOrderChange.bind(this);
        this.state = {};
	  }
   handleFieldChange(e)
   {   

	var sort = {};
	sort.selectedfieldid = e.target.value;
    sort.order = this.props.sort.order;
    this.props.changesort(sort);
//	Dispatcher.action("sort.change",sort);
   }

   handleOrderChange(e)
   {   

	var sort = {};
	
	sort.selectedfieldid = this.props.sort.selectedfieldid;
    sort.order = e.target.value;
    this.props.changesort(sort);
//	Dispatcher.action("sort.change",sort);
   }

render () {	

        var self = this;
        if(self.props.fields.length===0)
        return null;

        var options = self.props.fields.map(function(field) {
            return (
                <option key={field.id} value={field.id}>
                    {field.name}
                </option>
            )
        });

		return (
			<div id="sorteditor">
           <select
                    value={this.props.sort.selectedfieldid} 
                    onChange={this.handleFieldChange}>
				<option  value="">                    
                </option>
                {options}
            </select>
			<select value={this.props.sort.order} onChange={this.handleOrderChange}>>
			<option value=""></option>
			<option value="AZ">A->Z</option>
			<option value="ZA">Z->A</option>
			</select>
			</div>
				);				
			  }		  
	}
export default SortEditor;