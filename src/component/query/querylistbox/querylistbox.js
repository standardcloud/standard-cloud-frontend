import React from 'react';
import { withRouter } from 'react-router-dom';
import './querylistbox.css';



class QueryListBox extends React.Component {
	constructor(props) {
	super(props);
	this.filterInput = React.createRef();
	this.handleQueryClick = this.handleQueryClick.bind(this);
	this.handleSearchTextChange = this.handleSearchTextChange.bind(this);
	this.state = {results: [],searchtext:""};
  }
			  
handleSearchTextChange(event) {
         this.setState({searchtext: event.target.value.toLowerCase()});             
 }
 /*
loadquerys()
{
	var selfload =this;
	if(window.location.href.indexOf("/listfilter/") > -1)
	{	
		    var list = window.location.href.match("listfilter/(.*)/");
			if(list !=null)
			{	
				QueryManager.getqueriesfiltered(list[1]).then(data=>
				{
				selfload.setState({results:data});		
				});
					
			}
	}		
	else
	{
		QueryManager.getmyqueries().then(data =>
				{
				selfload.setState({results:data});		
				});
	}
}
*/
handleQueryClick(queryid)
{
//	console.log(queryid);
	this.props.history.push("/query/" + queryid +"/");
}

componentDidMount() {
this.filterInput.focus(); 
}

render() {
 var selfrender = this;

			 var results = [];
			 this.props.data.forEach(function(query) {
					 var description = " - " + query.des;
	
        if(selfrender.state.searchtext !== "")
        {

            if(query.title.toLowerCase().indexOf(selfrender.state.searchtext)> -1 || query.des.toLowerCase().indexOf(selfrender.state.searchtext) > -1)
            {

            }else
            {
               return null;
            }          
        }


		results.push(
			<tr key={query.id} onClick={selfrender.handleQueryClick.bind(this,query.id)}>
            <td className="leftboxquery" >
			<span className="listnametext" dangerouslySetInnerHTML={{__html: query.title}}></span>			  
            <span  className="listdescription" dangerouslySetInnerHTML={{__html: description}} ></span>             
			</td>
       		 </tr> );  
                  
               });

	return (
				<div id="querylist-contentbox-div">
<input id="querysearchbox" value={this.state.searchtext}  ref={(input) => { this.filterInput=input;}}  onChange={this.handleSearchTextChange}  placeholder="Filter" type="search" autoComplete="off"/><br/>
<div id="querylistbox-box-div">
<table id="queryviewtable">
    <tbody>
{results}
    </tbody>
</table>
</div>
</div>

    );
	
}	  
  }

export default withRouter(QueryListBox);


