import React from 'react';
import "./fieldnameeditor.css";
class FieldNameEditor  extends React.Component {    
    constructor(props) {
        super(props);
        this.state = {};
      }

			  handlefieldnamechange(field,event)
			  {
				  field.queryfieldname = event.target.value;
				  //Dispatcher.action("queryfield.update",field); 				  
			  }

			  render() {	
		var self = this;			  
		var  renderfields = [];
		this.props.fields.forEach(function(field) {		
		if(field.selected)
		{
			renderfields.push(
		 <tr key={field.id}><td className="fieldnameeditor-fieldname-td">{field.name}</td><td><input type="text" onChange={self.handlefieldnamechange.bind(self, field)} value={field.queryfieldname} /></td></tr>
				);       
	  	}
    	});
		return (
				<div>
				<table id="queryfieldnametbl">
				<tbody>
				{renderfields}
				</tbody>
				</table>
				</div>
				);
			  }
			
	}

export default FieldNameEditor;
