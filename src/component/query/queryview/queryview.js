
import React from 'react';
import dialog from '../../dialog/stddialog';
import sortColumn from '../../list/sortcolumn';
import {BACKEND} from '../../../stdsetup';
import FieldFilter from '../../list/fieldfilter';
import '../../list/fieldfilter.css';
import './queryview.css';

class QueryView  extends React.Component {    
    constructor(props) {
super(props);
this.isfullscreen = false;
this.handleSearchTextChange = this.handleSearchTextChange.bind(this);
this.handleSortClick = this.handleSortClick.bind(this);
this.handleFilterClick = this.handleFilterClick.bind(this);
this.filtercallback = this.filtercallback.bind(this);
this.closefilterdialog = this.closefilterdialog.bind(this);
this.createfilterlist = this.createfilterlist.bind(this);
this.handleHideClick = this.handleHideClick.bind(this);
this.getcolumnnumber = this.getcolumnnumber.bind(this);
this.dofullscreen = this.dofullscreen.bind(this);
this.state = {invisiblefields:[],visiblecount:250,filterobjectids:[],filterfields:[],searchtext:"",serversearchtext:"",sort:{fieldid:"",order:""}};
this.infinitescroll = this.infinitescroll.bind(this);

      }
              handleSearchTextChange(e) {
                 this.setState({searchtext: e.target.value});                   
                }
                componentDidMount() {
                    var element = document.getElementById("queryview-box-div");
                    var searchbox = document.getElementById("listview-searchbox-div");
                     var self = this;
                     document.addEventListener("fullscreenchange", function(){ 
                       var isFullScreen = document.fullScreen || 
                       document.mozFullScreen || 
                       document.webkitIsFullScreen;
                       if(!isFullScreen)
                       {
                        self.isfullscreen = false;  
                        element.style.overflowY = "visible";  
                        const tableheaders = document.querySelectorAll('#fixtableheader th');
                       
                        if(window.location.href.indexOf("public") > 1 )    
                            {           
                                                                      
                           searchbox.style.top ="0";                           
                           tableheaders.forEach(t => {
                                t.style.top ="35px";
                             });
                        }else
                        {

                            searchbox.style.top ="50px";                           
                            tableheaders.forEach(t => {
                                 t.style.top ="80px";
                              });
                        }
                                  
                       
                    }else
                       {
                                searchbox.style.top ="0px";
                                const tableheaders = document.querySelectorAll('#fixtableheader th');
                                tableheaders.forEach(t => {
                                        t.style.top ="35px";                            
                                    });
                        }
                   });
                
                    this.infinitescroll();
                    document.getElementsByClassName("stdpage-region")[0].style.paddingTop = "0";
                }

                componentWillUnmount()
                    {
                        document.getElementsByClassName("stdpage-region")[0].style.paddingTop = "80px";
                    }

                infinitescroll()
                {
                    var self = this;
                    const lazyloadrows = document.getElementById('div-bottom-visible');
                    const config = {
                  rootMargin: '2000px 0px'
                    };
                
                let observer = new IntersectionObserver(
                    function(entry)
                    {
                             if (entry[0].intersectionRatio > 0) {
                                self.setState({visiblecount:self.state.visiblecount+250});
                                
                            
                           if(self.props.query.data.length < self.state.visiblecount && self.props.query.data.length !== 0)
                           {     
                               stopObserve(entry);
                           }
                      }
                   
                    }
                    , config);
                 
                observer.observe(lazyloadrows);
                
                function stopObserve(entry)
                {             
                    observer.unobserve(entry[0].target);
                }
                
                }

                
              closefilterdialog(field)
              {
                  var self = this;
                  var newfieldfilterlist = [];
                  for(let i=0;self.state.filterfields.length > i; i++)
                  {
                      var ob = self.state.filterfields[i];
                      if(ob.id!==field.id)
                      {
                       newfieldfilterlist.push(ob);
                      }                          
                  }
                this.setState({filterfields:newfieldfilterlist});


                var newfilters = Object.assign({}, this.state.filters);
                newfilters[field.id] = [];
                this.setState({filters:newfilters},this.createfilterlist);


              }


              handleSortClick(field)
              {   
                  var newsort = {};
                  if(field.id === this.state.sort.fieldid)
                  {
                    if(this.state.sort.order ==="asc")
                    {
                    newsort.order = "desc";
                    newsort.fieldid = field.id;   
                    }else
                    {
                    newsort.order = "asc";
                    newsort.fieldid = field.id;   
                    }

                  }else
                  {                  
                  newsort.order = "asc";
                  newsort.fieldid = field.id;                  
                  }
                  this.setState({sort:newsort});   
                sortColumn.clientsortcolumn(field,this.props.query.header,this.props.query.data,newsort.order);        
                        
              }
              createfilterlist()
              {
                  var self = this;
                var  newfilterlist = [];
                for(var fieldidobject in this.state.filters) {
                    newfilterlist.push(...self.state.filters[fieldidobject]);
                 }
                this.setState({filterobjectids:newfilterlist});
              }
            

handleFilterClick(e,field)
{
    var self = this;
    e.stopPropagation();

   var newfieldfilterlist = [];
   for(let i=0;self.state.filterfields.length > i; i++)
   {
       var ob = self.state.filterfields[i];
       if(ob.id!==field.id)
       {
        newfieldfilterlist.push(ob);
       }
           
   }
   newfieldfilterlist.push(field);
this.setState({filterfields:newfieldfilterlist});
}

filtercallback(fieldid,filterids)
{
  var newfilters = Object.assign({}, this.state.filters);
  newfilters[fieldid] = filterids;
  this.setState({filters:newfilters},this.createfilterlist);
}
              
handleHideClick = function(field)
{
  this.setState(prevState => ({
      invisiblefields: [...prevState.invisiblefields, field.id]
    }));
    dialog.okdialog(field.name,"wird ausgeblendet.");
}
handleShowFieldClick(fieldid)
{
  var fieldarray = [...this.state.invisiblefields]; 
  var index = fieldarray.indexOf(fieldid);
  if (index !== -1) {
      fieldarray.splice(index, 1);
    this.setState({invisiblefields: fieldarray});
  }
}

dofullscreen()
{
 var elem = document.getElementById("queryview-box-div");
 var searchbox = document.getElementById("listview-searchbox-div");
 var self = this;
      if(this.isfullscreen)
      {

          if (document.exitFullscreen) {
              document.exitFullscreen();
          } else if (document.mozCancelFullScreen) { /* Firefox */
              document.mozCancelFullScreen();
          } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
              document.webkitExitFullscreen();
          } else if (document.msExitFullscreen) { /* IE/Edge */
              document.msExitFullscreen();
          }
       //   elem.style.overflowY = "visible";
       searchbox.style.top = "50px";
          self.isfullscreen = false;;
      }else
      {
       // overflow-y: auto;
          elem.style.overflowY = "auto";
          searchbox.style.top = "0";
          if (elem.requestFullscreen) {
              elem.requestFullscreen();
          } else if (elem.mozRequestFullScreen) { /* Firefox */
              elem.mozRequestFullScreen();
          } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
              elem.webkitRequestFullscreen();
          } else if (elem.msRequestFullscreen) { /* IE/Edge */
              elem.msRequestFullscreen();
          }

          self.isfullscreen = true;
      }

}


              getcolumnnumber(fieldid)
              {
                for(let i=0;i<this.props.query.header.length;i++)
                {

                    if(fieldid === this.props.query.header[i].id)
                    {
                        return i;
                    }
                    
                }
                return 0;
              }

        

			  render() {
				      var selftd = this;
      /*      var fieldlen = this.props.listsetting.fields.length; old*/
       var fieldlen =0
            for(var xx=0;xx<this.props.query.header.length;xx++)
            {

                    fieldlen++;
             }

       
                /*header */
            var headertable =[]
            this.props.query.header.forEach(function(field,i) {


                if(!selftd.state.invisiblefields.includes(field.id))
                {
      

                        /*define css class*/
                        var cssclassarrow ="";
                        if(selftd.state.sort.fieldid === field.id)
                        {
                            if(selftd.state.sort.order ==="asc")
                            {
                                cssclassarrow="fa-arrow-up";                        
                            }else
                            {
                                cssclassarrow="fa-arrow-down";
                            }
                        }

                        
                        var fieldfilterdialog = null;
                    for(var ii=0;ii< selftd.state.filterfields.length;ii++)
                        {
                            if(field.id === selftd.state.filterfields[ii].id)
                            {
                                var indexnumber = selftd.getcolumnnumber(field.id) -1;
                                fieldfilterdialog = <FieldFilter closedialog={selftd.closefilterdialog.bind(this,field)} field={field} filtercallback={selftd.filtercallback} fieldindex={indexnumber}  objectresult={selftd.props.query.data} />
                            }
                        }


                        headertable.push(
                            <th key={field.id} className="sortable" onClick={selftd.handleSortClick.bind(this,field)}>
                                <div className="fieldbuttondiv">
                                <span className="listheadertbl" dangerouslySetInnerHTML={{__html: field.name}}></span><span className={cssclassarrow}></span>
                                <div title="Spalte verstecken" className="listview-hide-icon-div" onClick={selftd.handleHideClick.bind(this,field)}><img src="https://publicfile.standard-cloud.com/icons/hide_icon.png" alt="Spalte verstecken"/></div>
                                <div title="Spaltenfilter anzeigen" onClick={(e) => selftd.handleFilterClick(e,field)} className="filterbuttonlistview"><img alt="Filter" src="https://publicfile.standard-cloud.com/icons/filter_listview.png"/>
                                </div>
                            </div>
                            {fieldfilterdialog}
                    </th>
                    );  
                }
                 
               }); 
                

                /*header */


            var objectcount = 0;
            var tblentrys = [];
            for(var jj=0;jj<this.props.query.data.length;jj++)
            {
                var item = this.props.query.data[jj];
                var resultrow = [];
  
                     if(selftd.state.searchtext.length !==0)
                     {
                        var filteritem = item.toString().toLowerCase().replace(item[item.length-1],"");
                      if(selftd.state.searchtext.indexOf(' ') >= 0)
                      {                                     
                            var strarray = selftd.state.searchtext.split(" ");
                            for (var i2 = 0; i2 < strarray.length; i2++) 
                            {
                            if (filteritem.indexOf(strarray[i2].toLowerCase()) === -1 )
                                {                           
                                       continue;
                                } 
                            }
                      }else
                      {   
                            if (filteritem.indexOf(selftd.state.searchtext.toLowerCase()) === -1 )
                            {                           
                                continue;
                            }
                       }
                    }

                                        /*Suche läuft auch über die GUID */
                                        const objectid = item[fieldlen];
                                        if(selftd.state.filterobjectids.includes(objectid))
                                        {
                                            continue;
                                        }



                    /*Suche läuft auch über die GUID */
                for(let i=0;i<=fieldlen-1;i++)
                {
                    var val = item[i];
     
                    if(!this.state.invisiblefields.includes(selftd.props.query.header[i].id))
                    {
                        switch(selftd.props.query.header[i].fieldtypeid)
                        {
                            case "8787e054-7bb9-411a-8946-da658fbc8e9f":                      
                            if(val !=="")
                            {
                                val = "<img src=" + BACKEND +"/file/" + val +" />";
                            }
                            break;
                            default:
                        }                     
                        resultrow.push(<td key={i} dangerouslySetInnerHTML={{__html: val}}></td>);       
                    }          
                }
                objectcount++;
                if(this.state.visiblecount > objectcount)
                {
                tblentrys.push (
                    <tr key={objectid} >{resultrow}</tr>        
                );       
                }
    }
let loadingdiv = "";
if(selftd.props.loading)
{
     loadingdiv =<div id="bottomdivbox"><div className="loader"></div>Lade..</div>
}

var hiddenfiled = this.state.invisiblefields.map((fieldid)=>{
    var fieldname = selftd.props.query.header.find(f=>f.id===fieldid).name; 
    return(<div onClick={this.handleShowFieldClick.bind(this,fieldid)} className="invisiblefield-field-box" key={fieldid}><img alt="Anzeigen" src="https://publicfile.standard-cloud.com/icons/show_list.png"></img>{fieldname}</div>);
});


				return (
				<div id="queryview-box-div">
                                    <div id="listview-searchbox-div">
                                        <div><input id="listsearchbox" value={this.state.searchvalue}  onChange={this.handleSearchTextChange}  placeholder="Filter" type="search" autoComplete="off"/></div>                                   
                                        <div id="listfilderstd">{hiddenfiled}</div>
                                        <div className="tblsearchresultcount">{objectcount} Objekte</div>
                                        <div id="fullscreentd"><img className="fullscreen-icon" alt="Vollbild" src="https://publicfile.standard-cloud.com/icons/fullscreen.png" onClick={this.dofullscreen} /></div>    
                                    </div>         
                <table id="tbllistdataresult">
                <thead id="fixtableheader"><tr>
                {headertable}               
                </tr>
                </thead>
                <tbody>
                {tblentrys}
                </tbody>
                </table>
                {loadingdiv}
               <div id="div-bottom-visible"></div>
		        </div>
				);

    }
}

export default QueryView;
