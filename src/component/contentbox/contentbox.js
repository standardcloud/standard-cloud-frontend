import React from 'react';
import  './contentbox.css';
class ContentBox extends React.Component {
		  render() {
					return (  
	<div className="contentboxcontainer">
	<div className="contentcontainerheader"><h2 dangerouslySetInnerHTML={{__html: this.props.title}}></h2></div>
	<div className="ontentcontainertext">
	{this.props.children}
	</div>
	</div>
);			
			  }
			  }
export default ContentBox;
