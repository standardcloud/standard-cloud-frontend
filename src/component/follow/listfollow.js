import React from 'react';
import $ from 'jquery';
import {BACKEND} from '../../stdsetup';
import Button from 'muicss/lib/react/button';
class FollowList extends React.Component {
	constructor(props) {
    super(props);
   /* this.getfollowListdata = this.getfollowListdata.bind(this); */
    this.updatafollowListdata = this.updatafollowListdata.bind(this);
    this.onCheckboxAllChanged = this.onCheckboxAllChanged.bind(this);
    this.onCheckboxDelChanged = this.onCheckboxDelChanged.bind(this);
    this.onCheckboxAddChanged = this.onCheckboxAddChanged.bind(this);
    this.onCheckboxModChanged = this.onCheckboxModChanged.bind(this);
    this.state = {followdata:this.props.followdata};
  }



  onCheckboxAllChanged(e)
  {
    var newstate = this.state.followdata;
    if(e.target.checked)
    {
      newstate.add = true;
      newstate.del = true;
      newstate.mod = true;
    }else
    {
      newstate.add = false;
      newstate.del = false;
      newstate.mod = false;
    }
    this.setState({followdata:newstate},this.updatafollowListdata());
  } 
  



updatafollowListdata()
{
    var streamdata = {};
    streamdata.del =  this.state.followdata.del;
    streamdata.add =  this.state.followdata.add;
    streamdata.mod =  this.state.followdata.mod;
    streamdata.objecttype = '0';
    streamdata.listid = "listid";
    streamdata.objectid = this.props.listsetting.list.id;

        $.ajax({
        type: "POST",
        url: BACKEND + "/api/follow/addobject/",
        data: streamdata,
        xhrFields: {
          withCredentials: true
       },
        dataType: "json",
        success: function( data, textStatus, jqXHR) {
      
      
         },
        error: function(jqXHR, textStatus, errorThrown){
        console.log( textStatus);
        }
        
      }); 
}
onCheckboxDelChanged()
{
  var newstate = this.state.followdata;
  newstate.del = !newstate.del; 
  this.setState({followdata:newstate},this.updatafollowListdata());
}
onCheckboxAddChanged()
{
  var newstate = this.state.followdata;
  newstate.add = !newstate.add; 
  this.setState({followdata:newstate},this.updatafollowListdata());
}
onCheckboxModChanged()
{
  var newstate = this.state.followdata;
  newstate.mod = !newstate.mod; 
  this.setState({followdata:newstate},this.updatafollowListdata());
}
render() {

    var objectname = this.props.listsetting.objecttype.name;
    var listname = this.props.listsetting.list.name;  
return(<div className="followlist" onClick={this.closemodule}>
 <div className="stdcheckboxregion"><legend>Bei welchen Aktionen von <b> {objectname}  </b> aus der Liste <b>{listname} </b> 
 wollen Sie im Stream benachrichtigt werden?</legend>
 </div>
<br/>
<div className="stdcheckboxregion"><input className="checkbox" onChange={this.onCheckboxAllChanged}  type="checkbox" id="All" name="All" value="All" /><label htmlFor="All">Allen Änderungen</label></div>
<div className="stdcheckboxregion"><input className="checkbox" onChange={this.onCheckboxDelChanged} checked={this.state.followdata.del}  type="checkbox" name="del" id="del" value="del" /><label htmlFor="del">{objectname} wird gel&ouml;scht</label></div>
<div className="stdcheckboxregion"><input className="checkbox" onChange={this.onCheckboxAddChanged} checked={this.state.followdata.add}  type="checkbox" name="add" id="add" value="add" /><label htmlFor="add">{objectname} wird wird hinzugef&uuml;gt</label></div>
<div className="stdcheckboxregion"><input className="checkbox" onChange={this.onCheckboxModChanged}checked={this.state.followdata.mod} type="checkbox" name="mod" id="mod"  value="mod" /><label htmlFor="mod">{objectname} wird ge&auml;ndert</label></div>
  
  <div><br/>
  <Button  onClick={this.props.closemodule}>OK</Button>
    </div>
 </div>);
}	  
  }   

export default FollowList;