import React from 'react';
import $ from 'jquery';
import {BACKEND} from '../../stdsetup';
import Button from 'muicss/lib/react/button';
import './userfollow.css';
class FollowUser extends React.Component {
	constructor(props) {
    super(props);
    this.updatafollowUserdata = this.updatafollowUserdata.bind(this);
    this.onCheckboxAllChanged = this.onCheckboxAllChanged.bind(this);
    this.onCheckboxDelChanged = this.onCheckboxDelChanged.bind(this);
    this.onCheckboxAddChanged = this.onCheckboxAddChanged.bind(this);
    this.onCheckboxModChanged = this.onCheckboxModChanged.bind(this);
    this.state = {followdata:this.props.followdata};
  }



  onCheckboxAllChanged(e)
  {
    var newstate = this.state.followdata;
    if(e.target.checked)
    {
      newstate.add = true;
      newstate.del = true;
      newstate.mod = true;
      newstate.profilecomment = true;
      newstate.person =true;
      newstate.share = true;
    }else
    {
      newstate.add = false;
      newstate.del = false;
      newstate.mod = false;
      newstate.profilecomment = false;
      newstate.person =false;
      newstate.share = false;
    }
    this.setState({followdata:newstate},this.updatafollowUserdata());
  } 
  



updatafollowUserdata()
{
    var streamdata = {};
    streamdata.del =  this.state.followdata.del;
    streamdata.add =  this.state.followdata.add;
    streamdata.mod =  this.state.followdata.mod;
    streamdata.share =  this.state.followdata.share;
    streamdata.profilecomment =  this.state.followdata.profilecomment;
    streamdata.person =  this.state.followdata.person;
    streamdata.objecttype = '1';
    streamdata.listid = "listid";
    streamdata.objectid = this.props.user.id;

        $.ajax({
        type: "POST",
        url: BACKEND + "/api/follow/addobject/",
        data: streamdata,
        xhrFields: {
          withCredentials: true
       },
        dataType: "json",
        success: function( data, textStatus, jqXHR) {
      
      
         },
        error: function(jqXHR, textStatus, errorThrown){
        console.log( textStatus);
        }
        
      }); 
}
onCheckboxDelChanged()
{
  var newstate = this.state.followdata;
  newstate.del = !newstate.del; 
  this.setState({followdata:newstate},this.updatafollowUserdata());
}
onCheckboxAddChanged()
{
  var newstate = this.state.followdata;
  newstate.add = !newstate.add; 
  this.setState({followdata:newstate},this.updatafollowUserdata());
}
onCheckboxModChanged()
{
  var newstate = this.state.followdata;
  newstate.mod = !newstate.mod; 
  this.setState({followdata:newstate},this.updatafollowUserdata());
}
onCheckboxPersonChanged()
{
  var newstate = this.state.followdata;
  newstate.person = !newstate.person; 
  this.setState({followdata:newstate},this.updatafollowUserdata());
}
onCheckboxShareChanged()
{
  var newstate = this.state.followdata;
  newstate.share = !newstate.share; 
  this.setState({followdata:newstate},this.updatafollowUserdata());
}
onCheckboxCommentChanged()
{
  var newstate = this.state.followdata;
  newstate.profilecomment = !newstate.profilecomment; 
  this.setState({followdata:newstate},this.updatafollowUserdata());
}

render() {
    var username = this.props.user.name
  return(<div className="followlist" onClick={this.closemodule}>
 <div className="stdcheckboxregion">
 <legend>Bei welchen Aktivitäten von <b> {username}  </b> wollen Sie im Stream benachrichtigt werden?</legend>
 </div>
<br/>
<div className="stdcheckboxregion"><input className="checkbox" onChange={this.onCheckboxAllChanged}  type="checkbox" id="All" name="All" value="All" /><label htmlFor="All">Allen Änderungen</label></div>
<div className="stdcheckboxregion"><input className="checkbox" onChange={this.onCheckboxDelChanged} checked={this.state.followdata.del}  type="checkbox" name="del" id="del" value="del" /><label htmlFor="del">{username} löscht ein Objekt</label></div>
<div className="stdcheckboxregion"><input className="checkbox" onChange={this.onCheckboxAddChanged} checked={this.state.followdata.add}  type="checkbox" name="add" id="add" value="add" /><label htmlFor="add">{username} erzeugt ein Objekt</label></div>
<div className="stdcheckboxregion"><input className="checkbox" onChange={this.onCheckboxModChanged}checked={this.state.followdata.mod} type="checkbox" name="mod" id="mod"  value="mod" /><label htmlFor="mod">{username} bearbeitet ein Objekt</label></div>
<div className="stdcheckboxregion"><input className="checkbox" onChange={this.onCheckboxCommentChanged}checked={this.state.followdata.profilecomment} type="checkbox" name="mod" id="profilecomment"  value="profilecomment" /><label htmlFor="profilecomment">{username} kommentiert ein Objekt</label></div>
<div className="stdcheckboxregion"><input className="checkbox" onChange={this.onCheckboxShareChanged}checked={this.state.followdata.share} type="checkbox" name="share" id="share"  value="share" /><label htmlFor="share">{username} teilt eine Aktion</label></div>
<div className="stdcheckboxregion"><input className="checkbox" onChange={this.onCheckboxPersonChanged}checked={this.state.followdata.person} type="checkbox" name="person" id="person"  value="person" /><label htmlFor="person">{username}  veröffentlicht etwas auf der Startseite</label></div>
<div><br/>
  <Button  onClick={this.props.closemodule}>OK</Button>
    </div>
 </div>);
}	  
  }   

export default FollowUser;