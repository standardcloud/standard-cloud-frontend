import React from 'react';
import $ from 'jquery';
import {BACKEND} from '../../stdsetup';
import Button from 'muicss/lib/react/button';
import DialogBox from '../../component/dialogbox/dialogbox';
class FollowObject extends React.Component {
	constructor(props) {
    super(props);
    this.updatafollowObjectdata = this.updatafollowObjectdata.bind(this);
    this.onCheckboxAllChanged = this.onCheckboxAllChanged.bind(this);
    this.onCheckboxDelChanged = this.onCheckboxDelChanged.bind(this);
    this.onCheckboxAddChanged = this.onCheckboxAddChanged.bind(this);
    this.onCheckboxModChanged = this.onCheckboxModChanged.bind(this);
    this.state = {followdata:this.props.followdata};
  }



  onCheckboxAllChanged(e)
  {
    var newstate = this.state.followdata;
    if(e.target.checked)
    {
      newstate.add = true;
      newstate.del = true;
      newstate.mod = true;
    }else
    {
      newstate.add = false;
      newstate.del = false;
      newstate.mod = false;
    }
    this.setState({followdata:newstate},this.updatafollowObjectdata());
  } 
  




updatafollowObjectdata()
{
    var streamdata = {};
    streamdata.del =  this.state.followdata.del;
    streamdata.mod =  this.state.followdata.mod;
    streamdata.objecttype = '3';
    streamdata.listid = "listid";
    streamdata.objectid = this.props.objectid;

        $.ajax({
        type: "POST",
        url: BACKEND + "/api/follow/addobject/",
        data: streamdata,
        xhrFields: {
          withCredentials: true
       },
        dataType: "json",
        success: function( data, textStatus, jqXHR) {
      
      
         },
        error: function(jqXHR, textStatus, errorThrown){
        console.log( textStatus);
        }
        
      }); 
}
onCheckboxDelChanged()
{
  var newstate = this.state.followdata;
  newstate.del = !newstate.del; 
  this.setState({followdata:newstate},this.updatafollowObjectdata());
}
onCheckboxAddChanged()
{
  var newstate = this.state.followdata;
  newstate.add = !newstate.add; 
  this.setState({followdata:newstate},this.updatafollowObjectdata());
}
onCheckboxModChanged()
{
  var newstate = this.state.followdata;
  newstate.mod = !newstate.mod; 
  this.setState({followdata:newstate},this.updatafollowObjectdata());
}
render() {

    var objectname = this.props.listsetting.objecttype.name;
return(
<DialogBox title="Folgen" closedialog={this.props.closedialog}>
<div className="followlist">
 <div className="stdcheckboxregion"><legend>Bei welchen Aktionen wollen Sie im Stream benachrichtigt werden?</legend>
 </div>
<br/>
<div className="stdcheckboxregion"><input className="checkbox" onChange={this.onCheckboxAllChanged}  type="checkbox" id="All" name="All" value="All" /><label htmlFor="All">Allen Änderungen</label></div>
<div className="stdcheckboxregion"><input className="checkbox" onChange={this.onCheckboxDelChanged} checked={this.state.followdata.del}  type="checkbox" name="del" id="del" value="del" /><label htmlFor="del">{objectname} wird gel&ouml;scht</label></div>
<div className="stdcheckboxregion"><input className="checkbox" onChange={this.onCheckboxModChanged} checked={this.state.followdata.mod} type="checkbox" name="mod" id="mod"  value="mod" /><label htmlFor="mod">{objectname} wird ge&auml;ndert</label></div>
  
  <div><br/>
  <Button  onClick={this.props.closedialog}>OK</Button>
    </div>
 </div></DialogBox>);
}	  
  }   

export default FollowObject;