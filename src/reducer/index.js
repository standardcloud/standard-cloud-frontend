import { combineReducers } from "redux";
import UserReducer from "./user_reducer";
import UsersReducer from "./users_reducer";
import {reducer as notificationsReducer} from 'reapop';
import StreamReducer from "./stream_reducer";
import NavigationReducer from "./navigation_reducer";
import TimelineReducer from "./timeline_reducer";
import HistoryReducer from "./history_reducer";
import ListReducer from "./list_reducer";
import FieldReducer from "./field_reducer";
import AnalyticsReducer from "./analytics_reducer";
import ObjectReducer from "./object_reducer";
import ObjectTypeReducer from "./objecttype_reducer";
import QueryTypeReducer from "./query_reducer";

const rootReducer = combineReducers({
  user: UserReducer,
  users: UsersReducer,
  stream: StreamReducer,
  navigation: NavigationReducer,
  timeline: TimelineReducer,
  history: HistoryReducer,
  list: ListReducer,
  analytics: AnalyticsReducer,
  field: FieldReducer,
  object: ObjectReducer,
  objecttype: ObjectTypeReducer,
  query: QueryTypeReducer,
  notifications: notificationsReducer()
});

export default rootReducer;