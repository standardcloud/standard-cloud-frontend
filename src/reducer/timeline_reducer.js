import { SET_TIMELINE} from "../actions/actiontypes";
export default function(state = [], action) {
  switch (action.type) {
    case SET_TIMELINE:
    return action.payload;
    default:
      return state;
  }
}