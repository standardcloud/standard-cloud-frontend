import { SET_STREAM,SET_STREAM_ADDLIKE,SET_STREAM_ADDCOMMENT,SET_STREAM_UPDATECOMMENT,SET_STREAM_REMOVECOMMENT,SET_STREAM_ADD_PERSONALPOST} from "../actions/actiontypes";

export default function(state = [], action) {
  switch (action.type) {
    case SET_STREAM:
    return action.payload;
    case SET_STREAM_ADDLIKE:
    return getNewLikeAddedState(state,action.payload.content.ID);
   // return state;
    case SET_STREAM_ADDCOMMENT:
    return addComment(state,action.payload.content);
    case SET_STREAM_UPDATECOMMENT:
    return updateComment(state,action.payload.content);
    case SET_STREAM_REMOVECOMMENT:
    return removeComment(state,action.payload);
    case SET_STREAM_ADD_PERSONALPOST:
    return addPersonalPost(state,action.payload.content);
    default:
      return state;
  }
}

function getNewLikeAddedState(state, id)
{
  var mm = JSON.parse(JSON.stringify(state))
  for(let i=0;i<mm.length;i++)
  {
    if(mm[i].ID===id)
    {
      mm[i].LikesCount = mm[i].LikesCount +1; 
    }
  } 
  return mm;
}
function addComment(state,comment)
{
  var mm = JSON.parse(JSON.stringify(state))
  for(let i=0;i<mm.length;i++)
  {
    if(mm[i].ID===comment.changelogid)
    {
      mm[i].comments.push(comment);
    }
  } 
  return mm;  
}
function addPersonalPost(state,newpost)
{
  var mm = state.slice()
  //var mm = JSON.parse(JSON.stringify(state));
  mm.unshift(newpost);
  return mm;  
}

function updateComment(state,newcomment)
{
  var mm = JSON.parse(JSON.stringify(state))
  for(let i=0;i<mm.length;i++)
  {
    if(mm[i].ID===newcomment.changelogid)
    {
      for(var j=0;j< mm[i].comments.length;j++)
      {
        console.log(mm[i].comments[j].id);
        if(mm[i].comments[j].id === newcomment.id)
        {
          mm[i].comments[j].comment = newcomment.comment;
        }
      }
    }
  } 
  return mm;  
}

function removeComment(state,delcomment)
{
  var mm = JSON.parse(JSON.stringify(state))
  for(let i=0;i<mm.length;i++)
  {
    if(mm[i].ID===delcomment.changelogid)
    {
      for(var j=0;j< mm[i].comments.length;j++)
      {
        if(mm[i].comments[j].id === delcomment.id)
        {
          mm[i].comments.splice(j, 1);
          return mm; 
        }
      }      
    }
  } 
  
  return mm;  
}