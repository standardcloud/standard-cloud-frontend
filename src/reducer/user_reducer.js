import { SET_USER} from "../actions/actiontypes";

export default function(state = {profileurl:"",firstname:"",lastname:"",picturetext:"",img:"https://publicfile.standard-cloud.com/icons/anonymususer.jpg",isuseronline:false}, action) {
  switch (action.type) {
    case SET_USER:
    return action.payload;
    default:
      return state;
  }
}
