import { SET_ANALYTICS} from "../actions/actiontypes";
export default function(state = [], action) {
  switch (action.type) {
    case SET_ANALYTICS:
    return action.payload;
    default:
      return state;
  }
}