import { SET_LISTS} from "../actions/actiontypes";

export default function(state = [], action) {
  switch (action.type) {
    case SET_LISTS:
    return action.payload;
    default:
      return state;
  }
}
