import { SET_OBJECTS, SETMORE_OBJECTS,RESET_OBJECTS} from "../actions/actiontypes";
export default function(state = [], action) {
  switch (action.type) {
    case SET_OBJECTS:
    return { ...state, [action.listid]: action.payload };
    case SETMORE_OBJECTS:
    var newstate = {};

    newstate.meta = action.payload.meta;
    newstate.data = state[action.listid].data.concat(action.payload.data);

    return { ...state, [action.listid]: newstate };
    case RESET_OBJECTS:
    return [];
    default:
      return state;
  }
}