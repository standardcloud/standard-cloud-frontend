import DisplayObject from '../module/displayobject/stddisplayobject';
import { SET_HISTORY,UPDATE_HISTORY_OBJECT} from "../actions/actiontypes";
export default function(state = [], action) {
  switch (action.type) {
    case SET_HISTORY:
    return action.payload;
    case UPDATE_HISTORY_OBJECT:
    return UpdateObject(state,action.payload);
    default:
      return state;
  }
}

function UpdateObject(state,historyobject)
{
  var mm = JSON.parse(JSON.stringify(state))
  for(let i=0;i<mm.length;i++)
  {
    if(mm[i].objectid===historyobject.objectid)
    {
      mm[i].content = DisplayObject.getcontent(historyobject);
    }
  } 
  return mm;  

}