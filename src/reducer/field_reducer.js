import { SET_FIELDS, SET_FIELD, REMOVE_FIELD } from "../actions/actiontypes";

export default function(state = [], action) {
  switch (action.type) {
    case REMOVE_FIELD:
      return state.filter(field=> field.id !==action.payload.id); //removeField(state, action.payload);
    case SET_FIELD:
      return { ...state, [action.payload.data.id]: action.payload.data };
    case SET_FIELDS:
      return action.payload.content;
    default:
      return state;
  }
}

/*
function removeField(fieldsstate,field)
{
var newfieldlist = [];
  for(let i=0;i<fieldsstate.length;i++)
  {
    if(fieldsstate[i].id!==field.id)
    {
      newfieldlist.push(fieldsstate[i]);
    }
  }   
  return newfieldlist;  
}
*/