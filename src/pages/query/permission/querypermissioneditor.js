import React from 'react';
import { withRouter } from 'react-router-dom';
import {connect} from 'react-redux';
import {setNavigation} from '../../../actions/navigation';
import Permissionmanager from '../../../module/permissionmanager/stdpermissionmanager';
import dialog from '../../../component/dialog/stddialog';
import BT from '../../../component/button/stdbutton';
import AddQueryPermission from '../../../component/permission/querypermission/addquerypermission';
import Contentbox from '../../../component/contentbox/contentbox';
import Helpbox from '../../../component/helpbox/helpbox';
import style from './querypermissioneditor.module.css';

class QueryPermissionPage  extends React.Component {   
	constructor(props) {
    super(props);
    this.showAddKey = this.showAddKey.bind(this);
		this.state = {query:{title:"",description:"",permission:[],permissionlevels:[]},addkey:true}
		var nav = [{title:"Start",url:"/"},{title:"Abfragen",url:"/query/"},{title:"Abfragen hinzufügen"}];
		this.props.setNavigation(nav);
	  }
 

loadquerypermissionsettings()
{
  var selfquerysetting = this;
  var queryidurl = window.location.href;
	var testRE = queryidurl.match("/edit/(.*)/permission");
	var queryid = testRE[1];
	  Permissionmanager.getquerypermission(queryid).then(data =>
    {     
         if(data.state==='ok')
         {
              selfquerysetting.setState({query:data.content});            
         }
    });
}

deleteQueryPermission(query)
{
  var qsself = this;
    var queryidurl = window.location.href;
	var testRE = queryidurl.match("/edit/(.*)/permission");
	var queryid = testRE[1];
 
  Permissionmanager.removequerykey(query.keyid,queryid,query.rightid).then(data =>
  {
        if(data.state==="ok")
        {
            var newpermissions =[];
            var content = data.content;
            for(let i=0;i<qsself.state.query.permission.length;i++)
            { 
              var old = qsself.state.query.permission[i];
              if(content.queryid===queryid && content.rightid===old.rightid && content.keyid === old.keyid )
              {
                  
              }else
              {
                     newpermissions.push(old); 
              }
            }
            var qs = qsself.state.query;
            qs.permission = newpermissions;
            qsself.setState({query:qs});
            dialog.okdialog("Berechtigung", "wurde entfernt.");	
        }
  });

}
addQueryPermission(querypermissionresult)
{
            var qs = this.state.query;
            var newpem = qs.permission;         
            newpem.push(querypermissionresult.content);
            qs.permission = newpem;
            this.setState({query:qs});
            this.setState({addkey:true});
}

showAddKey()
{
  this.setState({addkey:false});
}
componentDidMount() {
this.loadquerypermissionsettings();
//Dispatcher.registerevent("querypermission.add",this.addQueryPermission);
}

render () {
 var selfrender = this;

      var tblentrys = this.state.query.permission.map(function(item) {
      var url = "/admin/permissionmgmt/key/" + item.keyid +"/edit/";
      return (
                <tr key={item.keyid}><td>{item.rightname}</td><td><a href={url}>{item.keyname}</a></td><td><img alt="Löschen" className="stdbuttonicon" onClick={selfrender.deleteQueryPermission.bind(selfrender, item)}  src="https://publicfile.standard-cloud.com/icons/delete.png"></img></td></tr>

      );       
    }); 

var addkey='';
if(!this.state.addkey)
{
  addkey = <Contentbox title="Berechtigung anfügen"><AddQueryPermission queryid={this.state.query.queryid} permissionlevels={this.state.query.permissionlevels}/></Contentbox>
}
var contentboxtitle= "Berechtigungen für " +this.state.query.title;

return (
        <div className="querypermissioneditor">
          <Contentbox title={contentboxtitle}>
         <h4 dangerouslySetInnerHTML={{__html: this.state.query.description}}></h4>
            <br/>
            <div>
             <table className="stdtable">
              <thead>
                <tr><th>Recht</th><th>Schlüssel</th><th className={style.width50}></th></tr>
                </thead>
              <tbody>
                {tblentrys}
            </tbody>
            </table><br/>
            {addkey}
            </div>   
         </Contentbox>
         <div id="querylistfooter">           
           <BT name="Schlüssel anfügen" callbackfunction={this.showAddKey} iconurl="https://publicfile.standard-cloud.com/icons/key.png" />  
               <BT url="https://standard-cloud.zendesk.com/hc/de"   name="Hilfe" iconurl="https://publicfile.standard-cloud.com/icons/help.png" />                       
        </div>
         <Helpbox title="Abfrageberechtigungen">
        Berechtigungen auf Abfragen..
        </Helpbox>
</div>
    );
	
}	  
}

  export default withRouter(connect(null,{setNavigation})(QueryPermissionPage));


