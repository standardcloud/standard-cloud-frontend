import React from 'react';
import BT from '../../component/button/stdbutton';
import { withRouter } from 'react-router-dom';
import QueryManager from '../../module/querymanager/stdquerymanager';
import Contentbox from '../../component/contentbox/contentbox';
import Helpbox from '../../component/helpbox/helpbox';
import {connect} from 'react-redux';
import {setNavigation} from '../../actions/navigation';
import './querylist.css';
import Trans from '../../module/translatoinmanager/translationmanager';


class Querylistpage extends React.Component {
	    constructor(props) {
    super(props);
		this.handleSearchTextChange = this.handleSearchTextChange.bind(this);
	this.state = {results: [],searchtext:""};
	var qq = Trans.t("queries");
	var nav = [{title:"Start",url:"/"},{title:qq}];
    this.props.setNavigation(nav,qq);
  }
			  
handleSearchTextChange(event) {
         this.setState({searchtext: event.target.value.toLowerCase()});             
 }
loadquerys()
{

	var selfload =this;
	if(window.location.href.indexOf("/listfilter/") > -1)
	{	
		    var list = window.location.href.match("listfilter/(.*)/");
			if(list !=null)
			{	
				QueryManager.getqueriesfiltered(list[1]).then(data=>
				{
					selfload.setState({results:data});		
				});
					
			}
	}		
	else
	{
		QueryManager.getmyqueries().then(data =>
				{
				selfload.setState({results:data});		
				});
	}
}

handleQueryClick(queryid)
{
	this.props.history.push("/query/" + queryid +"/");
}

componentDidMount() {
this.loadquerys();
}

render() {
 var selfrender = this;

			 var results = [];
			  this.state.results.forEach(function(query) {
				 var description = " - " + query.des;

        if(selfrender.state.searchtext !== "")
        {

            if(query.title.toLowerCase().indexOf(selfrender.state.searchtext)> -1 || query.des.toLowerCase().indexOf(selfrender.state.searchtext) > -1)
            {

            }else
            {
               return null;
            }          
        }


		results.push(
			<tr key={query.id} onClick={() => selfrender.handleQueryClick(query.id)}>
            <td className="leftboxquery" >
			<span className="listnametext" dangerouslySetInnerHTML={{__html: query.title}}></span>			  
            <span  className="listdescription" dangerouslySetInnerHTML={{__html: description}} ></span>             
			</td>
       		 </tr> );  
                  
			   });
			   
			   console.log(results);

	return (
        <div>
			<Contentbox title="Abfragen">
		
<input id="querysearchbox" value={this.state.searchtext}  onChange={this.handleSearchTextChange}  placeholder="Filter.." type="search" autoComplete="off"/><br/>
<table id="queryviewtable">
    <tbody>
{results}
    </tbody>
</table>

</Contentbox>
<div id="querylistfooter">
<BT url="/selectqueries" name="Abfrage auswählen & Einstellungn" iconurl="https://publicfile.standard-cloud.com/icons/addquery.png" />  
</div>
<Helpbox title="Liste an Abfragen">
Links sehen Sie alle Abfagen. Solle die Abfage nicht sichtbar sein, können Sie mit dem Button <strong>"Abfrage auswählen & Einstellungen"</strong> vordefinierte Abfragen hinzufügen und neue Abfragen erstellen.
</Helpbox>
</div>
    );
	
}	  
  }

export default withRouter(connect(null,{setNavigation})(Querylistpage));


