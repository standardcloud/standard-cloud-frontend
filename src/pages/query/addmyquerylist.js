import React from 'react';
import { withRouter } from 'react-router-dom';
import {connect} from 'react-redux';
import {setNavigation} from '../../actions/navigation'
import QueryManager from '../../module/querymanager/stdquerymanager';
import BT from '../../component/button/stdbutton';
import Contentbox from '../../component/contentbox/contentbox';
import dialog from '../../component/dialog/stddialog';
import Button from 'muicss/lib/react/button';
import DialogBox from '../../component/dialogbox/dialogbox';
import './addmyquerylist.css';
class Addquerylistpage  extends React.Component {   
	constructor(props) {
		super(props);
		this.handleSearchTextChange = this.handleSearchTextChange.bind(this);
		this.handleQueryClick = this.handleQueryClick.bind(this);
		this.permissionQuery = this.permissionQuery.bind(this);
		this.editQuery = this.editQuery.bind(this);
		this.viewQuery = this.viewQuery.bind(this);
		this.removeQuery = this.removeQuery.bind(this);
		this.finalremove = this.finalremove.bind(this);
		this.closedeletebox = this.closedeletebox.bind(this);
		this.state = {results: [],searchtext:"",deletequery:{}};
		var nav = [{title:"Start",url:"/"},{title:"Abfragen",url:"/query/"},{title:"Abfragen hinzufügen"}];
		this.props.setNavigation(nav,"Abfrage hinzufügen");
	  }


handleSearchTextChange(e) {
         this.setState({searchtext: e.target.value.toLowerCase()});             
}

loadquerys()
{
	var selfrender = this;
	QueryManager.getqueries().then(data =>
{
selfrender.setState({results:data});
});
}

handleQueryClick(queryid)
{

	this.props.history.push("/query/" + queryid +"/");
}

componentDidMount() {
this.loadquerys();
}

enableQuery(queryid)
{
	var old = this.state.results;
	var newarray = [];
	for(let i=0;i<old.length;i++)
	{
		var oldval = old[i];
		var add = {};
		if(oldval.id === queryid)
		{
         add = oldval;
		 add.myselectgedquery= !oldval.myselectgedquery;
		 QueryManager.setmyquery(queryid,add.myselectgedquery).then(data =>
			{
			
			});
		}else
		{
		add = oldval;			
		}
	newarray.push(add);
	}
	this.setState({results:newarray});
}

permissionQuery(queryid)
{
 this.props.history.push("/query/edit/" + queryid + "/permission/");
}

finalremove()
{
	
	var self =this;
	QueryManager.removequery(this.state.deletequery.id).then(ret=>
	{
		if(ret.state === "ok")
		{						
			dialog.okdialog("Abfrage", "wurde gel&ouml;scht.");	
			self.loadquerys();
			
		}else
		{
			dialog.errordialog("Abfrage", "konnte nicht gel&ouml;scht werden.");						
		}
		self.setState({deletequery:{}});
	});	
}

removeQuery(query)
{
	this.setState({deletequery:query});
}
editQuery(queryid)
{
 this.props.history.push("/query/edit/" + queryid + "/");
}

viewQuery(queryid)
{
 this.props.history.push("/query/" + queryid + "/");
}
closedeletebox()
{
	this.setState({deletequery:{}});
}

render() {
	
 var selfrender = this;
 var deletebox =null;
 if((this.state.deletequery.length > 0))
{ 
	var query = this.state.deletequery;
	
	var abfragetitle = "Abfrage " + query.title + " wirklich l&ouml;schen?";
 deletebox = <DialogBox closedialog={()=>{this.setState({deletequery:{}})}}   title={abfragetitle}>
<div className="addmyquerylist-deletedialogbox-div">{query.des}<br/><br/>
<Button onClick={this.closedeletebox}>Abbrechen</Button>
<Button color="danger" onClick={this.finalremove}>Löschen</Button>
</div>
	 </DialogBox>
}


             var querys = this.state.results.map(function(query) {
				
        if(selfrender.state.searchtext !=="")
        {

            if(query.title.toLowerCase().indexOf(selfrender.state.searchtext)> -1 || query.des.toLowerCase().indexOf(selfrender.state.searchtext) > -1)
            {

            }else
            {
               return null;
            }          
        }


                return (
            <tr key={query.id}><td  >
            <input type="checkbox" onChange={() => selfrender.enableQuery(query.id)} checked={query.myselectgedquery}  title="Abfrage zur Übersicht hinzufügen"/>
       		</td>
            <td onClick={() => selfrender.enableQuery(query.id)} title="Abfrage anzeigen" >
			  <div className="leftboxquery" >
                              <div className="queryheader">{query.title}</div>
                              <div  className="querydescription">{query.des}</div>
              </div>  
			</td>
			<td onClick={() => selfrender.viewQuery(query.id)} className="deletequery">
			<img src="https://publicfile.standard-cloud.com/icons/filter.png" title="Ansicht anzeigen" alt="Ansicht anzeigen" />
			</td>
			<td onClick={() => selfrender.editQuery(query.id)}  className="deletequery">
			<img src="https://publicfile.standard-cloud.com/icons/edit.png" title="Abfrage bearbeiten" alt="Abfrage bearbeiten" />
			</td>
            <td  onClick={() => selfrender.permissionQuery(query.id)} className="deletequery">
			<img src="https://publicfile.standard-cloud.com/icons/key.png" title="Berechtigungen bearbeiten" alt="Berechtigungen bearbeiten" />
			</td>
            <td onClick={() => selfrender.removeQuery(query)}  className="deletequery">
			<img src="https://publicfile.standard-cloud.com/icons/delete.png" title="Abfrage löschen" alt="Abfrage löschen" />
			</td>
        </tr> );  
                  
               });




	return (
	<div>
		{deletebox}
		<Contentbox title="Verfügbare Abfragen">

<input id="querysearchbox" placeholder="Filter" type="search" onChange={selfrender.handleSearchTextChange} autoComplete="off"/><br/>
<table id="addquerytomyquerytable">
<tbody data-bind="foreach: queries">
{querys}
</tbody>
</table>
</Contentbox>
<div id="querylistfooter">
<BT url="/queryeditor/" name="Neue Abfrage" iconurl="https://publicfile.standard-cloud.com/icons/add.png" />  
</div>

</div>
    );
	
}	  

}  

export default withRouter(connect(null,{setNavigation})(Addquerylistpage));

