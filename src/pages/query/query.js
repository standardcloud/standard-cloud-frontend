import React from 'react';
import { withRouter } from 'react-router-dom';
import QueryManager from '../../module/querymanager/stdquerymanager';
import {connect} from 'react-redux';
import {setNavigation} from '../../actions/navigation';
import QueryView from '../../component/query/queryview/queryview';

class Querypage extends React.Component {
  constructor(props) {
    super(props);
    this.loadData = this.loadData.bind(this);
	  this.state = {ispublicquery:false,query:{title:"",description:"",header:[],data:[]},loading:false};
   this.loadquerysettings();
}
        
loadquerysettings()
{
  var selfquerysetting = this;
  var queryid = selfquerysetting.props.match.params.id

  if(this.props.location.pathname.indexOf("publicquery") > -1)
  {
    
    QueryManager.getpublicquery(queryid).then(data=>
    {
        selfquerysetting.setState({ispublicquery:true});			      
        selfquerysetting.loadData(data);
    });
  }else
  {
    QueryManager.getquery(queryid).then(data =>
    {		
      if(data.state==="error")
      {
        selfquerysetting.props.history.push("/error/" + data.content.logid+ "/");
      }else
      {
        selfquerysetting.loadData(data);
      }
    });
   } 
	

}

loadData(data)
{

		var elem = document.createElement('textarea');
		elem.innerHTML = data.title;
		var decoded = elem.value;
		document.title = decoded;
  this.setState({query:data});
  this.setState({loading:false});
  if(window.location.href.indexOf("public") === -1 )
  {
   var nav = [{title:"Start",url:"/"},{title:"Abfragen",url:"/query/"},{title:data.title }];
  this.props.setNavigation(nav,data.title);
  }
  document.body.className = "stdui";
}


render () {
 var selfrender = this;
var isbulicquery;
if(this.state.ispublicquery)
{
  isbulicquery = "publicquery-div-box"
}
	return (  
		 <div className={isbulicquery}>           
            <QueryView loading={selfrender.state.loading} query={selfrender.state.query} />        
		 </div>
    );
	
}
}	  
export default withRouter(connect(null,{setNavigation})(Querypage));

