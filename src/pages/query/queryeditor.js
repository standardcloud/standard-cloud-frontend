
import React from 'react';
import { withRouter } from 'react-router-dom';
import QueryManager from '../../module/querymanager/stdquerymanager';
import {connect} from 'react-redux';
import {setNavigation} from '../../actions/navigation';
import FieldNameEditor from '../../component/query/fieldnameeditor';
import Contentbox from '../../component/contentbox/contentbox';
import ListManager from '../../module/listmanager/stdlistmanager';
import FieldManager from '../../module/fieldmanager/stdfieldmanager';
import SearchAbleDropdown from '../../component/searchabledropdown/searchabledropdown';
import JoinEditor from '../../component/query/joineditor';
import SortEditor from '../../component/query/sorteditor';
import FilterEditor from '../../component/query/filtereditor';
import dialog from '../../component/dialog/stddialog';
import $ from 'jquery';
import Helpbox from '../../component/helpbox/helpbox';

import BT from '../../component/button/stdbutton';
import './queryeditor.css';




class QueryEditorpage extends React.Component {
	constructor(props) {
		super(props);
		this.onChangeQueryName = this.onChangeQueryName.bind(this);
		this.onChangeQueryDescription = this.onChangeQueryDescription.bind(this);
		this.addlist = this.addlist.bind(this);
		this.removelist = this.removelist.bind(this);
		this.saveQuery = this.saveQuery.bind(this);
		this.changesort = this.changesort.bind(this);
		this.onChangeObjectlimit = this.onChangeObjectlimit.bind(this);
		this.onChangePublicQueryName = this.onChangePublicQueryName.bind(this);
		this.onChangePublicQuery = this.onChangePublicQuery.bind(this);
		this.state = {querydescription:"",queryname:"",queryid:"",filters:"",lists:[],availablefields:[],fields:[],joins:[],allowpublicquery:false,publicquery:false,publicqueryname:"", sort:{order:"",selectedfieldid:""},objectlimit:""};
		var nav = [{title:"Start",url:"/"},{title:"Abfragen",url:"/query/"},{title:"Abfrageeditor"}];
		this.props.setNavigation(nav,"Abfrage bearbeiten");
		this.myQueryFilterRef = React.createRef();
	  }

componentDidMount() {
this.initComonent();
}

changesort(changesort)
{
	this.setState({sort:changesort});
}
removejoin(join)
{
      var oldjoins = this.state.joins;
       var newjoins = [];
      for(var j=0;j<oldjoins.length;j++ )
      {         
          if(oldjoins[j].left.id !== join.left.id)
          {
              newjoins.push(oldjoins[j]);
          }
      }
 this.setState({joins:newjoins});
}

addjoin(newjoin)
{
	var joins = this.state.joins;
	joins.push(newjoin);
	this.setState({joins:joins});
}

fieldupdate(field)
{
	// var newFields = Helper.replaceObject(field,this.state.fields);
	// this.setState({fields:newFields});
}
checkloadquery()
{
var selfquery = this;
	if(window.location.toString().indexOf("queryeditor") ===-1 )
	{
			var queryid = window.location.href.match("query/edit/(.*)/");
			if(queryid !=null)
			{
				selfquery.loadquery(queryid[1]);
			}
	}
}

loadquery(queryid)
{
	var qsself = this;
	QueryManager.getquerysetting(queryid).then(data =>
	{
	  qsself.setState({queryname:data.content.title});
	  qsself.setState({querydescription:data.content.description});
	  qsself.setState({queryid:data.content.queryid});
      qsself.setState({joins:data.content.join});
      qsself.setState({sort:data.content.sort});
	  qsself.setState({objectlimit:data.content.objectlimit});
	  
			var lists = qsself.state.lists;
	        var listen = [];

		for(var j=0;j<lists.length;j++)
		{
			var el = lists[j];

			for(var f=0;f<data.content.selectedlist.length;f++)
			{
				if(data.content.selectedlist[f] === el.id)
				{
					el.selected = true;
				}
			}
			listen.push(el);
		}
		qsself.setState({lists:listen});

		var felder = [];
		for(let i=0;i<data.content.selectedfield.length;i++)
		{
			var field = data.content.selectedfield[i];
			var newfield = {};
			newfield.id = field.id;
			newfield.name = field.name;
			newfield.objecttypeid = field.objecttypeid;
			newfield.queryfieldname = field.name;
            newfield.selected = true;
			newfield.order = i;
			felder.push(newfield);
		}

		qsself.setState({fields:felder});
		$( "#sortable" ).sortable().disableSelection();
        $("#queryeditor").queryBuilder('setRules',data.content.filter );

    /* Public Query */
    		qsself.setState({allowpublicquery: data.content.allowpublicquery});
        qsself.setState({publicquery: data.content.publicquery});
        qsself.setState({publicqueryname: data.content.publicqueryname});

	});
}

addlist(element)
{
	var lists = this.state.lists;
	var listen = [];
		for(var j=0;j<lists.length;j++)
		{
			var el = lists[j];
			if(el.id === element.id)
			{
					el.selected = true;
			}
			listen.push(el);
		}
		this.setState({lists:listen});
		this.getfields(element);

}

removelist(element)
{
	var lists = this.state.lists;
	var listen = [];
		for(var j=0;j<lists.length;j++)
		{
			var el = lists[j];
			if(el.id === element.id)
			{
					el.selected = false;
			}
			listen.push(el);
		}
		this.setState({lists:listen});
		this.removefields(element);

}

getfields(list)
{
	var fieldsself = this;
	FieldManager.getfield4list(list.id).then(result=>
					{
					var newfields = fieldsself.state.fields;
					 for(var j=0;j<result.length;j++)
					 {
						 var add = true;
						 for(var k=0;k<newfields.length;k++)
						 {
							 if(newfields[k].id === result[j].id)
							 {
								 add = false;
							 }
						 }
						 if(add)
						 {
							result[j].queryfieldname = result[j].name;
							result[j].selected = true;
						  newfields.push(result[j]);
						 }
					 }

					fieldsself.setState({fields:newfields});
					$( "#sortable" ).sortable().disableSelection();
					});
}

removefields(list)
{
	var fieldsself = this;
	FieldManager.getfield4list(list.id).then(result=>
					{
					var newfields = [];
					for(let i=0;i<fieldsself.state.fields.length;i++)
					{
							var remove = false;
							for(var j=0;j<result.length;j++)
							{
								if(fieldsself.state.fields[i].id === result[j].id)
								{
									remove = true;
								}
						    }
						    if(remove===false)
							{
								newfields.push(fieldsself.state.fields[i]);
							}
					}
					fieldsself.setState({fields:newfields});
					$( "#sortable" ).sortable().disableSelection();
					});
}

initComonent()
{
	var selflists = this;


	/* Loading Lists */
	var listen = [];
   ListManager.getlists(function(lists)
	{
		for(var j=0;j<lists.length;j++)
		{
			var el = lists[j];
			el.selected = false;
		    el.id = el.listid;
			listen.push(el);
		}
		selflists.setState({lists:listen});
		selflists.checkloadquery();
	});

	/* End Loading Lists */


}
handleRemoveField(field)
{
	var newfieldlist = [];
	var oldfieldlist = this.state.fields;
	for(let i=0;i<oldfieldlist.length;i++)
	{
		if(oldfieldlist[i].id !== field.id)
		{
			newfieldlist.push(oldfieldlist[i]);
		}else
		{
			oldfieldlist[i].selected = false;
			newfieldlist.push(oldfieldlist[i]);
		}
	}
	this.setState({fields:newfieldlist});

}
saveQuery()
{
	var title = this.state.queryname;
	var description = this.state.querydescription;
	var queryid = this.state.queryid;
  var publicquery = this.state.publicquery;
  var publicqueryname = this.state.publicqueryname;
    var objectlimit = this.state.objectlimit;
   var sort = this.state.sort;

	/*Listen speichern */
var selectedlists = [];
		for(var j=0;j<this.state.lists.length;j++)
		{
			var el = this.state.lists[j];
			if(el.selected)
			{
					selectedlists.push(el);
			}
		}
		/*Sort fields */
		var ul = document.getElementById("sortable");
	    var items = ul.getElementsByTagName("li");
		var sortedfields = [];
		var self = this;
		for(var fieldcount=0;fieldcount<this.state.fields.length; fieldcount++)
		{

			for (var i = 0; i < items.length; ++i) {
				if(self.state.fields[fieldcount].id === $(items[i]).attr("data-fieldid") )
				{
					var sorteditem = self.state.fields[fieldcount];
					sorteditem.order = i;
					sortedfields.push(sorteditem);

				}
			}
		}
		self.setState({fields: sortedfields});



		/*End sort fields */


	var joins = this.state.joins;
	var filters = $("#queryeditor").queryBuilder('getSQL', 'question_mark');
	var filterjson = $("#queryeditor").queryBuilder('getRules', { get_flags: true });
	QueryManager.newquery(queryid,title,description,selectedlists,this.state.fields,joins,filters,filterjson,publicquery,publicqueryname,sort,objectlimit,function(data)
	{
		if(data.state==="ok")
		{
			window.location = '/query/' + data.content.queryid + '/'
		}else
		{
			dialog.errordialog("Abfrage", data.content.msg);
		}
	});

}

onChangePublicQueryName(e)
{
	this.setState({publicqueryname: e.target.value});
}
onChangeQueryName(e)
{
	this.setState({queryname: e.target.value});
}
onChangeQueryDescription(e)
{
	this.setState({querydescription: e.target.value});
}
onChangeObjectlimit(e)
{
	this.setState({objectlimit: e.target.value});
}
onChangePublicQuery(e)
{
   this.setState({publicquery: !this.state.publicquery});
}

render () {

	var joinedit ='';

	var countselectedlists=0;
	for(var j=0;j< this.state.lists.length;j++)
	{
		if(this.state.lists[j].selected ===true)
		{
			countselectedlists++;
		}
	}

	if(countselectedlists > 1)
	{
		joinedit = <JoinEditor lists={this.state.lists} joins={this.state.joins} />
	}

var selfrenderfield = this;
	var  renderfields = this.state.fields.map(function(field) {
	if(field.selected)
	{
      return (
		 <li key={field.id} data-inuse="true" data-fieldid={field.id} className="ui-state-default">{field.name}<img alt="Löschen" onClick={selfrenderfield.handleRemoveField.bind(selfrenderfield, field)} className="imgdeletefield" src="https://publicfile.standard-cloud.com/icons/delete_klein.png" /></li>
      );
	}
	return null;
    });
    var publicquerycheckbox='';
    var publicqueryconfig='';
	var hostname = window.location.hostname;
    if(this.state.publicquery)
    {
		publicqueryconfig =   <div><br/><h3>Urls</h3><div>https://{hostname}/publicquery/<input id="publicqueryname" type="text" onChange={this.onChangePublicQueryName} value={this.state.publicqueryname}/>/ <strong>und</strong><br/>https://{hostname}/publicquery/{this.state.queryid}/</div></div>
    }


	if(this.state.allowpublicquery)
      {
       
        publicquerycheckbox= <Contentbox title="Public Query"><div className="queryeditor-contentbox-div"><div><input type="checkbox" id="ispublicquery" onChange={this.onChangePublicQuery} checked={this.state.publicquery} /> <span>  <label htmlFor="ispublicquery"> Diese Abfrage im Internet ohne Authentifizierung veröffentlichen.</label></span></div>{publicqueryconfig}</div></Contentbox>
      }


	var permissionbutton ='';

	if(!this.state.queryid==="")
	{
permissionbutton = <BT url="permission/"name="Berechtigungen" iconurl="https://publicfile.standard-cloud.com/icons/key.png" />
	}


		return (
	<div>
				<Contentbox title="Name & Beschreibung">
		<input id="qtitle" placeholder="Name" type="text" onChange={this.onChangeQueryName} value={this.state.queryname}/><br/>
		<textarea id="tbDescription" placeholder="Beschreibung" onChange={this.onChangeQueryDescription} value={this.state.querydescription} rows="4" >
		</textarea>
		<br/>
		</Contentbox><br/>
		<Contentbox title="Listen">
	<SearchAbleDropdown onselect={this.addlist} onremove={this.removelist} objects={this.state.lists}/>
</Contentbox><br/>

	<Contentbox title="Felder sortieren (Drag&Drop)">
			<div>
			<ul id="sortable" >
			{renderfields}
			</ul>
			</div>
			</Contentbox><br/>
			<Contentbox title="Feldnamen">
	<div>
	<FieldNameEditor fields={this.state.fields} />
		{joinedit}
	</div>
	</Contentbox><br/>

			<Contentbox title="Filter">
		<FilterEditor fields={this.state.fields} />
		</Contentbox><br/>
		<Contentbox title="Sortierung">
	<div>
	<SortEditor fields={this.state.fields} changesort={this.changesort} sort={this.state.sort}/>
	</div>
	</Contentbox><br/>
	<Contentbox title="Limit">
	<div className="queryeditor-contentbox-div">
	Maximal <input id="iobjectlimit" onChange={this.onChangeObjectlimit} name="iobjectlimit" value={this.state.objectlimit} /> Objekte anzeigen
	</div>
	</Contentbox>
	
      {publicquerycheckbox}

	<div id="queryeditor-footer-div">
		<BT callbackfunction={this.saveQuery} name="Abfrage speichern" iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
		{permissionbutton}
		</div>
		<Helpbox title="Hilfe">
		</Helpbox>
	</div>
        );

		}
	}
	export default withRouter(connect(null,{setNavigation})(QueryEditorpage));




