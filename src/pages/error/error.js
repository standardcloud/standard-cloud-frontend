
import {connect} from 'react-redux';
import {setNavigation} from '../../actions/navigation';
import React from 'react';
import logger from '../../module/logger/stdlogger';
import Support from '../../module/support/stdsupport';
import BT from '../../component/button/stdbutton';
import Dialog from '../../component/dialog/stddialog';
import HelpBox from '../../component/helpbox/helpbox';
import Trans from '../../module/translatoinmanager/translationmanager';
import ContentBox from '../../component/contentbox/contentbox';
import './error.css';


class ErrorPage extends React.Component {
	constructor(props) {
	super(props);
	this.state = {log:[],searchterm:""};
	this.loadlogs = this.loadlogs.bind(this);
	this.clickback = this.clickback.bind(this);
	this.contactsupport = this.contactsupport.bind(this);
	var nav = [{title:"Start",url:"/"},{title:"Error"}];
	this.props.setNavigation(nav,"Error");
}


componentDidMount() {
	this.loadlogs();
}

loadlogs()
{
	var logself = this;
	logger.getlog(this.props.match.params.logid).then(data=>
	{
		if(data.state==="ok")
		{
		logself.setState({log:data.content});	
		}
	});
}
clickback()
{
	window.history.back();
}
contactsupport()
{
Support.sendticket(this.state.log.requestid,this.state.log.msg,this.state.log.logid).then(data=>
{
	Dialog.ok("Ticket", "wurde übermittelt.");
});
}

render() {
		return (
	<div>
		<ContentBox title="Error">
		<table id="errorpage-table">
		<tbody>
			<tr>
<td>LogID</td><td>{this.state.log.logid}</td></tr>
<tr>
<td>RequestID</td><td>{this.state.log.requestid}</td>
				</tr>
				<tr>
<td>UserID</td><td>{this.state.log.userid}</td>
				</tr>
				<tr>
<td>Date</td><td>{this.state.log.date}</td>
				</tr>
 				<tr>
<td>Fehler</td><td>{this.state.log.msg}</td>
				</tr>
				</tbody>
		</table>
			</ContentBox>
			<div id="errorpage-button-div">
		       <BT  callbackfunction={this.clickback} name={Trans.t("back")}  iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
	           <BT  callbackfunction={this.contactsupport} name="Contact Support"   iconurl="https://publicfile.standard-cloud.com/icons/support.png" />
			</div>
			<HelpBox title="Help">
			test
			</HelpBox>
</div>
        );

		}
}



export default connect(null,{setNavigation})(ErrorPage);

