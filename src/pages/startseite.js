import React,{useState} from 'react';
import UserStream from '../component/stream/userstream';
import {useDispatch} from 'react-redux';
import {setNavigation} from '../actions/navigation';
import Startpagcenter from '../component/startpage/startpagcenter';
import Analyticswidget from '../component/analyticswidget/analyticswidget';
import StartpageEditor from '../component/texteditor/startpageeditor';
import Trans from '../module/translatoinmanager/translationmanager';
import {addPersonalPost} from '../actions/stream';
import Autolinker from 'autolinker';
import './startseite.css';
import './startseite-mobil.css';

function Startseite(props) {    
    const dispatch = useDispatch();
    let nav = [{title:"Start",url:"/"}];
    const [personalposttext,setPersonalposttext] = useState("");
    const [resetstring,setResetstring] = useState("");
    const [showaddpost,setShowaddpost] = useState(false);
    const screenwidth = document.body.offsetWidth;
    dispatch(setNavigation(nav, window.company + " - Start"));
    document.body.scrollTop = document.documentElement.scrollTop = 0;

 function personalPostTextUpdate(e)
  {
  setPersonalposttext(e.target.innerHTML);
  }

  function postPersonalPost()
  {
   var linkedtext = Autolinker.link(personalposttext);
   dispatch(addPersonalPost(linkedtext));
   setPersonalposttext("");
   setResetstring("");
  }


        var personalpost = null;
        var cssheight = "nocss";
        var addpersonalpostbutton = null;
        var analytics = null;   

        if(showaddpost)
        {
          addpersonalpostbutton = <div  onClick={()=> setShowaddpost(!showaddpost)} id="addpersonpostbutton-disable">-</div>;
          cssheight ="startseite-reducestreamheight"
          personalpost = <div id="startseite-addpersonalpostbox-div"><StartpageEditor fieldid="Startseite" value={resetstring} onChange={personalPostTextUpdate} />
                           <div onClick={postPersonalPost} id="addpersonpostbutton"><div>{Trans.t("publishpost")}</div></div></div>
        }else
        {
          addpersonalpostbutton = <div onClick={()=> setShowaddpost(!showaddpost)} id="addpersonpostbutton"><div>+</div></div>
        }


        if(screenwidth > 1500)
        {
          analytics = <div id="start-analytics"> <Analyticswidget /> </div>

          personalpost =  <div id="start-userstream" className={cssheight}>
          {addpersonalpostbutton}
          {personalpost}
          <UserStream />
          </div>
        }

        return (
            <div id="startseite">
            {analytics}
           <div id="start-middle-buttons">
          <Startpagcenter/>  
          </div> 
          {personalpost}     
         </div>
        );
      }
      Startseite.whyDidYouRender = true
export default Startseite;

