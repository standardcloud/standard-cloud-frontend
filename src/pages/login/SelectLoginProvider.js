import React,{useState,useEffect} from 'react';
import StdAuthenticationManager from '../../module/authenticationmanager/stdauthenticationmanager';
import StdDialog from '../../component/dialog/stddialog';
import { withRouter } from 'react-router-dom';
import {connect,useDispatch} from 'react-redux';
import {setNavigation} from '../../actions/navigation';
import styles from './SelectLoginProvider.module.css';

function SelectLoginProviderPage(props) {
    const [providers,setProviders] = useState([]);
    const dispatch = useDispatch();
    const nav = [{title:"Start",url:"/"}];
    dispatch(setNavigation(nav,"Login-Provider auswählen"));
      
useEffect(()=>
{
function loadproviders()
{
   StdAuthenticationManager.authenticationproviderlist().then(data=>
    {
        if(data.state==="ok")
        {
                if(data.providers.length ===1)
                {
                   props.history.push(data.providers[0].url);
                }else
                {
                  setProviders(data.providers);
                }
        }else
        {
              StdDialog.errordialog("AuthenticationProviderSettings", data.msg);	 
        }
    });  
}
loadproviders();
},[]);

function handleUserClick(provider)
{
    props.history.push(provider.url);
}

      let results = providers.map(function(provider) {
            var icon = "https://publicfile.standard-cloud.com/icons/" + provider.icon;
                return (
                <div key={provider.url} onClick={() => handleUserClick(provider)} >
                <div><img alt={provider.name} src={icon} /></div>    
                    <div><h1 className={styles.loginnamefield}>{provider.name}</h1></div>           	
                </div>
			 );                  
            });

	return (
          <div className={styles.authenticationbox}>
		          {results}
            <div>

	</div>
 </div>      
    );
	

}
export default withRouter(SelectLoginProviderPage);