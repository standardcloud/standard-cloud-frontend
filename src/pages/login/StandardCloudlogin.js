import React,{useState,useEffect} from 'react';
import StdButton from '../../component/button/stdbutton';
import StdDialog from '../../component/dialog/stddialog';
import StdAuthenticationmanager from '../../module/authenticationmanager/stdauthenticationmanager';
import {setNavigation} from '../../actions/navigation';
import {useDispatch} from 'react-redux';
import { setUser} from "../../actions/user";
import styles from './StandardCloudlogin.module.css';

function Loginpage(props) {
    const dispatch = useDispatch();  
    const [changevalue,setChangeValue] = useState(false);
    const [autologin,setAutologin] = useState(false);
    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");
    const nav = [{title:"Start",url:"/"},{title:"Login-Provider",url:"/auth/"},{title:"Standard-Cloud",url:"/auth/standard-cloud"}];
    dispatch(setNavigation(nav,"Standard-Cloud"));
    

 useEffect(()=>
  {
let slider = true;
//document.addEventListener("keypress", keydownfunction);
if(window.location.href.indexOf("musterfirma") !== -1)
{
	if(email.length ===0)
	{
		setEmail("Max.Mustermann@standard-cloud.com");
    setPassword("Start123");
    setChangeValue(true);
    standardcloudlogin();
	}
}


async function startslider()
{
  if(slider ===false)
  return;

  const images = [
  { src:'https://publicfile.standard-cloud.com/login/images/wegwasser.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/strand1.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/update3.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/update6.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/herbst3.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/herbst4.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/herbst5.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/herbst6.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/countryside2.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/countryside.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/hillside.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/green.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/land.jpg' },	     
  { src:'https://publicfile.standard-cloud.com/login/images/way.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/weg.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/alaska.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/beach.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/lake2.jpg' },	
  { src:'https://publicfile.standard-cloud.com/login/images/lake.jpg' }, 	 
  { src:'https://publicfile.standard-cloud.com/login/images/natur.jpg' }	,
  { src:'https://publicfile.standard-cloud.com/login/images/wasser.jpg' },	
  { src:'https://publicfile.standard-cloud.com/login/images/australien.jpg' },	 
  { src:'https://publicfile.standard-cloud.com/login/images/strand2.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/holz.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/meer.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/girl.jpg' }, 	  
  { src:'https://publicfile.standard-cloud.com/login/images/opa.jpg' }, 	 		 
  { src:'https://publicfile.standard-cloud.com/login/images/bluelake.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/bluelake.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/banff.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/way.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/yellow.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/blumen.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/frucht.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/china.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/wald.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/berlin2.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/berlin8.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/berlin3.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/berlin13.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/berlin12.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/berlin4.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/berlin7.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/berlin6.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/berlin10.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/berlin9.jpg' },
  { src:'https://publicfile.standard-cloud.com/login/images/berlin5.jpg' }
  ];
  
  for(let i=0;i<images.length;i++)
  {
    setImage(images[i].src);
    await sleep(30000);
    if(slider===false)
    {
      return;
    }
  }
}

const keydownfunction = (e) =>
{
  if(e.which === 13) {
    standardcloudlogin();
   }
}
     

function setImage(imageurl)
{
  if(slider ===false)
{
  document.body.style.backgroundImage = "";
  document.body.style.backgroundSize= "";
}else
{
document.body.style.backgroundImage = "url("+ imageurl +")";
document.body.style.backgroundSize= "100%";
}
}

startslider();

      return () =>{
        slider= false;
        document.body.style.backgroundImage = "";
        document.body.style.backgroundSize= "";
        document.removeEventListener("keypress", keydownfunction, false);        
      };
    },[]);
  



async function sleep(ms) {
   return new Promise(resolve => setTimeout(resolve, ms));
}

function changeEmail(e)
{
setChangeValue(true);
setEmail(e.target.value);
}
function changePassword(e)
{
  setChangeValue(true);
  setPassword(e.target.value);
}

function changeAutologin(e)
{
  setChangeValue(true);
  setAutologin(!autologin);
}

function standardcloudlogin()
{
  if(email.length < 2)
  {
    StdDialog.errordialog("Anmeldung","Bitte E-Mail nennen.");
    return;
  }
    if(password.length < 2)
  {
    StdDialog.errordialog("Anmeldung","Passwort erforderlich.");
    return;
  }


   
  StdAuthenticationmanager.standardcloudlogin(email,password,autologin,function(returndata)
    {
      if(returndata.state==="ok")
      {            
       dispatch(setUser(returndata.content.userdata.content)); 
       props.history.push('/');
      }else
      {
        StdDialog.errordialog("Anmeldung","leider nicht erfolgreich.");
      }
    });
}


	var savebutton = <StdButton disabled={true} name="Login" iconurl="https://publicfile.standard-cloud.com/icons/login.png" />
	if(changevalue)
	{
		savebutton = <StdButton  name="Login" callbackfunction={standardcloudlogin} iconurl="https://publicfile.standard-cloud.com/icons/login.png" />
	}


	return (
          <div className={styles.loginbox}>
        <div>
			    <div className={styles.textboxgroup}>      
                    <input  id="loginboxemal" required className="stdtextbox" size="30" maxLength="60" type="text" onChange={changeEmail} value={email} />
                    <span className="texthighlight"></span>
                    <span className="textbar"></span>
                    <label>E-Mail</label>
			    </div>
                	<div className={styles.textboxgroup}>   
                    <input required name="Password"  id="loginboxpassword" className="stdtextbox"  type="password" size="30" maxLength="60" onChange={changePassword} value={password}/>
                    <span className="texthighlight"></span>
                    <span className="textbar"></span>
                    <label>Passwort</label>
	</div>
   </div><br/>
<div id="ceckboxregion" >
	              <div className="stdcheckboxregion">
                            <input onChange={changeAutologin} id="logincheckbox" checked={autologin}  name="checkbox" type="checkbox"/>
                            <label  htmlFor="logincheckbox"><strong>Autologin</strong></label>
                </div> 
</div>
			<div>
		<br/>
		<div id="loginbutton">
        {savebutton}
	</div>
	</div>
 </div>   
    );	
}	  
Loginpage.whyDidYouRender = true
export default Loginpage;