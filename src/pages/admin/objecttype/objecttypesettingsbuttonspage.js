import React from 'react';
import BT from '../../../component/button/stdbutton';
import ObjectTypeManager from '../../../module/objecttypemanager/stdobjecttypemanager';
import dialog from '../../../component/dialog/stddialog';
import Dialogbox from '../../../component/dialogbox/dialogbox';
import Button from 'muicss/lib/react/button';
import {connect} from 'react-redux';
import {setNavigation} from '../../../actions/navigation';
import {loadObjecttypes} from '../../../actions/objecttype';
class ObjecttypeSettingPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {deleteobjecttype:false};
		this.deleteObjecttype = this.deleteObjecttype.bind(this);
		this.DeleteObjecttype = this.DeleteObjecttype.bind(this);
		this.props.loadObjecttypes();
		if(this.props.objecttype)
		{
		var title = this.props.objecttype.name + " Einstellungen";
		var nav = [{title:"Start",url:"/"},{title:"Administration", url:"/admin/"},{title:"Listenvorlagen",url:"/admin/objecttype/"},{title:title}];
		this.props.setNavigation(nav,title);
		}
		}
	  
DeleteObjecttype(objecttypeid) {
	var self = this;
	ObjectTypeManager.deleteobjecttype(objecttypeid).then(returnobject =>
{
		if(returnobject.state ==="ok")
		{
        dialog.okdialog('Listenvorlage',"wurde gelöscht.");
		self.props.loadObjecttypes();
		self.props.history.push("/admin/objecttype/");
						
		}else
		{
            dialog.errordialog('Listenvorlage',"konnte nicht gelöscht werden.");
		}
});
}
	  
deleteObjecttype()
{
	this.setState({deleteobjecttype:true});

}
	  
render () {
	var self = this;
	if(!this.props.objecttype)
	{
		return(<div>Lade.</div>);
	}
	var dialog =null;
	if(this.state.deleteobjecttype)
	{
		dialog = <Dialogbox title="Listenvorlage löschen?" closedialog={()=>{this.setState({deleteobjecttype:false});}}>
		<div id="objecttypesettingspage-removedialog-div">
		Nach dem Lösungen dieses Obejekttyp sind alle Daten, Listen inkl. Inhalt unwiederruflich gelöscht. <br/>
  <Button onClick={()=>{self.DeleteObjecttype(self.props.objecttype.id)}} color="danger">Listenvorlage löschen</Button>
  <Button onClick={()=>{this.setState({deleteobjecttype:false});}}>Zurück</Button>
  </div>
		</Dialogbox>
	}


		return (
	<div>
		{dialog}
		<BT count="9" url="generalsetting/"  name="Basiseinstellungen" iconurl="https://publicfile.standard-cloud.com/icons/settings.png" />
        <BT url="permission/"  name="Berechtigungen" iconurl="https://publicfile.standard-cloud.com/icons/lock.png" />
		<BT  url="view/"  name="Ansichten" iconurl="https://publicfile.standard-cloud.com/icons/view.png" />
		<BT callbackfunction={this.deleteObjecttype}  name="Listenvorlage l&ouml;schen" iconurl="https://publicfile.standard-cloud.com/icons/delete.png" />
	    <BT reload url="https://standard-cloud.zendesk.com/hc/de/articles/115001526429" name="Hilfe" iconurl="https://publicfile.standard-cloud.com/icons/help.png" />
	   
	</div>
        );
		}

  }


function mapStateToProps({objecttype}, ownProps) {
	return {   objecttype: objecttype.find(o =>o.id ===ownProps.match.params.objecttypeid)};
  }


export default connect(mapStateToProps,{setNavigation,loadObjecttypes})(ObjecttypeSettingPage);