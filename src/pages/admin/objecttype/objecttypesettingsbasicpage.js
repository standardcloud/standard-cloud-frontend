
import React from 'react';
import BT from '../../../component/button/stdbutton';
import ObjectTypemanager from '../../../module/objecttypemanager/stdobjecttypemanager';
import Dialog from '../../../component/dialog/stddialog';
import Contentbox from '../../../component/contentbox/contentbox';
import Helpbox from '../../../component/helpbox/helpbox';
import {setNavigation} from '../../../actions/navigation';
import './objecttypesettingsbasicpage.css';

import {connect} from 'react-redux';

class ObjectTypeBasicSettings extends React.Component {
    constructor(props) {
		super(props);
		this.state = {name:"",nameplural:"",enableanalytics:false,changedvalue:false,fields:[]};
		this.loadobjecttypesettings = this.loadobjecttypesettings.bind(this);
		this.saveBasicSettings = this.saveBasicSettings.bind(this);
		this.changeAnalytics = this.changeAnalytics.bind(this);
		this.changeObjectTypeName = this.changeObjectTypeName.bind(this);
		this.changeObjectTypePluralName = this.changeObjectTypePluralName.bind(this);
		this.changeObjectTypeDescription = this.changeObjectTypeDescription.bind(this);
		this.changeNavigation = this.changeNavigation.bind(this);
		var nav = [{title:"Start",url:"/"},{title:"Administration", url:"/admin/"},{title:"Listenvorlagen",url:"/admin/objecttype/"},{title:"Basiseinstellungen"}];
		this.props.setNavigation(nav,"Listenvorlage Basiseinstellungen");
	}

	  

componentDidMount() {
this.loadobjecttypesettings();
}

loadobjecttypesettings()
{

		var selfobjecttypesettings = this;
        this.setState({objecttypeid:this.props.match.params.objecttypeid});
		ObjectTypemanager.getobjecttypesettings(this.props.match.params.objecttypeid).then(settings =>
			{          
				selfobjecttypesettings.setState({id:settings.id});
				selfobjecttypesettings.setState({name:settings.name});
				selfobjecttypesettings.setState({nameplural:settings.nameplural});
				selfobjecttypesettings.setState({description:settings.description});            
				selfobjecttypesettings.setState({enableanalytics:settings.enableanalytics	});  	
				selfobjecttypesettings.setState({fields:settings.fields}); 
		     	selfobjecttypesettings.setState({navigationfieldid:settings.navigationfieldid}); 	
				selfobjecttypesettings.setState({changedvalue:false});	
			});
}
saveBasicSettings()
{
	ObjectTypemanager.setobjecttypesettings(this.state.id,this.state.name,this.state.nameplural,this.state.description,
this.state.enableanalytics,this.state.navigationfieldid).then(returndata =>
{
	if(returndata.state ==="ok")
	{
		Dialog.okdialog("Basiseinstellungen", "erfolgreich gespeichert.");
	}
});
this.setState({changedvalue:false});
}

changeAnalytics(e)
{
this.setState({changedvalue:true});
this.setState({enableanalytics:!this.state.enableanalytics});
}


changeObjectTypeName(e)
{
this.setState({changedvalue:true});
this.setState({name:e.target.value});
}
changeObjectTypePluralName(e)
{
this.setState({changedvalue:true});
this.setState({nameplural:e.target.value});
}

changeObjectTypeDescription(e)
{
this.setState({changedvalue:true});
this.setState({description:e.target.value});
}

changeNavigation(nav)
{
	this.setState({changedvalue:true});
	console.log(nav.target.value);
	this.setState({navigationfieldid:nav.target.value});
}

render () { 
    var selfrender = this;
	var savebutton = <BT disabled={true} name="Speichern" iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
	if(this.state.changedvalue)
	{
		savebutton = <BT name="Speichern" callbackfunction={this.saveBasicSettings} iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
	}
	  var options = selfrender.state.fields.map(function(field) {
            return (
            <option key={field.id} value={field.id}>
                    {field.name}
            </option>
            )
        });
var windowstitle = "Einstellungen für " + this.state.name;
		return (

        <div>
			 <Contentbox title={windowstitle}>
				 <div id="objecttypesettingsbasicpageinnerbox">
			<div id="listnamebox">
				<div className="textboxgroup">      
				<input required className="stdtextbox" type = "text" name= "tbName" onChange={this.changeObjectTypeName} value={this.state.name} />
				<span className="texthighlight"></span>
				<span className="textbar"></span>
				<label>Name</label>
			</div>
			<div className="textboxgroup">      
				<input required className="stdtextbox" type = "text" name= "tbListName" onChange={this.changeObjectTypePluralName} value={this.state.nameplural} />
				<span className="texthighlight"></span>
				<span className="textbar"></span>
				<label>Plural(Mehrzahl)</label>
			</div>
			
			</div>
			<div className="textboxgroup">      
				<textarea required name= "tbListDescription" className="stdtextbox" onChange={this.changeObjectTypeDescription} value={this.state.description}></textarea>
				<span className="texthighlight"></span>
				<span className="textbar"></span>
				<label>Beschreibung</label>
			</div>
	
		 Bestimmen Sie das Feld für die Navigation (oben Breadcrumb)<br/>
		 <select value={this.state.navigationfieldid} onChange={this.changeNavigation}>
				<option  value="">                    
                </option>
                {options}
            </select>
			
		
          <br/> <br/> 
						<div className="stdcheckboxregion">
                            <input id="enableanalytics"  checked={this.state.enableanalytics} type="checkbox" value="enableanalytics"   className="checkbox"/>
                            <label onClick={this.changeAnalytics}  htmlFor="enableanalytics"> <strong>Analytics</strong>  erlauben.</label>
     	             </div><br/>    	
					</div>
			</Contentbox>
		<div id="objecttypesettingsbasicpagefooter">
	 {savebutton}
			 <BT name="Hilfe" url="https://standard-cloud.zendesk.com/hc/de/articles/115002416889-Listeneinstellungen" iconurl="https://publicfile.standard-cloud.com/icons/help.png" />
	
	</div>
				 <Helpbox title="Hilfe">
Hier können Sie grundlegende Einstellungen vornehmen.
</Helpbox>

			 </div>
        );

		}
	}

export default connect(null,{ setNavigation })(ObjectTypeBasicSettings);


