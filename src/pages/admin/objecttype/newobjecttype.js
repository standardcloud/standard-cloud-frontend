import BT from '../../../component/button/stdbutton';
import dialog from '../../../component/dialog/stddialog';
import Helpbox from  '../../../component/helpbox/helpbox';
import Contentbox from  '../../../component/contentbox/contentbox';
import ObjectTypemanager from '../../../module/objecttypemanager/stdobjecttypemanager';
import React from 'react';
import { withRouter } from 'react-router-dom';

class NewObjectTypePage extends React.Component {
	    constructor(props) {
		super(props);
		this.changeName = this.changeName.bind(this);
		this.changeNameplural = this.changeNameplural.bind(this);
		this.changeDescription = this.changeDescription.bind(this);
		this.clicknewobjecttype = this.clicknewobjecttype.bind(this);		
	    this.state = {name:"",nameplural:"",description:""};
  }
			  
			  changeName(event) {
         this.setState({name: event.target.value});             
				}
		changeNameplural(event) {
         this.setState({nameplural: event.target.value});             
                }
				
				changeDescription(event) {
         this.setState({description: event.target.value});             
                }


clicknewobjecttype()
{
	var self = this;
	ObjectTypemanager.new(this.state.name,this.state.nameplural,this.state.description).then(data=>
  {
	  if(data.state ==='ok')
	  {
		self.props.history.push('/admin/objecttype/modify/' + data.content.id + "/");
	  }else
	  {
	dialog.errordialog("Liste", "konnte nicht erzeugt werden.");	
	  }
	
	  
  }); 

}


clickback()
{
window.history.back();
}

render() {

var savebutton = null;

if(this.state.nameplural.length > 3 &&this.state.name.length > 3 && this.state.description.length > 3)
{
savebutton = <BT callbackfunction={this.clicknewobjecttype} name="Listenvorlage erzeugen" iconurl="https://publicfile.standard-cloud.com/icons/save.png" />  
}else
{
	savebutton = <BT callbackfunction={this.clickobjecttype} disabled={true} name="Listenvorlage erzeugen" iconurl="https://publicfile.standard-cloud.com/icons/save.png" />  
}
	return (
     <div id="newlist_page">
   <Contentbox title="Neue Listenvorlage">
		 <div id="newlist_content">
        			<div id="listnamebox"><br/>
			    <div className="textboxgroup">      
				<input required className="stdtextbox" type = "text" name= "tbListName" onChange={this.changeName} value={this.state.name} />
				<span className="texthighlight"></span>
				<span className="textbar"></span>
				<label>Name</label>
			    </div>
				<div className="textboxgroup">      
				<input required className="stdtextbox" type = "text" name= "tbListName" onChange={this.changeNameplural} value={this.state.nameplural} />
				<span className="texthighlight"></span>
				<span className="textbar"></span>
				<label>Name-Plural</label>
			    </div>
			</div>
			<div id="listdescriptionbox">
			
			<div className="textboxgroup">      
				<textarea required name= "tbListDescription" className="stdtextbox" onChange={this.changeDescription} value={this.state.description}></textarea>
				<span className="texthighlight"></span>
				<span className="textbar"></span>
				<label>Beschreibung</label>
			</div>
			</div>
					<br/>

   </div><br/>
        </Contentbox>
		<div>
<Helpbox title="Hilfe">
		Eine Listenvorlage benötigt zunächst einen <strong>Namen</strong> - z.b. Aufgabe. Dieser sollte immer in der Singular/Einzahl Form benannt werden.<br/><br/>Danach folgt die <strong>Plural/Mehrzahl</strong> - z.B. Aufgaben. <br/>Später werden diese Begriffe im Newsstream erscheinen - z.B. "Es wurde eine Aufgabe hinzugefügt".
		<br/><br/>
		Im Anschluss muss eine <strong>minimale Beschreibung</strong> erfolgen. Anhand des Namen und der Beschreibung sollte der Listenersteller <br/>(Administration -> Neue Liste)<br/>wissen, was er darin ablegen kann.<br/><br/>Alle Einstellungen
		können nachträglich geändert werden.
</Helpbox>
       
<div id="newlistfooter">
       {savebutton} 
			 <BT  callbackfunction={this.clickback} name="Abbrechen"   iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
			 <BT url="https://standard-cloud.zendesk.com/hc/de/articles/202566021-Erstellen-einer-Liste"  name="Hilfe" iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
      </div>
				</div>
	</div>
    );
	
}	  
}
    
export default withRouter(NewObjectTypePage);
