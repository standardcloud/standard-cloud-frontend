
import React from 'react';
import StdButton from '../../../component/button/stdbutton';
import Stdobjecttypemanager from '../../../module/objecttypemanager/stdobjecttypemanager';
import Dialog from '../../../component/dialog/stddialog';
import Helpbox from '../../../component/helpbox/helpbox';
import $ from 'jquery';
import {connect} from 'react-redux';
import Contentbox from '../../../component/contentbox/contentbox';
import DialogBox from '../../../component/dialogbox/dialogbox';
import Button from 'muicss/lib/react/button';
import {setNavigation} from '../../../actions/navigation';
import {BACKEND} from '../../../stdsetup';
import Addfieldtoobjecttype from '../../../component/objecttype/addfieldtoobjecttype/addfieldtoobjecttype';
import Trans from '../../../module/translatoinmanager/translationmanager';
import './singleobjecttype.css';

class SingleObjectTypePage extends React.Component {
	constructor(props) {
    super(props);
	this.state = {objecttype:{fields:[],lists:[]},deletefield:{},deletefielddialog:false};
	this.handleClickDeleteField = this.handleClickDeleteField.bind(this);
	this.delfield = this.delfield.bind(this);
	this.followbuttonclick = this.followbuttonclick.bind(this);
	this.showfollowdialog = this.showfollowdialog.bind(this);
	this.loadobjecttype = this.loadobjecttype.bind(this);

	
  }
  loadobjecttype()
  {


	var fs = this;
		Stdobjecttypemanager.getobjecttypesettings(this.props.match.params.objecttypeid).then(data=>
	{		
			fs.setState({objecttype:data});	
			document.body.scrollTop = document.documentElement.scrollTop = 0;
		
			var title =  data.name + " "+Trans.t("edit");
			var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"), url:"/admin/"},{title:Trans.t("listtemplates"),url:"/admin/objecttype/"},{title:title}];
			fs.props.setNavigation(nav,title);
	});

  }



  		componentDidMount() {
		 this.loadobjecttype();
				}


handleClickField(field)
{
	this.props.history.push("/admin/field/modify/" + field.id + "/");
}
delfield()
{
	var selfdel = this;
	var field = this.state.deletefield;
	Stdobjecttypemanager.deletefield(field.id,this.props.match.params.objecttypeid).then(data=>
	{		
		selfdel.setState({deletefielddialog:false});
		selfdel.setState({deletefield:{}});
			if(data.state==="ok")
				{
					Dialog.ok(field.name,"wurde gelöscht");

					var objecttype = selfdel.state.objecttype;
					var newfields =[];
					for(let i=0;i<objecttype.fields.length;i++)
						{
							if(objecttype.fields[i].id !== field.id)
								{
								newfields.push(objecttype.fields[i]);
								}
						}
					objecttype.fields = newfields;
					selfdel.setState({objecttype:objecttype});
				}else
				{
					Dialog.error(field.name,"konnte nicht gelöscht werden.");
				}	
		
	});
}

showfollowdialog(dialoghtml)
{
var self = this;
$.Zebra_Dialog(dialoghtml, {
    'type':     'question',
    'title':    'Folgen',
    'buttons':  [
                    {caption: 'Speichern', callback: function() {

  var streamdata = {};
  streamdata.del = $('#del').prop('checked') ;
  streamdata.add = $('#add').prop('checked');
  streamdata.mod = $('#mod').prop('checked');
 streamdata.objecttype = '2';
  streamdata.objectid = self.state.objecttype.id;
 /* 
  var countselectededit = 0;

  if(streamdata.del === true)
  {
  countselectededit++;
  }
  if(streamdata.add === true)
  {
  countselectededit++;
  }
  if(streamdata.mod === true)
  {
  countselectededit++;
  }
*/
$.ajax({
  type: "POST",
  url: BACKEND + "/api/follow/addobject/",
  data: streamdata,
  dataType: "json",
  success: function( data, textStatus, jqXHR) {
	Dialog.okdialog("Folgen", "Sie erhalten nun bei sämtlichen &Auuml;nderungen einen Eintrag im Strem");
   },
  error: function(jqXHR, textStatus, errorThrown){
  console.log( textStatus);
  }
  
}); 
					
					}},                 
                    {caption: 'Abbrechen', callback: function() { }}
                ]
});
			$("#All").change(function()
{
if($(this).is(":checked")) {
$("#del").prop('checked', true);
$("#add").prop('checked', true);
$("#mod").prop('checked', true);
}else
{
$("#del").prop('checked', false);
$("#add").prop('checked', false);
$("#mod").prop('checked', false);
}
});
}

followbuttonclick()
{
	var self = this;
	var objectname = this.state.objecttype.name;
	var dialoghtml = "<div id=\"singleobjecttypfollowdialog\" class=\"stdcheckboxregion\"><legend>Bei welchen Änderungen wollen Sie eine Benachichtigung im Stream?</legend>" +
"<br><input className=\"checkbox\" type=\"checkbox\" id=\"All\" name=\"All\" value=\"All\" /><label for=\"All\">Allen Änderungen</label>" +
"<br><input className=\"checkbox\" type=\"checkbox\" name=\"del\" id=\"del\" value=\"del\" /><label for=\"del\">Ein " + objectname + " der Listenvorlage wird gelöscht</label>" +
"<br><input className=\"checkbox\" type=\"checkbox\" name=\"add\" id=\"add\" value=\"add\" /><label for=\"add\">Ein " + objectname + " des Listenvorlage wird hinzugefügt</label>" +
"<br><input className=\"checkbox\" type=\"checkbox\" name=\"mod\" id=\"mod\"  value=\"mod\" /><label for=\"mod\">Ein " + objectname + " des Listenvorlage wird geändert</label></div>";
  var infodata ={};
  infodata.objectid = this.state.objecttype.id;
 $.ajax({
  type: "POST",
  url: BACKEND + "/api/follow/getstatus/",
  data: infodata,
  xhrFields: {
    withCredentials: true
 },
  dataType: "json",
  success: function( data, textStatus, jqXHR) {
  //var countselected = 0;
if(data.del === 'true')
{
	dialoghtml = dialoghtml.replace("id=\"del\"","id=\"del\" checked=\"checked\"");
	//countselected++;
}

if(data.add === 'true')
{
	dialoghtml = dialoghtml.replace("id=\"add\"","id=\"add\" checked=\"checked\"");
	//countselected++;
}
if(data.mod === 'true')
{
	dialoghtml = dialoghtml.replace("id=\"mod\"","id=\"mod\" checked=\"checked\"");
	//countselected++;
}
self.showfollowdialog(dialoghtml);
   },
  error: function(jqXHR, textStatus, errorThrown){
  console.log( textStatus);
  }
  
});


}
handleClickDeleteField(field)
{
this.setState({deletefielddialog:true});
this.setState({deletefield:field});
}

render(){
var self = this;

var dialogbox = null;
if(this.state.deletefielddialog)
{
  dialogbox = <DialogBox closedialog={()=>{this.setState({deletefielddialog:false});}} title="Feld löschen?">
  <div id="singleobjecttype-removefiled-box-div">
Möchten Sie das Feld {self.state.deletefield.name} wirklich von der Listenvorlage entfernen?<br/><br/>

  <Button color="danger" onClick={()=>{self.delfield()}}>
  ENTFERNEN
  </Button>	
  <Button onClick={()=>{this.setState({deletefielddialog:false});}}>
  ABBRECHEN
  </Button>	
  </div>
  </DialogBox>
}



var windowtitle = Trans.t("listtemplate") + " " + this.state.objecttype.name;

var sortfields = this.state.objecttype.fields.sort(function(a, b){
    return a.systemfield-b.systemfield
})


var fieldrows = sortfields.map(function(field)
{
var delicon = <img alt="Löschen" src="https://publicfile.standard-cloud.com/icons/delete.png"/>
var title = ""
var cssclass = "fieldrowrow";
if(field.systemfield)
	{
		delicon = null;
		cssclass += " fielddisabled";
		title = "Systemfelder können nicht gelöscht werden.";
	}
return(<tr key={field.id} title={title} className={cssclass}><td className="fieldtd" onClick={() => self.handleClickField(field)} dangerouslySetInnerHTML={{__html:field.name}}></td><td onClick={() => self.handleClickField(field)} dangerouslySetInnerHTML={{__html:field.description}}></td><td onClick={() => self.handleClickField(field)}>{field.type}</td><td onClick={() => self.handleClickDeleteField(field)} className="delfiledtd">{delicon}</td></tr>);
});

var listshtml = this.state.objecttype.lists.map(function(list)
{
	var listlink = "/list/" + list.id + "/";
	return(<span key={list.id} className="listnamelink"><a href={listlink}><span dangerouslySetInnerHTML={{__html:list.name}}/>; </a></span>);
});



		return (
	<div id="singleobjecttypelistpage">		
		   <Contentbox title={windowtitle}>
			<div id="singleobjecttypeboxcontent">
			<table id="singleobjecttypeboxcontentinfotbl">
			<tbody>
			<tr>
			<td className="infoboxtd">{Trans.t("name")}:</td><td dangerouslySetInnerHTML={{__html:this.state.objecttype.name}}></td></tr>
			<tr><td  className="infoboxtd">{Trans.t("plural")}:</td><td dangerouslySetInnerHTML={{__html:this.state.objecttype.nameplural}}></td></tr>
			<tr><td  className="infoboxtd">{Trans.t("description")}:</td><td dangerouslySetInnerHTML={{__html:this.state.objecttype.description}}></td></tr>
			<tr><td  className="infoboxtd"><img alt="Die nachfolgenden Listen basieren auf dieser Vorlage" src="https://publicfile.standard-cloud.com/icons/list.png"/> Listen:</td><td>{listshtml}</td></tr>
			</tbody>
			</table><br/>
			<Contentbox title={Trans.t("fields")}>
			<table className="stdtable" id="singleobjecttypeboxcontentfelder">
				<thead>
				<tr>
					<th>{Trans.t("name")}</th><th>{Trans.t("description")}</th><th>Typ</th><th></th>
				</tr>
				</thead>
			<tbody>
			{fieldrows}
			</tbody>
			</table><br/>
			<Addfieldtoobjecttype update={this.loadobjecttype} objecttype={this.state.objecttype} />
			</Contentbox>	
			</div>
			</Contentbox>

		
	<div id="objecttypelistpagebuttons">
	<StdButton  callbackfunction={this.returnbuttonclick} name={Trans.t("back")} iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
	<StdButton  url="edit/" name={Trans.t("settings")}  iconurl="https://publicfile.standard-cloud.com/icons/admin.png" />
	<StdButton  url="/admin/field/new/" name={Trans.t("createfield")} iconurl="https://publicfile.standard-cloud.com/icons/addcolumn.png" />
	<StdButton  url="https://standard-cloud.zendesk.com/hc/de/articles/202445021-Was-ist-ein-Objekttyp-" name={Trans.t("help")} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />
	</div>
<Helpbox title={Trans.t("help")}>
Auf dieser Seite können Sie Felder der Listenvorlage anfügen oder entfernen. Zum Enfernen eines Feldes/Splate klicken Sie auf
<img id="singleobjecttypehelpdeleteimg" src="https://publicfile.standard-cloud.com/icons/delete.png" alt="Feld löschen" /><br/>und bestätigen den folgenden Dialog.<br/><br/>
Das <strong>Hinzufügen eines Feldes/Spalte</strong> erfolgt über <strong>"Feld hinzufügen"</strong> und den <img alt="Hilfe" id="singleobjecttypehelpaddimg" src="https://publicfile.standard-cloud.com/icons/add.png"/>-Button.<br/>
Sollte das passende Feld nicht existieren, kann über <strong>"Neues Feld"</strong> ein neues Feld erstellt werden.<br/>
Die <strong>Systemfelder</strong> Erzeugt, Erzeuger, Geänder, Geändert von <strong>können nicht entfernt</strong> werden.<br/>
<br/><br/>Wenn Sie <strong>Namen, Beschreibung, Plural(Mehrzahl)</strong> anpassen wollen, müssen Sie auf <br/>("Einstellungen"->Basiseinstellungen)<br/>klicken.
</Helpbox>
{dialogbox}
	</div>
        );
		}
  }
  

  export default connect(null,{ setNavigation })(SingleObjectTypePage);