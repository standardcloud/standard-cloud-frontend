import React from 'react';
import StdButton from '../../../component/button/stdbutton';
import Helpbox from '../../../component/helpbox/helpbox';
import {connect} from 'react-redux';
import {setNavigation} from '../../../actions/navigation';
import {loadObjecttypes} from '../../../actions/objecttype';
import Trans from '../../../module/translatoinmanager/translationmanager';
import './objecttype.css';

class ObjectTypeListPage extends React.Component {
    constructor(props) {
    super(props);
	this.state = {objecttypes:[],searchtext:""};
	this.handleSearchTextChange = this.handleSearchTextChange.bind(this);
	this.showobjecttype = this.showobjecttype.bind(this);
	var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"), url:"/admin/"},{title:Trans.t("listtemplates")}];
	this.props.setNavigation(nav,Trans.t("listtemplates"));
  }


showobjecttype(objecttype)
{
	this.props.history.push("/admin/objecttype/modify/" + objecttype.id + "/");
	//window.location.href= "/admin/objecttype/modify/" + objecttype.id + "/";
}

  handleSearchTextChange(event) {           
   this.setState({searchtext: event.target.value.toLowerCase()});                    
 }

  		componentDidMount() {		 
		 this.props.loadObjecttypes();
				}




render(){
var self = this;
		var objecttyperow = this.props.objecttypes.map(function(ob)
		{

        if(self.state.searchtext !== "")
        {
            if(ob.name.toLowerCase().indexOf(self.state.searchtext) > -1 || ob.description.toLowerCase().indexOf(self.state.searchtext) > -1 )
            {
					
			}else
			{
				return null;
			}    
        }


		
			var lists = self.props.lists.filter(f=>f.objecttype===ob.id).map(function(list)
			{
				
				var listlink = "/list/" + list.id + "/";
				return (<span key={list.id}><a title="Liste anzeigen" href={listlink}>{list.name}; </a></span>);
			});
		return (<tr key={ob.id} className="objecttyperow" title="Listenvorlage anzeigen" onClick={() => self.showobjecttype(ob)}><td dangerouslySetInnerHTML={{__html:ob.name}}></td><td dangerouslySetInnerHTML={{__html:ob.description}}></td><td>{lists}</td></tr>);
		});

		return (
	<div id="objecttypelistpage">
		<div id="objecttypelistpagebuttons">

<StdButton  callbackfunction={() => window.history.back()} name={Trans.t("back")} iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
<StdButton  url="new/" name={Trans.t("newlisttemplate")}  iconurl="https://publicfile.standard-cloud.com/icons/addlist.png" />
<StdButton reload  url="https://standard-cloud.zendesk.com/hc/de/articles/202445021-Was-ist-ein-Objekttyp-" name={Trans.t("help")} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />

</div>
		<div id="objecttypelistcard">
				<div><h2>{Trans.t("listtemplates")}</h2></div>	
				 <input id="objecttypesearchbox"   onChange={this.handleSearchTextChange}  placeholder="Filter" type="search"  autoComplete="off"/><br/>
			<table className="stdtable" id="objecttypelisttable">
		<thead>
			<tr>
			<th>{Trans.t("name")}</th><th>{Trans.t("description")}</th><th>{Trans.t("lists")}</th>
			</tr>
		</thead>
		<tbody>
		{objecttyperow}
		</tbody>	
		</table>
	</div>

<Helpbox title={Trans.t("help")}>
	<strong>Eine Liste basiert immer auf einer Listenvorlage.</strong><br/>Diese Vorlage ist der Bauplan für die Liste und bestimmt welche Felder/Spalten diese enthalten soll.<br/><br/> Mit einem Klick auf die Listenvorlage k&ouml;nnen Sie diese bearbeiten, um z.B. weiter Felder anzufügen.<br/><br/>Sollte die passende Listenvorlage nicht existieren, können Sie einen neue Vorlage &uuml;ber den Button <strong>"Neue Listenvorlage"</strong> erzeugen.
	<br/><br/>In der Spalte "Listen" sehen Sie welche Listen bereits mit dieser Vorlage erzeugt wurden.
	<br/><br/>Weitere Hilfe finden Sie über den Button "Hilfe".
</Helpbox>

	</div>
        );
		}
  };
  
  function mapStateToProps({objecttype,list}) {
	return { objecttypes:objecttype,lists:list};
  }

export default connect(mapStateToProps,{setNavigation,loadObjecttypes})(ObjectTypeListPage);

