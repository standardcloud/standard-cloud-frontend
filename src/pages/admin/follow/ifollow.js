import React from 'react';

import BT from '../../../component/button/stdbutton';
import {connect} from 'react-redux';
import {setNavigation} from '../../../actions/navigation';
import Helpbox from '../../../component/helpbox/helpbox';
import Trans from '../../../module/translatoinmanager/translationmanager';

import dialog from '../../../component/dialog/stddialog';
import displayobject from '../../../module/displayobject/stddisplayobject';
import FollowManager from '../../../module/administration/stdfollowmanager';
import { Link } from "react-router-dom";
import ObjectUrlManager from '../../../module/objecturlmanager/stdobjecturlmanager';
import {BACKEND} from '../../../stdsetup';
import "./ifollow.css";

class IFollowPage extends React.Component {
    constructor(props) {
    super(props);
	this.state = {results:[],selectedobjecttype:"2"};
	this.loadfollows = this.loadfollows.bind(this);
	this.createlistaborow = this.createlistaborow.bind(this);	
	this.deleteabo = this.deleteabo.bind(this);
	var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"), url:"/admin/"},{title:Trans.t("ifollow")}];
	this.props.setNavigation(nav,Trans.t("ifollow"));
}
  loadfollows()
  {

	var fs = this;
	FollowManager.getuserabos().then(data=>
	{
		if(data.state==="ok")
		{
			fs.setState({results:data.content});		
		}
	});

  }
  returnbuttonclick()
  {
	  window.history.back();
  }

deleteabo(actionabo)
{
	var self = this;
	FollowManager.removeuserabo(actionabo.id).then(data=>
	{
		if(data.state==="ok")
		{
			dialog.ok("Abo","wurde gelöscht.")
			var newabos = self.state.results.filter(function(abo) {
				return abo.id !== data.content.id;
			});
			self.setState({results:newabos});
		}else
		{
			dialog.error("Abo","wurde gelöscht.")
		}
	});
}

createlistdata(results)
{
	if(results===undefined)
	{
		return [];
	}
	var listabodata = [];

	for(let i=0;i < results.length;i++)
	{
		var curobj = results[i];
		if(curobj.abotype !== "0")
		{
			continue;
		}
		let isinlist = false;
			for(var x=0;x < listabodata.length;x++)
				{
					if(listabodata[x].id === curobj.listid)
					{
						isinlist = true;
					}
				}
			 // liste nicht bereits vorhanden		
				if(isinlist===false)
				{
					var newlistdataobject = {};
					newlistdataobject.id =  curobj.listid;;
					newlistdataobject.name = curobj.listname;
					var listactinrows = results.filter(function(abo) {
						return ((abo.listid === curobj.listid) && (abo.abotype==="0"));
					});
					var actions = [];
					for(var j=0;j < listactinrows.length;j++)
					{
						actions.push(listactinrows[j]);
					}				
					newlistdataobject.actions = actions;				
					listabodata.push(newlistdataobject);				
				}
		
	}
	return listabodata;		
}

createuserdata(results)
{
	if(results===undefined)
	{
		return [];
	}
	var userabodata = [];

	for(let i=0;i < results.length;i++)
	{
		var curobj = results[i];
		if(curobj.abotype !== "1")
		{
			continue;
		}
	    	let isinlist = false;
			for(var x=0;x < userabodata.length;x++)
				{
					if(userabodata[x].userid === curobj.userid)
					{
						isinlist = true;
					}
				}
			 // liste nicht bereits vorhanden		
				if(isinlist===false)
				{
					var newuserdataobject = {};
					newuserdataobject.userid =  curobj.userid;;
					newuserdataobject.name = curobj.username;
					newuserdataobject.userimg = curobj.userimg;
					var useractinrows = results.filter(function(abo) {
						return ((abo.userid === curobj.userid) && (abo.abotype==="1"));
					});
					var actions = [];
					for(var j=0;j < useractinrows.length;j++)
					{
						actions.push(useractinrows[j]);
					}				
					newuserdataobject.actions = actions;				
					userabodata.push(newuserdataobject);				
				}
		
	}
	return userabodata;		
}



createobjectdata(results)
{
	if(results===undefined)
	{
		return [];
	}
	var objectabodata = [];

	for(let i=0;i < results.length;i++)
	{
		var curobj = results[i];
		if(curobj.abotype !== "3")
		{
			continue;
		}
	    	let isinlist = false;
			for(var x=0;x < objectabodata.length;x++)
				{
					if(objectabodata[x].objectid === curobj.objectid)
					{
						isinlist = true;
					}
				}
			 // liste nicht bereits vorhanden		
				if(isinlist===false)
				{
					var objectdataobject = {};
					objectdataobject.objectid =  curobj.objectid;;
					objectdataobject.listid = curobj.listid;
					objectdataobject.objecttypename = curobj.objecttypename;
					if(curobj.content !== "")
					{
					objectdataobject.content = 	displayobject.getcontent(curobj.content);  	
				}else
				{
					objectdataobject.content ="";
				}
					var objectinrows = results.filter(function(abo) {
						return ((abo.objectid === curobj.objectid) && (abo.abotype==="3"));
					});
					var actions = [];
					for(var j=0;j < objectinrows.length;j++)
					{
						actions.push(objectinrows[j]);
					}				
					objectdataobject.actions = actions;				
					objectabodata.push(objectdataobject);				
				}
		
	}
	return objectabodata;		
}


createobjecttypedata(results)
{
	if(results===undefined)
	{
		return [];
	}
	var objecttypeabodata = [];
	
	for(let i=0;i < results.length;i++)
	{
		var curobj = results[i];
		if(curobj.abotype !== "2")
		{
			continue;
		}
	    	let isinlist = false;
			for(var x=0;x < objecttypeabodata.length;x++)
				{
					if(objecttypeabodata[x].objecttypeid === curobj.objecttypeid)
					{
						isinlist = true;
					}
				}
			 // liste nicht bereits vorhanden		
				if(isinlist===false)
				{
					var objecttypedataobject = {};
					objecttypedataobject.objecttypeid =  curobj.objecttypeid;;
					objecttypedataobject.objecttypename = curobj.objecttypename;
		    		objecttypedataobject.list = curobj.list;
					var objecttypeactinrows = results.filter(function(abo) {
						return ((abo.objecttypeid === curobj.objecttypeid) && (abo.abotype==="2"));
					});
					var actions = [];
					for(var j=0;j < objecttypeactinrows.length;j++)
					{
						actions.push(objecttypeactinrows[j]);
					}				
					objecttypedataobject.actions = actions;				
					objecttypeabodata.push(objecttypedataobject);				
				}
		
	}
	return objecttypeabodata;		
}


	getactionicon(actionabo)
	{
		if(actionabo===undefined)
		return null;
		switch(actionabo.actionid)
		{
			case "1":	
				return (<div key={actionabo.actionid}  onClick={this.deleteabo.bind(actionabo)}  className="ifollow_addimage ifollow_deleteimage ifollow_actionimg"/>);
			case "2":
				return (<div key={actionabo.actionid}  onClick={this.deleteabo.bind(actionabo)} className="ifollow_delimage ifollow_deleteimage ifollow_actionimg"/>);				
			case "3":
			return (<div key={actionabo.actionid}  onClick={this.deleteabo.bind(actionabo)} className="ifollow_editimage ifollow_deleteimage ifollow_actionimg"/>);	
			case "4":
				return (<div key={actionabo.actionid}  onClick={this.deleteabo.bind(actionabo)} className="ifollow_personalpostimage ifollow_deleteimage ifollow_actionimg" />);			
			case "5":
				return (<div key={actionabo.actionid}  onClick={this.deleteabo.bind(actionabo)} className="ifollow_deleteimage ifollow_commentimage ifollow_actionimg" />);			
		    case "6":
				return (<div key={actionabo.actionid}  onClick={this.deleteabo.bind(actionabo)} className="ifollow_deleteimage ifollow_shareimage  ifollow_actionimg" />);	
			default:			
		}
	}

	createuserobjectrow(abo)
	{
		var profilurl = "/profile/" + abo.userid + "/";
		var imgurl = BACKEND + "/file/" +abo.userimg;
		var actiontext = this.gettext4actionid(abo.actionid);
		var titletext = "Sie erhalten eine Benachrichtigung im Stream sobald der Benutzer " + abo.username + " ein Objekt " + actiontext +" wurde.";
		var action = this.createaction(abo.actions);	
	     return (<tr title={titletext} key={abo.userid}><td><Link to={profilurl}><img alt="" className="ifollowuserimage" src={imgurl}/>{abo.name}</Link></td><td className="ifollow_actionstd">{action}</td></tr>);
	}
	
	createobjectaborow(abo)
	{
		var action = this.createaction(abo.actions);	
		var contentbox =  <div  onClick={()=>this.clickviewobject(abo.listid,abo.objectid)} className="objectbox">
                        <div className="headerobjectbox" dangerouslySetInnerHTML={{__html: abo.objecttypename}}></div>
                        <div className="analyticsbox">
                                <div className="analyticsboxwrapper" dangerouslySetInnerHTML={{__html: abo.content}} ></div>
                        </div>
                    </div>


	
		return (<tr key={abo.objectid}><td>{contentbox}</td><td className="ifollow_actionstd">{action}</td></tr>);
	}
	
	gettext4actionid(actionid)
	{
		switch(actionid)
		{
			case "1":
			return "hinzufügt";
			case "2":
			return "gelöscht";				
	    	case "3":
			return "bearbeitet";			
			case "4":
			return "einen persönlichen Beitrag erstellt";			
			case "5":
			return "kommentiert";			
		    case "6":
			return "teilen";
			default:			
		}
	}

	createaction(actionarray)
	{
		var returnresult = [];
		for(let i=0;i<= actionarray.length;i++)
		{
			returnresult.push(this.getactionicon(actionarray[i]))		
		}
		return returnresult;
	}

	createlistaborow(abo)
	{
		var listlink = "/list/" + abo.id + "/";
		var actiontext = this.gettext4actionid(abo.actionid);
		var titletext = "Sie erhalten eine Benachrichtigung im Stream sobald ein " + abo.objecttypename + "-Objekt " + actiontext;
		var action = this.createaction(abo.actions);
		return (<tr title={titletext} key={abo.id}><td className="ifollow_listname"><Link to={listlink}><strong>{abo.name}</strong></Link></td><td className="ifollow_actionstd">{action}</td></tr>);
	}

	clickviewobject(listid,objectid)
	{
		var self = this;
		        ObjectUrlManager.geturl(listid,objectid,function(url)
                {
					self.props.history.push(url);
                });
	}


	createobjecttypeaborow(abo)
	{
		
        var lists = abo.list.map(function(currentlist)
		{
			var listlink = "/list/" + currentlist.listid + "/";
			return (<Link key={currentlist.listid} to={listlink}> {currentlist.listname};</Link>);
		});
		var actiontext = this.gettext4actionid(abo.actionid);
		var action = this.createaction(abo.actions);	
		var titletext = "Sie erhalten eine Benachrichtigung im Stream sobald ein " + abo.objecttypename + "-Objekt in einer der nachfolgenden Listen " + actiontext +" wurde.";
		return (<tr title={titletext} key={abo.objecttypeid}><td>{abo.objecttypename}</td><td>{lists}</td><td className="ifollow_actionstd">{action}</td></tr>);
	}

  		componentDidMount() {
		 this.loadfollows();
				}

getacordeonicon(number)
{
	if(number===this.state.selectedobjecttype)
	{
	return <img alt="Öffnen" src="https://publicfile.standard-cloud.com/icons/acordeonopen.png"/>
	}
	return <img alt="Schließen" src="https://publicfile.standard-cloud.com/icons/acordeonclosed.png"/>
}

clickacordeon(number)
{
if(number === this.state.selectedobjecttype)
{
	this.setState({selectedobjecttype:"0"});
}else
{
	this.setState({selectedobjecttype:number});
}
}

render(){
    var ts = this;
	if(this.state.selectedobjecttype==="1")
	{
		var objecttyprows = this.createobjecttypedata(this.state.results).map(function(abo)
		{
				return ts.createobjecttypeaborow(abo);			
		});
		var objecttypehtml =<table className="stdtable stdifollowtable">
	<thead><tr>
	<th>{Trans.t("listtemplate")}</th><th>{Trans.t("lists")}</th><th>{Trans.t("actions")}</th></tr>
	</thead><tbody>
	{objecttyprows}
	</tbody>
	</table>
	}
		if(this.state.selectedobjecttype==="2")
		{
			var userrows = this.createuserdata(this.state.results).map(function(abo)
			{
					return ts.createuserobjectrow(abo);			
			});

			var userhtml =<table className="stdtable stdifollowtable">
			<thead>
				<tr>
			<th>{Trans.t("user")}</th><th>{Trans.t("actions")}</th></tr>
			</thead><tbody>
			{userrows}
			</tbody>
			</table>

		}
	if(this.state.selectedobjecttype==="3")
	{
		var objectrows = this.createobjectdata(this.state.results).map(function(abo)
		{
				return ts.createobjectaborow(abo);	
		});
		    var objecthtml=<table  className="stdtable stdifollowtable">
			<thead><tr>
			<th>{Trans.t("object")}</th><th>{Trans.t("actions")}</th></tr>
			</thead><tbody>
			{objectrows}
			</tbody>
			</table>
	}

	if(this.state.selectedobjecttype==="4")
	{
		var listrows = this.createlistdata(this.state.results).map(function(abo)
		{
				return ts.createlistaborow(abo);			
		});
			var listhtml = <table  className="stdtable stdifollowtable">
	<thead><tr>
	<th>{Trans.t("list")}</th><th>{Trans.t("actions")}</th></tr>
	</thead><tbody>
	{listrows}
	</tbody>
	</table>
	}




		return (
	<div>
<div className="ifollowtypeboxcontainer">	
					<div className="ifollowtypebox">		
	<div onClick={() => this.clickacordeon("4")}><h2>{Trans.t("lists")}{this.getacordeonicon("4")}</h2></div>
{listhtml}
	</div>

		<div className="ifollowtypebox">
			<div onClick={() => this.clickacordeon("2")}><h2>{Trans.t("users")}{this.getacordeonicon("2")}</h2></div>
{userhtml}
			</div>
	
			<div className="ifollowtypebox">
			<div onClick={() => this.clickacordeon("3")}><h2>{Trans.t("objects")}{this.getacordeonicon("3")}</h2></div>
{objecthtml}
			</div>


		<div className="ifollowtypebox">
	<div onClick={() => this.clickacordeon("1")}><h2>{Trans.t("listtemplates")}{this.getacordeonicon("1")}</h2></div>	
{objecttypehtml}
	</div>
</div>

	<Helpbox title={Trans.t("help")}>
	Sie fragen sich, warum auf der Startseite im Newsstream (rechts) so viele Events/Updates erscheinen?
	<br/>Die Erkläung ist einfach: Sie haben auf <a href="https://standard-cloud.zendesk.com/hc/de/articles/115000591652-Was-ist-ein-Benutzer-">Profilseiten (Benutzer)</a>, bei einer <a href="https://standard-cloud.zendesk.com/hc/de/articles/202453262-Was-ist-eine-Liste-">Liste</a>, einem <a href="https://standard-cloud.zendesk.com/hc/de/articles/202454302-Was-ist-ein-Objekt-">Objekt</a> oder einer <a href="https://standard-cloud.zendesk.com/hc/de/articles/202445021-Was-ist-ein-Objekttyp-">Listenvorlage</a> auf den <a href="https://standard-cloud.zendesk.com/hc/de/articles/203016612-Der-Stream-und-die-Funktion-Folgen-">Folgen-Button</a><br/>
	<img src="https://publicfile.standard-cloud.com/icons/folgen-button.png" id="followbuttonifollowpage" alt="Folgen-Button"/><br/> geklickt und dabei die Benachrichtigung aktiviert.<br/><img alt="" id="ifollowpagedialogimg" src="https://publicfile.standard-cloud.coM/icons/ifollowpagedialog.png"/><br/>Diese Benachrichtigungen können Sie hier wieder entfernen.<br/>
	<img alt="Hinzufügen" src="https://publicfile.standard-cloud.com/icons/stream-new-object.png"/>hinzufügen <br/>
	<img alt="Löschen" src="https://publicfile.standard-cloud.com/icons/stream-delete.png"/>Löschen <br/>
	<img alt="Bearbeiten" src="https://publicfile.standard-cloud.com/icons/stream-edit-object.png"/>Bearbeiten <br/>
	<img alt="Teilen" src="https://publicfile.standard-cloud.com/icons/share.png"/>Teilen <br/>
	<img alt="Kommentieren" src="https://publicfile.standard-cloud.com/icons/comment25.png"/>Kommentieren <br/>
	
 Fahren Sie mit der Maus auf unnötige Aktionen und löschen Sie diese mit einem Klick auf<img alt="Löschen" src="https://publicfile.standard-cloud.com/icons/delete30.png"/>
	</Helpbox>

	
	<div>
			<div id="ifollowpagebuttons">
	<BT  callbackfunction={this.returnbuttonclick} name={Trans.t("back")} iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
	<BT  url="https://standard-cloud.zendesk.com/hc/de/articles/115000551352-Ich-folge-" name={Trans.t("help")} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />
	</div>
	</div>
	</div>
        );
		}
}
  


export default connect(null,{setNavigation})(IFollowPage);