
import React from 'react';
import {connect} from 'react-redux';
import SearchManager from '../../../module/search/search';
import dialog from '../../../component/dialog/stddialog'
import {setNavigation} from '../../../actions/navigation';
import Trans from '../../../module/translatoinmanager/translationmanager';
import BT from '../../../component/button/stdbutton';


  class SearchSettingsPage extends React.Component {
	    constructor(props) {
		super(props);
		this.resetindex = this.resetindex.bind(this);
	this.state = {};
	var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"),url:"/admin/"},{title:Trans.t("search")}];
	this.props.setNavigation(nav,Trans.t("search"));
  }
  
resetindex()
{
	SearchManager.resetindex().then(data =>
		{
			if(data.state==="ok")
			{
			dialog.ok("Suche",data.content.msg); 
			}else
			{
			 dialog.error("Suche",data.content.msg);  
			}
		});
}

render(){
		return (
	<div>
		<BT count="1" callbackfunction={this.resetindex}  name="Index Reset" iconurl="https://publicfile.standard-cloud.com/icons/indexreset.png" />
		<BT reload url="https://standard-cloud.zendesk.com/hc/de" name={Trans.t("help")} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />	
	</div>
        );
		}
  };
  

export default connect(null,{setNavigation})(SearchSettingsPage);
