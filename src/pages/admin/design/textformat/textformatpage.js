import React from 'react';

import BT from '../../../../component/button/stdbutton';
import {connect} from 'react-redux';
import {setNavigation} from '../../../../actions/navigation';
import Helpbox from '../../../../component/helpbox/helpbox';
import Contentbox from '../../../../component/contentbox/contentbox';
import stdadministration from '../../../../module/administration/stdadministration';
import dialog from '../../../../component/dialog/stddialog';
import Trans from '../../../../module/translatoinmanager/translationmanager';
import './textformatpage.css';
class TextFormatPage extends React.Component {
	   constructor(props) {
	   super(props);
	   this.state = {textformat:""};
	   this.loadtextformat = this.loadtextformat.bind(this);
	   this.textchange = this.textchange.bind(this);
	   this.savetextformat = this.savetextformat.bind(this);
	   var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"), url:"/admin/"},{title:Trans.t("design"), url:"/admin/design/"},{title:Trans.t("texttemplates")}];
	   this.props.setNavigation(nav,Trans.t("texttemplates"));
		
  }


savetextformat()
{
	stdadministration.updatetextformat(this.state.textformat.trim()).then(data =>
	{
		if(data.state==="ok")
			{
				dialog.ok("Text-Formatvorlagen","erfolgreich gepeichert.");
			}else
			{
				dialog.error("Text-Formatvorlagen","konnten leider nicht gespeichert werden.");
			}
	});
}
clickback()
{
	window.history.back();
}

		  componentDidMount() 
		  {
		 	this.loadtextformat();
		  }

loadtextformat()
{
	var self = this;
	stdadministration.gettextformat().then(data=>
	{	
		if(data.state ==="ok")
			{	
				self.setState({textformat:data.content.formats});
			}
	});
}
textchange(e)
{
	this.setState({textformat:e.target.value});
}


render(){
		return (
	<div id="texttemplatepage">
<Contentbox title={Trans.t("texttemplates")}>
<textarea  id="textformatcontent" value={this.state.textformat} onChange={this.textchange}></textarea>
</Contentbox>
<div id="admindesignboxfooter">
	 <BT  callbackfunction={this.clickback} name={Trans.t("back")}  iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
	 <BT  callbackfunction={this.savetextformat} name={Trans.t("save")}    iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
	</div>
	<Helpbox title={Trans.t("help")}>
		Auf dieser Seite können Sie Formatvorlagen für Textfelder bestimmen. So entsteht kein Design-Chaos.
	</Helpbox>
	</div>
        );
		}
  };
  

  export default connect(null,{setNavigation})(TextFormatPage);