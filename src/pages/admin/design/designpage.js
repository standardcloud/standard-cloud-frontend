import React from 'react';
import BT from '../../../component/button/stdbutton';
import {connect} from 'react-redux';
import {setNavigation} from '../../../actions/navigation';
import Trans from '../../../module/translatoinmanager/translationmanager';
class DesignAdminOverview extends React.Component {
	    constructor(props) {
		super(props);
		var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"), url:"/admin/"},{title:Trans.t("design")}];
		this.props.setNavigation(nav,Trans.t("design"));
}

render(){
		return (
	<div>
		<BT  url="/admin/design/editorformat/"  name={Trans.t("texttemplates")}  iconurl="https://publicfile.standard-cloud.com/icons/textformat.png" />
	    <BT  url="/admin/design/customcss/"  name="CSS" iconurl="https://publicfile.standard-cloud.com/icons/css.png" />
	</div>
        );
		}
}
  
export default connect(null,{setNavigation})(DesignAdminOverview);