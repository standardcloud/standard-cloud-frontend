import React from 'react';
import BT from '../../../../component/button/stdbutton';
import {connect} from 'react-redux';
import {setNavigation} from '../../../../actions/navigation';
import Helpbox from '../../../../component/helpbox/helpbox';
import Contentbox from '../../../../component/contentbox/contentbox';
import stdadministration from '../../../../module/administration/stdadministration';
import dialog from '../../../../component/dialog/stddialog';
import Trans from '../../../../module/translatoinmanager/translationmanager';


class EditCSSPage extends React.Component {
	   constructor(props) {
	   super(props);
	   this.state = {css:""};
	   this.loadcss = this.loadcss.bind(this);
	   this.textchange = this.textchange.bind(this);
	   this.savecss = this.savecss.bind(this);
	   var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"), url:"/admin/"},{title:Trans.t("design"), url:"/admin/design/"},{title:"CSS"}];
	   this.props.setNavigation(nav,"CSS");
		
}
clickback()
{
	window.history.back();
}

savecss()
{
	stdadministration.updatecustomcss(this.state.css.trim()).then(data=>
	{
		if(data.state==="ok")
			{
				dialog.ok("CSS","erfolgreich gepeichert.");
			}else
			{
				dialog.error("CSS","konnten leider nicht gespeichert werden.");
			}
	});
}

componentDidMount() 
{
	this.loadcss();
}

loadcss()
{
	var self = this;
	stdadministration.getcustomcss().then(data =>
	{	
		if(data.state ==="ok")
			{	
				self.setState({css:data.content.css});
			}
	});
}
textchange(e)
{
	this.setState({css:e.target.value});
}


render(){
		return (
	<div id="texttemplatepage">
<Contentbox title="CSS">
<textarea  id="textformatcontent" value={this.state.css} onChange={this.textchange}></textarea>
</Contentbox>
<div id="admindesignboxfooter">
	 <BT  callbackfunction={this.clickback} name={Trans.t("back")}   iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
	 <BT  callbackfunction={this.savecss} name={Trans.t("save")}   iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
	</div>
	<Helpbox title={Trans.t("help")}>
		Sie wollen das Design verbessern, dem eigenen CI anpassen? Ihr Entwicker kann hier die CSS-Klassen von Standard-Cloud überschreiben.
	</Helpbox>
	</div>
        );
		}
  };
  


  export default connect(null,{setNavigation})(EditCSSPage);