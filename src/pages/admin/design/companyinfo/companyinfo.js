
import React from 'react';

import BT from '../../../../component/button/stdbutton';
import {connect} from 'react-redux';
import {setNavigation} from '../../../../actions/navigation';
import Helpbox from '../../../../component/helpbox/helpbox';
import Contentbox from '../../../../component/contentbox/contentbox';
import stdadministration from '../../../../module/administration/stdadministration';
import dialog from '../../../../component/dialog/stddialog';
import Trans from '../../../../module/translatoinmanager/translationmanager';


class CompanyInfo extends React.Component {
	   constructor(props) {
	   super(props);
	   this.state = {company:{name:""}};
	   this.loadcompanyinfo = this.loadcompanyinfo.bind(this);
	   this.companytextchange = this.companytextchange.bind(this);
	   this.savecompanyinfo = this.savecompanyinfo.bind(this);
	   var nav = [{title:"Start",url:"/"},{title:"Administration", url:"/admin/"},{title:Trans.t("companyinformation") }];
	   this.props.setNavigation(nav,Trans.t("companyinformation"));
		
  }
clickback()
{
	window.history.back();
}

savecompanyinfo()
{
	stdadministration.updatecompanyinformation(this.state.company.name.trim()).then(data =>
	{
		if(data.state==="ok")
			{
				dialog.ok("CSS","erfolgreich gepeichert.");
			}else
			{
				dialog.error("CSS","konnten leider nicht gespeichert werden.");
			}
	});
}

componentDidMount() 
{
	this.loadcompanyinfo();
}

loadcompanyinfo()
{
	var self = this;
	stdadministration.getcompanyinformation().then(data=>
	{	
		if(data.state ==="ok")
			{	
				self.setState({company:data.content});
			}
	});
}
companytextchange(e)
{
	var o = this.state.company;
	o.name = e.target.value.trim();
	this.setState({company:o});
}


render(){
		return (
	<div id="texttemplatepage">
<Contentbox title={Trans.t("companyinformation")} >
Name <input type="text" value={this.state.company.name} onChange={this.companytextchange}/>
</Contentbox>
<div id="admindesignboxfooter">
	 <BT  callbackfunction={this.clickback} name={Trans.t("back")}   iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
	 <BT  callbackfunction={this.savecompanyinfo} name={Trans.t("save")}  iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
	</div>
	<Helpbox title={Trans.t("help")}>
	Hier hinterlegen Sie grundlegende Informationen zur Instanz.
	</Helpbox>
	</div>
        );
		}
}
  
export default connect(null,{setNavigation})(CompanyInfo);

