import React from 'react';
import BT from '../../../../component/button/stdbutton';
import {connect} from 'react-redux';
import {setNavigation} from '../../../../actions/navigation';
import Helpbox from '../../../../component/helpbox/helpbox';
import Contentbox from '../../../../component/contentbox/contentbox';
import dialog from '../../../../component/dialog/stddialog';
import CodeMirror from  'codemirror';
import templatemanager from '../../../../module/templatemanager/stdtemplatemanager'; 
import Trans from '../../../../module/translatoinmanager/translationmanager';

  class TemplatePage extends React.Component {
	constructor(props) {
    super(props);
	this.state = {templates:[]};
	this.selecttemplate = this.selecttemplate.bind(this);
	this.savetemplateupdate = this.savetemplateupdate.bind(this);
	var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"), url:"/admin/"},{title:Trans.t("design"), url:"/admin/design/"},{title:Trans.t("templates")}];
	this.props.setNavigation(nav,Trans.t("templates"));
  }

loadtemplatelist()
{
	var self = this;
	templatemanager.gettemplates().then(data =>
	{
		self.setState({templates:data,templateid:""});
	});
}
savetemplateupdate()
{
			var self = this;
			templatemanager.updatetemplate(self.state.templateid,window.edit.getValue()).then(result =>
			{
				if(result != null)
				{			
				dialog.okdialog("Template", "wurde erfolgreich gespeichert.");
				}else
				{
					dialog.errordialog("Template", "konnte nicht gespeichert werden.");
					
				}					
			});
}
componentDidMount()
{
	this.loadtemplatelist();
					var textArea = document.getElementById('tbcode');
					var editor = CodeMirror.fromTextArea(textArea,{
					lineNumbers: true,
					mode: "htmlmixed",
					});
					editor.setSize("100%", "100%");	
				    window.edit = editor;
}

selecttemplate(e)
{
	this.setState({templateid:e.target.value});
		templatemanager.gettemplate(e.target.value).then(data =>
				{	
				var decodedData = window.atob(data.content);				
				window.edit.getDoc().setValue(decodedData);
				});
}

clickback()
{
window.history.back();
}



render(){

	var temps = this.state.templates.map(function(template)
	{
		return(<option key={template.id} value={template.id}>{template.name}</option>);
	});
	
		return (
	<div id="admintemplatepage">
<Contentbox title="Templates">
<select onChange={this.selecttemplate}>
	return(<option key="589888" value=""></option>);
{temps}
</select>
<textarea  id="tbcode" data-bind="html: selectedCode" ></textarea>
</Contentbox>
<div id="admindesignboxfooter">
	 <BT  callbackfunction={this.clickback} name={Trans.t("back")}   iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
	 <BT  callbackfunction={this.savetemplateupdate} name={Trans.t("save")}   iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
	</div>
	<Helpbox title={Trans.t("help")}>
<strong>Diese Seite sollte nur von Expterten - und in Absprache mit uns - angepasst werden.</strong>
</Helpbox>
	</div>
        );
		}
}
  
export default connect(null,{setNavigation})(TemplatePage);
