import React from 'react';
import DialogBox from '../../../component/dialogbox/dialogbox';
import BT from '../../../component/button/stdbutton';
import {connect} from 'react-redux';
import {setNavigation} from '../../../actions/navigation';
import Helpbox from '../../../component/helpbox/helpbox';
import Contentbox from '../../../component/contentbox/contentbox';
import Button from 'muicss/lib/react/button';
import dialog from '../../../component/dialog/stddialog';
import UserManager from '../../../module/usermanager/stdusermanager';
import Trans from '../../../module/translatoinmanager/translationmanager';

import './userlistmanagementpage.css';

class adminuserlistpage extends React.Component {

	    constructor(props) {
		super(props);
		this.state = {users:[],searchtext:"",removeuser:null};
		this.handleSearchTextChange = this.handleSearchTextChange.bind(this);			
		this.resetPassword = this.resetPassword.bind(this);	
		this.editProfile = this.resetPassword.bind(this);
		this.showProfile = this.showProfile.bind(this);
		this.deleteProfile = this.deleteProfile.bind(this);
		this.deleteuserfunction = this.deleteuserfunction.bind(this);
		var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"),url:"/admin/"},{title:Trans.t("userlist")}];
		this.props.setNavigation(nav,Trans.t("userlist"));
		
  }		

  handleSearchTextChange(event) {
         this.setState({searchtext: event.target.value.toLowerCase()});             
}

loadusers()
{
	var selfload =this;
	UserManager.getusers().then(data =>
	{
		selfload.setState({users:data});
	});

}

resetPassword(user)
{
	this.props.history.push("/admin/usermanagement/resetpassword/" + user.userid + "/");
}
editProfile(user)
{
	this.props.history.push("/list/99043cfa-9092-421b-8f34-566e1e6849fd/edit/" + user.profiledata.objectid + "/");
}
showProfile(user)
{
	this.props.history.push("/profile/" + user.userid + "/");
}

deleteProfile(user)
{
	this.setState({removeuser:user});
}
deleteuserfunction(userid)
{
	let self = this;
	UserManager.deleteuser(userid).then(data =>
		{
			if(data === false)
			{
					dialog.errordialog("Benutzer"," konnte nicht gel&ouml;scht werden.");				
			}
			else
			{
				if(data.state ==="ok")
				{
					dialog.okdialog("Benutzer","wurde gel&ouml;scht.");
					self.setState({removeuser:null});
					self.loadusers();			 
				}else
				{
					dialog.errordialog("Benutzer","konnt nicht gel&ouml;scht werden.");
				}					
			}
		});	
}

componentDidMount() {
this.loadusers();
}

render() {
var removedialog = null;
var selfrender = this;

	if(this.state.removeuser)
	{
	  var deltext = <div>Wollen Sie den <strong> {selfrender.state.removeuser.name} </strong> wirklich löschen?</div>
	removedialog = <DialogBox closedialog={()=>{selfrender.setState({removeuser:null})}}  title="Löschen ?">
	<div className="contentcontainertextremoveobject">
	<h1>{deltext}</h1>
	
		  <Button onClick={()=>{selfrender.deleteuserfunction(selfrender.state.removeuser.id);}}  color="danger">JA, LÖSCHEN</Button>
		  <Button  onClick={()=>{selfrender.setState({removeuser:null})}} >NEIN</Button>
		</div>
	 </DialogBox>
	}


	

             var users = this.state.users.map(function(user) {	
				if(selfrender.state.searchtext !== "")
				{
					if(user.name.toLowerCase().indexOf(selfrender.state.searchtext)> -1 || user.value.toLowerCase().indexOf(selfrender.state.searchtext) > -1)
					{

					}else
					{
					return null;
					}          
				}
				var status = <img alt="Login ist erlaubt" className="userprofileicon" src="https://publicfile.standard-cloud.com/icons/ok.png" title="Benutzer kann sich anmelden." />
				if(!user.active)
				{
					status = <img alt="Login nicht erlaubt" className="userprofileicon" src="https://publicfile.standard-cloud.com/icons/denied.png" title="Benutzer kann sich NICHT anmelden." />
				}

      var username = user.name + " (" + user.value + ")"
	                return (
			<tr key={user.userid}>
			<td>
			{status}
			</td>
            <td>
				<div dangerouslySetInnerHTML={{__html: username}}></div>		
			</td>
			<td>
				<img className="userprofileicon cursor" title="Profil anzeigen" onClick={() => selfrender.showProfile(user)} alt="Profil anzeigen" src="https://publicfile.standard-cloud.com/icons/user.png"/>	
			</td>
			<td>
				<img  className="userprofileicon cursor" alt="Passwort setzen"  title="Passwort setzen" onClick={() => selfrender.resetPassword(user)} src="https://publicfile.standard-cloud.com/icons/password.png" />	
			</td>
		    <td>
				<img  className="userprofileicon cursor" title="Profil bearbeiten" onClick={() => selfrender.editProfile(user)}  alt="Benutzer editieren" src="https://publicfile.standard-cloud.com/icons/edituser.png"/>	
			</td>
			<td>
				<img  className="userprofileicon cursor" title="Benutzer l&ouml;schen" onClick={() => selfrender.deleteProfile(user)} alt="Löschen" src="https://publicfile.standard-cloud.com/icons/delete.png"/>	
			</td>
			</tr>
		);  
                  
               });

	return (
<div>
{removedialog}
<Contentbox title={Trans.t("userlist")}>
       	 <div>
		 <input id="querysearchbox" value={this.state.searchtext}  onChange={this.handleSearchTextChange}  placeholder="Filter" type="search" autoComplete="off"/><br/>
		 <table id="usermanagementtbl">
		<thead>
		<tr key="123">
		<th className="iconcoluserlist headerusermanagerth">A</th><th>{Trans.t("user")}</th><th className="iconcoluserlist headerusermanagerth">B</th><th className="iconcoluserlist headerusermanagerth">PWD</th><th className="iconcoluserlist headerusermanagerth">E</th><th className="iconcoluserlist headerusermanagerth">R</th>
		</tr>
		</thead>
		<tbody>		
		{users}
		</tbody>		
		</table>
		 </div>
		 </Contentbox>
		 <div id="usermanagementfooter">
			<BT  callbackfunction={() => window.history.back()} name={Trans.t("back")} iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
			<BT url="/admin/usermanagement/newuser/" name={Trans.t("newuser")} iconurl="https://publicfile.standard-cloud.com/icons/adduser.png" />  
        	<BT url="/admin/usermanagement/authenticationprovider/" name="Authentication Provider" iconurl="https://publicfile.standard-cloud.com/icons/systemright.png" />  
			<BT url="https://standard-cloud.zendesk.com/hc/de/sections/115000172891" name={Trans.t("help")} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
		</div>
		<Helpbox title={Trans.t("help")}>
		<h3>A-Spalte</h3>
		<img className="usermanagerlisthelpicon" src="https://publicfile.standard-cloud.com/icons/ok.png" alt="Benutzer aktiv" />Benutzer aktiv<br/>
		<img className="usermanagerlisthelpicon" src="https://publicfile.standard-cloud.com/icons/denied.png" alt="Benutzer aktiv" />Benutzer darf sich nicht anmelden. 
		<br/>
		<h3>{Trans.t("user")}</h3>
		Login erfolgt immer E-Mail Adresse.
		<br/>
		<h3>B-Spalte</h3>
		<img className="usermanagerlisthelpicon" src="https://publicfile.standard-cloud.com/icons/user.png" alt="Profil anzeigen" />Link zum Profil des Nutzers.<br/>		
		<h3>PWD</h3>
		Aktivieren/Deaktivieren und Passwortvergabe.
		<br/>
		<h3>E-Spalte</h3>
		Profil bearbeiten
		<br/>
		<h3>R-Spalte</h3>
		Benutzer unwiederruflich löschen. (Daten bleiben erhalten)
		<br/>
		</Helpbox>
</div>
    );
	
}	     
  }
export default connect(null,{setNavigation})(adminuserlistpage);


