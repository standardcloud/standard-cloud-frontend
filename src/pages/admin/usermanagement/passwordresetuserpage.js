import Usermanager from '../../../module/usermanager/stdusermanager';
import BT from '../../../component/button/stdbutton';
import Dialog from '../../../component/dialog/stddialog';
import Contentbox from '../../../component/contentbox/contentbox';
import Helpbox from '../../../component/helpbox/helpbox';
import Displayobject from '../../../module/displayobject/stddisplayobject';
import {connect} from 'react-redux';
import {setNavigation} from '../../../actions/navigation';
import React from 'react';
import './passwordresetuserpage.css';

class PasswordResetPage extends React.Component {

	    constructor(props) {
		super(props);
		this.state = {userinfo:"",userdata:{},active:false,showsendbutton:false,modus:"",password:""};	
		this.loaduser = this.loaduser.bind(this);
		this.handlePasswordResetRadioChange = this.handlePasswordResetRadioChange.bind(this);
		this.handleChangePasswordText = this.handleChangePasswordText.bind(this);
		this.sendpassword = this.sendpassword.bind(this);
		this.enableaccount = this.enableaccount.bind(this);
	}		
componentDidMount() {
this.loaduser();
}
loaduser()
{
	var self = this;
	var testRE = window.location.pathname.match("resetpassword/(.*)/");
var resetuserid = testRE[1];
Usermanager.getuser(resetuserid).then(data=>
{
	self.setState({userdata:data});
	if(data.active ===true)
	{
		self.setState({active:true});
	}
	
	var result = Displayobject.profilelistdata(data.profiledata);
	self.setState({userinfo:result});
});
}

enableaccount()
{
var self = this;
var testRE = window.location.pathname.match("resetpassword/(.*)/");
var resetuserid = testRE[1];
var newstate = !this.state.active;
Usermanager.actvateuser(resetuserid,newstate ,function(data)
{
	if(newstate)
		{
			Dialog.ok("Benutzer","wurde aktiviert.");
		}else
		{
			Dialog.ok("Benutzer","wurde deaktiviert.");
		}
		self.setState({active:newstate});
});
}

sendpassword()
{
var self = this;
var testRE = window.location.pathname.match("resetpassword/(.*)/");
var resetuserid = testRE[1];
Usermanager.resetpassword(resetuserid,self.state.modus,self.state.password).then(data =>
{

if(data.state==="ok")
	{
		sessionStorage.clear();
	Dialog.ok("Aktion","erfolgreich druchgeführt.")
	setTimeout(function(){ window.location.href = "/admin/usermanagement/"; }, 3000);
	}else
{
	Dialog.error("Reset-Fehler",data.content.msg);
}
});
}
handleChangePasswordText(e)
{
	this.setState({password:e.target.value});
}
handlePasswordResetRadioChange(e)
{

	this.setState({modus:e.target.value});
}

render() {

 
 var buttontext = "Passwort per E-Mail senden"
 var sendbutton= null;
 var passwordfeld = null;
switch(this.state.modus)
{
	case "setpassword":
		buttontext = "Passwort speichern";
		passwordfeld = <input type="password" onChange={this.handleChangePasswordText} />
		var ic = "https://publicfile.standard-cloud.com/icons/save.png";
		sendbutton = <BT  callbackfunction={this.sendpassword} name={buttontext}  iconurl={ic} />
		break;
	case "sendmailcreatepassword":
		sendbutton = <BT  callbackfunction={this.sendpassword} name={buttontext}  iconurl="https://publicfile.standard-cloud.com/icons/emailpassword.png"/>
		break;
	case "sendmailsetpassword":
		sendbutton = <BT  callbackfunction={this.sendpassword} name={buttontext}  iconurl="https://publicfile.standard-cloud.com/icons/emailpassword.png" />
		passwordfeld = <input type="password" onChange={this.handleChangePasswordText} />
		break;
	default:
}

 var selfrender = this;
 var username = this.state.userdata.firstname + ", " + this.state.userdata.lastname + " (" + this.state.userdata.email + ")";

	return (
<div>
<Contentbox title="Passwort-Reset + Benutzer deaktivieren">
<div id="newuserboxcontent">
		</div>
       	 <div>
		 <Contentbox title="Passwort-Reset">
			 <div id="passwordresetcontentbox">
			                     <div className="stdradioboxregion" >
                                <input name="pwradio" value="sendmailcreatepassword" onChange={selfrender.handlePasswordResetRadioChange} id="sendpasswordtouser"  type="radio" className="radio" />
                                <label htmlFor="sendpasswordtouser" >Passwort an E-Mail ({this.state.userdata.email}) des Benutzer senden</label><br/>
								<input value="setpassword" name="pwradio" onChange={selfrender.handlePasswordResetRadioChange} id="custompassword"  type="radio" className="radio" />
                                <label htmlFor="custompassword" >Passwort selbst erstellen, keine E-Mail an den Benutzer senden</label><br/>
								<input value="sendmailsetpassword" name="pwradio" onChange={selfrender.handlePasswordResetRadioChange} id="custompasswordsendmail"  type="radio" className="radio" />
                                <label htmlFor="custompasswordsendmail" >Passwort selbst erstellen,  E-Mail an den Benutzer senden</label>
                           </div> <br/>  
			{passwordfeld}
	    
		{sendbutton}
		</div>
		</Contentbox>
   
		<div>
	</div><br/>
 <Contentbox title="Benutzer aktivieren">
	  <div className="content3pixmargin">
	 <br/>
	 			<div className="stdcheckboxregion">
                            <input type="checkbox" onClick={this.enableaccount} id="enableaccount" checked={this.state.active}   />
                            <label onClick={this.enableaccount}  htmlFor="enableaccount">Benutzer darf sich anmelden.</label>
			    </div>
				</div>
	<br/>
</Contentbox><br/>
 <Contentbox title={username}>
	 <div className="content3pixmargin">
       <div id="userinfobox" dangerouslySetInnerHTML={{__html: this.state.userinfo}}>
	   </div>   
	   </div>
	</Contentbox>  <br/>  
        </div>
		 </Contentbox>
		 <div id="usermanagementfooter">
			<BT  callbackfunction={() => window.history.back()} name="Zurück" iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
			<BT url="#" name="Hilfe" iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
		</div>
		<Helpbox title="Hilfe">
		Ein Benutzer hat sein Passwort vergessen?<br/>Kein Problem, hier können Sie ihm einen neues Passwort zukommen lassen. <br/>Sie haben die Möglichkeit, das Passwort selbst zu bestimmen
		oder dies vom System generieren zu lassen. <br/><br/>Außerdem können Sie ihm über Standard-Cloud dieses Passwort direkt per Mail zu senden. <br/><br/>Achten Sie daruf, dass die hier gelistete E-Mail dem gewünschen Empfänger gehört.
		</Helpbox>
</div>
    );
	
}	     
  }

  export default connect(null,{setNavigation})(PasswordResetPage);