import React from 'react';
import BT from '../../../component/button/stdbutton';
import Trans from '../../../module/translatoinmanager/translationmanager';
import {connect} from 'react-redux';
import {setNavigation} from '../../../actions/navigation';
import Dialog from '../../../component/dialog/stddialog';
import Helpbox from '../../../component/helpbox/helpbox';
import Contentbox from '../../../component/contentbox/contentbox';
import './authenticationprovidersetting.css';
import Authenticationmanager from '../../../module/authenticationmanager/stdauthenticationmanager';

class authenticationproviderpage extends React.Component {

	    constructor(props) {
		super(props);
		this.loadsettings = this.loadsettings.bind(this);
    this.handleenablestandardcloudlogin = this.handleenablestandardcloudlogin.bind(this);		
    this.handleadfslogin = this.handleadfslogin.bind(this);		
    this.handlefacebooklogin = this.handlefacebooklogin.bind(this);
    this.changefacebookapikey = this.changefacebookapikey.bind(this);
    this.changefacebookapisecret = this.changefacebookapisecret.bind(this);
    this.changeadfsclientid = this.changeadfsclientid.bind(this);
    this.changeadfsurl = this.changeadfsurl.bind(this);
    this.savesettings = this.savesettings.bind(this);     
		this.state = {settings:{enablestandardcloudlogin:false,enableadfslogin:false,enablefacebooklogin:false,facebook:{apikey:"",apisecret:""},adfs:{adfsclientid:"",adfsurl:"",adfsclaims:[]}}};
				

	}	

componentDidMount() {
	this.loadsettings();
	var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"),url:"/admin/"},{title:Trans.t("userlist"),url:"/admin/usermanagement/"},{title:Trans.t("authenticationprovider")}];
	this.props.setNavigation(nav,Trans.t("authenticationprovider"));
}
savesettings()
{
	Authenticationmanager.setauthenticationprovidersetting(this.state.settings,function(data)
{
if(data.state==="ok")
  {
    Dialog.ok("Einstellungen","erfolgreich gespeichert.");
  }
});
}
handleenablestandardcloudlogin()
{
	var settings = this.state.settings;
	settings.enablestandardcloudlogin = !settings.enablestandardcloudlogin;
	this.setState({settings:settings});
}
handleadfslogin()
{
	var settings = this.state.settings;
	settings.enableadfslogin = !settings.enableadfslogin;
	this.setState({settings:settings});
}
handlefacebooklogin()
{
	var settings = this.state.settings;
	settings.enablefacebooklogin = !settings.enablefacebooklogin;
	this.setState({settings:settings});
}

loadsettings()
{
var self = this;
Authenticationmanager.getauthenticationprovidersetting(function(data)
{
	self.setState({settings:data.content});
});
}
changefacebookapikey(e)
{
	var settings = this.state.settings;
	settings.facebook.apikey = e.target.value;
	this.setState({settings:settings});
}
changefacebookapisecret(e)
{
	var settings = this.state.settings;
	settings.facebook.apisecret = e.target.value;
	this.setState({settings:settings});
}
changeadfsclientid(e)
{
	var settings = this.state.settings;
	settings.adfs.adfsclientid = e.target.value;
	this.setState({settings:settings});
}

changeadfsurl(e)
{
	var settings = this.state.settings;
	settings.adfs.adfsurl = e.target.value;
	this.setState({settings:settings});
}



adfsurl
render() {
	

var claims = this.state.settings.adfs.adfsclaims.map(function(claim)
{
	return(<tr key={claim.stdcloudpropertyid}className="claimrow"><td>{claim.stdcloudpropertyid}</td><td>{claim.jwtproperty}</td><td><img className="authproviderpage-delicon-img" alt="Löschen" src="https://publicfile.standard-cloud.com/icons/delete.png"/></td></tr>);
});


	return (
<div>
<Contentbox title={Trans.t("configauthprovider")} >
<div id="newuserboxcontent">
        <div><br/>
       <Contentbox title="Standard-Cloud">
	   <div id="authenticationprofiderpage-standardcloudbox-div">
		   	              <div className="stdcheckboxregion">
                            <input id="enablestandardcloud" onClick={this.handleenablestandardcloudlogin} checked={this.state.settings.enablestandardcloudlogin} type="checkbox" className="checkbox"/>
                            <label   htmlFor="enablestandardcloud" dangerouslySetInnerHTML={{__html: Trans.t("authenticationproviderallowlogin")}}></label>
                </div>
				</div>
	</Contentbox><br/>
	 <Contentbox title="ADFS (Windows)">
	 <div id="authenticationprofiderpage-windowsbox-div">
		 		   	              <div className="stdcheckboxregion">
                                    <input type="checkbox" id="enableadfs" onClick={this.handleadfslogin}  checked={this.state.settings.enableadfslogin} name="enableadfslogin" value="enableadfslogin"  />
                            <label   htmlFor="enableadfs" dangerouslySetInnerHTML={{__html: Trans.t("allowadfslogin")}}></label>
                </div>
       	 <table id="authenticationprofiderpage-windowsbox-table">
				<tbody>
          <tr> 
            <td>Client-ID</td><td> <input type="password" onChange={this.changeadfsclientid} value={this.state.settings.adfs.adfsclientid}  /></td> 
           </tr>
            <tr>
           <td>ADFS-URL</td><td> <input type="password" onChange={this.changeadfsurl} value={this.state.settings.adfs.adfsurl}  />(z.B. https://adfs.firma.com)</td> 
           </tr>
		   </tbody>
          </table>
          
           <table id="ClaimEditor">
			   <thead>
          <tr>
              <th>Profil-Eigenschaft</th>
              <th>JWT-Eigenschaft</th>
            <th></th>
          </tr></thead>
		  <tbody>
  		{claims}
        </tbody>
	    </table>
		</div>
             </Contentbox> <br/>    
			 <Contentbox title="Facebook">
			 <div id="authenticationprofiderpage-facebookbox-div">
			<div className="stdcheckboxregion">
                            <input type="checkbox"onClick={this.handlefacebooklogin} checked={this.state.settings.enablefacebooklogin} id="enablefacebookbutton" value="enableadfslogin"  />
                            <label htmlFor="enablefacebookbutton" dangerouslySetInnerHTML={{__html: Trans.t("allowfacebooklogin")}}></label>
			</div>
        <table id="authenticationprovider-facebook-table">		
			<tbody>	
          <tr>
            <td>apiKey</td><td> <input type="password" value={this.state.settings.facebook.apikey}  onChange={this.changefacebookapikey} /></td> 
           </tr>
            <tr>
           <td>apiSecret</td><td> <input type="password" onChange={this.changefacebookapisecret} value={this.state.settings.facebook.apisecret} /></td> 
           </tr>
		   </tbody>
          </table>
		  </div>
		  </Contentbox>
		</div>
		<div>

		<br/><br/>
			
	</div>
        </div>
		 </Contentbox>
		 <div id="usermanagementfooter">
       	<BT  callbackfunction={() => window.history.back()} name={Trans.t("back")} iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
		   <BT callbackfunction={this.savesettings} name={Trans.t("save")} iconurl="https://publicfile.standard-cloud.com/icons/save.png" />  
			<BT url="#" name={Trans.t("help")} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
		</div>
		<Helpbox title={Trans.t("help")}>
		Wenn Sie die Facebook oder Windows(ADFS) Authentifizierung wünschen, sollen Sie hierfür den Support kontaktieren. <br/><br/>Dieser teilt Ihnen die nötigen Schritt für die Konfiguration der Authentifizierungssysteme mit.
		</Helpbox>
</div>
    );
	
}	     
  }


  export default connect(null,{setNavigation})(authenticationproviderpage);