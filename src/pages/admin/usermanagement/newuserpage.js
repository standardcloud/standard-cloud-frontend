import Usermanager from '../../../module/usermanager/stdusermanager';
import Trans from '../../../module/translatoinmanager/translationmanager';
import React from 'react';
import BT from '../../../component/button/stdbutton';
import Dialog from '../../../component/dialog/stddialog';
import {connect} from 'react-redux';
import {setNavigation} from '../../../actions/navigation';
import Helpbox from '../../../component/helpbox/helpbox';
import Contentbox from '../../../component/contentbox/contentbox';
import './newuserpage.css';

class newuserpage extends React.Component {

	    constructor(props) {
		super(props);
		this.state = {user:{email:"",firstname:"",lastname:"",
		password:"",passwordinfo:""}};
		this.handleEMailChange = this.handleEMailChange.bind(this);		
		this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
		this.handleLastNameChange = this.handleLastNameChange.bind(this);
		this.handleUserInfoChange = this.handleUserInfoChange.bind(this);
		this.handlePasswordChange = this.handlePasswordChange.bind(this);		
		this.saveuser = this.saveuser.bind(this);
		var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"),url:"/admin/"},{title:Trans.t("userlist"),url:"/admin/usermanagement/"},{title:Trans.t("newuser")}];
		this.props.setNavigation(nav,Trans.t("newuser"));		
  }		

handleEMailChange(event) {
		var user = this.state.user;
		user.email = event.target.value;
        this.setState({user: user});             
}

handlePasswordChange(event) {
		var user = this.state.user;
		user.password = event.target.value;
        this.setState({user: user});             
}

	handleUserInfoChange(event)
	{
		var user = this.state.user;
		user.passwordinfo = event.target.value;
        this.setState({user: user});   
	}
handleFirstNameChange(event)
{
		var user = this.state.user;
		user.firstname = event.target.value;
        this.setState({user: user});      
}
handleLastNameChange(event)
{
		var user = this.state.user;
		user.lastname = event.target.value;
        this.setState({user: user});    
}
saveuser()
{
	var user = this.state.user;

	if(user.email.indexOf("@")<1)
		{
			Dialog.error("Keine E-Mail","Eine gültige E-Mail enthält ein @-Zeichen");
			return;
		}
		if(user.email.indexOf(".")<1)
		{
			Dialog.error("Kein Punkt","Eine gültige E-Mail enthält einen Punkt");
			return;
		}
	Usermanager.newuser(user.email,user.firstname,user.lastname,user.passwordinfo,user.password).then(data =>
	{
		if(data.state==="ok")
			{
				var username = user.firstname + ", " + user.lastname;
				Dialog.ok(username,"erfolgreich erzeugt.");
         		  window.location.href = "/admin/usermanagement/"
			}else
			{
				Dialog.error(username,"konnte nicht erzeugt werden.");
			}
			sessionStorage.clear();
	});
}



render() {
	
 var selfrender = this;
 var senduserpassword = false;
 var custompasswordsendmail = false;
 var custompassword = false;
 var passwordfield = null;

 switch(this.state.user.passwordinfo)
 {
	case "senduserpassword":
	senduserpassword = true;
	break;
	case "custompasswordsendmail":
	custompasswordsendmail = true;
	passwordfield = <div>{Trans.t("password")}:<input name="userpwd" onChange={this.handlePasswordChange} class="forminputfield" type="password"/></div>
	break;
	case "custompassword":
	custompassword = true;
	passwordfield = <div>{Trans.t("password")}:<input name="userpwd" onChange={this.handlePasswordChange} class="forminputfield" type="password"/></div>
	break;
	default:

 }

	return (
<div>
<Contentbox title={Trans.t("newuser")}>
<div id="newuserboxcontent">
					   <br/>		
			    <div className="textboxgroup">      
				<input required className="stdtextbox" type = "text" onChange={this.handleEMailChange} value={this.state.user.email} />
				<span className="texthighlight"></span>
				<span className="textbar"></span>
				<label>E-Mail</label>
			    </div>
				<div className="textboxgroup">      
				<input required className="stdtextbox" type = "text" onChange={this.handleFirstNameChange} value={this.state.user.firstname} />
				<span className="texthighlight"></span>
				<span className="textbar"></span>
				<label>{Trans.t("firstname")}</label>
			    </div>		
				<div className="textboxgroup">      
				<input required className="stdtextbox" type = "text" onChange={this.handleLastNameChange} value={this.state.user.lastname} />
				<span className="texthighlight"></span>
				<span className="textbar"></span>
				<label>{Trans.t("lastname")}</label>
			    </div>
				<div className="stdradioboxregion" >
                                <input name="senduserpassword" checked={senduserpassword}  id="senduserpassword"  onChange={selfrender.handleUserInfoChange}  type="radio" class="radio" value="senduserpassword" />
                                <label htmlFor="senduserpassword">{Trans.t("sendpwdtouser")}</label>
                </div>		
								<div className="stdradioboxregion" >
                                <input id="custompasswordsendmail" checked={custompasswordsendmail}  name="custompasswordsendmail" onChange={selfrender.handleUserInfoChange}   type="radio" class="radio" value="custompasswordsendmail" />
                                <label htmlFor="custompasswordsendmail" dangerouslySetInnerHTML={{__html: Trans.t("newuserpage-createpassword-sendmail")}}></label>
                </div>
								<div className="stdradioboxregion" >
                                <input id="custompassword" checked={custompassword}  name="custompassword" onChange={selfrender.handleUserInfoChange}   type="radio" class="radio" value="custompassword" />
                                <label htmlFor="custompassword" dangerouslySetInnerHTML={{__html: Trans.t("newuserpage-createpassword-nomail")}}></label>
                </div>
				{passwordfield}		
          <br/> 
		  <BT callbackfunction={this.saveuser} name={Trans.t("usercreate")} iconurl="https://publicfile.standard-cloud.com/icons/save.png" />  
  <br/> 
		</div>
       	 <div>

	    
        </div>
		 </Contentbox>
		 <div id="usermanagementfooter">
			<BT  callbackfunction={() => window.history.back()} name={Trans.t("back")} iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
			<BT url="#" name={Trans.t("help")} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
		</div>
		<Helpbox title={Trans.t("help")}>
	   Ohne E-Mail kann kein Benutzer erzeugt werden. Denn der Login erfolt per Mail. <br/><br/>Außerdem müssen <strong>Vor- und Nachnamen</strong> bestimmt werden.<br/><br/>Das Passwort
	   kann automatisch generiert werden (Option 1) und an die E-Mail des Nutzers gesendet werden.<br/><br/>Als Administartor können Sie aber auch selbst das Passwort bestimmen und dieses automatisiert 
	   versenden (Option 2). <br/><br/>Wenn Sie lieber das Passwort persönlich mitteilen, wählen Sie Option (3).
		</Helpbox>
</div>
    );
	
}	     
  }

  export default connect(null,{setNavigation})(newuserpage);
