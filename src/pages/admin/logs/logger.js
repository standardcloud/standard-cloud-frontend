
import {connect} from 'react-redux';
import {setNavigation} from '../../../actions/navigation';
import React from 'react';
import logger from '../../../module/logger/stdlogger';
import './logger.css';
import Trans from '../../../module/translatoinmanager/translationmanager';

class LoggerPage extends React.Component {
	constructor(props) {
	super(props);
	this.state = {log:[],searchterm:""};
	this.loadlogs = this.loadlogs.bind(this);
	var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"),url:"/admin/"},{title:"Systemlogs"}];
	this.props.setNavigation(nav,"Systemlogs");
}


componentDidMount() {
	this.loadlogs();
}

loadlogs()
{
	var logself = this;
	logger.getlogs().then(data =>
	{
		logself.setState({log:data.log});	
	});
}


render() {
var self = this;
if(this.state.log)
{
	            var logrows = this.state.log.map(function(logentry) {
					if(self.state.searchterm.length > 2)
					{
						if(JSON.stringify(logentry).toLocaleLowerCase().indexOf(self.state.searchterm.toLocaleLowerCase()) ===-1)
						{
							return null;
						}
					}
				
					
      return (
        <tr key={logentry.id}><td>{logentry.loglevel}</td><td>{logentry.date}</td><td>{logentry.msg}</td><td>{logentry.requestguid}</td><td>{logentry.user}</td></tr>
      );       
    });  
	}
		return (
	<div>
		<input type="text" onChange={(e)=>{this.setState({searchterm:e.target.value.toLowerCase()})}}  placeholder="Filter" id="logger-searchbox" />
		<table id="tbllistdataresult">
			<thead>
		<tr><th>{Trans.t("state")}</th><th>{Trans.t("date")}</th><th>{Trans.t("message")}</th><th>Request ID</th><th>{Trans.t("user")}</th></tr>
		</thead><tbody>
		{logrows}
			</tbody>
		</table>
</div>
        );

		}
}



export default connect(null,{setNavigation})(LoggerPage);

