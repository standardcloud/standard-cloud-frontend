import React from 'react';
import Trans from '../../../module/translatoinmanager/translationmanager';

import BT from '../../../component/button/stdbutton';
import {connect} from 'react-redux';
import {setNavigation} from '../../../actions/navigation';

class PermissionOverview extends React.Component {
		constructor(props) {
		super(props);
		var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"), url:"/admin/"},{title:Trans.t("permissionmanagement")}];
		this.props.setNavigation(nav,Trans.t("permissionmanagement"));
  }

render(){
		return (
		<div>
		<BT url="/admin/permissionmgmt/key/" name={Trans.t("keys")} iconurl="https://publicfile.standard-cloud.com/icons/claim.png" /> 
		<BT url="/admin/permissionmgmt/systemright/" name={Trans.t("systemrights")} iconurl="https://publicfile.standard-cloud.com/icons/systemright.png" /> 		
		</div>
        );
		}
  }
  

	export default connect(null,{setNavigation})(PermissionOverview);

