import React from 'react';
import BT from '../../../../component/button/stdbutton';
import {connect} from 'react-redux';
import {setNavigation} from '../../../../actions/navigation';
import Helpbox from '../../../../component/helpbox/helpbox';
import Contentbox from '../../../../component/contentbox/contentbox';
import permissionmanager from '../../../../module/permissionmanager/stdpermissionmanager';
import Trans from '../../../../module/translatoinmanager/translationmanager';
import './keylist.css';
class KeyListPage extends React.Component {
	    constructor(props) {
		super(props);
		this.state = {keys:[],searchtext:""};
		this.handleSearchTextChange = this.handleSearchTextChange.bind(this);			
		var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"), url:"/admin/"},{title:Trans.t("permissionmanagement"), url:"/admin/permissionmgmt/"},{title:Trans.t("keylist")}];
		this.props.setNavigation(nav,Trans.t("keylist"));
  }			  
componentDidMount() 
{
	this.loadkeylist();
}

loadkeylist()
{
	var self = this;
	permissionmanager.getkeylist().then(data =>
	{
		self.setState({keys:data.content});
	});
}

clickshowkey(key)
{
	this.props.history.push("/admin/permissionmgmt/key/" + key.id +"/edit/");
}


  handleSearchTextChange(event) {           
   this.setState({searchtext: event.target.value.toLowerCase()});                    
 }

clickback()
{
window.history.back();
}

render() {
	var self = this;
	var tblrows = this.state.keys.map(function(key) {
        if(self.state.searchtext !== "")
        {
            if(key.name.toLowerCase().indexOf(self.state.searchtext) > -1 || key.description.toLowerCase().indexOf(self.state.searchtext) > -1 )
            {
					
			}else
			{
				return null;
			}    
		}	
		if(key.id ==="88D5C7EA-A222-41ED-8407-02453C89D23F")
			{
						return(<tr key={key.id} className="keylistuserkey"><td className="keylisttdimg"><img alt="" src="https://publicfile.standard-cloud.com/icons/userfalse.png"/></td><td  className="keylisttdname" dangerouslySetInnerHTML={{__html:key.name}}></td><td dangerouslySetInnerHTML={{__html:key.description}}></td></tr>);
			}


		if(key.userkey)
			{
			return(<tr key={key.id} className="keylistuserkey"><td className="keylisttdimg"><img  alt="" src="https://publicfile.standard-cloud.com/icons/userfalse.png"/></td><td  className="keylisttdname" dangerouslySetInnerHTML={{__html:key.name}}></td><td dangerouslySetInnerHTML={{__html:key.description}}></td></tr>);
			}else
			{
			return(<tr key={key.id} className="keylistkey" onClick={() => self.clickshowkey(key)} ><td className="keylisttdimg"><img alt="Schlüssel" src="https://publicfile.standard-cloud.com/icons/claim.png"/></td><td  className="keylisttdname" dangerouslySetInnerHTML={{__html:key.name}}></td><td dangerouslySetInnerHTML={{__html:key.description}}></td></tr>);
			}	
		});

	return (
     <div id="trash_page">
   <Contentbox title={Trans.t("key")}>
		 <div id="trash_content">
	      <input id="trashsearchbox"   onChange={this.handleSearchTextChange}  placeholder="Filter" type="search"  autoComplete="off"/><br/>
		  <table id="key_tablelist" className="stdtable"><thead>
			<tr><th className="imgrowkeylist"></th><th>{Trans.t("name")}</th><th>{Trans.t("description")}</th></tr>
			</thead>
			<tbody>
			{tblrows}
			</tbody>
			</table>
	    </div>
      </Contentbox>
		<div>
<Helpbox title={Trans.t("help")}>
  Mit einem Klick auf den Schlüssel sehen Sie, welche Nutzer über diesen verfügen. <br/><br/><strong>Listen, Listenvorlagen, Objekte</strong> können mit einem Schloss für bestimmte Schlüssel berechtigt werden. <br/><br/>Die Beschreibung des Schlüssel sollte einen Rückschluss auf den Einsatzzweck (z.B. "Zugriff auf Liste Kunden") <br/>oder die Funktion <br/>(z.B. "Schlüssel der Auszubildenden") erlauben.<br/><br/>
	Jeder Anwender <img id="helpboxkeylistpageimg" alt="" src="https://publicfile.standard-cloud.com/icons/userfalse.png"/> besitzt einen persönlichen Schlüssel. Diesem können keine weitern Nutzer zugeordnet werden.<br/>Dem Schlüssel <strong>All</strong> er alle Benutzer zugeordnet, auch dieser kann nicht bearbeitet werden.
	
</Helpbox>
       
<div id="newlistfooter">
			 <BT  callbackfunction={this.clickback} name={Trans.t("back")}  iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
			 <BT  url="new/" name={Trans.t("keynew")}   iconurl="https://publicfile.standard-cloud.com/icons/claim.png" />
			 <BT reload url="https://standard-cloud.zendesk.com/hc/de/articles/115000611651-Papierkorb"  name={Trans.t("help")} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
      </div>
				</div>
	</div>
    );
	
}	  
  }
    
export default connect(null,{setNavigation})(KeyListPage);

