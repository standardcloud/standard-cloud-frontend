import React from 'react';
import BT from '../../../../component/button/stdbutton';
import Helpbox from '../../../../component/helpbox/helpbox';
import Contentbox from '../../../../component/contentbox/contentbox';
import PermissionManager from '../../../../module/permissionmanager/stdpermissionmanager';
import {connect} from 'react-redux';
import {setNavigation} from '../../../../actions/navigation';
import Trans from '../../../../module/translatoinmanager/translationmanager';

  class NewKeyPage extends React.Component {
	    constructor(props) {
		super(props);
		this.state = {key:{name:"",description:""}};
		this.handlechangename = this.handlechangename.bind(this);
		this.handlechangedescription = this.handlechangedescription.bind(this);
		this.clicksavekey = this.clicksavekey.bind(this);
		var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"), url:"/admin/"},{url:"/admin/permissionmgmt/",title:Trans.t("permissionmanagement")},{url:"/admin/permissionmgmt/key/",title:Trans.t("keylist")},{title:Trans.t("keynew")}];
		this.props.setNavigation(nav,Trans.t("keynew"));

  }			  
clicksavekey() 
{
	var self = this;
	var key = this.state.key;
	PermissionManager.newkey(key.name,key.description).then(data =>
{
	if(data.state==="ok")
		{
			self.props.history.push("/admin/permissionmgmt/key/");
		}
});
}


clickback()
{
window.history.back();
}

handlechangename(e)
{
	var oldst = this.state.key;
	oldst.name = e.target.value;
	this.setState({key:oldst});
}
handlechangedescription(e)
{
	var oldst = this.state.key;
	oldst.description = e.target.value;
	this.setState({key:oldst});
}


render() {
	var title =" "+Trans.t("key")+" " + this.state.key.name;
	return (
     <div id="singlekey_page">
  	 <Contentbox title={title}>
		 <div id="singlekey_content">
			 <table id="singlekey_content_data">
				 <tr>
					 <td className="infotd">{Trans.t("name")}</td><td><input type="text" onChange={this.handlechangename} value={this.state.key.name}/></td>
					 </tr>
					 <tr>
					 <td>{Trans.t("description")}  </td><td><input type="text" maxLength="249" onChange={this.handlechangedescription} value={this.state.key.description}/></td>
					 </tr>
				</table><br/>
			   <div id="savesingekeybuttonbar">
				<BT  callbackfunction={this.clicksavekey} name={Trans.t("save")}  iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
			 </div>
	    </div>
      </Contentbox>
		<div>
<Helpbox title={Trans.t("help")}>
Neuen Schlüssel erzeugen
</Helpbox>
       
<div id="newlistfooter">
			 <BT  callbackfunction={this.clickback} name={Trans.t("back")}   iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
			 <BT url="https://standard-cloud.zendesk.com/hc/de/articles/115000611651-Papierkorb"  name={Trans.t("help")} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
      </div>
				</div>
	</div>
    );
	
}	  
  }


export default connect(null,{setNavigation})(NewKeyPage);