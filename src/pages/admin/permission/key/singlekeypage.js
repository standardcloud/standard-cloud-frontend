import React from 'react';
import BT from '../../../../component/button/stdbutton';
import dialog from '../../../../component/dialog/stddialog';
import Helpbox from '../../../../component/helpbox/helpbox';
import Contentbox from '../../../../component/contentbox/contentbox';
import PermissionManager from '../../../../module/permissionmanager/stdpermissionmanager';
import Userlist from '../../../../component/user/userlist/userlist';
import {connect} from 'react-redux';
import {setNavigation} from '../../../../actions/navigation';
import Dialogbox from '../../../../component/dialogbox/dialogbox';
import Trans from '../../../../module/translatoinmanager/translationmanager';
import './singlekeypage.css';

class SingleKeyPage extends React.Component {
	    constructor(props) {
		super(props);
		this.state = {key:{users:[],name:"",description:""},removedialoguser:null};
		this.handlechangename = this.handlechangename.bind(this);
		this.handlechangedescription = this.handlechangedescription.bind(this);
		this.removeuser = this.removeuser.bind(this);
		this.removekeyfromuser = this.removekeyfromuser.bind(this);
		this.adduser = this.adduser.bind(this);
		this.removeuserkeydialg = this.removeuserkeydialg.bind(this);
		this.clicksavekey = this.clicksavekey.bind(this);
		var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"), url:"/admin/"},{url:"/admin/permissionmgmt/",title:Trans.t("permissionmanagement")},{url:"/admin/permissionmgmt/key/",title:Trans.t("keylist")},{title:Trans.t("keyedit")}];
		this.props.setNavigation(nav,Trans.t("keyedit"));
  }			  
componentDidMount() 
{
	this.loadkey();
}

loadkey()
{
	var self = this;
		var test = window.location.href;
		var testRE = test.match("key/(.*)/edit");
		var keyid = testRE[1];	
		PermissionManager.getkey(keyid).then(data =>
	{
		if(data.state==="ok")
			{
			self.setState({key:data.content});
			}
	});
}



clickback()
{
window.history.back();
}

handlechangename(e)
{
	this.setState({ key: { ...this.state.key, "name": e.target.value } });
}
handlechangedescription(e)
{
	var oldst = this.state.key;
	oldst.description = e.target.value;
	this.setState({key:oldst});
}
removekeyfromuser(user)
{
	var self = this;
	PermissionManager.removekeyfromuser(user.id,this.state.key.id).then(data =>
{
	if(data.state==="ok")
		{
    dialog.ok(user.name,"wurde Schlüssel entzogen")		
	var oldstate = self.state.key;
	var newuserlist = [];
	for(var j=0;j< self.state.key.users.length;j++)
		{
			var newu = self.state.key.users[j];
			
			if(newu.id !== user.id)
				{
				newuserlist.push(newu);
				}
		}
		oldstate.users = newuserlist;
		self.setState({key:oldstate});

		}
});
}
clicksavekey()
{
	var key = this.state.key;
	var userids = "";
	for(var x=0; x < key.users.length;x++)
		{
			userids += key.users[x].id + ";";
		}

		PermissionManager.updatekey(key.id,key.name,key.description,userids).then(data =>
{
	if(data.state==="ok")
		{
			dialog.ok(key.name,"erfolgreich gespeichert.");
		}else
		{
			dialog.error(key.name,"konnnte nicht gepeichert werden.")
		}
});
}
adduser(user)
{
	for(let i=0;i<this.state.key.users.length;i++)
		{
			var u = this.state.key.users[i];
			if(u.userid === user.userid)
				{
					dialog.error(user.name,"bereits in Liste vorhanden.")
					return;
				}
		}

	var oldst = this.state.key;
	oldst.users.push(user);
	this.setState({key:oldst});
}
removeuser(user)
{
		this.setState({removedialoguser:user});
}

removeuserkeydialg()
{
	var self =this;
	return (<Dialogbox title="Schlüsselabnahme">
	Dem Benutzer <strong>{this.state.removedialoguser.name}</strong>  wirklich den Schlüssel  {this.state.key.name} entziehen?
	<button type="button" onClick={()=>{self.removekeyfromuser(this.state.removedialoguser);self.setState({removedialoguser:null})}}>Ja</button>
	<button type="button" onClick={()=>{self.setState({removedialoguser:null})}}>NEIN</button>
	</Dialogbox>);
}

render() {
	var self = this;
	var title = " " + Trans.t("key") + " " + this.state.key.name;
	var removedialog = null;
	if(this.state.removedialoguser)
	{
		console.log(this.state.removedialoguser);
		removedialog = this.removeuserkeydialg();
	}


	return (
     <div id="singlekey_page">
		 {removedialog}
  	 <Contentbox title={title}>
		 <div id="singlekey_content">
			 <table id="singlekey_content_data">
				 <tbody>
				 <tr>
					 <td className="infotd">{Trans.t("name")}</td><td><input type="text" onChange={this.handlechangename} value={this.state.key.name}/></td>
					 </tr>
					 <tr>
					 <td>{Trans.t("description")}</td><td><input type="text" maxLength="249" onChange={this.handlechangedescription} value={this.state.key.description}/></td>
					 </tr>
					 </tbody>
				</table><br/>
				   <Contentbox title={Trans.t("owner")} >
					   <div id="singlekeyboxowner">
					   <br/>
						<Userlist adduser={self.adduser} clickevent={self.removeuser} users={this.state.key.users} />
						<br/>
						</div>
				   </Contentbox>
				   <div id="savesingekeybuttonbar">
				   <BT  callbackfunction={this.clicksavekey} name={Trans.t("save")}   iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
				   </div>
	    </div>
      </Contentbox>
		<div>
<Helpbox title={Trans.t("help")}>
	Hier können Sie die Informationen eines Schlüssel aktualisieren oder bestimmten Nutzern diesen entreißen. <br/>Fahren Sie hierfür mit der Maus über den Namen des Nutzers.
	<br/>Anschließend müssen Sie die Abgabe des Schlüssel bestätigen.
</Helpbox>
       
<div id="newlistfooter">
			 <BT  callbackfunction={this.clickback} name={Trans.t("back")} iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
			 <BT  url="new/" name={Trans.t("keynew")}    iconurl="https://publicfile.standard-cloud.com/icons/claim.png" />
			 <BT reload url="https://standard-cloud.zendesk.com/hc/de/articles/115000611651-Papierkorb"  name={Trans.t("help")} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
      </div>
				</div>
	</div>
    );
	
}	  
}

export default connect(null,{setNavigation})(SingleKeyPage);