
import React from 'react';
import BT from '../../../../component/button/stdbutton';
import {connect} from 'react-redux';
import {setNavigation} from '../../../../actions/navigation';
import { withRouter } from 'react-router-dom';
import dialog from '../../../../component/dialog/stddialog';
import $ from 'jquery';
import Helpbox from '../../../../component/helpbox/helpbox';
import Contentbox from '../../../../component/contentbox/contentbox';
import PermissionManager from '../../../../module/permissionmanager/stdpermissionmanager';
import Keylistv2 from '../../../../component/permission/keylist/keylistv2';
import Trans from '../../../../module/translatoinmanager/translationmanager';


class SingleSystemRightPage extends React.Component {
	    constructor(props) {
		super(props);
		this.state = {systemright:{keys:[]}};
		this.removekey = this.removekey.bind(this);
		this.addkey = this.addkey.bind(this);
		this.clicksavesystemright = this.clicksavesystemright.bind(this);
		var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"), url:"/admin/"},{url:"/admin/permissionmgmt/",title:Trans.t("permissionmanagement")},{url:"/admin/permissionmgmt/systemright/",title:Trans.t("systemrights")},{title:Trans.t("systemright")}];
		this.props.setNavigation(nav,Trans.t("systemright"));
  }			  
componentDidMount() 
{
	this.loadsystemright();
}

loadsystemright()
{
	var self = this;
	var test = window.location.href;
	var testRE = test.match("systemright/(.*)/");
	var systemrightid = testRE[1];	
	PermissionManager.getsystemright(systemrightid).then(data=>
	{
		if(data.state==="ok")
			{
			self.setState({systemright:data.content});
			}
	});
}

clickback()
{
    window.history.back();
}

removekeyfromsystemright(key)
{
    var self = this;		
	var oldstate = self.state.systemright;
	var newkeylist = [];
	for(var j=0;j< self.state.systemright.keys.length;j++)
		{
			var newu = self.state.systemright.keys[j];
				if(newu.id !== key.id)
				{
				newkeylist.push(newu);
				}
		}
		oldstate.keys = newkeylist;
		self.setState({key:oldstate});
}

clicksavesystemright()
{
    	var test = window.location.href;
	var testRE = test.match("systemright/(.*)/");
	var systemrightid = testRE[1];	
	var systemright = this.state.systemright;
    var keyids = "";
    
	for(var x=0; x < systemright.keys.length;x++)
		{
			keyids +=  systemright.keys[x].id + ";";
		}

PermissionManager.updatesystemrights(systemrightid,keyids).then(data =>
{
	if(data.state==="ok")
		{
			dialog.ok("Systemberechtigungen","erfolgreich gespeichert.");
		}else
		{
			dialog.error("Systemberechtigungen","konnnte nicht gepeichert werden.")
		}
});
}
addkey(key)
{
    console.log(key);
	for(let i=0;i<this.state.systemright.keys.length;i++)
		{
			var u = this.state.systemright.keys[i];
			if(u.id === key.id)
				{
					dialog.error(key.name,"bereits in Liste vorhanden.")
					return;
				}
		}

	var oldst = this.state.systemright;
	oldst.keys.push(key);
	this.setState({systemright:oldst});
}

showkey(key)
{
	
    window.location.href ="/admin/permissionmgmt/key/" + key.id + "/edit/";
}

removekey(key)
{
		var self = this;
			$.Zebra_Dialog('Das Systemrecht ' + this.state.systemright.name + ' dem Schlüssel ' + key.name + ' entziehen?<br/><br/>Wenn Sie nicht wissen, welche Benutzer den Schlüssel besitzen, dann klicken Sie auf "Benutzer anzeigen."', {
				'type':    'question',
				'title':    'Schlüssel entwenden',
				'buttons':  [
								{caption: 'JA', callback: function() { 	
												
								self.removekeyfromsystemright(key);
					
								}},
                                {caption: 'NEIN', callback: function() { }},
                                {caption: 'Benutzer anzeigen', callback: function() { self.showkey(key); }},
								
							]
			});
}

render() {
	var self = this;
	var title ="test";
	return (
     <div id="singlekey_page">
  	 <Contentbox title={title}>
		 <div id="singlekey_content">
			 <table id="singlekey_content_data">
				 <tbody>
				 <tr>
					 <td className="infotd">Name</td><td><div>{this.state.systemright.name}</div></td>
					 </tr>
					 <tr>
					 <td>Beschreibung</td><td><div>{this.state.systemright.description}</div></td>
					 </tr>
				</tbody>
				</table><br/>
				   <Contentbox title="Besitzer">
					   <div id="singlekeyboxowner">
					   <br/>
						<Keylistv2 addkey={self.addkey} clickevent={self.removekey} keys={this.state.systemright.keys} />
						<br/>
						</div>
				   </Contentbox>
				   <div id="savesingekeybuttonbar">
				   <BT  callbackfunction={this.clicksavesystemright} name="Speichern"   iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
				   </div>
	    </div>
      </Contentbox>
		<div>
<Helpbox title="Hilfe">
Sie können Personen mit weitreichenden Rechten unterstützen, indem Sie den persönlichen Schlüssel (Name des Anwenders) berechtigen. Alternativ können Sie mehrere Personen über gemeinesame Schlüssel berechtigen.
</Helpbox>
       
<div id="newlistfooter">
			 <BT  callbackfunction={this.clickback} name="Zurück"   iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
					 <BT reload url="https://standard-cloud.zendesk.com/hc/de/articles/115000611651-Papierkorb"  name="Hilfe" iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
      </div>
				</div>
	</div>
    );
	
}	  
  }

  export default withRouter(connect(null,{setNavigation})(SingleSystemRightPage));