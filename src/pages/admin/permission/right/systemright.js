import React from 'react';
import BT from '../../../../component/button/stdbutton';
import {connect} from 'react-redux';
import {setNavigation} from '../../../../actions/navigation';


import Helpbox from '../../../../component/helpbox/helpbox';
import Contentbox from '../../../../component/contentbox/contentbox';
import permissionmanager from '../../../../module/permissionmanager/stdpermissionmanager';
import Trans from '../../../../module/translatoinmanager/translationmanager';
class SystemRightListPage extends React.Component {
	    constructor(props) {
		super(props);
		this.state = {systemrights:[],searchtext:""};
		this.handleSearchTextChange = this.handleSearchTextChange.bind(this);
		var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"), url:"/admin/"},{title:Trans.t("permissionmanagement"), url:"/admin/permissionmgmt/"},{title:Trans.t("systemrights")}];
		this.props.setNavigation(nav,Trans.t("systemrights"));			
  }			  
componentDidMount() 
{
	this.loadloadsystemrightlist();
}

loadloadsystemrightlist()
{
	var self =this;
	permissionmanager.getsystemrights().then(data=>
	{
		self.setState({systemrights:data.content});
	});
}

clickshowsystemright(right)
{
	this.props.history.push("/admin/permissionmgmt/systemright/" + right.id +"/");
	//window.location.href = "/admin/permissionmgmt/systemright/" + right.id +"/"; 
}

  handleSearchTextChange(event) {           
   this.setState({searchtext: event.target.value.toLowerCase()});                    
 }

clickback()
{
window.history.back();
}

render() {
	var self = this;
	var tblrows = this.state.systemrights.map(function(right) {
        if(self.state.searchtext !== "")
        {
            if(right.name.toLowerCase().indexOf(self.state.searchtext) > -1 || right.description.toLowerCase().indexOf(self.state.searchtext) > -1 )
            {
					
				}else
				{
					return null;
				}    
			}	
		var keys = right.keys.map(function(key)
		{
			var keyurl = "/admin/permissionmgmt/key/" + key.id + "/";
			return(<span key={key.id}><a href={keyurl}>{key.name};</a> </span>);
		});

			return(<tr key={right.id}  className="keylistkey" onClick={() => self.clickshowsystemright(right)} ><td className="keylisttdimg"><img alt={right.name} src="https://publicfile.standard-cloud.com/icons/claim.png"/></td><td  className="keylisttdname" dangerouslySetInnerHTML={{__html:right.name}}></td><td>{right.description}</td><td>{keys}</td></tr>);
			
		});

	return (
     <div id="trash_page">
   <Contentbox title={Trans.t("systemrights")}>
		 <div id="trash_content">
	      <input id="trashsearchbox"   onChange={this.handleSearchTextChange}  placeholder="Filter" type="search"  autoComplete="off"/><br/>
		  <table id="key_tablelist" className="stdtable"><thead>
			<tr><th className="imgrowkeylist"></th><th>{Trans.t("name")}</th><th>{Trans.t("description")}</th><th>{Trans.t("keys")}</th></tr>
			</thead>
			<tbody>
			{tblrows}
			</tbody>
			</table>
	    </div>
      </Contentbox>
		<div>
<Helpbox title={Trans.t("help")}>
SystemRight Hilfe
	
</Helpbox>
       
<div id="newlistfooter">
			 <BT  callbackfunction={this.clickback} name={Trans.t("back")} iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
			 <BT url="https://standard-cloud.zendesk.com/hc/de/articles/115000611651-Papierkorb"  name={Trans.t("help")} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
      </div>
				</div>
	</div>
    );
	
}	  
  }
    
export default connect(null,{setNavigation})(SystemRightListPage);