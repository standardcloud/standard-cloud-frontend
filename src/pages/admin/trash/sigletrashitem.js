
import React from 'react';

import BT from '../../../component/button/stdbutton';
import {connect} from 'react-redux';
import {setNavigation} from '../../../actions/navigation';
import Helpbox from '../../../component/helpbox/helpbox';
import Contentbox from '../../../component/contentbox/contentbox';

import dialog from '../../../component/dialog/stddialog';
import TrashManager from '../../../module/trashmanager/stdtrashmanager';
import Trans from '../../../module/translatoinmanager/translationmanager';


class TrashPage extends React.Component {
	    constructor(props) {
		super(props);
		this.state = {trashitem:{}};
		this.restoreitem = this.restoreitem.bind(this);
		this.finaldeleteitm = this.finaldeleteitm.bind(this);
		var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"), url:"/admin/"},{title:Trans.t("trash"), url:"/admin/trash/"},{title:Trans.t("recoverobject")}];
		this.props.setNavigation(nav,Trans.t("trash"));			
  }
			  
		  componentDidMount() 
		  {
		 	this.loadtrashitem(this.props.match.params.trashid);
		  }


  loadtrashitem(trashitemid)
  {
	var self = this;
	TrashManager.getTrashItem(trashitemid).then(data =>
	{
		    if(data.state==="ok")
			{
				self.setState({trashitem:data.content});
			}
	});
  }

  restoreitem()
  {
	TrashManager.restoreTrashItem(this.props.match.params.trashid).then(data =>
	{
			if(data.state==="ok")
			{
				dialog.ok("Objekt", "wurde wiederhergestellt");
			}else
			{
				dialog.error("Objekt", "konnte nicht wiederhergestellt werden.");
			}
	});
  }

clickback()
{
window.history.back();
}
finaldeleteitm()
{
	TrashManager.deleteTrashItem(this.props.match.params.trashid).then(data=>
	{
			if(data.state==="ok")
			{
				dialog.ok("Objekt", "wurde endgültig gelöscht");
			}else
			{
				dialog.error("Objekt", "konnte nicht endgültig gelöscht werden.");
			}
	});
}
render() {

	if(!this.state.trashitem.fields)
	{
		return null;
	}
	
	var rows = this.state.trashitem.fields.map(function(field)
	{
		var fieldtypeid = field.fieldtypeid;
		var value = JSON.stringify(field[fieldtypeid]);
	return(<tr key={field.fieldid}><td>{field.name}</td><td>{value}</td></tr>);
	});

	return (
     <div id="trash_page">
   <Contentbox title={Trans.t("trash")}>
		 <div id="trash_content">
	     <table id="trash_tablelist" className="stdtable">
			<tbody>
			{rows}		
			</tbody>
			</table>
	    </div><br/>
        </Contentbox>
		<div>
<Helpbox title={Trans.t("help")}>
Hier können Sie gelöschte Objekte wiederherstellen. Klicken Sie für hierfür auf das Objekt und im Anschluss auf "Wiederherstellen":
</Helpbox>
       
<div id="newlistfooter">
			 <BT  callbackfunction={this.clickback} name={Trans.t("back")}   iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
			 <BT  callbackfunction={this.restoreitem} name={Trans.t("recover")}    iconurl="https://publicfile.standard-cloud.com/icons/reuse.png" />
			 <BT  callbackfunction={this.finaldeleteitm} name={Trans.t("finalremove")}  iconurl="https://publicfile.standard-cloud.com/icons/trashfinal.png" />
			 <BT reload url="https://standard-cloud.zendesk.com/hc/de/articles/115000611651-Papierkorb"  name={Trans.t("help")} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
      </div>
				</div>
	</div>
    );
	
}	  
  }
    
export default connect(null,{setNavigation})(TrashPage);
  


