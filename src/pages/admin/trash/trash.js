
import React from 'react';

import BT from '../../../component/button/stdbutton';
import {connect} from 'react-redux';
import {setNavigation} from '../../../actions/navigation';
import Helpbox from '../../../component/helpbox/helpbox';
import Contentbox from '../../../component/contentbox/contentbox';
import './trash.css';
import { withRouter } from 'react-router-dom';
import Trans from '../../../module/translatoinmanager/translationmanager';

import TrashManager from '../../../module/trashmanager/stdtrashmanager';



class TrashPage extends React.Component {
	    constructor(props) {
		super(props);
		this.state = {results:[],searchtext:""};
		this.handleSearchTextChange = this.handleSearchTextChange.bind(this);
		var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"), url:"/admin/"},{title:"trash"}];
		this.props.setNavigation(nav,Trans.t("trash"));
			
  }
			  
		  componentDidMount() 
		  {
		 	this.loadtrashitems();
		  }


  loadtrashitems()
  {
	var self = this;
	TrashManager.getall().then(data =>
	{
		    if(data.state==="ok")
			{
				self.setState({results:data.content});
			}
	});
  }


showTrashItem(item)
{
	this.props.history.push("/trash/" + item.id + "/");
}
  handleSearchTextChange(event) {           
   this.setState({searchtext: event.target.value.toLowerCase()});                    
 }

clickback()
{
window.history.back();
}

render() {
	var self = this;
	var tblrows = this.state.results.map(function(trashitem) {

        if(self.state.searchtext !=="")
        {
            if(trashitem.objecttypename.toLowerCase().indexOf(self.state.searchtext) > -1 || trashitem.listname.toLowerCase().indexOf(self.state.searchtext) > -1 || trashitem.user.toLowerCase().indexOf(self.state.searchtext) > -1  )
            {
					
			}else
			{
		
				return null;
			}    
        }





		var userlink = "/profile/" + trashitem.userid + "/";
		if(trashitem.orginalid==="")
			{				
				return(<tr key={trashitem.id} onClick={() => self.showTrashItem(trashitem)}  className="trashrow"><td><img className="trashiconpageimg" alt="Liste" title="Liste" src="https://publicfile.standard-cloud.com/icons/list.png"/></td><td></td><td>{trashitem.listname}</td><td>{trashitem.delateddate}</td><td><a href={userlink}>{trashitem.user}</a></td></tr>);
			}else
			{
				
				return(<tr key={trashitem.id} onClick={() => self.showTrashItem(trashitem)} className="trashrow"><td><img className="trashiconpageimg" alt="Lieste" title="Liste" src="https://publicfile.standard-cloud.com/icons/edit.png"/></td><td>{trashitem.objecttypename}</td><td>{trashitem.listname}</td><td>{trashitem.delateddate}</td><td><a href={userlink}>{trashitem.user}</a></td></tr>);
			}

	});

	return (
     <div id="trash_page">
   <Contentbox title={Trans.t("trash")}>
		 <div id="trash_content">
	    <input id="trashsearchbox"   onChange={this.handleSearchTextChange}  placeholder="Filter" type="search"  autoComplete="off"/><br/>
		<table id="trash_tablelist" className="stdtable"><thead>
			<tr><th className="trashicontd"></th><th>{Trans.t("object")}</th><th>{Trans.t("list")}</th><th>{Trans.t("date")}</th><th>{Trans.t("deletedby")}</th></tr>
			</thead>
			<tbody>
				{tblrows}
			</tbody>
			</table>
	    </div><br/>
        </Contentbox>
		<div>
<Helpbox title={Trans.t("help")}>
Hier können Sie gelöschte Objekte wiederherstellen. Klicken Sie für hierfür auf das Objekt und im Anschluss auf "Wiederherstellen":
</Helpbox>
       
<div id="newlistfooter">
			 <BT  callbackfunction={this.clickback} name={Trans.t("back")}   iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
			 <BT reload url="https://standard-cloud.zendesk.com/hc/de/articles/115000611651-Papierkorb"  name={Trans.t("help")} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
      </div>
				</div>
	</div>
    );
	
}	  
  }
    
export default withRouter(connect(null,{setNavigation})(TrashPage));
  


