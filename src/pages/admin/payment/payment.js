import React from 'react';
import {connect} from 'react-redux';
import StripeCheckout from 'react-stripe-checkout';
import {setNavigation} from '../../../actions/navigation';
import Contentbox from '../../../component/contentbox/contentbox';
import Helpbox from '../../../component/helpbox/helpbox';
import './payment.css';
import Paymentmanager from '../../../module/payment/payment';
import Trans from '../../../module/translatoinmanager/translationmanager';

  class Payment extends React.Component {
	  constructor(props) {
		super(props);
    this.state = {};
    this.getPayment = this.getPayment.bind(this);
    var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"),url:"/admin/"},{title:Trans.t("paymentinformation")}];
		this.props.setNavigation(nav,Trans.t("paymentinformation"));
  }


  getPayment()
  {
    Paymentmanager.getPayment(function(data)
    {
    
    });
  }

  componentDidMount() {
this.getPayment();
    }
	
  onToken = (token) => {
   
    Paymentmanager.saveStripeToken(token,function(data)
  {
   
  });
  }

render(){
		return (
  
	<div id="payment-pagebox-div">
        <Contentbox title={Trans.t("paymentinformation")}>
        <div id="payment-contentbox-div">
        Lieber Kunde,<br/><br/>
        derzeit befindet sich der Service noch in der Beta-Phase. Diese wird aber bald zu Ende sein. <br/>Danach wird pro Benutzer eine Gebühr von 6€/Monat fällig. 
        Zuvor werden Sie natürlich eine E-Mail erhalten.
        <br/>
        Sie können eine SEPA-Lastschirft oder Kreditkatenabbuchung hinterlegen.<br/><br/>
        Die Zahlungsinformationen werden von Stripe verwaltet, dem führenden Online-Bezahldienst.
         <br/><br/>
        <div id="payment-buttonbox">
      <StripeCheckout
				token={this.onToken}
				label="Zahlundsinformationen hinterlegen"
				description="Lists" 
				name="Standard-Cloud"
        shippingAddress={true}
        data-zip-code="true"
				currency="EUR"
				bitcoin={true}
        stripeKey="pk_test_veBU3IKDfXpyRXRaP6AMYbeN"
      />
      </div>
      </div>
      </Contentbox>
     <Helpbox title={Trans.t("help")}>
     Nach Ablauf der Testphase von 30 Tagen muss eine Servicegebühr entrichtet werden. <br/>Sollten Sie keine Zeit für einen ausführlichen Test gehabt haben, können Sie sich gerne telefonisch oder per Mail melden.
     </Helpbox>
	</div>
        );
		}
  };
  

export default connect(null,{setNavigation})(Payment);
