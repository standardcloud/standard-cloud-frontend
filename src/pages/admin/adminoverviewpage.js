import React from 'react';
import StdButton from '../../component/button/stdbutton';
import Administration from '../../module/administration/stdadministration';
import {connect} from 'react-redux';
import {setNavigation} from '../../actions/navigation';
import Trans from '../../module/translatoinmanager/translationmanager';

class AdminOverview extends React.Component {
	    constructor(props) {
    	super(props);
			this.state = {objecttypecount: "",fieldcount:"",publicfilecount:"",tagcount:"",usercount:"",trashcount:""};
			var adminlabel = Trans.t("admin");
			var nav = [{title:"Start",url:"/"},{title:adminlabel}];
			this.props.setNavigation(nav,adminlabel);

}
loadoverviewsettings()
{
	  var ts = this;
		Administration.getadminoverview(function(data)
	{
		if(data.state==="ok")
		{
			var vals = data.content;
		  ts.setState({objecttypecount:vals.objecttypecount});
		  ts.setState({fieldcount:vals.fieldcount});
		  ts.setState({publicfilecount:vals.publicfilecount});
		  ts.setState({tagcount:vals.tagcount});
		  ts.setState({usercount:vals.usercount});
		  ts.setState({trashcount:vals.trashcount});
		}
	});

}

componentDidMount() {
		 this.loadoverviewsettings();
}

render(){
		return (
	<div>
		<StdButton count="1" url="/admin/list/new/"  name={Trans.t("newlist")} iconurl="https://publicfile.standard-cloud.com/icons/addlist.png" />
    <StdButton  url="/admin/objecttype/" count={this.state.objecttypecount} name={Trans.t("listtemplates")} iconurl="https://publicfile.standard-cloud.com/icons/objecttype.png" />
		<StdButton  url="/admin/field/" count={this.state.fieldcount}  name={Trans.t("fields")} iconurl="https://publicfile.standard-cloud.com/icons/addcolumn.png" />
		<StdButton  url="/ifollow/" name={Trans.t("subscriptions")} iconurl="https://publicfile.standard-cloud.com/icons/follow.png" />
		<StdButton  url="/admin/usermanagement/" count={this.state.usercount}  name={Trans.t("users")} iconurl="https://publicfile.standard-cloud.com/icons/adduser.png" />
	  <StdButton  url="/admin/permissionmgmt/" name={Trans.t("accessmanagement")} iconurl="https://publicfile.standard-cloud.com/icons/claim.png" />	
	  <StdButton  url="/admin/trash/" count={this.state.trashcount}  name={Trans.t("trash")} iconurl="https://publicfile.standard-cloud.com/icons/trash.png" />
	  <StdButton  url="/admin/design/"  name={Trans.t("customisation")} iconurl="https://publicfile.standard-cloud.com/icons/design.png" />	
		<StdButton  url="/admin/corpsettings/"  name={Trans.t("companyinformation")} iconurl="https://publicfile.standard-cloud.com/icons/organization.png" />
		<StdButton  url="/admin/payment/"  name={Trans.t("paymentinfo")} iconurl="https://publicfile.standard-cloud.com/icons/payment.png" />	
		<StdButton  url="/admin/log/" name={Trans.t("logs")} iconurl="https://publicfile.standard-cloud.com/icons/logger.png" />	
		<StdButton  url="/admin/search/" name={Trans.t("search")} iconurl="https://publicfile.standard-cloud.com/icons/searchsettings.png" />	
		<StdButton reload  url="https://standard-cloud.zendesk.com/hc/de" name={Trans.t("help")} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />	
	</div>
        );
		}
}
  

export default connect(null,{setNavigation})(AdminOverview);