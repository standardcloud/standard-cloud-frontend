import FieldTypeText from '../../../component/fieldtype/text/fieldtypetext';
import FieldTypeSelector from '../../../component/fieldtype/selector/fieldtypeselector.js';
import FieldTypePicture from '../../../component/fieldtype/picture/fieldtypepicture.js';
import FieldTypeDateTime from '../../../component/fieldtype/datetime/fieldtypedatetime.js';
import FieldTypeInteger from '../../../component/fieldtype/integer/fieldtypeinteger.js';
import FieldTypeFloat from '../../../component/fieldtype/float/fieldtypefloat.js';
import FieldTypeUser from '../../../component/fieldtype/user/fieldtypeuser.js';
import FieldTypeSwitch from '../../../component/fieldtype/switch/fieldtypeswitch.js';
import FieldTypeUrl from '../../../component/fieldtype/url/fieldtypeurl.js';
import FieldTypeSubList from '../../../component/fieldtype/sublist/fieldtypesublist.js';
import FieldTypeLookup from '../../../component/fieldtype/lookup/fieldtypelookup.js';
import FieldTypeAnyList from '../../../component/fieldtype/anylist/fieldtypeanylist.js';
import BT from '../../../component/button/stdbutton';
import dialog from '../../../component/dialog/stddialog';
import Button from 'muicss/lib/react/button';
import React from 'react';
import fieldmanager from '../../../module/fieldmanager/stdfieldmanager';
import Helpbox from '../../../component/helpbox/helpbox';
import Contentbox from '../../../component/contentbox/contentbox';

import {connect} from 'react-redux';
import {setNavigation} from '../../../actions/navigation';
import { withRouter } from 'react-router-dom';
import Dialogbox from '../../../component/dialogbox/dialogbox';
import Trans from '../../../module/translatoinmanager/translationmanager';
import './newfield.css';

class NewFieldPage extends React.Component {
	  constructor(props) {
		super(props);
		this.changeFieldDescription = this.changeFieldDescription.bind(this);
		this.changeMandatory = this.changeMandatory.bind(this);
		this.changeFieldName = this.changeFieldName.bind(this);
		this.changeUnique = this.changeUnique.bind(this);
		//this.loadfieldtypes = this.loadfieldtypes.bind(this);
		this.finnalremovefield = this.finnalremovefield.bind(this);
		this.changeWriteonce = this.changeWriteonce.bind(this);
		this.handleFieldTypeChange = this.handleFieldTypeChange.bind(this);
		this.changeUniqueType = this.changeUniqueType.bind(this);
		this.savefield = this.savefield.bind(this);
		this.deletefield = this.deletefield.bind(this);
		this.getfieldtypedata = this.getfieldtypedata.bind(this);
		this.componentDidMount = this.componentDidMount.bind(this);
		this.state = {deletefielddialog:false,fieldtypehelptext:"",fieldtypedata:[],field:{id:"",objecttypes:[],fieldtypeid:"",name:"",description:"",mandatory:false,unique:false,uniquetype:"1",writeonce:false},fieldtypes:[],selectedfieldtype:{id:"",name:""}};
		var navtext = Trans.t("fieldedit");
		if(window.location.href.indexOf("new") > 0)
		{
			navtext = Trans.t("fieldnew");
		}
		var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"), url:"/admin/"},{title:Trans.t("fields"), url:"/admin/field/"},{title:navtext}];
		this.props.setNavigation(nav,navtext);
	
	}	
	changeFieldName(e)
	{
		var oldstate = this.state.field;
		oldstate.name = e.target.value;
		this.setState({field:oldstate});
	}
	changeFieldDescription(e)
	{
		var oldstate = this.state.field;
		oldstate.description = e.target.value;
		this.setState({field:oldstate});
	}		  
	changeMandatory(e)
	{
		var oldstate = this.state.field;
		oldstate.mandatory = !oldstate.mandatory;
		this.setState({field:oldstate});
	}
	changeUnique(e)
	{
		var oldstate = this.state.field;
		oldstate.unique = !oldstate.unique;
		this.setState({field:oldstate});
	}
	changeWriteonce(e)
	{
		var oldstate = this.state.field;
		oldstate.writeonce = !oldstate.writeonce;
		this.setState({field:oldstate});
	}
componentDidMount() 
{
	var self = this;
	if(window.location.href.indexOf("/modify/") === -1)
		{
		//	this.loadfieldtypes();
		}else
		{
			//var testRE = test.match("/modify/(.*)/");


			self.setState({field:this.props.field});


			var fieldid = this.props.match.params.id;
			fieldmanager.getfieldsettings(fieldid).then(data=>
			{
				
				self.setState({field:data.content});
				var selectedfieldtype = {};
				selectedfieldtype.id = data.content.fieldtypeid;
				selectedfieldtype.name = data.content.fieldtypename;
				self.setState({selectedfieldtype:selectedfieldtype});
				
			});
		}

	
}
loadhelp(fieldtypeid)
{
	switch(fieldtypeid)
	{
		case "32b60712-1f4d-4a49-87a6-90de0363eef4":
		return "Nutzen Sie diesen Feldtyp um Texte abzuspreichen. Die Anzahl der Zeichen können Sie über die Textlänge beschränken. Außerdem können Sie den Editor des Anwenders bestimmen. Den besten Editor erhalten Sie bei der Auswahl des Richtext Editors.  ";
		case "52a3bb09-fc62-4099-a7ff-c314074f274a":
		return "In disem Feld können Sie Links, Webadressen (z.B. https:// oder http://) ablegen";
		case "bb46d3a0-a9cc-411a-9f2a-b3b016a863f1":
		return "Sie wollen eine Verbindung zu einem beliebingen anderen Objekt. Dieses Feld macht es möglch. Es wird jedoch i. d. R. von Expterten eingesetzt.";
		case "971c3761-5565-4a65-b546-7885b8bef160":
		return "Dieser Feldtyp kommt sehr häufig zum Einsatz. Er laubt es Listen miteinander zu Verbinden. So können Sie z.B. die Projektliste mit der Kundenliste verbinden. Der Anwender kann dann das Projekt einem Kunden zuordnen. ";
		case "8c2a7b49-2584-4296-a39b-32717e56510b":
		return "Der Feldtyp Unterliste findet häufigen Einsatz. Einsatzbeispiele sind z.B. die Verbindung eines Projektes mit Projektaufgaben oder eine Rechnung mit Bestellpositionen. (Der Experte spricht von 1:N (Unterliste))";
		case "433711f1-0eec-4be4-b552-da385ebddbb8":
		return "Tags sind derzeit deaktiviert.";
		case "e4fe0940-7542-46ef-85a4-50b2ed3289a5":
		return "In diesem Feld kann eine Sprache ausgwählt werden. Das macht bei Übersetzungen Sinn. Der Feldtyp wird vorwiegend von Expterten eingesetzt.";
		case "1EF87E19-23E5-43F8-8C94-C8B8107FC573":
		return "In disem Feld können Sie Schlüssel (Zugriffskarten) ablegen. Dieser Feldtyp wird nur von Experten genutzt. ";
		case "fd3eb39b-3f6c-4e43-baea-12135ed5bee7":
		return "Sie sollen in dieser Spale lediglich eine einfache Checkbox (Auswahlfeld mit JA oder Nein), dann ist dieser Feldtyp richtig für Sie.";
		case "cc333aeb-196d-4331-b1df-3b530fa67722":
		return "Der Anwender kann aus einer Suchbox andere Anwender auswählen.  In der Spalte wird später ein Bild der Person, sowie ein Link auf die Profilseite erstellt. Dieser Feldtyp findet vielseitigen Einsatz.";
		case "278bd56c-e463-4ec4-a702-82a9ecd59137":
		return "In disesm Feld können Sie beliebige Zahlen - Ganzahlen und Kommazahlen - ablegen. Dieses Feld könnte z.B. zum Speichern von Preisen verwendet werden. In den Feldeinstellungen können die Dezimalstellen (Stellen nach dem Komma) bestimmt werden. Sie können in den Feldeinstellungen den Postfix(Anhand) die Einheit definieren (Jahre, Tage, Euro) etc.";
		case "fac261ca-829e-4f44-92fe-51a0d9d080c3":
		return "In diesem Feld können Geodaten - GPS-Koordinaten auf der Weltkarte abgelegt werden. Die Darstellung erfolgt über Google-Maps. Ein Klick uns Sie sind an der Lokation.";
		case "39f6c3e6-d446-4e53-9706-67f556b06bdb":
		return "Mit diesem Feldtyp können nur Ganzzahlen(1,2,3,..) abgelegt werden. Kommazahlen - z.B. 3,3 - sind nicht erlaubt! Verwenden Sie für Kommazahlen den Feldtyp Kommazahl. ";
		case "17a8dd83-0068-4e4a-bfcc-082f7f64e6f8":
		return "In dies Feld kann der Anwender aus einem Datums-Picker ein Datum auswählen. In disem Feld können Sie Termine, Events, Geburtstage etc. abspeichern. Auf Wunsch kann sogar die Uhrzeit abgelegt werden.";
		case "59d19ffb-7090-46ca-9021-b79c697142d1":
		return "In diesem Feld können Sie eine Datei ablegen. Das könnte z.B. ein Text-Dokument oder eine Musikdatei sein. Einschränkungen bzgl. des Dateitypes gibt es nicht.";
		case "8787e054-7bb9-411a-8946-da658fbc8e9f":
		return "Mit diesem Feldtyp versetzen Sie den Anwender in die Lage, Bilder in einer Liste zu speichern. Beim Upload können Sie dem Anwender das Zuschneiden des Bildes erlauben und ggf. das Format bestimmen. Dieser Feldtyp verwendet man z.B. für Listen mit Autos, Immoblien oder Baustellen - eben all den Objekten die sie direkt visuell angezeigt haben wollen.";
		case "6b95017e-d96c-42ad-aa23-9a7d650217e7":
		return "In diesem Feldtyp können Sie eine oder mehrere Listen verlinken. In einem Dropdown-Feld kann die zu verlinkende Liste ausgewählt werden. In der Praxis kommt diser Feldtyp eher selten zum Einsatz. Dennoch gibt es spezielle Anwendungsfälle. ";
		case "8FE72E9F-09CC-4225-9135-58E042DC98A8":
		return "In einem Dropdown oder Optionsfeld können vorgegebene Werte ausgewählt werden. Diesen Feldtyp findet man in sehr vielen Listen. Zum Beispiel für die Anrede (Herr, Frau) oder beim Auto (Diesel, Benzin, Elektro).";
		default:
		return "FeldTyp unbekannt";
	}
}

deletefield()
{
	this.setState({deletefielddialog:true});
}
finnalremovefield()
{
	fieldmanager.removefield(this.state.field.id).then(data=>
	{
		if(data.state==="ok")
		{
			window.location.href= "/admin/field/";
		}
	});
}

/*
loadfieldtypes()
{
	console.log(this.props.field);
var self = this;
fieldmanager.getfieldtypelist().then(data =>
{
	if(data.state==="ok")
		{
			self.setState({fieldtypes:data.content});
		}else
		{
			dialog.error("Feldtypen","konnten nicht geladen werden.");
		}
});

}
*/
clickback()
{
window.history.back();
}
handleFieldTypeChange(field)
{
	this.setState({selectedfieldtype:field});
	var oldstate = this.state.field;
	oldstate.fieldtypeid = field.id;
	this.setState({field:oldstate});
	this.setState({fieldtypehelptext:this.loadhelp(field.id)});
}
getfieldtypedata(fieldtypedata)
{
this.setState({fieldtypedata:fieldtypedata});
}
changeUniqueType(e)
{
	var oldstate = this.state.field;
	oldstate.uniquetype = e.target.value;
	this.setState({field:oldstate});
}
savefield()
{
	var self = this;
fieldmanager.newfield(this.state.field,this.state.fieldtypedata,function(data)
{
	if(data.state==="ok")
		{

					self.props.history.goBack();
					dialog.ok("Feldeinstellungen","erfolgreich gepseichert.");


		}else
		{
			dialog.error("Feldeinstellungen","konnten nicht gespeichert werden.");
		}
});
}
render() {
	var navtext = Trans.t("fieldedit");
	if(window.location.href.indexOf("new") > 0)
	{
		navtext = Trans.t("fieldnew");
	}
	
var selfrender = this;
var fieldtypesettingscomponent = null;
var removebutton = null;
var listenvorlagen = "Feld wird nicht verwendet.";

var dialogbox = null;
if(this.state.deletefielddialog)
{
	dialogbox = <Dialogbox closedialog={()=>{this.setState({deletefielddialog:false});}}  title="Feld löschen?">
	<div id="newfield-deletefieldbox-div">
	<div>Wollen Sie das Feld wirklich löschen?</div>
	<div id="newfield-deletebox-div">
   <Button color="danger"  onClick={()=>{selfrender.finnalremovefield()}}>
  Feld löschen
  </Button>	
  <Button onClick={()=>{selfrender.setState({deletefielddialog:false});}}>
  Zurück
  </Button>	
  </div>
  </div>
	</Dialogbox>
}



//if(this.state.field.objecttypes.length ===0)
if(false)
{
	removebutton =<BT  callbackfunction={this.deletefield} name="Feld löschen"   iconurl="https://publicfile.standard-cloud.com/icons/delete.png" />
}else
{
	listenvorlagen = this.state.field.objecttypes.map(function(listtemplate)
	{
		var url = "/admin/objecttype/modify/" + listtemplate.id + "/";
		return (<span key={listtemplate.id}><a href={url} >{listtemplate.name} </a></span>)
	}); 
}

var fieldtypeid = this.state.field.fieldtypeid;
if(fieldtypeid.length === 0)
	{
		fieldtypeid = this.state.selectedfieldtype.id;		
	}

switch(fieldtypeid)
{
	case "32b60712-1f4d-4a49-87a6-90de0363eef4":
	fieldtypesettingscomponent = <FieldTypeText setcustomsettings={this.state.field.customsettings}  getcustomsettings={this.getfieldtypedata} />;
	break;
//	return "Nutzen Sie diesen Feldtyp um Texte abzuspreichen (Ob der Text kurz oder lang ist, spielt keine Roll)";
	case "52a3bb09-fc62-4099-a7ff-c314074f274a":
	fieldtypesettingscomponent = <FieldTypeUrl setcustomsettings={this.state.field.customsettings} getcustomsettings={this.getfieldtypedata} />
	break;
//	return "In disem Feld können Sie Links, Webadressen (z.B. https:// oder http://) ablegen";
	case "bb46d3a0-a9cc-411a-9f2a-b3b016a863f1":
	break;
//	return "Verbindung zu einem beliebigen Objekt";
	case "971c3761-5565-4a65-b546-7885b8bef160":	
	fieldtypesettingscomponent = <FieldTypeLookup setcustomsettings={this.state.field.customsettings} getcustomsettings={this.getfieldtypedata} />
	break;
//	return "Verbindung zu einem Objekt aus einer bestimmten Liste";
	case "8c2a7b49-2584-4296-a39b-32717e56510b":
	fieldtypesettingscomponent = <FieldTypeSubList setcustomsettings={this.state.field.customsettings} getcustomsettings={this.getfieldtypedata} />
	break;
//	return "Unterliste";
	case "433711f1-0eec-4be4-b552-da385ebddbb8":
	break;
//	return "Tag..(Derzeit deaktiviert)";
	case "e4fe0940-7542-46ef-85a4-50b2ed3289a5":
	break;
//	return "Das ist der Sprachtyp";
	case "1EF87E19-23E5-43F8-8C94-C8B8107FC573":
	break;
//	return "Schlüssel";
	case "fd3eb39b-3f6c-4e43-baea-12135ed5bee7":	
	fieldtypesettingscomponent = <FieldTypeSwitch setcustomsettings={this.state.field.customsettings} getcustomsettings={this.getfieldtypedata} />
	break;
//	return "Schalter";
	case "cc333aeb-196d-4331-b1df-3b530fa67722":
	fieldtypesettingscomponent = <FieldTypeUser setcustomsettings={this.state.field.customsettings} getcustomsettings={this.getfieldtypedata} />
	break;
//	return "Person";
	case "278bd56c-e463-4ec4-a702-82a9ecd59137":	
	fieldtypesettingscomponent = <FieldTypeFloat setcustomsettings={this.state.field.customsettings} getcustomsettings={this.getfieldtypedata} />
	break;
//	return "Kommazahl";
	case "fac261ca-829e-4f44-92fe-51a0d9d080c3":
	break;
//	return "Geolocation";
	case "39f6c3e6-d446-4e53-9706-67f556b06bdb":
	
	fieldtypesettingscomponent = <FieldTypeInteger setcustomsettings={this.state.field.customsettings} getcustomsettings={this.getfieldtypedata} />
	break;
//	return "Kommazahlen sind nicht erlaubt";
	case "17a8dd83-0068-4e4a-bfcc-082f7f64e6f8":
	fieldtypesettingscomponent = <FieldTypeDateTime setcustomsettings={this.state.field.customsettings} getcustomsettings={this.getfieldtypedata} />
	break;
//	return "Datum und Uhrzeit";
	case "59d19ffb-7090-46ca-9021-b79c697142d1":

	break;
//	return "Datei";
	case "8787e054-7bb9-411a-8946-da658fbc8e9f":
	fieldtypesettingscomponent = <FieldTypePicture setcustomsettings={this.state.field.customsettings} getcustomsettings={this.getfieldtypedata} />
	break;

	//	return "Bild";
	case "6b95017e-d96c-42ad-aa23-9a7d650217e7":
	fieldtypesettingscomponent = <FieldTypeAnyList setcustomsettings={this.state.field.customsettings} getcustomsettings={this.getfieldtypedata} />	
	break;
//	return "Beliebige Liste";
	case "8FE72E9F-09CC-4225-9135-58E042DC98A8":
	fieldtypesettingscomponent = <FieldTypeSelector setcustomsettings={this.state.field.customsettings} getcustomsettings={this.getfieldtypedata} />;	
	break;
//	return "Auswahlfeld";
	default:
//	return "FeldTyp unbekannt";
}

var fieldtypes = this.state.fieldtypes.map(function(field)
{
	if(field.id ==="bb46d3a0-a9cc-411a-9f2a-b3b016a863f1" || field.id === "6b95017e-d96c-42ad-aa23-9a7d650217e7" || field.id==="433711f1-0eec-4be4-b552-da385ebddbb8" || field.id ==="e4fe0940-7542-46ef-85a4-50b2ed3289a5")
	{
	return null;
	}
	return(<div key={field.id} className="stdradioboxregion"><input onChange={()=>selfrender.handleFieldTypeChange(field)} className="radio"  type="radio" key={field.id} id={field.id} name="fieldtype" value={field.id}/>
	<label htmlFor={field.id} dangerouslySetInnerHTML={{__html: field.name}}></label>
 </div>)
});
var helpfieldtypebox = null;

if(this.state.selectedfieldtype.id !=="")
{
	var fieldtypehelpboxtext ="Beschreibung Feldtyp: " + this.state.selectedfieldtype.name;
	helpfieldtypebox = <div id="fieldtypehelpbox"><div id="fieldtypehelpboxtop"><h2>{fieldtypehelpboxtext}</h2></div><div id="fieldtypehelpboxcontent">{this.state.fieldtypehelptext}</div></div>;
}

var dropdownunique = null;
var self = this;
if(this.state.field.unique)
{
dropdownunique =  <div id="dropdownuniquenewfieldsetting"><select onChange={self.changeUniqueType}>
	<option value="1">Einzigartig in der Liste</option>
	<option value="2">Einzigartig in der Listenvorlage</option>
	<option value="3">Einzigartig im gesamten System</option>
	</select>
	</div>
}
if(fieldtypesettingscomponent !==null)
{
	var customfieldproptitle = "Feldspezifischen Einstellungen (" + this.state.selectedfieldtype.name +")";
	var fieldsettingsbox = 	<Contentbox title={customfieldproptitle}><div id="fieldtypesettingsboxdiv">{fieldtypesettingscomponent}</div></Contentbox>
}

var feldtypbox = null;
if(this.state.field.id.length === 0)
	{
feldtypbox =<Contentbox title="Feldtyp">
<div id="fieldtypeselectorbox">
<fieldset>
{fieldtypes}
</fieldset>
</div>
{helpfieldtypebox}
</Contentbox>	
}	

	
	return (
     <div id="newfield_page">
	 {dialogbox}
   <Contentbox title={navtext}><br/>
	 <Contentbox title="Basisfeldeinstelluingen">
		 <div id="newfielddefaultpropsbox">
	 <div className="textboxgroup">      
				<input required className="stdtextbox" type = "text" name= "tbFieldName" onChange={this.changeFieldName} value={this.state.field.name} />
				<span className="texthighlight"></span>
				<span className="textbar"></span>
				<label>{Trans.t("fieldname")} </label>
	</div>
	<div className="textboxgroup">      
				<textarea required name= "tbFieldDescription" className="stdtextbox" onChange={this.changeFieldDescription} value={this.state.field.description}></textarea>
				<span className="texthighlight"></span>
				<span className="textbar"></span>
				<label>{Trans.t("description")} </label>
			</div>
			<div title="Muss in diesem Feld immer ein Wert enthalten sein?" className="stdcheckboxregion">
            <input id="mandatory"  checked={this.state.field.mandatory} type="checkbox" value="confirmobjectremove"   className="checkbox"/>
            <label onClick={this.changeMandatory}  htmlFor="mandatory"><strong>Pflichtfeld</strong></label>
      </div> 
			<div title="Mit diesem Haken können Sie doppelte Werte verhinden." className="stdcheckboxregion">
          <input id="uniqueu"  checked={this.state.field.unique} type="checkbox" value="confirmobjectremove"   className="checkbox"/>
          <label onClick={this.changeUnique}  htmlFor="uniqueu"><strong>Einzigartig</strong></label>
		  {dropdownunique}
      </div>
	<div title="Nach dem Klick auf Speichern nicht mehr veränderbar." className="stdcheckboxregion">
          <input id="writeonce"  checked={this.state.field.writeonce} type="checkbox" value="confirmobjectremove"   className="checkbox"/>
          <label onClick={this.changeWriteonce}  htmlFor="writeonce"><strong>Einmalig beschreibbar</strong></label>
    </div>
	<div><br/>
	<strong>Listenvorlage(n): </strong>{listenvorlagen}
	</div>
			</div></Contentbox>
			{feldtypbox}
			{fieldsettingsbox}
			
				<div id="fieldtypesettingssavefooterregion">
				<BT  callbackfunction={this.savefield} name="Feld speichern"   iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
			
				</div>
   </Contentbox>
		<div>
<Helpbox title={Trans.t("help")}>
Auf diser Seite können Sie ein Feld/Spalte anlegen und bearbetein. Dieses Feld kann dann im Anschluss einer Listenvorlage zugeordnet werden. Neben Pflichangaben (Name,Beschreibung) können Sie bestimmen, ob dieses Feld immer ausgefüllt werden muss (<strong>"Pflichtfeld"</strong>).
<br/><br/>Dopplete Einträge können Sie mit dem Haken <strong>"Einzigartig"</strong> verhinden. <br/>
<br/>Außerdem können Sie bestimmen, dass dieses Feld nur bei der Anlage ausgefüllt werden darf und danach nicht mehr editiert werden kann (<strong>"Einmalig beschreibbar"</strong>).
<br/><br/>In der Box <strong>"Feldspezifische Einstellungen"</strong> können besondere Funktionen genutzt werden.<br/><br/>Weitere Infos unter "Hilfe".
</Helpbox>
       
<div id="newlistfooter">
			 <BT  callbackfunction={this.clickback} name={Trans.t("back")}   iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
			 <BT reload url="https://standard-cloud.zendesk.com/hc/de/articles/115000611651-Papierkorb"  name={Trans.t("help")}  iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
			 {removebutton}
      </div>
				</div>
	</div>
    );
	
}	  
  }

function mapStateToProps({field},ownProps) {
	let newfield = field.find(f=>f.id ===ownProps.match.params.id);
	return { field:newfield};
  }

export default withRouter(connect(mapStateToProps,{setNavigation})(NewFieldPage));