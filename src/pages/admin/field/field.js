import React from 'react';
import { withRouter } from 'react-router-dom';
import BT from '../../../component/button/stdbutton';
import {connect} from 'react-redux';
import {setNavigation} from '../../../actions/navigation';
import {loadFields} from '../../../actions/field';
import Helpbox from '../../../component/helpbox/helpbox';
import Contentbox from '../../../component/contentbox/contentbox';
import Trans from '../../../module/translatoinmanager/translationmanager';



class FieldListPage extends React.Component {
	  constructor(props) {
		super(props);
		this.state = {results:[],searchtext:""};
		this.handleSearchTextChange = this.handleSearchTextChange.bind(this);			
		var nav = [{title:"Start",url:"/"},{title:Trans.t("admin"), url:"/admin/"},{title:Trans.t("fields")}];
		this.props.setNavigation(nav,"Feld bearbeiten");
  }			  
componentDidMount() 
{
	this.loadfielditems();
}

  loadfielditems()
  {

	this.props.loadFields();
  }

clicknewfield()
{
	this.props.history.push("/admin/field/new/");
}
clickshowfield(field)
{
	this.props.history.push("/admin/field/modify/" + field.id +"/");
}


  handleSearchTextChange(event) {           
   this.setState({searchtext: event.target.value.toLowerCase()});                    
 }

clickback()
{
window.history.back();
}

getfieldtype(fieldtypeid)
{
	switch(fieldtypeid)
	{
		case "fd3eb39b-3f6c-4e43-baea-12135ed5bee7":
		return "Schalter";
		break;
		case "32b60712-1f4d-4a49-87a6-90de0363eef4":
		return "Text";
		break;
		case "17a8dd83-0068-4e4a-bfcc-082f7f64e6f8":
		return "Datum";
		break;
		case "278bd56c-e463-4ec4-a702-82a9ecd59137":
		return "Dezimal";
		break;
		case "8FE72E9F-09CC-4225-9135-58E042DC98A8":
		return "Auswahlfeld";
		break;
		case "8787e054-7bb9-411a-8946-da658fbc8e9f":
		return "Bild";
		break;
		case "52a3bb09-fc62-4099-a7ff-c314074f274a":
		return "URL";
		break;
		case "59d19ffb-7090-46ca-9021-b79c697142d1":
		return "Datei";
		break;
		case "39f6c3e6-d446-4e53-9706-67f556b06bdb":
		return "Ganzzahl";
		break;
		case "cc333aeb-196d-4331-b1df-3b530fa67722":
		return "Person";
		break;
		case "fac261ca-829e-4f44-92fe-51a0d9d080c3":
		return "Geolokation";
		break;
		case "8c2a7b49-2584-4296-a39b-32717e56510b":
		return "Unterliste";
		break;
		case "971c3761-5565-4a65-b546-7885b8bef160":
		return "Lookup";
		break;
		case "1EF87E19-23E5-43F8-8C94-C8B8107FC573":
		return "Schlüssel";
		break;
		case "bb46d3a0-a9cc-411a-9f2a-b3b016a863f1":
		return "Objektverbindung";
		break;
		case "433711f1-0eec-4be4-b552-da385ebddbb8":
		return "Tag";
		break;
		case "6b95017e-d96c-42ad-aa23-9a7d650217e7":
		return "Listenverbindung";
		break;

	}
}

render() {
	var self = this;
	if(!this.props.fields)
	return(<div>Lade Felder</div>);


	var tblrows = this.props.fields.map(function(field) {
        if(self.state.searchtext !== "")
        {
            if(field.name.toLowerCase().indexOf(self.state.searchtext) > -1 || field.description.toLowerCase().indexOf(self.state.searchtext) > -1 || field.fieldgroup.toLowerCase().indexOf(self.state.searchtext) > -1 || field.fieldtyp.toLowerCase().indexOf(self.state.searchtext) > -1  )
            {
					
			}else
			{
				return null;
			}    
				}
		
		var objecttypes = self.props.objecttypes.filter(f=>f.fields.includes(field.id)).map(function(ob)
	 {
			return(<span key={ob.id}>{ob.name}; </span>);
	 });		

	return(<tr key={field.id} onClick={() => self.clickshowfield(field)} className="trashrow"><td dangerouslySetInnerHTML={{__html:field.name}}></td><td dangerouslySetInnerHTML={{__html:self.getfieldtype(field.type)}}></td><td dangerouslySetInnerHTML={{__html:field.description}}></td><td>{objecttypes}</td></tr>);
	});

	return (
     <div id="trash_page">
	 <div id="newlistfooter">
			 <BT  callbackfunction={this.clickback} name={Trans.t("back")}  iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
			<BT  url="new/" name={Trans.t("fieldnew")}   iconurl="https://publicfile.standard-cloud.com/icons/addcolumn.png" />
			 <BT reload url="https://standard-cloud.zendesk.com/hc/de/articles/115000611651-Papierkorb"  name={Trans.t("help")} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
      </div><br/>
   <Contentbox title={Trans.t("fields")}>
		 <div id="trash_content">
	    <input id="trashsearchbox"   onChange={this.handleSearchTextChange}  placeholder="Filter" type="search"  autoComplete="off"/><br/>
		  <table id="trash_tablelist" className="stdtable"><thead>
			<tr><th>{Trans.t("name")}</th><th>Typ</th><th>{Trans.t("description")}</th><th>{Trans.t("usedinlisttemplate")}</th></tr>
			</thead>
			<tbody>
			{tblrows}
			</tbody>
			</table>
	    </div><br/>
      </Contentbox>
		<div>
<Helpbox title={Trans.t("help")}>
Hier sehen Sie alle Felder. Manche davon werden nur in einer Listenvorlage verwendet, andere in mehreren. <br/><br/>Ist nicht im Feld <strong>"Verwendet in Listenvorlagen:"</strong> keine Liste enthalten, 
dann wurde dieses Feld noch keiner Vorlage zugeordnet. <br/>Vielleicht kann dieses Feld gelöscht werden. Klicken Sie hierzu auf das Feld und im Anschluss auf Löschen.<br/><br/>
Ein neues Feld wird über den Button <strong>"Neues Feld" (Am Ende dieser Seite)</strong> erzeugt.
</Helpbox>
       

				</div>
	</div>
    );
	
}	  
}



function mapStateToProps({field,objecttype}, ownProps) {
    return {fields:field,objecttypes:objecttype};
  }


export default withRouter(connect(mapStateToProps,{setNavigation,loadFields})(FieldListPage));