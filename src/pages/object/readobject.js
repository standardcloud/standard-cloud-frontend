import React from 'react';
import { Link } from "react-router-dom";
import {connect} from 'react-redux';
import {setNavigation} from '../../actions/navigation';
import moment from 'moment';
import ObjectManager from '../../module/objectmanager/stdobjectmanager';
import {loadLists} from '../../actions/list';
import {loadFields} from '../../actions/field';
import ShowReadField from '../../component/fieldtype/renderread';
import EditObjectBar from '../../component/editobjectbar/editobjectbar';
import Trans from '../../module/translatoinmanager/translationmanager';
import './readobject.css';

class ReadObjectPage extends React.Component {

  constructor(props) {
        super(props); 
        this.state ={element:{data:[]}};
        var ssself = this;      
        var translateliste = Trans.t("lists");
            var url = "/list/" + this.props.match.params.listid + "/";
            var titlename = ssself.props.objecttype.name;
            var nav = [{title:"Start",url:"/"},{title:translateliste,url:"/list/"},{title:this.props.list.name,url:url},{title:titlename}];
            this.props.setNavigation(nav,titlename);      
    document.body.scrollTop = document.documentElement.scrollTop = 0;
      }




loadobjectdata()
{   
    var self = this;
    ObjectManager.getSingleObject(this.props.match.params.listid,this.props.match.params.objectid).then(data => self.setState({element: data}));
}

componentDidMount() {
this.loadobjectdata();
}



render() {
    var selfrender = this;

console.log(this.props.objecttype);
    if(!this.props.fields  || this.props.fields.length===0)
    {
        return(<div>Lade Felder</div>);
    }
    if(!this.props.list)
    {
        return(<div>Lade Liste</div>);
    }

    if(this.state.element.data.length === 0)
    {
        if(this.props.list)
        {            //fixit {this.props.list.listsettings.objecttype.name}
            return(<div>Lade </div>);
        }else
        {
        return(<div>Lade Object-Data</div>);
        }
    }


    if(this.state.element.data.length ===0 || this.props.fields.length===0)
        {
            return(<div>Loading</div>);
        }
        var modified = null;
        var modifier = null;
        var created = null;
        var creater = null;
        var authorline = null;
var result = this.state.element.fields.map(function(fieldid,index)
{
    var field = selfrender.props.fields.find(f=> f.id === fieldid);
    var fieldvalue = selfrender.state.element.data[index];
    switch(field.id)
    {
        case "0cecad40-c0a1-46e3-a7a3-4bd99b7236d6":
        created = moment(fieldvalue.datetime).locale("de").format("llll");;
        break;
        case "77f42b3d-8e2c-4869-bd67-6273afe7cf89":
        modified = moment(fieldvalue.datetime).locale("de").format("llll");
        break;
        case "6dacefca-1e0d-4bdd-9c82-fe89b517242b":
        creater = fieldvalue;        
        break;
        case "1d57184b-430b-44c8-9708-d3e3dd8bcce4":
        modifier = fieldvalue;
        break;
        case "3b825480-8d07-4677-a097-29ad90a753db":
        case "c8e7d134-1005-4756-b851-828767c84e8e":
        case "5be2d63b-b05a-421e-957b-61b288061956":
        case "7628c3c9-6e30-4b1a-8f74-cc8ae8366d0c":
        default:
    }

return(<tr key={fieldid}><td>{field.name}</td><td>
    <ShowReadField field={field} value={fieldvalue} />
    </td></tr>);
});
var modname = null;
var creatorstr = null;
var profileurl
if(modifier.length!==0)
{
    if(modified !== created)
    {
        profileurl = "/profile/" + modifier[0].id + "/";
        modname = <span>Bearbeitet am  {modified} von <Link to={profileurl}>  {modifier[0].name} </Link>.</span>;
    }

}
if(creater.length!==0)
{
     profileurl = "/profile/" + creater[0].id + "/";
creatorstr = <span>Erzeugt am  {created} von <Link to={profileurl}>  {creater[0].name} </Link>.</span>;
}


authorline = <div>{modname} {creatorstr} </div>


	return (
        <div>
        <table id="readobjecttbl">
            <tbody>
        {result}
        </tbody>
        </table>
        <div id="authorboxline">
        {authorline}
        </div>
        <EditObjectBar mode="read" list={this.props.list} objectid={this.props.match.params.objectid}/>
        </div>
           );
	
}	  

  
  }


function mapStateToProps({ list,field,objecttype}, ownProps) {
    var thelist = list.find(f=>f.id ===ownProps.match.params.listid);
    return { list: thelist ,fields:field,objecttype:objecttype.find(o=>o.id === thelist.objecttype)};
  }

export default connect(mapStateToProps,{setNavigation,loadLists,loadFields})(ReadObjectPage);


