import React from 'react';
import { Link } from "react-router-dom";
import {loadUser} from '../../actions/user';
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom';
import {setNavigation} from '../../actions/navigation';
import Dialog from '../../component/dialog/stddialog';
import Preloader from '../../component/preloader/preloader';
import ObjectManager from '../../module/objectmanager/stdobjectmanager';
import ShowEditField from '../../component/fieldtype/renderedit';
import EditObjectBar from '../../component/editobjectbar/editobjectbar';
import Trans from '../../module/translatoinmanager/translationmanager';
import moment from 'moment';
import './editobject.css';
import {loadListObjects} from '../../actions/objects';

class EditObjectPage extends React.Component {

  constructor(props) {
        super(props); 
        this.valueUpdate = this.valueUpdate.bind(this);
        this.updatedata={};
        this.state ={element:{data:[],id:""},disabled:true};
        this.saveObject = this.saveObject.bind(this);
        this.saveNewObject = this.saveNewObject.bind(this);
        this.loadobjectdata = this.loadobjectdata.bind(this);
        this.saveEditObject = this.saveEditObject.bind(this);
        this.setupNavigation = this.setupNavigation.bind(this);
        this.uuidv4 = this.uuidv4.bind();
        this.loadobjectdata(this.props);
        document.body.scrollTop = document.documentElement.scrollTop = 0;
      }
      componentDidMount()
      {
        this.setupNavigation(this.props);
    }

    shouldComponentUpdate(nextProps, nextState)
    {
        return (this.state.element.id !== nextState.element.id || (this.state.disabled !== nextState.disabled)) ? true:false;
    }


setupNavigation(nextprops)
{
    var titletext = " bearbeiten";
    if(window.location.href.indexOf("newobject") > 0)
    {
        titletext = " hinzufügen";
    }

        var titlename = nextprops.objecttype.name + titletext;
        var translateliste = Trans.t("lists");
        var nav = [{title:"Start",url:"/"},{title:translateliste,url:"/list/"}];
        nav =  nav.concat(nextprops.list.navigation);
        nav.push({title:titlename});
        this.props.setNavigation(nav,titlename);
}

      

uuidv4() {
        return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
          (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        )
}
      
isMandatory(field)
{
    Dialog.ok("Feld " + field.name,"muss definiert sein.");
}
fieldInfo(field)
{
    Dialog.ok(field.name,field.description);
}
isUnique(field)
{
    Dialog.ok("Feld " + field.name,"muss einzigartig sein.");
}



componentWillReceiveProps(nextProps)
{
  var self = this;
  if (this.props.match.params.objectid !== nextProps.match.params.objectid) {
    self.loadobjectdata(nextProps);
    self.setupNavigation(nextProps);
    
     }
}



loadobjectdata(newprops)
{   
    var self = this;
    
    if(window.location.href.indexOf("newobject") > 0)
    {
        var objectguid = this.uuidv4();
        self.objectid = objectguid;
        self.updatedata["ElementID"] = objectguid;
    }else
    {
        self.objectid = newprops.match.params.objectid;
        self.updatedata["ElementID"] = objectguid;
        console.log(newprops.match.params.listid);
        console.log(newprops.match.params.objectid);
    
        ObjectManager.getSingleObject(newprops.match.params.listid,newprops.match.params.objectid).then(data => {console.log(data); self.setState({element: data})});
    }  
}


saveNewObject()
{
    var self = this;
    ObjectManager.addObject(this.props.match.params.listid,this.updatedata).then(data=>
    {
        if(data.state==="ok")
        {
            self.props.loadListObjects(self.props.match.params.listid);
            self.props.history.push("/list/"+self.props.match.params.listid+"/");
            Dialog.ok(self.props.list.objecttype, "gespeichert");
        }else
        {
          
            Dialog.error(self.props.list.objecttype, "konnte nicht gespeichert werden.");
            for(let i=0;i< data.content.log.length;i++)
            {
                var fieldid = data.content.log[i].fieldid;
               var field = self.props.fields.find(function(element) {
                return element.id === fieldid;
              });
                var msg = data.content.log[i].msg;
                setTimeout(function(){Dialog.error(field.name, msg)}, 500); 
            }
        }
    });
}

saveEditObject()
{
    var self = this;
    
    ObjectManager.editObject(this.props.match.params.listid,this.props.match.params.objectid,this.updatedata).then(data=>
    {
        console.log(data);
        if(data.state==="ok")
        {
            self.props.loadListObjects(self.props.match.params.listid);
            if(self.props.match.params.listid ==="99043cfa-9092-421b-8f34-566e1e6849fd")
            {
                self.props.loadUser();
            } 
            self.props.history.push("/list/"+self.props.match.params.listid+"/");
            Dialog.ok(self.props.list.objecttype, "aktualisiert");
        }else
        {
           
            Dialog.error(self.props.list.objecttype, "konnte nicht gespeichert werden.");
            for(let i=0;i< data.content.log.length;i++)
            {
                var fieldid = data.content.log[i].fieldid;
                if(fieldid.length > 2)
                {
               var field = self.props.fields.find(function(element) {
                return element.id === fieldid;
              });
                
                setTimeout(function(){Dialog.error(field.name, msg)}, 500); 
                }else
                {
                    var msg = data.content.log[i].msg;
                    Dialog.error("Error", msg);
                }
            }
        }
    });  
}


saveObject()
{
    var isnewobject = (window.location.href.indexOf("newobject") > -1);
    if(isnewobject)
    {
        this.saveNewObject();
    }else
    {
        this.saveEditObject();
    }
}

valueUpdate(FieldID,Value)
{
  this.updatedata[FieldID] = Value;
  this.setState({disabled:false});
}

render() {
    var isnewobject = (window.location.href.indexOf("newobject") > -1);
    var selfrender = this;

    if(!this.props.fields || this.props.fields.length===0)
    {
        return(<div>Lade Felder</div>);
    }
    if(!this.props.list)
    {
        return(<div>Lade Liste</div>);
    }
    if(!this.state.element)
    {
        return(<div>Lade Objekt</div>);
    }

    var Editbarmode = "edit";
    if(isnewobject)
    {
        Editbarmode = "new";
    }

    if(this.state.element.data.length === 0  && window.location.href.indexOf("newobject") ===-1)
        {
            if(this.props.list)
            {
                
                return(<div>Lade {this.props.list.objecttype}<Preloader/></div>);
            }else
            {
            return(<div>Lade Object-Data</div>);
            }
        }

var modified = null;
var modifier = null;
var created = null;
var creater = null;
var authorline = null;
var result = this.props.objecttype.fields.map(function(fieldid,index)
{

    var field = selfrender.props.fields.find(function(li) {
        return li.id === fieldid;
      });

    var fieldvalue =null;
    if(!isnewobject)
    {   

        var index = selfrender.state.element.fields.findIndex(f=> f === field.id);
  
        fieldvalue = selfrender.state.element.data[index];
       if(fieldvalue === undefined)
       fieldvalue = "";
 
    
       }
   
    if(!isnewobject)
    {
        switch(field.id)
        {
            case "0cecad40-c0a1-46e3-a7a3-4bd99b7236d6":
            created = moment(); //moment(fieldvalue.datetime).locale("de").format("llll");;
            return;
            case "77f42b3d-8e2c-4869-bd67-6273afe7cf89":
            modified = moment(); // moment(fieldvalue.datetime).locale("de").format("llll");
            return;
            case "6dacefca-1e0d-4bdd-9c82-fe89b517242b":
            creater = fieldvalue;        
            return;     
            case "1d57184b-430b-44c8-9708-d3e3dd8bcce4":
            modifier = fieldvalue;
            return; 
            case "3b825480-8d07-4677-a097-29ad90a753db":
            case "c8e7d134-1005-4756-b851-828767c84e8e":
            case "5be2d63b-b05a-421e-957b-61b288061956":
            return;
            default:
        }
        

    }
    {
       
        switch(field.id)
        {
            case "0cecad40-c0a1-46e3-a7a3-4bd99b7236d6":
            case "77f42b3d-8e2c-4869-bd67-6273afe7cf89":
            case "6dacefca-1e0d-4bdd-9c82-fe89b517242b":      
            case "1d57184b-430b-44c8-9708-d3e3dd8bcce4":
            case "7628c3c9-6e30-4b1a-8f74-cc8ae8366d0c":
            return; 
            default:
        }
    }
var mandatory = null;
var unique = null;
if(field.mandatory)
{
    mandatory = <img onClick={()=>{selfrender.isMandatory(field)}} src="https://publicfile.standard-cloud.com/icons/Ausrufezeichen_klein.png" alt="Pflichtfeld" />
}
var fieldinfo = <img  onClick={()=>{selfrender.fieldInfo(field)}} src="https://publicfile.standard-cloud.com/help/Frage_klein.png" alt="Felderklärung anzeigen" />

if(field.unique)
{
    unique = <img  onClick={()=>{selfrender.isUnique(field)}} src="https://publicfile.standard-cloud.com/icons/star_klein.png" alt="Feld muss eindeutig sein" /> 
}

return(<tr key={fieldid}><td className="fieldnametd">{field.name} {mandatory}{fieldinfo}{unique}</td><td className="editobject-fildvaluetd">
    <ShowEditField field={field} objectid={selfrender.objectid} valueUpdate={selfrender.valueUpdate} value={fieldvalue} />
    </td></tr>);
});
if(!isnewobject)
{
    var modname = null;
    var creatorstr = null;
if(modifier.length!==0)
{
    if(modified !== created)
    {
        var profileurl = "/profile/" + modifier[0].id + "/";
        modname = "test "; //<span>Bearbeitet am  {modified} von <Link to={profileurl}>  {modifier[0].name} </Link>.</span>;
    }
}
if(creater.length!==0)
{
    var profileurl1 = "/profile/" + creater[0].id + "/";
    creatorstr = "test.."; //<span>Erzeugt am  {created} von <Link to={profileurl1}>  {creater[0].name} </Link>.</span>;
}


authorline = <div>{modname} {creatorstr} </div>
}



	return (
        <div>
        <table id="editobjecttbl">
            <tbody>
        {result}
        </tbody>
        </table>
        <div id="authorboxline">
        {authorline}
        </div>
        <EditObjectBar mode={Editbarmode} disabled={this.state.disabled} saveObject={this.saveObject} list={this.props.list} objectid={this.props.match.params.objectid}/>
        <div className="bottomeditpage">
        </div>
        </div>
           );
	
}	  

  
  }


function mapStateToProps({ list,field,objecttype}, ownProps) {
    let ls = list.find(l=> l.id === ownProps.match.params.listid);
    let objecttypes = objecttype.find(o=> o.id === ls.objecttype);
    return { list: ls,fields:field,objecttype:objecttypes};
  }

export default withRouter(connect(mapStateToProps,{setNavigation,loadListObjects,loadUser})(EditObjectPage));