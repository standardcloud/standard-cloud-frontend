import React from 'react';
import BT from '../../component/button/stdbutton';
import {connect} from 'react-redux';
import $ from 'jquery';
import sort from 'jquery-ui-sortable-npm';
import dialog from '../../component/dialog/stddialog';
import listmanager from '../../module/listmanager/stdlistmanager';
import {setNavigation} from '../../actions/navigation';
import {loadLists} from  '../../actions/list';
import {resetListObjects} from  '../../actions/objects';
import Trans from '../../module/translatoinmanager/translationmanager';
import './list_viewsettings.css';

class ListViewSettings extends React.Component {
    constructor(props) {
    super(props);
    this.saveViewSettingsPage = this.saveViewSettingsPage.bind(this);
    this.handleObjectPreviewChange = this.handleObjectPreviewChange.bind(this);
    this.changeFieldSort = this.changeFieldSort.bind(this);
    this.handleShowEditbuttonChange = this.handleShowEditbuttonChange.bind(this);
    this.handleShowdeletebuttonChange = this.handleShowdeletebuttonChange.bind(this);
    this.handleEnableSingleClickChange = this.handleEnableSingleClickChange.bind(this)
    this.handleOrderChange = this.handleOrderChange.bind(this);

    this.state =  {changedvalue:false,helpurl:"https://standard-cloud.zendesk.com/hc/de/articles/115001738509-Listeneinstellungen-Ansichten",list:this.props.list};
    if(this.props.list)
      {
      var listurl = "/list/" + this.props.list.id + "/";
      var listsetting =  "/list/" + this.props.list.id + "/edit/";
      var urldarstellung =  "/list/" + this.props.list.id + "/edit/view/";
      var nav = [{title:"Start",url:"/"},{title:Trans.t("lists"), url:"/List/"},{title:this.props.list.name, url:listurl},{title:Trans.t("listsettings"),url:listsetting},{title:Trans.t("display"),url:urldarstellung},{title:Trans.t("listview")}];
      this.props.setNavigation(nav, Trans.t("listview"));
     }
    }	


componentDidMount() {
//this.loadlistsettings();
var self = this;
$( "#sortable" ).sortable({change: function(event, ui) {
 self.setState({changedvalue:true});
        }}).disableSelection();
}

sortFieds(callback)
{
        /*Sort fields */
        var sortedfields = [];
		var ul = document.getElementById("sortable");
	    var items = ul.getElementsByTagName("li");
			for (var i = 0; i < items.length; ++i) {
                var item = $(items[i])
                
	     	if($(item).find("input").prop('checked'))
             {
                sortedfields.push(item.attr("data-fieldid"));
             }
		}
        callback(sortedfields);

		/*End sort fields */
}
saveViewSettingsPage()
{
   this.setState({changedvalue:false});
    var self = this;
    this.sortFieds(function(sortfieldids)
    {
      let newlist = {...self.state.list,defaultviewfields:sortfieldids};
      listmanager.setsettings(newlist).then(returndata =>  
      {
            if(returndata.state==="OK")
            {
             dialog.okdialog("Listeneinstellungen", returndata.msg);
         //   self.props.loadLists();
          //  self.props.resetListObjects();
            }else
            {
              dialog.errordialog("Listeneinstellungen", returndata.msg);  
            }
        });
    });
}
handleShowHideField(field)
{
 this.setState({changedvalue:true});
 var newfieldarray =[];
      if(this.state.list.defaultviewfields.includes(field.id))
      {
          newfieldarray = this.state.list.defaultviewfields.filter(e => e !== field.id);
      }
      else
      {
          newfieldarray = this.state.list.defaultviewfields;
          newfieldarray.push(field.id);
      }
    
 let newlist = {...this.state.list,defaultviewfields:newfieldarray};
 console.log(newlist);
 this.setState({list:newlist});
}

handleOrderChange(e)
{   
    	this.setState({changedvalue:true}); 
        let newlist = {...this.state.list,sortorder:e.target.value};
        this.setState({list:newlist});
}
changeFieldSort(e)
{
    	this.setState({changedvalue:true});
        let newlist = {...this.state.list,defaultsortfield:e.target.value};
        this.setState({list:newlist});
}

handleObjectPreviewChange(e)
{
        this.setState({changedvalue:true});
        let newlist = {...this.state.list,objectpreview:!this.state.list.objectpreview};
        this.setState({list:newlist});
}
handleEnableSingleClickChange(e)
{
    this.setState({changedvalue:true});
    let newlist = {...this.state.list,singleclick:!this.state.list.singleclick};
    this.setState({list:newlist});
}

handleShowEditbuttonChange(e)
{
    this.setState({changedvalue:true});
    let newlist = {...this.state.list,editbutton:!this.state.list.editbutton};
    this.setState({list:newlist});
}
handleShowdeletebuttonChange(e)
{
    this.setState({changedvalue:true});
    let newlist = {...this.state.list,deletebutton:!this.state.list.deletebutton};
    this.setState({list:newlist});
}

render () { 

    var selfrender = this;
	var savebutton = <BT  disabled={true} name= {Trans.t("save")} iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
	if(this.state.changedvalue)
	{
		savebutton = <BT name= {Trans.t("save")} callbackfunction={this.saveViewSettingsPage}  iconurl="https://publicfile.standard-cloud.com/icons/save.png" />  
	}

    var  renderfields = [];

    this.state.list.defaultviewfields.forEach(fieldid => {        
        let field = selfrender.props.fields.find(f=>f.id === fieldid);
      renderfields.push (
        <li key={field.id} data-inuse="true" data-fieldid={field.id} className="ui-state-default"><span dangerouslySetInnerHTML={{__html: field.name}} ></span> <input type="checkbox" checked={true} onChange={selfrender.handleShowHideField.bind(selfrender, field)}  /></li>
     );
    });



   this.props.objecttype.fields.forEach(function(fieldid) {
        let field = selfrender.props.fields.find(f=>f.id === fieldid);
        let selectedfield = false;
        if(!selfrender.state.list.defaultviewfields.includes(field.id))
        {
            renderfields.push(
                <li key={field.id} data-inuse="true" data-fieldid={field.id} className="ui-state-default"><span dangerouslySetInnerHTML={{__html: field.name}} ></span> <input type="checkbox" checked={selectedfield} onChange={selfrender.handleShowHideField.bind(selfrender, field)}  /></li>
             );          
        }	
    });
var renderselectedoptionssoftfield =  this.props.objecttype.fields.map(function(fieldid) {
    let field = selfrender.props.fields.find(f=>f.id === fieldid);
      return (
		<option value={field.id} key={field.id}>{field.name}</option>
      );	
    });


		return (
<div id="listsettingcountainer">

            <div id="listsettingcountainerdefaultviewbox">
            <div className="stdcard stdcard-1">
                <h1><img alt="Listenansicht" src="https://publicfile.standard-cloud.com/icons/list.png"/>  {Trans.t("listview")}</h1>
                     <h6>Horizontale Spaltenanordnung</h6>
            <div id="sorteditor">	
			<ul id="sortable" >
			{renderfields}
			</ul>		
            </div><br/><br/><br/><br/><br/>
            <div>
            <h6>Sortierung</h6>
            <select value={this.state.list.defaultsortfield}  onChange={this.changeFieldSort}>
				<option value=""></option>				
			    {renderselectedoptionssoftfield} 
				</select>
                <select value={this.state.list.sortorder} onChange={this.handleOrderChange}>>
                <option value=""></option>
                <option value="ASC">A->Z</option>
                <option value="DESC">Z->A</option>
                </select>
            <br/><br/>
        						<div className="stdcheckboxregion">
                            <input id="enableobjectpreview"  checked={this.state.list.objectpreview} type="checkbox" value="enableobjectpreview"   className="checkbox"/>
                            <label onClick={this.handleObjectPreviewChange}  htmlFor="enableobjectpreview"><strong>Objectvorschau</strong> anzeigen</label>
     	             </div>
                    <div className="stdcheckboxregion">
                            <input id="enablesingleclick"  checked={this.state.list.singleclick} type="checkbox" value="enableobjectpreview"   className="checkbox"/>
                            <label onClick={this.handleEnableSingleClickChange}  htmlFor="enablesingleclick"><strong>Objekt</strong> mit einem Klick anzeigen.</label>
     	            </div>
                    <div className="stdcheckboxregion">
                            <input id="showeditbutton"  checked={this.state.list.editbutton} type="checkbox" value="showeditbutton"   className="checkbox"/>
                            <label onClick={this.handleShowEditbuttonChange}  htmlFor="showeditbutton">Schaltfläche <strong>Bearbeiten</strong> anzeigen</label>
     	             </div>   
                    <div className="stdcheckboxregion">
                            <input id="showdeletebutton"  checked={this.state.list.deletebutton} type="checkbox" value="showdeletebutton"   className="checkbox"/>
                            <label onClick={this.handleShowdeletebuttonChange}  htmlFor="showdeletebutton">Schaltfläche <strong>Löschen</strong> anzeigen</label>
     	             </div><br/>    
            </div>
            </div>
            <div id="savebuttonsettings">
            {savebutton}
            <BT name= {Trans.t("help")} url={this.state.helpurl} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
            </div>
            </div>
</div>
        );

		}


  }


  function mapStateToProps({ list,objecttype,field}, ownProps) {
      let singlelist = list.find(l=> l.id===ownProps.match.params.listid);
      let singleobjecttype = objecttype.find(f=>f.id === singlelist.objecttype);     
    return {list:singlelist,objecttype:singleobjecttype,fields:field};
  }


  export default connect(mapStateToProps,{setNavigation,loadLists,resetListObjects})(ListViewSettings);