import React from 'react';
import BT from '../../../component/button/stdbutton';
import {connect} from 'react-redux';
import {setNavigation} from '../../../actions/navigation';
import Contentbox from '../../../component/contentbox/contentbox';
import Helpbox from '../../../component/helpbox/helpbox';
import Listmanager from '../../../module/listmanager/stdlistmanager';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import ShowReadField from '../../../component/fieldtype/renderread';
import Trans from '../../../module/translatoinmanager/translationmanager';

import './objectversions.css';
  class ObjectVersions extends React.Component {
	    constructor(props) {
			 super(props);
			 this.loadversions = this.loadversions.bind(this);
			 this.clickback = this.clickback.bind(this);
	   		 this.state = {versions:[]};
  }

loadversions()
{
	var self = this;
Listmanager.getVersions4Object(	this.props.match.params.listid,	this.props.match.params.objectid, function(data)
{
	self.setState({versions:data});
}
);
}
clickback()
{
	var url = "/list/" + this.props.list.id + "/edit/" + this.props.match.params.objectid + "/editmode/";
	this.props.history.push(url);
}

createuserobject(value)
{
	var userarray =[];
	var user = this.props.users.find(function(user)
{
return user.id === value;
});
userarray.push(user);
return userarray;
}

componentDidMount() {
this.loadversions();
var url = "/list/" + this.props.list.id + "/";
var url2 = "/list/" + this.props.list.id + "/edit/" + this.props.match.params.objectid + "/editmode/";

var nav = [{title:"Start",url:"/"},{title:Trans.t("lists"),url:"/list/"},{title:this.props.list.name,url:url},{title:this.props.list.objecttype,url:url2},{title:Trans.t("versioning")}];
this.props.setNavigation(nav,Trans.t("versioning"));
}

render() {
	var self = this;
	if(!this.state.versions)
	{
		return(<div>Lade Version</div>);
	}

	var versions = [];
	this.state.versions.forEach(function(version)
	{
		var title = "Version " + version.version;
		var tablecontent = [];
		version.data.forEach(function(data)
		{
			var field = self.props.fields.find(f=>f.id ===data.fieldid);
		var created = null;
		var modified = null;
		var creater = null;
		var modifier = null;
			switch(field.id)
			{
					case "0cecad40-c0a1-46e3-a7a3-4bd99b7236d6":
				created = moment(data.value.datetime).locale("de").format("llll");;
				tablecontent.push(<tr key="created"><td>{data.field}</td><td>{created}</td></tr>);
					break
					case "77f42b3d-8e2c-4869-bd67-6273afe7cf89":
				modified = moment(data.value.datetime).locale("de").format("llll");
				tablecontent.push(<tr key="modified"><td>{data.field}</td><td>{modified}</td></tr>);
					break;
					case "6dacefca-1e0d-4bdd-9c82-fe89b517242b":       
			creater = self.createuserobject(data.value);
			tablecontent.push(<tr key="creater"><td>{data.field}</td><td><ShowReadField field={field} value={creater}/></td></tr>);
					break;   
					case "1d57184b-430b-44c8-9708-d3e3dd8bcce4":
					modifier = self.createuserobject(data.value);
					tablecontent.push(<tr key="modifier"><td>{data.field}</td><td><ShowReadField field={field} value={modifier}/></td></tr>);
					break;
			  	case "3b825480-8d07-4677-a097-29ad90a753db":
					case "c8e7d134-1005-4756-b851-828767c84e8e":
					case "5be2d63b-b05a-421e-957b-61b288061956":
					break;
				/*	continue;*/
					default:
			}
		
			tablecontent.push(<tr key={data.fieldid}><td>{data.field}</td><td><ShowReadField field={field} value={data.value}/></td></tr>);
		});
		versions.push(<div key={version.version}><br/><Contentbox title={title}>
<table id="objectversoins-objecttable">
<tbody>
{tablecontent}
</tbody>
	</table>
</Contentbox></div>);
	});

	return (
     <div id="objectversions-page">
	<Contentbox  title={Trans.t("versions")}>
{versions}
    </Contentbox>
		<div>
<Helpbox title={Trans.t("help")}>
Versionen - Einstellungen
</Helpbox>
       
<div id="obectversion-footer">
      <BT  callbackfunction={this.clickback} name={Trans.t("back")}   iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
			<BT url="https://standard-cloud.zendesk.com/hc/de/articles/360001533571-Versionierung"  name={Trans.t("help")}  iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
      </div>
				</div>
	</div>
    );
	
}	  
  }

	function mapStateToProps({ list,field,users}, ownProps) {
		return { list: list.find(f=>f.id === ownProps.match.params.listid),fields:field,users:users};
	}

export default withRouter(connect(mapStateToProps,{setNavigation})(ObjectVersions));



