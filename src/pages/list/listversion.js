import React from 'react';
import BT from '../../component/button/stdbutton';
import Listmanager from '../../module/listmanager/stdlistmanager';
import {connect} from 'react-redux';
import {setNavigation} from '../../actions/navigation';
import {loadLists} from '../../actions/list';
import dialog from '../../component/dialog/stddialog';
import Helpbox from '../../component/helpbox/helpbox';
import Contentbox from '../../component/contentbox/contentbox';
import Trans from '../../module/translatoinmanager/translationmanager';
import Style from './listversion.module.css';
  class ListVersionPage extends React.Component {
	  constructor(props) {
		super(props);
		this.state = {list:this.props.list,pagechanged:false,noversionlimit:false};	
		this.changeEnableVersion = this.changeEnableVersion.bind(this);
		this.changeVersionLimit = this.changeVersionLimit.bind(this);
		this.saveversionsetting = this.saveversionsetting.bind(this);			
	}
saveversionsetting()
{
	  var self = this;
		var test = window.location.href;
		var testRE = test.match("list/(.*)/edit");
		var listid = testRE[1];	
		Listmanager.setsettings(this.state.list).then(data =>
		{
				if(data.state==="OK")
					{
						dialog.ok("Einstellungen","wurden gespeichert.");
						self.setState({pagechanged:false});

					}else
					{
						dialog.error("Versions-Einstellungen","konnten nicht gespeichert werden.");
					}
		});
}		  
componentDidMount() 
{
	let self = this;
	var url = "/list/" + this.props.list.id;
	var url2 = "/list/" + this.props.list.id + "/edit";
	var nav = [{title:"Start",url:"/"},{title:Trans.t("lists"),url:"/list/"},{title:self.props.list.name,url:url},{title:Trans.t("listsettings"),url:url2},{title:Trans.t("versioning")}];
	self.props.setNavigation(nav, "Versionierung");
}
changeEnableVersion(e)
{
	if(this.state.list.listversiontype <= 0)
	{
		this.changeProp("listversiontype",1);
	}else
	{
		this.changeProp("listversiontype",0);
	}

	this.setState({pagechanged:true});
}
changeVersionLimit(e)
{
this.setState({noversionlimit:!this.state.noversionlimit});
this.setState({pagechanged:true});
}

changeProp(propname,value)
{
	this.setState({changedvalue:true});
	let newlist = {...this.state.list, [propname]: value}
	this.setState({list:newlist});
	this.setState({pagechanged:true});
}

clickback()
{
window.history.back();
}

//listversiontype
//1 = enalbed
//2 = unlimited
//3 = limited

render() {
console.log(this.state.list.listversiontype);
	var versionlimithtml = null;
  var limitbuttonhtml = null 
	if(this.state.list.listversiontype === 3)
		{
			versionlimithtml = 	<div id="version_versionlimit"><div className="textboxgroup"><input required  className="stdtextbox" type = "text" name="tbmaxobjectsonclient" onChange={(e)=>this.changeProp("numbermajorversion",parseInt(e.target.value,10))} value={this.state.list.numbermajorversion}  /><span className="texthighlight"></span><span className="textbar"></span><label>Folgende Anzahl an Versionen aufbewahren</label></div></div>
		}
		
		if(this.state.list.listversiontype)
		{
			limitbuttonhtml=<div><div className="stdcheckboxregion"><input id="nolimit"  checked={(this.state.list.listversiontype===2)} type="checkbox" value="enableanalytics" className="checkbox"/><label onClick={()=>{(this.state.list.listversiontype===2) ? this.changeProp("listversiontype",parseInt(3,10)):this.changeProp("listversiontype",parseInt(2,10))}}   htmlFor="nolimit">Unendlich viele Versionen erstellen</label></div><br/></div>
		}
	return (
		
     <div id="trash_page">
   	<Contentbox title={Trans.t("versioning")}>
		 <div id="version_content" className={Style.version_content}>

	<div className="stdcheckboxregion">
    <input id="enableversion"  checked={(this.state.list.listversiontype > 0)} type="checkbox" value="enabled" className="checkbox"/>
    <label onClick={this.changeEnableVersion}  htmlFor="enableversion"><strong>Versionierung</strong>  aktivieren.</label>
    </div>
		{limitbuttonhtml}
		{versionlimithtml}
    </div><br/>
      </Contentbox>
		<div>
<Helpbox title={Trans.t("help")}>
Die Versionierung hilft Ihnen festzustellen, wer wann etwas verändert hat. <br/><br/>Darüber hinaus können Sie bestimmen wie viele Versionen gespeichert werden sollen.
Wenn Sie die Einstellung "Unendlich viele Versionen erstellen" aktivieren, dann werden alle Änderungen gespeichert.<br/>Alternativ können Sie auf die Anzahl der gespeicherten 
Versionen limitieren. Dann werden ältere Versionen gelöscht. 
</Helpbox>
       
<div id="newlistfooter">
		  	 <BT  callbackfunction={this.clickback} name={Trans.t("back")}   iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
				 <BT  callbackfunction={this.saveversionsetting} disabled={!this.state.pagechanged} name={Trans.t("save")}   iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
		     <BT reload url="https://standard-cloud.zendesk.com/hc/de/articles/115000611651-Papierkorb"  name={Trans.t("help")} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
      </div>
				</div>
	</div>
    );
	
}	  
  }

  function mapStateToProps({ list}, ownProps) {
	return { list:list.find(function(li) {return li.id === ownProps.match.params.listid})};
  }
    
  export default connect(mapStateToProps,{setNavigation,loadLists})(ListVersionPage);
