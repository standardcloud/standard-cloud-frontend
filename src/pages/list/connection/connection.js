import React from 'react';
import BT from '../../../component/button/stdbutton';
import {connect} from 'react-redux';
import {setNavigation} from '../../../actions/navigation';
import Contentbox from '../../../component/contentbox/contentbox';
import Helpbox from '../../../component/helpbox/helpbox';
import Connectionmanager from '../../../module/connection/connection';
import { withRouter } from 'react-router-dom';
import Dialog from '../../../component/dialog/stddialog';
import DisplayObject from '../../../module/displayobject/stddisplayobject';

import ObjectManager from '../../../module/objectmanager/stdobjectmanager';
import ObjectUrlManager from '../../../module/objecturlmanager/stdobjecturlmanager';

import './connection.css';
  class ObjectConnections extends React.Component {
	    constructor(props) {
			 super(props);
			 this.loadConnections = this.loadConnections.bind(this);
			 this.getobjectdata = this.getobjectdata.bind(this);
			 this.openobject = this.openobject.bind(this);
			 this.clickback = this.clickback.bind(this);
		this.state = {connections:[]};

  }

  componentDidMount() {
	this.loadConnections();  
}

getobjectdata(listid,objectid,callback)
{
		ObjectManager.getobject(listid,objectid).then(data=>
		{		
			if(data)
			{
				callback(data);
			}			
		});
}

openobject(connectionobject)
{
    var sself = this;
								ObjectUrlManager.geturl(connectionobject.listid,connectionobject.objectid,function(url)
									{
                                         sself.props.history.push(url);
									});
								 
}


loadConnections()
{
	var list = this.props.lists.find(f=>f.id ===this.props.match.params.listid);

var url = "/list/" + list.id + "/";
var url2 = "/list/" + list.id + "/edit/" + this.props.match.params.objectid + "/editmode/";

var nav = [{title:"Start",url:"/"},{title:"Listen",url:"/list/"},{title:list.name,url:url},{title:list.objecttype,url:url2},{title:"Verbindungen"}];
this.props.setNavigation(nav,"Verbindungen");

	var self = this;
Connectionmanager.getConnection4Object(this.props.match.params.objectid, this.props.match.params.listid).then(data=>
{
	if(data.state ==="ok")
	{
	var result = [];
    for (let i = 0; i < data.content.length; i++) { 		
		var objectbox = {};			
		objectbox.listid = data.content[i][1];
		objectbox.objectid = data.content[i][2];
		self.getobjectdata(objectbox.listid,objectbox.objectid,function(data)
		{
			objectbox.content = DisplayObject.getcontent(data);
			objectbox.listname = self.props.lists.find(l=>l.id ===objectbox.listid).name;
			result.push(objectbox);	
			self.setState({connections:result});
		});                  
            						
        }

	}else
	{
		Dialog.error("Verbindungen","konnten nicht geladen werden.");
	}
}
); 
}

clickback()
{
	var list = this.props.lists.find(l=>l.id===this.props.match.params.listid);
	var url = "/list/" + list.id + "/edit/" + this.props.match.params.objectid + "/editmode/";
	this.props.history.push(url);
}

render() {
	var self = this;
	if(!this.state.connections)
	{
		return(<div>Lade Verbindungen</div>);
	}


	var connections = this.state.connections.map(function(connection)
	{
		return (
			<div key={connection.objectid} onClick={()=>self.openobject(connection)} className="objectbox">
				<div className="headerobjectbox" dangerouslySetInnerHTML={{__html: connection.listname}}></div>
				<div className="analyticsbox">
						<div className="analyticsboxwrapper" dangerouslySetInnerHTML={{__html: connection.content}} ></div>
				</div>
			</div>
		  
				);     

	});

	return (
     <div id="objectconnections-page">
	<Contentbox  title="Verbindungen">
	<div id="connection-contentbox-div">
{connections}
</div>
    </Contentbox>
		<div>
<Helpbox title="Hilfe">
Verbindungen.
</Helpbox>
       
<div id="connection-footer">
      <BT  callbackfunction={this.clickback} name="Zurück"   iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
			<BT url=""  name="Hilfe" iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
      </div>
				</div>
	</div>
    );
	
}	  
  }

	function mapStateToProps({ list}) {
		return { lists: list};
	}

export default withRouter(connect(mapStateToProps,{setNavigation})(ObjectConnections));



