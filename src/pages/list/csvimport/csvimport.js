import React from 'react';
import BT from '../../../component/button/stdbutton';
import {BACKEND} from '../../../stdsetup';
import Helpbox from './../../../component/helpbox/helpbox';

import Contentbox from './../../../component/contentbox/contentbox';
import './csvimport.css';

import Dropzone from "dropzone";
import $ from 'jquery';
import listmanager from '../../../module/listmanager/stdlistmanager';


class ImportPageCSVPage extends React.Component {
         constructor(props) {
          super(props);
          this.loadListSettings = this.loadListSettings.bind(this);
          this.setSelectedFields = this.setSelectedFields.bind(this);
          this.removefield = this.removefield.bind(this);
          this.addfield = this.addfield.bind(this);
          this.addfile = this.addfile.bind(this);
          this.handleImportTypeChange = this.handleImportTypeChange.bind(this);
          this.state = {csvprotokoll:[],importtype:"0",loading:false,fields:[],nameplural:"",selectedfields:""};
    }
        
componentDidMount() {
           var self = this;
           var test = window.location.href;
           var testRE = test.match("list/(.*)/edit");
           var listid = testRE[1];	
   
           var uploadurl = BACKEND + "/list/" +  listid + "/csvimport/"
            var myDropzone = new Dropzone("#dropzonediv", { url: uploadurl,  headers: {
                'Cache-Control': null,
                'X-Requested-With': null
            },  withCredentials: true,	sending: function(file, xhr, formData){
            formData.append('importtype', self.state.importtype);
                  formData.append('selectedfields', self.state.selectedfields);
                  self.setState({loading:true});
      }});
   myDropzone.on("success", function(file, responseText) {
       self.setState({csvprotokoll:responseText});
       self.setState({loading:false});
       });
  
    Dropzone.options.myDropzone = {
    paramName: "file", // The name that will be used to transfer the file
    maxFilesize: 10
  };
  
  this.loadListSettings();
}
      
  setSelectedFields()
          {
              var fields = "";
              for(let i=0;i < this.state.fields.length;i++)
              {
                  var el = this.state.fields[i];
              if(el.selected)
              {
                  fields += el.id + ";";
              }			
          }
          this.setState({selectedfields:fields});
          }
  
          loadListSettings()
          {
          var test = window.location.href;
          var testRE = test.match("list/(.*)/edit");
          var listid = testRE[1];	
  
          var self = this;
              listmanager.getlistsettings(listid,function(settings)
              {
  
                  self.setState({nameplural:settings.objecttype.nameplural});
                  self.setState({fields:settings.fields});
              });
          }
  
          handleImportTypeChange(changeEvent) {
    this.setState({importtype: changeEvent.target.value});
  }
  
  removefield(element)
  {
      var fields = this.state.fields;
      var newfields = [];
          for(var j=0;j<fields.length;j++)
          {
              var el = fields[j];
              
              if(el.id === element.id)
              {
                      el.selected = false;
              }
              newfields.push(el);
          }
          this.setState({fields:newfields});
          this.setSelectedFields();
  }
  addfield(element)
  {
      var fields = this.state.fields;
      var newfields =  [];
          for(var j=0;j<fields.length;j++)
          {
              var el = fields[j];
              
              if(el.id === element.id)
              {
                      el.selected = true;
              }
              newfields.push(el);
          }
          this.setState({fields:newfields});
          this.setSelectedFields();
  }
  
  addfile()
      {
      $("#dropzonediv").trigger("click");
      }
  
      
                render() {
                    
              
  var protokollheader = <tr><th>Datum</th><th>Status</th><th>Beschreibung</th></tr>
              var renderselectedoptionsright= this.state.csvprotokoll.map(function(protokoll){	
        return (	  
              <tr><td>{protokoll.date}</td><td>{protokoll.state}</td><td dangerouslySetInnerHTML={{__html: protokoll.msg}}></td></tr>
                  );     
        
      });
  
  var importbutton =""
  
  if((this.state.importtype ==="1" || this.state.importtype ==="2") || this.state.selectedfields.length>0)
  {
  importbutton=<BT name="CSV hochladen" callbackfunction={this.addfile} iconurl="https://publicfile.standard-cloud.com/icons/attachfile.png" />
  }
  var loadericon ="";
  if(this.state.loading)
  {
      loadericon =<div><br/><br/><br/><img src="https://publicfile.standard-cloud.com/icons/loader.gif" alt="Loading.."/><br/><div>Loading..</div></div>
  }
  var identcomponent ="";
  if(this.state.importtype==="3" || this.state.importtype==="4")
  {
      identcomponent =<div>
      <Contentbox title="Ident-Felder bestimmen">

      </Contentbox>
  </div>
  }
  
  
  var protokollcomponent ="";
  
  if(this.state.csvprotokoll.length >0)
  {
  protokollcomponent =<div className="csvprotokollbox">
        <Contentbox title="Import-Protokoll">
          <div><table className="stdtable">
              <thead>
                  {protokollheader}
    {renderselectedoptionsright}
              </thead><tbody>	  
          </tbody>
         </table></div>
         </Contentbox>
         </div>
  
  }
  
                  return (
                  <div>
      <Contentbox title="CSV-Import"><br/>
          <Contentbox title="Type">
        <label htmlFor="mc"><input type="radio" id="mc" name="importtype" value="1" onChange={this.handleImportTypeChange}  checked={this.state.importtype === "1"}/>
      Alle vorhandenen {this.state.nameplural} (unwiederherstellbar) l&ouml;schen. Danach CSV-Datei importieren. </label><br/> 
      <label htmlFor="vi"> <input type="radio" id="vi" name="importtype" value="2" onChange={this.handleImportTypeChange}  checked={this.state.importtype === "2"}/>
       Die vorhandenen {this.state.nameplural} nicht löschen. Alle in der CSV-Datei enthaltenen Objekte anfügen.</label><br/> 
      <label htmlFor="ae"><input type="radio" id="ae" name="importtype" value="3" onChange={this.handleImportTypeChange}   checked={this.state.importtype === "3"}/>
        {this.state.nameplural} aus der CSV nur dann importieren, wenn Sie nicht bereis vorhanden sind. </label> <br/> 
          <label htmlFor="ae"> <input type="radio" id="ae" name="importtype" value="4"  onChange={this.handleImportTypeChange}  checked={this.state.importtype === "4"}/>
       {this.state.nameplural} aus der CSV nur dann &uuml;bernehmen, wenn Sie bereis in der Liste vorhanden sind. </label> <br/> 
       </Contentbox>
          
          <div id="dropzonediv">
  {importbutton}<br/>
  {loadericon}<br/>
   </div>
   <div>
  
  <br/>
  {identcomponent}
  <br/><br/>
  {protokollcomponent}		
  </div>
  
  </Contentbox>
  
  <div id="csvimporthelpregion">
          <BT name="Hilfe" url="https://standard-cloud.zendesk.com/hc/de/articles/214375345" iconurl="https://publicfile.standard-cloud.com/icons/help.png" />
  </div>
  <Helpbox title="Hilfe">
  CSV-Import Help
  </Helpbox>
        </div>
                  );
                }
}
  
export default ImportPageCSVPage;

  