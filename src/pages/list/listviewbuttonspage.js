import React from 'react';
import BT from '../../component/button/stdbutton';
import {connect} from 'react-redux';
import {setNavigation} from '../../actions/navigation'
import Trans from '../../module/translatoinmanager/translationmanager';

class ListpListViewPageage extends React.Component {
  constructor(props) {
    super(props);
    
    var listurl = "/list/" + this.props.list.id + "/";
    var listsetting =  "/list/" + this.props.list.id + "/edit/";
		var nav = [{title:"Start",url:"/"},{title:Trans.t("lists"), url:"/List/"},{title:this.props.list.name, url:listurl},{title:Trans.t("listsettings"),url:listsetting},{title:Trans.t("display")}];
		this.props.setNavigation(nav,Trans.t("display"));
  }		
	    

render() {
		return (
	<div>
		<BT  url="object_view/"  name={Trans.t("objectview")} iconurl="https://publicfile.standard-cloud.com/icons/editobjectview.png" />
		<BT  url="list_view/"  name={Trans.t("listview")} iconurl="https://publicfile.standard-cloud.com/icons/list.png" />
     
	</div>
        );
		}

  }
  function mapStateToProps({ list}, ownProps) {
    return { list: list.find(l=> l.id===ownProps.match.params.listid)};
  }


  export default connect(mapStateToProps,{setNavigation})(ListpListViewPageage);
