
import React from 'react';
import {connect} from 'react-redux';
import {setNavigation} from '../../actions/navigation';
import Listmanager from '../../module/listmanager/stdlistmanager';
import BT from '../../component/button/stdbutton';
import dialog from '../../component/dialog/stddialog';
import Button from 'muicss/lib/react/button';
import Dialogbox from '../../component/dialogbox/dialogbox';

import './listsettingsbuttonspage.css';
import Trans from '../../module/translatoinmanager/translationmanager';
class ListSettingPage extends React.Component {
	constructor(props) {
	  super(props);
	  this.DeleteList = this.DeleteList.bind(this);

	  this.deleteListButton = this.deleteListButton.bind(this);
	  this.state = {deletedialogvisible:false};
	  var url = "/list/" + this.props.list.id+ "/";
	  var nav = [{title:"Start",url:"/"},{title:Trans.t("lists"),url:"/list/"},{title:this.props.list.name,url:url},{title:Trans.t("listsettings")}];
	  this.props.setNavigation(nav,Trans.t("listsettings"));
	}


DeleteList() {
Listmanager.deletelist(this.props.list.id,function(returnobject)
{
		if(returnobject === false)
		{
			dialog.errordialog('Liste',"konnte nicht gel&ouml;scht werden.");			
		}else
		{
					dialog.okdialog('Liste',"wurde gel&ouml;scht.");
					window.setTimeout(window.location ="/list/", 5000 );
		}
});
}
	  

deleteListButton()
{
	this.setState({deletedialogvisible:true});
}

	  
render () {

var deletedialog = null;
	if(this.state.deletedialogvisible)
	{
		var listtext = <div>Wollten Sie die List {this.props.list.name} löschen?</div>
			deletedialog = <Dialogbox closedialog={()=>{this.setState({deletedialogvisible:false})}}  title="Löschen ?">
			<div id="deletelistbox">
			{listtext}
			<Button onClick={()=>{this.DeleteList(this.props.list.id)}} color="danger">JA, LÖSCHEN</Button>
     		 <Button  onClick={()=>{this.setState({deletedialogvisible:false})}} >NEIN</Button>
			  </div>
			</Dialogbox>
	}



	var generalsetting = "/list/" +  this.props.list.id+ "/edit/generalsetting/";
	var version = "/list/" + this.props.list.id + "/edit/version/";
	var permission = "/list/" +  this.props.list.id + "/edit/permission/";
	var view = "/list/" +  this.props.list.id+ "/edit/view/";
	var trash = "/list/" +  this.props.list.id + "/edit/trash/";
	var csvimport = "/list/" +  this.props.list.id + "/edit/csvimport/";
	console.log( this.props.list);
	var objecttypeurl = "/admin/objecttype/modify/" + this.props.list.objecttype + "/";
		return (
	<div>
				{deletedialog}
		<BT count="9" url={generalsetting}  name={Trans.t("listbasicsettings")} iconurl="https://publicfile.standard-cloud.com/icons/settings.png" />
		<BT count="4" url={version}  name={Trans.t("versioning")} iconurl="https://publicfile.standard-cloud.com/icons/versions.png" />
        <BT  url={permission}  name={Trans.t("permissions")}  iconurl="https://publicfile.standard-cloud.com/icons/lock.png" />
	    <BT  url={view} name={Trans.t("display")} iconurl="https://publicfile.standard-cloud.com/icons/iql.png" />
		<BT url={trash}  name={Trans.t("trash")} iconurl="https://publicfile.standard-cloud.com/icons/trash.png" />
		<BT url={objecttypeurl} name="Listenvorlage bearbeiten" iconurl="https://publicfile.standard-cloud.com/icons/objecttype.png" />	
		<BT callbackfunction={this.deleteListButton}  name={Trans.deletelist(this.props.list.name)} iconurl="https://publicfile.standard-cloud.com/icons/delete.png" />
	    <BT url="https://standard-cloud.zendesk.com/hc/de/sections/201694815-Feldeinstellungen" name={Trans.t("help")} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />
	    <BT url={csvimport} name="CSV-Import" iconurl="https://publicfile.standard-cloud.com/icons/csv.png" />	

	</div>
        );
		}
}

function mapStateToProps({list}, ownProps) {
	return { list: list.find(f=>f.id === ownProps.match.params.listid)};
}

export default connect(mapStateToProps,{setNavigation})(ListSettingPage);
