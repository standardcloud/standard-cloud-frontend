import React from 'react';
import ListManager from '../../module/listmanager/stdlistmanager';
import BT from '../../component/button/stdbutton';
import dialog from '../../component/dialog/stddialog';
import Contentbox from '../../component/contentbox/contentbox';
import Helpbox from '../../component/helpbox/helpbox';
import {connect} from 'react-redux';
import {setNavigation} from '../../actions/navigation';
import Style from './listsettingsbasic.module.css';
import {loadLists} from '../../actions/list';
import {resetListObjects} from '../../actions/objects';
import Trans from '../../module/translatoinmanager/translationmanager';

class ListBasicSettings extends React.Component {    
      		constructor(props) {
			super(props);
			this.state ={list:this.props.list}
			this.saveBasicSettings = this.saveBasicSettings.bind(this);
			this.changeProp = this.changeProp.bind(this);
			this.clickback = this.clickback.bind(this);
			this.setupNavi = this.setupNavi.bind(this);
      }
	  componentDidMount() {
			this.setupNavi();
	}

setupNavi()
{
	var selflistsettings = this;
	const list = this.props.list;
	var url = "/list/" + list.id + "/";
	var url2 = "/list/" + list.id + "/edit/";
	var nav = [{title:"Start",url:"/"},{title:Trans.t("lists"),url:"/list/"},{title:list.name,url:url},{title:Trans.t("listsettings"),url:url2},{title:Trans.t("listbasicsettings")}];
	selflistsettings.props.setNavigation(nav,Trans.t("listbasicsettings"));
}

saveBasicSettings()
{
	var self = this;
	ListManager.setsettings(this.state.list).then(returndata=>
{
	if(returndata.state ==="OK")
	{
		dialog.okdialog("Basiseinstellungen", "erfolgreich gespeichert.");
	//	self.props.loadLists();
	//	console.log(returndata.content);
		self.props.resetListObjects();
		self.props.loadLists();
		self.setupNavi();
	}	
});
this.setState({changedvalue:false});

}

changeProp(propname,value)
{
	this.setState({changedvalue:true});
	let newlist = {...this.state.list, [propname]: value}
	this.setState({list:newlist});
}


clickback()
{
	var url = "/list/" + this.state.list.id + "/edit"
	this.props.history.push(url);
}

render () { 

	var savebutton = <BT disabled={true} name={Trans.t("save")} iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
	if(this.state.changedvalue)
	{
		savebutton = <BT name={Trans.t("save")} callbackfunction={this.saveBasicSettings} iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
	}

		return (
        <div>
			   <Contentbox title={Trans.t("listbasicsettings")}>
				   <div id="listbasicsettingsbox" className={Style.listbasicsettingsbox}>
					   <br/>
			<div id="listnamebox" className="listnamebox">
			    <div className="textboxgroup">      
				<input required className="stdtextbox" type="text" name= "tbListName" onChange={(e)=>this.changeProp("name",e.target.value)} value={this.state.list.name} />
				<span className="texthighlight"></span>
				<span className="textbar"></span>
				<label>{Trans.t("name")}</label>
			    </div>
			</div>
			<div id="listdescriptionbox">
			
			<div className="textboxgroup">      
				<textarea required name= "tbListDescription" className="stdtextbox" onChange={(e)=>this.changeProp("description",e.target.value)} value={this.state.list.description}></textarea>
				<span className="texthighlight"></span>
				<span className="textbar"></span>
				<label>{Trans.t("description")}</label>
			</div>
			</div>
			
		
          <br/> 
	              <div className="stdcheckboxregion">
                            <input id="analytics"  defaultChecked={this.state.list.geoanalytics} type="checkbox" value="enableanalytics" className="checkbox"/>
                            <label onClick={(e)=>this.changeProp("geoanalytics",!this.state.list.geoanalytics)}   htmlFor="analytics"><strong>Analytics</strong>  erlauben.</label>
                </div>

		        <div className="stdcheckboxregion">
                            <input id="enablecomment"  defaultChecked={this.state.list.comment} type="checkbox"  value="enablecomments" className="checkbox"/>
                            <label onClick={(e)=>this.changeProp("comment",!this.state.list.comment)}  htmlFor="enablecomment"><strong>Kommentare</strong> erlauben.</label>
                </div>
		        <div className="stdcheckboxregion">
                            <input id="enablefollow"  defaultChecked={this.state.list.follow} type="checkbox" value="enablefollow"   className="checkbox"/>
                            <label onClick={(e)=>this.changeProp("follow",!this.state.list.follow)}  htmlFor="enablefollow"><strong>Folgen-Funktion</strong> - Benutzer können einem einzelnen Objekt der Liste folgen.</label>
                </div>
				<div className="stdcheckboxregion">
                            <input id="confirmobjectremove"  defaultChecked={this.state.list.confirmobjectremove} type="checkbox" value="confirmobjectremove"   className="checkbox"/>
                            <label onClick={(e)=>this.changeProp("confirmobjectremove",!this.state.list.confirmobjectremove)}  htmlFor="confirmobjectremove"><strong>L&ouml;schen</strong> von {this.state.nameplural} muss vom Benutzer in einem Dialog best&auml;tigt werden.</label>
                </div>
				<div className="stdcheckboxregion">
                            <input id="enablemanualsort"  defaultChecked={this.state.list.manualsort} type="checkbox" value="enablemanualsort"   className="checkbox"/>
                            <label onClick={(e)=>this.changeProp("manualsort",!this.state.list.manualsort)}   htmlFor="enablemanualsort"><strong>Manuel Sort</strong> - Benutzer können die Zeilenreihenfolge bestimmen.</label>
                </div>
				<br/>    	
   
	<div id="listrenderlimitbox" className={Style.listrenderlimitbox}>
			<div className="textboxgroup">      
				<input required className="stdtextbox" type="number" step="1" pattern="\d+" name= "tbmaxobjectsonclient" onChange={(e)=>{this.changeProp("renderlimit",parseInt(e.target.value,10))}}  value={this.state.list.renderlimit} />
				<span className="texthighlight"></span>
				<span className="textbar"></span>
				<label>Renderinglimit</label>
			</div>
			</div>
				Das Clientrendering-Limit bistimmt die maximale Anzahl an Objekten, die zum Client/Browser gesendet werden. <br/> <br/>	
</div>
</Contentbox>
		      	
			 <br/>
			 <div id="listbasicsettingfooter" className={Style.listbasicsettingfooter}>
			 {savebutton}
			 <BT  callbackfunction={this.clickback} name={Trans.t("back")}   iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
			
			 <BT  name={Trans.t("help")} url="https://standard-cloud.zendesk.com/hc/de/articles/115002416889-Listeneinstellungen" iconurl="https://publicfile.standard-cloud.com/icons/help.png" />
			 </div>
			 <Helpbox title={Trans.t("help")}>
			Hier können Sie grundlegende Einstellungen vornehmen.
			</Helpbox>
			 </div>
        );

		}
}

function mapStateToProps({list}, ownProps) {
	return { list: list.find(l=>l.id ===ownProps.match.params.listid)};
}

export default connect(mapStateToProps,{setNavigation,loadLists,resetListObjects})(ListBasicSettings);

