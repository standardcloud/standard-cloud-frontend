import React,{useState,useEffect} from 'react';
import BT from '../../component/button/stdbutton';
import ObjectManager from '../../module/objectmanager/stdobjectmanager';
import {useDispatch,useSelector} from 'react-redux';
import {setNavigation} from '../../actions/navigation';
import Listview from '../../component/list/listview';
import Contenboxview from '../../component/boxlistview/boxlistview';
import DialogBox from '../../component/dialogbox/dialogbox';
import FollowBox from '../../component/follow/listfollow';
import {loadListObjects} from '../../actions/objects';
import Preloader from '../../component/preloader/preloader';
import ListFollowManager from '../../module/followlistmanager/stdfollowlistmanager';
import './list.css';
import { BACKEND } from '../../stdsetup';
import $ from 'jquery';
import Trans from '../../module/translatoinmanager/translationmanager';
import sort from 'jquery-ui-sortable-npm';
import Listinfobox from '../../component/list/listinfobox/listinfobox';
import Listviewbuttons from '../../component/list/listviewbuttons/listviewbuttons';

function Listpage(props) {
const listid = props.match.params.listid;
const dispatch = useDispatch()
const list = useSelector(state=>state.list).find(f=>f.id === listid);
const objecttype = useSelector(state=>state.objecttype).find(f=>f.id === list.objecttype);
const objectresult = useSelector(state=>state.object[props.match.params.listid]);
const [viewmode, setViewMode] = useState(0);
const [followdata, setFollowData] = useState({mod:false,add:false,del:false});
const [openfollowmodule, SetOpenfollowmodule] = useState(false);
const [meta, setMeta] = useState({});
const [loading, setLoading] = useState(false);
const [selectedobjects, setSelectedObjects] = useState([]);


function closemodule()
{
  SetOpenfollowmodule(false);
}
  /*wird nicht verwendet */
   

  function getfollowListdata()
  {    
       ListFollowManager.getfollowListdata(props.list.id).then(data =>
        {
          setFollowData(data);
        });
  }

	
useEffect(()=> {

                   if(list.id ==="99043cfa-9092-421b-8f34-566e1e6849fd")
                   {
                       props.history.push("/profile/");
                   }
function loadListData()
{
   const nav =[{title:"Start",url:"/"},{title:Trans.t("lists"),url:"/list/"}].concat(list.navigation);
   dispatch(setNavigation(nav,list.name));  
   dispatch(loadListObjects(list.id)); 
}
loadListData();
},[listid]);

function sortbuttonClick()
{
  $( "#tbllistdataresult tbody" ).css("cursor","grab");
  $( "#tbllistdataresult tbody" ).sortable( {
    update: function( event, ui ) {
      var firstid = ui.item[0].id; // move this object
      var nextid = $(ui.item[0]).next().attr("id");
      ObjectManager.sortObject(list.id,firstid,nextid).then(data=>
      {
       if(data.state ==="ok")
       {
         dispatch(loadListObjects(list.id));
       }
      });
    }
  });
}


function selectedObjectsChange(objectid)
{
                var arraycontainsobject = (this.state.selectedobjects.indexOf(objectid) > -1);       
                if(arraycontainsobject===false)
                {
                    var oldarray = this.state.selectedobjects;
                    oldarray.push(objectid);
                    this.setState({selectedobjects:oldarray});
                }else
                {
                    var i = this.state.selectedobjects.indexOf(objectid);
                    var oldarray2 = this.state.selectedobjects;
                    if(i !== -1) {
                        oldarray2.splice(i, 1);
                    }
                     this.setState({selectedobjects:oldarray2});
                }
             

}
/*
editmultipleObjectsClick()
{
window.location.href = "editmultiple/" + this.props.list.id + "/objects/"+ this.state.selectedobjects.toString().replace(',',';') + "/" ;
}
*/

function sortbyServer(parameter)
{
 // console.log("sortbyServer - true");
  setLoading(true);
  ObjectManager.getlistdatasorted(parameter.listid,parameter.fieldid,parameter.order,parameter.filter).then(data =>
		{
//    	setObjectresult(data.data);
console.log(data);
      setMeta(data.meta);
      setLoading(false);
		});
}

function boxviewClick()
{
  if(viewmode===1)
  {
      setViewMode(0);
  }else
  {
    setViewMode(1);
  }
}

function followbuttonClick()
{
  SetOpenfollowmodule(true);
}

function setFollowCount(data)
{
setFollowCount(data);
}

function updateResult(result)
{
//setObjectresult(result);
}

function removeObject(objectid)
{
var arrayresult = objectresult;
arrayresult = arrayresult.filter(item => item[item.length-1] !== objectid);
//setObjectresult(arrayresult);
}

function exportexcelbuttonClick()
{
  let excelexporturl = BACKEND + "/export/list/" + props.list.id +"/excel/";
  let anchor = document.createElement("a");
  var self = this;
  
const request = new Request(excelexporturl, {
  method: 'GET',credentials: "include"
});

  fetch(request)
      .then(response => response.blob())
      .then(blobby => {
          let objectUrl = window.URL.createObjectURL(blobby);
  
          anchor.href = objectUrl;
          anchor.download = props.list.name + "-export.xls";
          anchor.click();
  
          window.URL.revokeObjectURL(objectUrl);
      });
}

var followdialog = null;

var listviewhtml;
if(!objectresult)
{ 
  var text = "Lade " + objecttype.nameplural;
  listviewhtml = <div>{text}<Preloader/></div>;
}
else
{
  if(viewmode === 0)
  {
  listviewhtml = <Listview objectresult={objectresult.data} loading={loading} meta={objectresult.meta} list={list} />  
  }
  else
  {
    listviewhtml = <Contenboxview objectresult={objectresult.data} loading={loading} meta={objectresult.meta} list={list} />  
  }
}

if(openfollowmodule)
{
    followdialog= <DialogBox  closedialog={closemodule} title="Folgen">
    <FollowBox followdata={followdata} closemodule={closemodule} listsetting={list} />
    </DialogBox>
}
var followcount = 0;
if(followdata.mod)
followcount++;
if(followdata.del)
followcount++;
if(followdata.add)
followcount++;


console.log("loadd");
return(
        <div>
          {followdialog}
<div className="page-region-content tiles listpageregion" id="listpageregion">
            <div className="tile-group">
            <Listviewbuttons exportexcelbuttonClick={exportexcelbuttonClick} followbuttonClick={followbuttonClick} boxviewClick={boxviewClick} viewmode={viewmode} selectedobjects={selectedobjects} list={list} objecttype={objecttype} followcount={followcount}/>
<Listinfobox list={list} />
			</div>

         </div>
		 <div>
           <table id="header-fixed"></table>
           {listviewhtml}
      
		 </div>



</div>
    );
	
}	  
Listpage.whyDidYouRender = true
export default Listpage;