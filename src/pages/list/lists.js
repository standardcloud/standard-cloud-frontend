
import React,{useEffect,memo} from 'react';
import {useSelector,useDispatch} from 'react-redux';
import {setNavigation} from '../../actions/navigation';
import {loadLists} from '../../actions/list';
import ListOfList from '../../component/list/listoflists';
import Trans from '../../module/translatoinmanager/translationmanager';
function Listspage(props) {
     const dispatch = useDispatch();
     const list = useSelector(state => state.list);
     const objecttype = useSelector(state => state.objecttype);
     const listname = Trans.t("lists");
     const nav = [{title:"Start",url:"/"},{title:listname}];
     useEffect(()=>
     {
     dispatch(setNavigation(nav,"Listen"));
     dispatch(loadLists());
   },[]);
  if(!list)
   {
      return(<div>Lade Liste</div>);
   }
	return (
        <ListOfList small={false} data={list} objecttypes={objecttype}/>
        );
}
export default memo(Listspage);

