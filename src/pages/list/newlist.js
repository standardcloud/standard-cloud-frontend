import React from 'react';
import ListManager from '../../module/listmanager/stdlistmanager';
import StdButton from '../../component/button/stdbutton';
import StdObjecttypeselector from '../../component/objecttype/objecttypeselector/stdobjecttypeselector';
import StdDialog from '../../component/dialog/stddialog';
import HelpBox from '../../component/helpbox/helpbox';
import ContentBox from '../../component/contentbox/contentbox';
import {connect} from 'react-redux';
import {setNavigation} from '../../actions/navigation';
import Trans from '../../module/translatoinmanager/translationmanager';
import './newlist.css';

 class NewListPage extends React.Component {
	    constructor(props) {
    super(props);
		this.changeListName = this.changeListName.bind(this);
		this.changeListDescription = this.changeListDescription.bind(this);
		this.updateobjecttypeid = this.updateobjecttypeid.bind(this);
		this.clicknewlist = this.clicknewlist.bind(this);		
		var administration = Trans.t("admin");
		var newlist = Trans.t("newlist");
		this.state = {objecttypeid: "",name:"",description:"",objecttypelist:[]};
		var nav = [{title:"Start",url:"/"},{title:administration, url:"/admin/"},{title:newlist}];
		this.props.setNavigation(nav,newlist);
  }
			  
			  changeListName(event) {
         this.setState({name: event.target.value});             
                }
				
				changeListDescription(event) {
         this.setState({description: event.target.value});             
                }

		changeObjectTypeInput(event) {

         this.setState({searchtext: event.target.value});             
                }

clicknewlist()
{
  ListManager.newlist(this.state.name,this.state.description,this.state.objecttypeid,function(data)
  {
	  if(data.state ==='ok')
	  {
	  
			StdDialog.okdialog("Liste", "wurde erzeugt.");	
		setTimeout(function(){ window.location.href = '/list/'; }, 4000);
	  }else
	  {
			StdDialog.errordialog("Liste", "konnte nicht erzeugt werden.");	
	  }
	
	  
  }); 

}

updateobjecttypeid(objecttypeid)
{
	console.log("test123");
	 this.setState({objecttypeid: objecttypeid}); 
}
clickback()
{
window.history.back();
}

render() {

var savebutton = null;

if(this.state.name.length > 3 && this.state.description.length > 3 && this.state.objecttypeid.length > 3)
{
savebutton = <StdButton callbackfunction={this.clicknewlist} name={Trans.t("createlist")} iconurl="https://publicfile.standard-cloud.com/icons/save.png" />  
}else
{
	savebutton = <StdButton callbackfunction={this.clicknewlist} disabled={true} name={Trans.t("createlist")} iconurl="https://publicfile.standard-cloud.com/icons/save.png" />  
}
var name = Trans.t("name");
var description = Trans.t("description");
	return (
     <div id="newlist_page">
   <ContentBox title={Trans.t("newlist")}>
		 <div id="newlist_content">
        			<div id="listnamebox"><br/>
			    <div className="textboxgroup">      
				<input required className="stdtextbox" type = "text" name= "tbListName" onChange={this.changeListName} value={this.state.name} />
				<span className="texthighlight"></span>
				<span className="textbar"></span>
				<label>{name}</label>
			    </div>
			</div>
			<div id="listdescriptionbox">
			
			<div className="textboxgroup">      
				<textarea required name= "tbListDescription" className="stdtextbox" onChange={this.changeListDescription} value={this.state.description}></textarea>
				<span className="texthighlight"></span>
				<span className="textbar"></span>
				<label>{description}</label>
			</div>
			</div>
					<br/>
				<StdObjecttypeselector objecttypeupdate={this.updateobjecttypeid} />
   </div><br/>
        </ContentBox>
		<div>
<HelpBox title={Trans.t("help")}>
	Bestimmen Sie einen <strong>aussagekr&auml;ftigen Namen</strong> und eine kurze (meist sichwortartige) <strong>Beschreibung</strong> (Was wird in dieser Liste abgelegt?).<br/>Anhand von Name und Beschreibung sollte der Anwender wissen, was in der Liste enthalten ist.<br/><strong>Tipp:</strong> Wenn Sie sich beim Namen nicht sicher sind, dann schreiben Sie die alternativen Namen in die Beschreibung.<br/><br/>Sollte keine passende <strong>Listenvorlage</strong> vorhanden sein, können Sie diese ggf. selbst unter <br/>(Administration->Listenvorlagen)<br/> erzeugen. Gerne helfen wir Ihnen per Telefon.

</HelpBox>
       
<div id="newlistfooter">
       {savebutton} 
			 <StdButton  callbackfunction={this.clickback} name={Trans.t("back")}   iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
			 <StdButton url="https://standard-cloud.zendesk.com/hc/de/articles/202566021-Erstellen-einer-Liste"  name={Trans.t("help")}  iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
      </div>
				</div>
	</div>
    );
	
}	  
}
export default connect(null,{setNavigation})(NewListPage);