
import React from 'react';
import {connect} from 'react-redux';
import {setNavigation} from '../../actions/navigation';
import {loadLists} from '../../actions/list';
import ListOfList from '../../component/list/listoflists';
class Listpage extends React.Component {
    
      constructor(props) {
            super(props);
            var nav = [{title:"Start",url:"/"},{title:"Meine Listen"}];
            this.props.setNavigation(nav, "Meine Listen");
            this.props.loadLists();
      }
    
render() {
	return (
        <ListOfList small={false} data={this.props.list.filter(l=>l.owner ===this.props.user.userid)} objecttypes={this.props.objecttypes}/>
        );
}
}
function mapStateToProps(state) {
    return { list: state.list,objecttypes:state.objecttypes,user:state.user};
  }
  export default connect(mapStateToProps,{setNavigation,loadLists})(Listpage);

