import React from 'react';
import BT from '../../component/button/stdbutton';
import {connect} from 'react-redux';
import Helpbox from './../../component/helpbox/helpbox';
import dialog from './../../component/dialog/stddialog';
import Contentbox from './../../component/contentbox/contentbox';
import Permissionmanager from '../../module/permissionmanager/stdpermissionmanager';
import AddKey from '../../component/permission/addkey/addkey';
import AddRight from '../../component/permission/addright/addright';
import Trans from '../../module/translatoinmanager/translationmanager';
import {setNavigation} from '../../actions/navigation';
import './list_permission.css';

  class ListPermission extends React.Component {
	    constructor(props) {
		super(props);
		this.addkey = this.addkey.bind(this);
		this.addright = this.addright.bind(this);
		this.addlock = this.addlock.bind(this);
		this.savelistpermission = this.savelistpermission.bind(this);
		this.saveobjectpermission = this.saveobjectpermission.bind(this);
		this.changeenablefieldpermission = this.changeenablefieldpermission.bind(this);
		this.changePermissionField = this.changePermissionField.bind(this);
		this.changePermissionFieldRight = this.changePermissionFieldRight.bind(this);
	    this.state = {updateobjectsettings:false,addlock:false,permission:{objecttype:{id:"",name:""},list:{name:"",id:"",fieldpermission:false},permissions:[],listpermissionrights:[],objectpermissionrights:[],fields:[]},addkey:{name:"",id:""},addright:{name:"",id:""}};
	}

componentDidMount() {
this.loadlistpermissions();
}
addlock()
{
	this.setState({addlock:true});
	this.setState({addkey:{name:"",id:""}});
	this.setState({addright:{name:"",id:""}});
}
changeenablefieldpermission()
{
	var oldstate = this.state.permission;
	oldstate.list.fieldpermission = ! this.state.permission.list.fieldpermission;
	this.setState({permission:oldstate});
	this.setState({updateobjectsettings:true});
}
loadlistpermissions()
{
		var self = this;
		var test = window.location.href;
		var testRE = test.match("list/(.*)/edit");
		var listid = testRE[1];	
		Permissionmanager.getlistpermission(listid).then(data =>
		{
			if(data.state==="ok")
			{
				self.setState({permission:data.content});
			}else
			{
				dialog.error("Berechtigungen","konnten nicht geladen werden.");
			}
			var listurl = "/list/" + self.props.match.params.listid + "/";
			var listsetting =  "/list/" + self.props.match.params.listid + "/edit/";
			var nav = [{title:"Start",url:"/"},{title:Trans.t("lists"),url:"/list/"},{title:self.props.list.name,url:listurl},{title:Trans.t("listsettings"),url:listsetting},{title:Trans.t("permissions")}];
	    	  self.props.setNavigation(nav,Trans.t("permissions"));
		});
}


clickback()
{
window.history.back();
}
saveobjectpermission()
{
	var self = this;
	var data = this.state.permission;
	Permissionmanager.setobjectpermission(data.list.id,data.list.fieldpermission,data.list.permissionfieldid,data.list.fieldpermissionrightid).then(result =>
	{
	if(result.state==="ok")
		{
			self.setState({updateobjectsettings:true});
			dialog.ok("Objektberechtigungen","erfolgreich gespeichert.");
		}else
		{
			dialog.error("Objektberechtigungen","konnten nicht gespeichert werden.");
		}
	});
}
savelistpermission()
{
	var self = this;
	if(this.state.addkey.id.length > 2 && this.state.addright.id.length > 2)
	{		
		var test = window.location.href;
		var testRE = test.match("list/(.*)/edit");
		var listid = testRE[1];
		Permissionmanager.addlistkey(self.state.addkey.id,self.state.addright.id,listid).then(data =>
			{
				if(data.state==="ok")
					{
						dialog.ok("Schloss","erfolgreich gespeichert.");
						self.loadlistpermissions();
						self.setState({addlock:false});
					}else
					{
						dialog.error("Schloss","konnte nicht hinzugefügt werden. Schlüssel in Liste bereits vorhanden?")
						self.setState({addlock:false});
					}
			});
	}
}
addkey(key)

{
	this.setState({addkey:key});
	var self= this;
	setTimeout(function(){ self.savelistpermission(); }, 500);
}
addright(right)
{
	this.setState({addright:right});
	var self= this;
	setTimeout(function(){ self.savelistpermission(); }, 500);

}
changePermissionField(e)
{
	var oldstate = this.state.permission;
	oldstate.list.permissionfieldid = e.target.value;
	this.setState({permission:oldstate});
	this.setState({updateobjectsettings:true});
}

showkey(permission)
{
	window.location.href = "/admin/permissionmgmt/key/" + permission.keyid + "/edit/";
}
changePermissionFieldRight(e)
{
	var oldstate = this.state.permission;
	oldstate.list.fieldpermissionrightid = e.target.value;
	console.log(oldstate);
	this.setState({permission:oldstate});
	this.setState({updateobjectsettings:true});
}
removepermission(permissionobj)
{
var self = this;
Permissionmanager.removelistkey(permissionobj.keyid,permissionobj.objectid).then(data =>
			{				
			if(data.state==="ok")
					{
						var oldst = self.state.permission;
						var newpermissionlist = [];
						for(let i=0;i<self.state.permission.permissions.length;i++)
							{
								var ob = self.state.permission.permissions[i];
								
	
								if(ob.keyid === permissionobj.keyid)
									{
										
									}else
									{
										newpermissionlist.push(ob);
									}

							}
							oldst.permissions = newpermissionlist;
							self.setState({permission:oldst});
						   dialog.ok("Schloss","erfolgreich entfernt.")
					}
					else
						{
							dialog.error("Schloss","konnte nicht entfernt werden.");
						}
			});
}

render() {
var self = this;
var urlobjecttypeurl ="/admin/objecttype/modify/" + this.state.permission.objecttype.id +"/edit/permission/";

var objecttypebutton  = "Berechtigungen der Listenvorlage (" + this.state.permission.objecttype.name + ")"
var rightrows = this.state.permission.permissions.map(function(permission)
{
	return(<tr><td className="permissionkeytd" onClick={()=>self.showkey(permission)} >{permission.keyname}</td><td dangerouslySetInnerHTML={{__html: permission.rightdescription}}></td><td className="deletepermissonobjecttypetd"><img onClick={()=>self.removepermission(permission)} className="deletepermissonobjecttypeimg" alt="Berechtigung löschen" src="https://publicfile.standard-cloud.com/icons/delete.png"/></td></tr>)
});


var addcontrol = null;
var addlockbutton = null;
if(this.state.addlock)
	{
addcontrol = 	<Contentbox title="Neues Schloss erstellen">
	<table id="objecttypepermissiontbl">
		<thead>
		<tr><th>{Trans.t("key")}</th><th>{Trans.t("right")}</th><th></th></tr>
		</thead><tbody>
	<tr><td dangerouslySetInnerHTML={{__html:self.state.addkey.name}} ></td><td dangerouslySetInnerHTML={{__html:self.state.addright.right}}></td><td></td></tr>
	<tr><td><AddKey addkey={self.addkey} /></td><td><AddRight rights={self.state.permission.listpermissionrights} addright={self.addright} /></td><td></td></tr>
	</tbody>
	</table>
    </Contentbox>
}else
{
	addlockbutton =<BT  callbackfunction={this.addlock} name="Neues Schloss erstellen"   iconurl="https://publicfile.standard-cloud.com/icons/lock.png" />
}


var rights = self.state.permission.objectpermissionrights.map(function(right) {
	return (
	<option key={right.id} value={right.id} dangerouslySetInnerHTML={{__html:right.name}}>
	</option>
	)
});

var options = self.state.permission.fields.map(function(field) {
	return (
	<option key={field.id} value={field.id}>
			{field.name}
	</option>
	)
});


 var boxtitle= "Berechtigungen für Liste " + this.state.permission.list.name;
	return (
     <div id="objecttypepermission_page">
   <Contentbox title={boxtitle}>
		 <div id="newlist_content">
  	<Contentbox  title="Berechtigungen / Schlösser">
	<table id="permissiontable">
		<thead>
		<tr><th>{Trans.t("key")}</th><th>{Trans.t("right")}</th><th></th></tr>
		</thead>
		<tbody>			
	{rightrows}
	</tbody>
	</table><br/>
	{addcontrol}
	<div className="listpermissionfooter">	
	{addlockbutton}
	</div>
	</Contentbox>

	<Contentbox  title="Berechtigungen auf Objektebene">
		<br/>
		<div>
			<div className="stdcheckboxregion">
				<input id="confirmfiedpermission" value="confimenablefieldpremission"  checked={this.state.permission.list.fieldpermission} type="checkbox"   className="checkbox"/>
				<label onClick={this.changeenablefieldpermission}  for="confirmobjectremove"><strong>Objekt-Berechtigungen</strong> aktivieren.</label>
			</div>
</div>
<br/>
<div>
<table id="listpermissionobjectrighttbl">
	<thead>
	<tr>
		<th>{Trans.t("field")}</th><th>{Trans.t("right")}</th>
	</tr>
	</thead>
	<tbody>
		<td>
	<select value={this.state.permission.list.permissionfieldid} onChange={this.changePermissionField}>
				<option  value="">                    
                </option>
                {options}
            </select>
		</td><td>
		<select value={this.state.permission.list.fieldpermissionrightid} onChange={this.changePermissionFieldRight}>
				<option  value="">                    
                </option>
                {rights}
            </select>
			</td>
	</tbody>
	</table>
</div>
<div className="listpermissionfooter">			
	<BT  disabled={!this.state.updateobjectsettings} callbackfunction={this.saveobjectpermission} name={Trans.t("save")}   iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
	</div>
	</Contentbox>	

   </div>
        </Contentbox>
		<div>
<Helpbox title={Trans.t("help")}>
Listenberechtigungen..
</Helpbox>
       
<div id="newlistfooter">
			<BT  callbackfunction={this.clickback} name={Trans.t("back")}    iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
			
			<BT  url="/admin/permissionmgmt/key/" name={Trans.t("keylist")}  iconurl="https://publicfile.standard-cloud.com/icons/key.png" />
			<BT  url={urlobjecttypeurl} name={objecttypebutton}  iconurl="https://publicfile.standard-cloud.com/icons/objecttype.png" />
			<BT url="https://standard-cloud.zendesk.com/hc/de/articles/202566021-Erstellen-einer-Liste"  name={Trans.t("help")}  iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
      </div>
				</div>
	</div>
    );
	
}	  
  }

  function mapStateToProps({list}, ownProps) {
	return { list: list.find( l=> l.id ===ownProps.match.params.listid)};
}
export default connect(mapStateToProps,{setNavigation})(ListPermission);

