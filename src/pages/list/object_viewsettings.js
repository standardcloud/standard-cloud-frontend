import React from 'react';
import BT from '../../component/button/stdbutton';
import {connect} from 'react-redux';
import dialog from '../../component/dialog/stddialog';
import listmanager from '../../module/listmanager/stdlistmanager';
import {setNavigation} from '../../actions/navigation'
import Trans from '../../module/translatoinmanager/translationmanager';
import './object_viewsettings.css';

class ObjectViewSettings extends React.Component {
    constructor(props) {
      super(props);
      this.changeMeeting = this.changeMeeting.bind(this);
      this.changeVersionButton = this.changeVersionButton.bind(this);
      this.changeConnection = this.changeConnection.bind(this);
      this.saveViewSettingsPage = this.saveViewSettingsPage.bind(this);
      this.handleViewChange = this.handleViewChange.bind(this);
      this.handleOpeninEditChange = this.handleOpeninEditChange.bind(this);     
      
      this.state =  {enablemeeting:false,showversionbutton:false,changedvalue:false,newlistviewurl:"",helpurl:"https://standard-cloud.zendesk.com/hc/de/articles/115001738509-Listeneinstellungen-Ansichten",views:[],list:this.props.list};
      var listurl = "/list/" + this.props.list.id + "/";
      var listsetting =  "/list/" + this.props.list.id + "/edit/";
      var urldarstellung =  "/list/" + this.props.list.id + "/edit/view/";
      var nav = [{title:"Start",url:"/"},{title:Trans.t("lists"), url:"/List/"},{title:this.props.list.name, url:listurl},{title:Trans.t("listsettings"),url:listsetting},{title:Trans.t("display"),url:urldarstellung},{title:Trans.t("objectview")}];
      this.props.setNavigation(nav,Trans.t("objectview"));
    }		
          

changeMeeting(e)
{

this.setState({changedvalue:true});
let newlist = {...this.state.list,meeting:!this.state.list.meeting};
this.setState({list:newlist});
}
changeVersionButton(e)
{
    this.setState({changedvalue:true});
    let newlist = {...this.state.list,version:!this.state.list.version};
    this.setState({list:newlist});
}

changeConnection(e)
{
    this.setState({changedvalue:true});
    let newlist = {...this.state.list,connection:!this.state.list.connection};
    this.setState({list:newlist});
}

saveViewSettingsPage()
{
    var self = this;
      this.setState({changedvalue:false});
        listmanager.setsettings(self.state.list).then(returndata =>
        {
           
            if(returndata.state==="OK")
            {
             dialog.okdialog("Objekt-Ansicht", returndata.msg);
            }else
            {
              dialog.errordialog("Objekt-Ansicht", returndata.msg);  
            }
        });   
}
handleViewChange(e)
{
      this.setState({changedvalue:true});
var handleself = this;
    switch(e.target.name)
    {
        case "newview":
        handleself.setState({newviewid:e.target.value})
        break;
        case "readview":
        handleself.setState({readviewid:e.target.value})
        break;
       case "editview":
        handleself.setState({editviewid:e.target.value})
        break;
        default:
        break;

    }

}
handleOpeninEditChange(e)
{
    this.setState({changedvalue:true});
    let newlist = {...this.state.list,openeditmode:!this.state.list.openeditmode};
    this.setState({list:newlist});
}
render () { 
    var selfrender = this;
    var count =0;
     var renderviewtablerows= this.state.views.map(function(view){	
         count++;
        var newchcked=false;
        var readchecked =false;
        var editchecked =false;
        if(view.id === selfrender.state.newviewid)
        {
            newchcked = true;
        }
        if(view.id === selfrender.state.readviewid)
        {
            readchecked = true;
        }
        if(view.id === selfrender.state.editviewid)
        {
            editchecked = true;
        }
        var viewediturl ="";
        if(view.id!=="")
        {
        viewediturl ="/admin/objecttype/modify/" + selfrender.state.objecttypeid+ "/edit/view/" + view.id +"/"
    }
    var readid = count + "read";
    var newid = count + "new";
    var editid = count + "edit";
      return (	  
			<tr key={view.id}><td>            
                            <div className="stdradioboxregion" >
                                <input name="newview"  onChange={selfrender.handleViewChange} id={newid}  type="radio" className="radio" value={view.id}  checked={newchcked}/>
                                <label htmlFor={newid} ></label>
                           </div>
                </td><td>
                    <div className="stdradioboxregion" >
                                <input name="readview"  onChange={selfrender.handleViewChange} id={readid}  type="radio" className="radio" value={view.id}  checked={readchecked}/>
                                <label htmlFor={readid} ></label>
                           </div>                                 
                    
                    </td><td>
                        <div className="stdradioboxregion" >
                                <input name="editview"  onChange={selfrender.handleViewChange} id={editid}  type="radio" className="radio" value={view.id}  checked={editchecked}/>
                                <label htmlFor={editid} ></label>
                           </div>
                        
                      </td><td><a href={viewediturl}>{view.name}</a></td><td>{view.description}</td></tr>
				);
     });     
     	var savebutton = <BT disabled={true} name={Trans.t("save")}  iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
	if(this.state.changedvalue)
	{
		savebutton = <BT name={Trans.t("save")}  callbackfunction={this.saveViewSettingsPage} iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
	}


		return (
<div id="listsettingcountainer">
            <div id="listsettingcountainerdefaultviewbox">
                   <div className="stdcard stdcard-1">
            <h1><img alt="Objekt-Ansicht" src="https://publicfile.standard-cloud.com/icons/editobjectview.png"/> Objekt-Ansicht</h1>
             <div>Welche Ansicht soll bei der Neuanlage, dem Darstellen (mit Link zum Edit-Formular) und zum Editieren verwendet werden?<br/><br/>
            <table id="tbllistviews"><thead><tr><th>Neuanlage</th><th>Lese-Ansicht</th><th>Edit-Ansicht</th><th>{Trans.t("name")}</th><th>{Trans.t("description")}</th></tr></thead><tbody>
                {renderviewtablerows}
                </tbody>
                </table>
            </div>
            <br/>
    				  <div className="stdcheckboxregion">
                            <input id="openineditmode"  defaultChecked={this.state.list.openeditmode} type="checkbox" value="enableanalytics"   className="checkbox"/>
                            <label onClick={this.handleOpeninEditChange}  htmlFor="openineditmode"><strong>Seite direkt im Edit-Modus &ouml;ffnen.</strong> Sollte der Benutzer kein Schreibe-Recht haben, wird die Lese-Ansicht dargestellt.</label>
     	             </div>  
                	<div className="stdcheckboxregion">
                            <input id="enableconnection"  defaultChecked={this.state.list.connection} type="checkbox" value="enableconnection"   className="checkbox"/>
                            <label onClick={this.changeConnection}  htmlFor="enableconnection"><strong>Button "Verbindungen"</strong> anzeigen.</label>
     	             </div>
                    <div className="stdcheckboxregion">
                            <input id="versionenabled"  defaultChecked={this.state.list.version} type="checkbox"   className="checkbox"/>
                            <label onClick={this.changeVersionButton}  htmlFor="versionenabled"><strong>Button "Versionen"</strong> anzeigen.</label>
     	             </div>     
                     <div className="stdcheckboxregion">
                            <input id="enablemeeting"  defaultChecked={this.state.list.meeting} type="checkbox" value="enablemeeting"   className="checkbox"/>
                            <label onClick={this.changeMeeting}  htmlFor="enablemeeting"><strong>Meeting</strong> Schaltfläche anzeigen.</label>
     	             </div>                            
            </div>
                       
            <div id="savebuttonsettings">
           {savebutton}
            <BT name={Trans.t("newview")} url={this.state.newlistviewurl} iconurl="https://publicfile.standard-cloud.com/icons/view.png" />  
            <BT name={Trans.t("help")} url={this.state.helpurl} iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
            </div>
            </div>
</div>
        );

		}


}  

function mapStateToProps({ list}, ownProps) {
    return { list: list.find(l=>l.id ===ownProps.match.params.listid)};
  }


  export default connect(mapStateToProps,{setNavigation})(ObjectViewSettings);


