import React from 'react';
import { withRouter } from 'react-router-dom';
import BT from '../../component/button/stdbutton';
import {connect} from 'react-redux';
import {setNavigation} from '../../actions/navigation';
import Dialog from '../../component/dialog/stddialog';
import Helpbox from '../../component/helpbox/helpbox';
import Contentbox from '../../component/contentbox/contentbox';
import Administration from '../../module/administration/stdadministration';

class PasswordPage extends React.Component {
	    constructor(props) {
		super(props);
		this.state = {oldpassword:"",newpassword1:"",newpassword2:""};
		this.changeoldpassword = this.changeoldpassword.bind(this);
		this.changepassword1 = this.changepassword1.bind(this);
		this.changepassword2 = this.changepassword2.bind(this);
		this.clickchangepassword = this.clickchangepassword.bind(this);	
		
		var username = this.props.currentuser.firstname + " " + this.props.currentuser.lastname;
		var url = "/profile/" +this.props.currentuser.userid + "/";
		var nav = [{title:"Start",url:"/"},{title:"Benutzer", url:"/profile/"},{title:username, url:url},{title:"Passwort ändern"}];
		this.props.setNavigation(nav,"Passwort ändern");		
  }
			  




changeoldpassword(e)
{
this.setState({oldpassword:e.target.value});
}

changepassword1(e)
{
this.setState({newpassword1:e.target.value});
}
changepassword2(e)
{
this.setState({newpassword2:e.target.value});
}


clickchangepassword()
{
	var self = this;
	Administration.updatepassword(this.state.oldpassword,this.state.newpassword1).then(data=>
{
	if(data.state==="ok")
		{
			Dialog.ok("OK","Passwort wurde aktualisiert.");
			self.setState({oldpassword:""});
			self.setState({newpassword1:""});
			self.setState({newpassword2:""});
			self.props.history.push("/");

		}else
		{
			Dialog.error("Passwort","konnnte leider nicht aktualisiert werden.");
		}
});
	
}


clickback()
{
// window.history.back();
}

render() {

	var enabled = true;
	if(this.state.newpassword1 === this.state.newpassword2 && this.state.newpassword1.length > 0 && this.state.oldpassword.length > 0)
		{
		enabled = false;
		}

	return (
     <div id="passwordreset_page">
   <Contentbox title="Passwort ändern">
		 <div id="passwordreset_content">
			 <br/>

		<table id="tblpassword">
		<tbody>
		<tr><td className="passwordchangecol">Aktuelles Passwort:</td><td>
		<input type="password" value={this.state.oldpassword} onChange={this.changeoldpassword}/></td></tr><tr>
		<td className="passwordchangecol">Neues Passwort:</td><td><input type="password" value={this.state.newpassword1} onChange={this.changepassword1}/></td></tr>
		<tr><td className="passwordchangecol">Neues Passwort:<br/>(Widerholung)</td><td><input type="password" value={this.state.newpassword2} onChange={this.changepassword2}/></td></tr></tbody></table>

	    </div><br/>
        </Contentbox>
		<div>
<Helpbox title="Hilfe">
Sie sollten in regelmäßigen Abständen Ihr Passwort ändern und darauf achten, dass dieses Passwort nur in Standard-Cloud verwendet wird.
Passwörter werden verschlüsselt und können nicht wiederhergestellt werden.<br/><br/>Sollen Sie Ihr Passwort vergessen haben, müssen Sie beim Administrator um ein neues bitten.<br/><br/>Um Tippfehler zu vermeiden, muss das neue Passwort wiederholt werden. Erst dann ist er Speichern-Button aktiv.
</Helpbox>
       
<div id="newlistfooter">
		     <BT  callbackfunction={this.clickchangepassword} name="Passwort ändern" disabled={enabled}  iconurl="https://publicfile.standard-cloud.com/icons/save.png" />
			 <BT  callbackfunction={this.clickback} name="Zurück"   iconurl="https://publicfile.standard-cloud.com/icons/return.png" />
		     <BT reload url="https://standard-cloud.zendesk.com/hc/de/articles/115000611651-Papierkorb"  name="Hilfe" iconurl="https://publicfile.standard-cloud.com/icons/help.png" />  
      </div>
				</div>
	</div>
    );
	
}	  
  }
    
  function mapStateToProps({user}, ownProps) {
    return { currentuser:user};
  }

  
  
  export default withRouter(connect(mapStateToProps,{setNavigation})(PasswordPage));


