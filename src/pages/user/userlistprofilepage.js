import React from 'react';
import { withRouter } from 'react-router-dom';
import {connect} from 'react-redux';
import {setNavigation} from '../../actions/navigation';

import {loadUsers} from '../../actions/users';
import DisplayObject from '../../module/displayobject/stddisplayobject';
import {BACKEND} from '../../stdsetup';
import style from './userlistprofilepage.module.css';

class UserListProfile  extends React.Component {   
	constructor(props) {
     super(props);
    this.handleSearchTextChange = this.handleSearchTextChange.bind(this);
    this.handleUserClick = this.handleUserClick.bind(this);

	this.state = {searchtext:""};
	var nav = [{title:"Start",url:"/"},{title:"Benutzer"}];
    this.props.setNavigation(nav,"Benutzer");
	  }
  
componentDidMount() {
    if(this.props.users.length===0)
    {
      this.props.loadUsers();
    }
}
              
handleSearchTextChange(event)
{
 this.setState({searchtext: event.target.value.toLowerCase()});             
}




handleUserClick(userid)
{
    this.props.history.push("/profile/" + userid + "/");
}


render () {
 var selfrender = this;

 if(this.props.users.length===0)
 return(<div>Lade Benutzer</div>);


var results = this.props.users.map(function(user) {
            
    if(selfrender.state.searchtext !== "")
    {
        
        if(user.name.toLowerCase().indexOf(selfrender.state.searchtext)> -1 || user.profiledata.toString().toLowerCase().indexOf(selfrender.state.searchtext) > -1)
        {
            
        }else
        {
           return null;
        }          
    }


       
var imgurl = BACKEND + "/file/" + user.img;
if(user.img.length===0)
{
    imgurl = "https://publicfile.standard-cloud.com/icons/anonymususer.jpg";
}
var display = DisplayObject.profilelistdata(user.profiledata);

                return (
                <div className={style.profilelistuserbox} key={user.userid} onClick={() => selfrender.handleUserClick(user.userid)}>
                    <div dangerouslySetInnerHTML={{__html: user.name}} className={style.headeruserobjectbox}></div>            
                    <div>
                    <img alt={user.name} align="left" src={imgurl}/><div className={style.userlistprofilepropertiesbox} dangerouslySetInnerHTML={{__html: display}}></div>
                    </div>
                </div>
			 );  
                  
               });

             
	return (
        <div>
   <input id={style.usersearchbox} value={this.state.searchtext}  onChange={this.handleSearchTextChange}  placeholder="Personensuche.." type="search" autoComplete="off"/><br/><br/>       
<div className={style.userlist}>
{results}
</div>
</div>
    );
}
}	  
function mapStateToProps({users}, ownProps) {
    return { users:users};
  }

export default withRouter(connect(mapStateToProps,{setNavigation,loadUsers})(UserListProfile));
