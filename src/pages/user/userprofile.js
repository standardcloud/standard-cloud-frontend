import React from 'react';
import { withRouter } from 'react-router-dom';
import {connect} from 'react-redux';
import {setNavigation} from '../../actions/navigation';
import Contentbox from '../../component/contentbox/contentbox';
import {BACKEND} from '../../stdsetup';
import {loadUsers} from '../../actions/users';
import ShowReadField from '../../component/fieldtype/renderread';
import BT from '../../component/button/stdbutton';
import FollowUserDialog from '../../component/follow/userfollow';
import Style from './userprofile.module.css';
import DialogBox from '../../component/dialogbox/dialogbox';
import Usermanager from '../../module/usermanager/stdusermanager';

class Userprofile  extends React.Component {    
  constructor(props) {
    super(props);
    var nav =[];
    var self = this;
    this.closefollow = this.closefollow.bind(this);
    if(!this.props.profileuser)
    {
        this.props.loadUsers();
        nav = [{title:"Start",url:"/"},{title:"Benutzer",url:"/profile/"},{title:"-"}];
        this.props.setNavigation(nav, "Benutzer");
    }else
    {
        nav = [{title:"Start",url:"/"},{title:"Benutzer",url:"/profile/"},{title:this.props.profileuser.name}];
        this.props.setNavigation(nav, this.props.profileuser.name);
        self.getfollowUserdata();
    }
    this.state = {openfollowmodule:false,followdata:{mod:false,add:false,del:false,profilecomment:false,share:false,person:false}};
  }


  closefollow()
  {
      this.setState({openfollowmodule:!this.state.openfollowmodule});
  }


  getfollowUserdata(userid)
  {
      var self = this;
    Usermanager.getfollowUserdata(self.props.profileuser.id).then(data =>
        {
            self.setState({followdata:data});
        });
  }

      render() {  
          var selfrender = this;
          if(!this.props.profileuser)
            return(<div>Lade Benutzer</div>);

var imagefield = null;
var profiletable = this.props.profileuser.profiledata.fields.map(function(datafield,index)
{
   
    var field = selfrender.props.fields.find(f=> f.id === datafield.fieldid);
    
switch(field.id)
{
    case "20b62a3a-2545-4dab-a5c4-b60badd589a8":
    case "3b825480-8d07-4677-a097-29ad90a753db":
    case "5be2d63b-b05a-421e-957b-61b288061956":
    case "26dfa998-2e2b-4abf-9f59-4a5d8853fd20":
    case "46e9b922-3add-4ce7-a030-ef550541c4a8":
    case "0cecad40-c0a1-46e3-a7a3-4bd99b7236d6":
    case "77f42b3d-8e2c-4869-bd67-6273afe7cf89":
    case "bd46f202-e2be-4c79-a7a0-025c7ddbb835":
    case "6dacefca-1e0d-4bdd-9c82-fe89b517242b":
    case "a11a1a60-0ce4-4a7a-a594-dcf5b20aeda8":
    case "1d57184b-430b-44c8-9708-d3e3dd8bcce4":
    case "245adad1-8745-4f5a-8a17-eb72f57a8dfe":
    case "76f26a5d-06a9-45e1-903e-bbf21a381835":
    case "7628c3c9-6e30-4b1a-8f74-cc8ae8366d0c":
    return null;
    case "c8e7d134-1005-4756-b851-828767c84e8e":
    imagefield = selfrender.props.profileuser.profiledata.fields[index][field.fieldtypeid];
    return null;
    default:
}
   
   var fieldvalue = selfrender.props.profileuser.profiledata.fields[index][field.fieldtypeid];
if(fieldvalue ==="")
return null;

return(<div className="fieldbox" key={datafield.fieldid}><span className="fieldname">{field.name}: </span><span>
    <ShowReadField field={field} value={fieldvalue} />
    </span></div>);
});


var imageurl = BACKEND + "/file/" + imagefield.filepath;
var changepasswordurl = "/profile/" + this.props.profileuser.id + "/changepassword/";
var changepasswordbutton = null;  
var editprofilebutton = null;
var followdialog = null;
var followbutton = null;

if(this.props.currentuser.userid===this.props.profileuser.id)
{
    changepasswordbutton = <BT url={changepasswordurl}  iconurl="https://publicfile.standard-cloud.com/icons/password.png"  name="Mein Passwort ändern" />
    var objecteditoid = this.props.currentuser.img.substring(37,73);
    var editprofileurl = "/list/99043cfa-9092-421b-8f34-566e1e6849fd/edit/" + objecteditoid + "/editmode/";
    editprofilebutton = <BT url={editprofileurl}  iconurl="https://publicfile.standard-cloud.com/icons/edituser.png"  name="Profil bearbeiten" />
}else
{
    var followcount = 0;
if(this.state.followdata.mod)
followcount++;
if(this.state.followdata.del)
followcount++;
if(this.state.followdata.add)
followcount++;
if(this.state.followdata.share)
followcount++;
if(this.state.followdata.person)
followcount++;
if(this.state.followdata.profilecomment)
followcount++;

    followbutton = <BT url={editprofileurl} count={followcount} callbackfunction={this.closefollow} iconurl="https://publicfile.standard-cloud.com/icons/follow.png"    name="Folgen" />
}

if(this.state.openfollowmodule)
{
      followdialog= <DialogBox  closedialog={this.closefollow} title="Folgen">
    <FollowUserDialog followdata={this.state.followdata} closemodule={this.closefollow} user={this.props.profileuser} />
    </DialogBox>
}

return (
            <div>
        {followdialog}
        <Contentbox title={this.props.profileuser.name}>
     <img src={imageurl} alt={this.props.profileuser.name}/>
     <div className={Style.userfieldbox}>
        {profiletable}
        </div>

        </Contentbox>
        <div className={Style.changepasswordfooter}>
        {changepasswordbutton}
        {editprofilebutton}
        {followbutton}
     </div>
    
         </div>
        );
      }
    }


function mapStateToProps({users,field,user}, ownProps) {
    return { profileuser: users.find(u=> u.id === ownProps.match.params.id),fields:field,currentuser:user};
  }

export default withRouter(connect(mapStateToProps,{setNavigation,loadUsers})(Userprofile));

