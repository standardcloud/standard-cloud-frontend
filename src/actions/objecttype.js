import {SET_OBJECTTYPES} from './actiontypes';
import Objecttypemanager from '../module/objecttypemanager/stdobjecttypemanager';

export function loadObjecttypes() {
    return (dispatch)=>
    {
        Objecttypemanager.getobjecttypes().then(data=>
         {		
            dispatch({type:SET_OBJECTTYPES, payload:data.content});       
         });	
    };
}