import {SET_OBJECTS,SETMORE_OBJECTS,RESET_OBJECTS} from './actiontypes';
import ObjectManager from '../module/objectmanager/stdobjectmanager';

export function loadListObjects(ListID) {
    return (dispatch)=>
    {
        ObjectManager.getlistdata(ListID).then(data=>{        
            dispatch({type:SET_OBJECTS, payload:data,listid:ListID});    
        });      
    };
}


export function resetListObjects() {
    return (dispatch)=>
    {
            dispatch({type:RESET_OBJECTS});           
    };
}


export function loadMore(result) {
    return (dispatch)=>
    {
       if(result.totalobjects === result.objectcount)
       {
         return;
       }
      
              ObjectManager.getmorelistdata(result.cacheid,result.listid,result.objectcount).then(data=>
              {
                dispatch({type:SETMORE_OBJECTS, payload:data,listid:result.listid});                       
              });
    };
}

export function filterbyServer(listid,filter) {
    return (dispatch)=>
    {
    console.log("filterbyServer - true");
  //if(filterterm.length > 2 || filterterm.length===0)
 // {
        console.log(filter);
		ObjectManager.getlistdatafiltered(listid,filter, function(data)
		{                 
            //document.body.className = "stdui";   
         console.log("filterbyServer - false");
        dispatch({type:SET_OBJECTS, payload:data,listid:listid});    
		});
  //  }

    };
}