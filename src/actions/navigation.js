
import {SET_NAVIGATION} from './actiontypes';

export function setNavigation(navarray,windowtitle) {
    return (dispatch)=>
    { 
        dispatch({type:SET_NAVIGATION, payload:navarray});          
      
        var elem = document.createElement('textarea');
        elem.innerHTML = windowtitle;
        var decoded = elem.value;
        document.title = decoded;
    };

}