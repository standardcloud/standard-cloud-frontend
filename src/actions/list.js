import {SET_LISTS} from './actiontypes';
import Listmanager from '../module/listmanager/stdlistmanager';

export function loadLists() {
    return (dispatch)=>
    {
    Listmanager.getlists().then(data =>
    {		
        dispatch({type:SET_LISTS, payload:data.content});       
    });	
        
};
}