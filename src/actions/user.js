import {SET_USER} from './actiontypes';
import AuthenticationManager from '../module/authenticationmanager/stdauthenticationmanager';



export function setUser(userdata) {
  return (dispatch)=>
  {
    dispatch({type:SET_USER, payload:userdata});        
  };
  }

  export function loadUser(usedata) {
    return (dispatch)=>
    {
      AuthenticationManager.getuserdata((data)=>{
          dispatch({type:SET_USER, payload:data.content});         
      });
          
};
}