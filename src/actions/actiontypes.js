export const SET_USER= "SET_USER";
export const SET_USERS= "SET_USERS";
export const SET_TIMELINE= "SET_TIMELINE";
export const SET_HISTORY= "SET_HISTORY";
export const UPDATE_HISTORY_OBJECT= "UPDATE_HISTORY_OBJECT";
export const SET_ANALYTICS= "SET_ANALYTICS";
export const LOAD_USER= "LOAD_USER";
export const SET_LISTS = "SET_LISTS";
export const SET_MYLISTS = "SET_MYLISTS";
export const SET_OKDIALOG = "SET_OKDIALOG";
export const SET_ERRORDIALOG = "SET_ERRORDIALOG";
export const SET_STREAM = "SET_STREAM";
export const SET_STREAM_ADDCOMMENT ="SET_STREAM_ADDCOMMENT";
export const SET_STREAM_REMOVECOMMENT ="SET_STREAM_REMOVECOMMENT";
export const SET_STREAM_UPDATECOMMENT ="SET_STREAM_UPDATECOMMENT";
export const SET_STREAM_ADDLIKE = "SET_STREAM_ADDLIKE";
export const SET_NAVIGATION = "SET_NAVIGATION";
export const SET_FIELDS = "SET_FIELDS";
export const SET_FIELD = "SET_FIELD";
export const REMOVE_FIELD = "SET_FIELD";
export const SET_OBJECTS = "SET_OBJECTS";
export const RESET_OBJECTS = "RESET_OBJECTS";
export const SET_OBJECTTYPES = "SET_OBJECTTYPES";
export const LOADMORE_OBJECTS = "LOADMORE_OBJECTS";
export const SETMORE_OBJECTS = "SETMORE_OBJECTS";
export const SET_STREAM_ADD_PERSONALPOST = "SET_STREAM_ADD_PERSONALPOST";
export const SET_QUERIES = "SET_QUERIES";
