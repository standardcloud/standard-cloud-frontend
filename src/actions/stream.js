import {SET_STREAM,SET_STREAM_ADDLIKE,SET_STREAM_ADDCOMMENT,SET_STREAM_UPDATECOMMENT,SET_STREAM_REMOVECOMMENT,SET_STREAM_ADD_PERSONALPOST} from './actiontypes';
import StdStream from '../module/stream/stdstream';
import Dialog from '../component/dialog/stddialog';
import LikeManager from '../module/likemanager/stdlikemanager';
import CommentManager from '../module/comment/stdcomment';
import Usermanager from '../module/usermanager/stdusermanager';

export function getUserStream(Startdate) {
  return (dispatch)=>
  {
       StdStream.getUserstream(Startdate).then(data=>{
                dispatch({type:SET_STREAM, payload:data.content});        
       });
 };
}

 export function addLike(objectid) {
  return (dispatch)=>
  {  			
    LikeManager.addlike(objectid).then(resultobject =>
      {		
       if(resultobject.state==="ok")
       {						
          dispatch({type:SET_STREAM_ADDLIKE, payload:resultobject});      
       }else
       {
        Dialog.errordialog(dispatch,"Like","konnte nicht gesetzt werden");		 
       }		 
      });
  };
 }

 
 export function addComment(eventid,commenttext) {
  return (dispatch)=>
  {  			
    CommentManager.addcomment(eventid,commenttext).then(rs=>
      {		
      
        if(rs.state !=="ok")
        {
        Dialog.errordialog(dispatch,"Kommentar", "konnte nicht gespeichert werden.");	
        }else
        {					
          dispatch({type:SET_STREAM_ADDCOMMENT, payload:rs});   
        }
      });
  };
 }



 export function addPersonalPost(text) {
  return (dispatch)=>
  {  			
    Usermanager.addpersonalpost(text).then(data=>
    {
     if(data.state==="ok")
     {
      dispatch({type:SET_STREAM_ADD_PERSONALPOST, payload:data});  
     }else
     {
       Dialog.error("Fehler","Nachricht konnte nicht veröffentlich werden.");
     }
    });
  };
 }


  
 export function updateComment(CommentGUID,updatecommenttext) {
  return (dispatch)=>
  {  			
  CommentManager.updatecomment(CommentGUID,updatecommenttext).then(newcomment =>
  {
  if(newcomment.state==="ok")
  {	
   dispatch({type:SET_STREAM_UPDATECOMMENT, payload:newcomment});   
    Dialog.okdialog("Kommentar", "wurde gespeichert.");			
  }else
  {
  Dialog.errordialog("Kommentar", "Konnte nicht gea&umlndert werden.");	
  }

});
  };
 }

 export function removeComment(comment) {
  return (dispatch)=>
  {  			
    CommentManager.removecomment(comment.id).then(status =>
    {	
      if(status.ok==="ok")
      {
        dispatch({type:SET_STREAM_REMOVECOMMENT, payload:comment});   
        Dialog.okdialog(dispatch,"Kommentar", "Kommentar wurde gel&ouml;scht.");	
      }else
      {
        Dialog.errordialog(dispatch, "Kommentar", "Konnte nicht gel&ouml;scht werden.");				
      }	 
    });
  };
 }