import {SET_QUERIES} from './actiontypes';
import Querymanager from '../module/querymanager/stdquerymanager';

export function loadQueries() {
    return (dispatch)=>
    {
        Querymanager.getqueries().then(data =>
    {		
        dispatch({type:SET_QUERIES, payload:data});       
    });	
        
};
}