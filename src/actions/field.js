import {SET_FIELDS} from './actiontypes';
import FieldManager from '../module/fieldmanager/stdfieldmanager';

export function loadFields() {
    return (dispatch)=>
    {
        FieldManager.getfieldlist().then(results=>
        {
            dispatch({type:SET_FIELDS, payload:results}); 
        });       
    };
}