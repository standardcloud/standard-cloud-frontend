import {SET_MYLISTS} from './actiontypes';
import Listmanager from '../module/listmanager/stdlistmanager';

export function loadMyLists() {
    return (dispatch)=>
    {
      Listmanager.getmylists(function(data)
      {		
        dispatch({type:SET_MYLISTS, payload:data});       
      });	
        
};
}