import {SET_TIMELINE} from './actiontypes';
import Usermanager from '../module/usermanager/stdusermanager';


  export function loadTimeline() {
    return (dispatch)=>
    {
        Usermanager.gettimeline(function(results)
        {
            dispatch({type:SET_TIMELINE, payload:results}); 
        });       
    };
}