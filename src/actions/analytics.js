import {SET_ANALYTICS} from './actiontypes';
import Analyticsmanager from '../module/analytics/stdanalytics';


  export function loadAnalytics() {
    return (dispatch)=>
    {
        Analyticsmanager.getobjects().then(results=>
        {
            dispatch({type:SET_ANALYTICS, payload:results}); 
        });       
    };
}