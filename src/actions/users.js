import {SET_USERS} from './actiontypes';
import UserManager from '../module/usermanager/stdusermanager';

export function loadUsers() {
  return (dispatch)=>
  {
    UserManager.getusers().then(data=>{
      console.log(data);
      if(data.state ==="ok")
      {
      dispatch({type:SET_USERS, payload:data});       
      }  
    });     
  };
  }

