import {SET_HISTORY,UPDATE_HISTORY_OBJECT} from './actiontypes';
import Usermanager from '../module/usermanager/stdusermanager';
import ObjectManager from '../module/objectmanager/stdobjectmanager';
export function loadHistory() {
    return (dispatch)=>
    {
        Usermanager.gethistory(function(results)
        {
            dispatch({type:SET_HISTORY, payload:results}); 
        });       
    };
}
export function updateHistoryObject(historyobject) {
    return (dispatch)=>
    {
           ObjectManager.getobject(historyobject.listid,historyobject.objectid).then(data=>
        {
            dispatch({type:UPDATE_HISTORY_OBJECT, payload:data}); ;
        });
            
    };
}